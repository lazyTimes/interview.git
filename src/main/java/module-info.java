/**
 * @Package :
 * @Description : 模块化
 * @Create on : 2023/6/19 14:29
 * @author Xander
 * @version v1.0.0
 **/
open module interview {
    requires static lombok;
    requires io.netty.transport;
    requires junit;
    requires jmh.core;
    requires org.apache.tomcat.embed.core;
    requires spring.web;
    requires spring.context;
    requires spring.beans;
    requires freemarker;
    requires com.google.common;
    requires java.desktop;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.tx;
    requires java.net.http;
    requires java.scripting;
    requires io.netty.buffer;
    requires org.apache.commons.lang3;
    requires io.netty.codec;
    requires org.slf4j;
    requires hutool.all;
    requires io.netty.common;
    requires fastjson;
    requires io.netty.handler;
    requires java.validation;
    requires spring.core;
//    requires hibernate.validator;
    requires spring.data.redis;
    requires spring.jdbc;
    requires poi;
    requires commons.io;
    requires spring.jcl;
    requires commons.configuration;
    requires org.apache.httpcomponents.httpclient;
    requires org.apache.httpcomponents.httpcore;
    requires org.apache.commons.codec;
    requires com.fasterxml.jackson.databind;
    requires curator.framework;
    requires curator.client;
    requires curator.recipes;
    requires org.mybatis;
    requires org.apache.logging.log4j;
    requires commons.beanutils;
    requires thymeleaf;
    requires commons.lang;
    requires poi.ooxml;
    requires org.apache.commons.collections4;
    requires zookeeper;
    exports com.zxd.interview;

}

