package com.zxd.interview.constant;

/**
 * 测试对象
 *
 * @author zxd
 */
public class QualityConstants {

    /**
     *  的督导事项id
     */
    public static final int EVENTID = 12;

    /**
     * 数字0
     */
    public static final int NUM_ZERO = 0;
    /**
     * 数字1
     */
    public static final int NUM_ONE = 1;

    /**
     * 数字2
     */
    public static final int NUM_TWO = 2;
    /**
     * 数字-1
     */
    public static final int NUM_MINUS_1 = -1;
    /**
     * 字节大小512
     */
    public static final int BYTE_512 = 512;
    /**
     * 500错误编码
     */
    public static final int CODE_500 = 500;
    /**
     * 500错误提示信息 - 状态非法
     */
    public static final String CODE_500_MSG_1 = "状态非法!";
    /**
     * 500错误提示信息 - 非督导用户不允许查看记录
     */
    public static final String CODE_500_MSG_2 = "非督导用户不允许查看记录!";
    /**
     * 500错误提示信息 - 这条质量监测已经完成！无法修改
     */
    public static final String CODE_500_MSG_3 = "这条质量监测已经完成！无法修改！";
    /**
     * 500错误提示信息 - 提交失败，材料上传不能为空
     */
    public static final String CODE_500_MSG_4 = "提交失败，材料上传不能为空";
    /**
     * 500错误提示信息 - 提交失败，请稍后重试或联系管理员
     */
    public static final String CODE_500_MSG_5 = "提交失败，请稍后重试或联系管理员！";
    /**
     * 500错误提示信息 - 提交失败，意见反馈不能为空
     */
    public static final String CODE_500_MSG_6 = "提交失败，意见反馈不能为空!";
    /**
     * 405错误编码
     */
    public static final int CODE_405 = 405;
    /**
     * 405错误提示信息 - 该信息只允许督导查看
     */
    public static final String CODE_405_MSG_1 = "该信息只允许督导查看!";
    /**
     * 200成功编码
     */
    public static final int CODE_200 = 200;
    /**
     * 200成功提示信息 - 该信息只允许督导查看
     */
    public static final String CODE_200_MSG_1 = "提交成功!";

    /**
     * 错误提示信息 - 尚未选择记录
     */
    public static final String DELETE_FAIRURE_MSG = "删除失败，尚未选择记录！";
    /**
     * 错误提示信息 - 尚未选择记录
     */
    public static final String NO_RECORD_SELECTED = "尚未选择记录！";
    /**
     * 字符编码utf-8
     */
    public static final String UTF_8 = "utf-8";
    /**
     * 默认pid
     */
    public static final int PID = 0;
    /**
     * 默认层级
     */
    public static final int DEFUALT_LAYER = 1;

    /**
     * 不适当最低得分
     */
    public static final Integer MIN_SCORE = 1;
    /**
     * 优秀最高得分
     */
    public static final Integer MAX_SCORE = 7;

    /**
     * map的hash初始值
     */
    public static final int HASH_MAP_INIT_VALUE = 32;

    /**
     * 全园平均分
     */
    public final static String WHOLE_AVERAGE = "全园平均分";
    /**
     * 查询失败
     */
    public final static String QUERY_FAIRURE = "查询失败";
    /**
     * 操作成功
     */
    public final static String SUCCESS_MSG = "操作成功！";
    /**
     * 操作失败
     */
    public final static String FARIURE_MSG = "操作失败！";
    /**
     * 导出失败
     */
    public final static String FARIURE_EXPORT = "导出失败！";

    /**
     * 请求头 - 文档
     */
    public final static String CONTENT_TYPE_WORD = "application/msword";
    /**
     * 请求头 - 下载
     */
    public final static String CONTENT_TYPE_DOWNLOAD = "application/x-download";
    /**
     * 请求头 - 二进制文件
     */
    public final static String CONTENT_TYPE_STEAM = "application/octet-stream;charset=UTF-8";
    /**
     * 请求头
     */
    public final static String USER_AGENT = "User-Agent";
    /**
     * 请求头
     */
    public final static String CONTENT_TYPE = "Content-Type";
    /**
     * 连接
     */
    public final static String CONNECTION = "Connection";
    /**
     * 关闭连接
     */
    public final static String CLOSE = "close";
    /**
     * 连接
     */
    public final static String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    /**
     * 连接
     */
    public final static String CONTENT_DISPOSITION = "Content-Disposition";

    /**
     * 浏览器 - ie
     */
    public final static String MSIE = "MSIE";
    /**
     * 浏览器 - Firefox
     */
    public final static String FIREFOX = "Firefox";

    /**
     * 填写报告的step
     */
    public final static String MODULE_STEP3_REPORT = "qualityreport";

    /**
     * 督导下园核实的材料
     */
    public final static String MODULE_STEP1_MATERIAL = "qualitymetrail";

    /**
     * 数字3
     */
    public final static int NUM_3 = 3;
    /**
     * 数字4
     */
    public final static int NUM_4 = 4;

    /**
     * 数字5
     */
    public final static int NUM_5 = 5;
    /**
     * 数字6
     */
    public final static int NUM_6 = 6;
    /**
     * 数字7
     */
    public final static int NUM_7 = 7;
    /**
     * 数字8
     */
    public final static int NUM_8 = 8;
    /**
     * 数字9
     */
    public final static int NUM_9 = 9;
    /**
     * 数字10
     */
    public final static int NUM_10 = 10;
    /**
     * 数字11
     */
    public final static int NUM_11 = 11;

    /**
     * 数字12
     */
    public final static int NUM_12 = 12;
    /**
     * 数字13
     */
    public final static int NUM_13 = 13;
    /**
     * 数字14
     */
    public final static int NUM_14 = 14;
    /**
     * 数字15
     */
    public final static int NUM_15 = 15;
    /**
     * 数字16
     */
    public final static int NUM_16 = 16;
    /**
     * 数字17
     */
    public final static int NUM_17 = 17;
    /**
     * 数字18
     */
    public final static int NUM_18 = 18;
    /**
     * 数字19
     */
    public final static int NUM_19 = 19;
    /**
     * 数字20
     */
    public final static int NUM_20 = 20;

    /**
     * 格式化数字
     */
    public final static String DECIMAL_Format = "######.00";

}
