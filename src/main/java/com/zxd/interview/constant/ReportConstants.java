package com.zxd.interview.constant;

/**
 * 报表测试
 *
 *
 */
public class ReportConstants {
    /**
     * 默认的int为空的值为0
     */
    public static final int EMPTY_INT_VALUE = 0;
    /**
     * 未填写
     */
    public static final String UN_FILLED = "未填写";

    /**
     * 状态码为403
     */
    public static final int CODE_403 = 403;
    /**
     * 状态码为403提示信息 权限不足,当前功能只限幼儿园和教育科!
     */
    public static final String CODE_403_MSG = "权限不足,当前功能只限幼儿园和教育科!";
    /**
     * 状态码为500
     */
    public static final int CODE_500 = 500;
    /**
     * 状态码为500提示信息 查询错误,请刷新后重试
     */
    public static final String CODE_500_MSG = "查询错误,请刷新后重试";
    /**
     * 数字0
     */
    public static final int NUM_ZERO = 0;
    /**
     * 数字1
     */
    public static final int NUM_ONE = 1;
    /**
     * 数字2
     */
    public static final int NUM_TWO = 2;
    /**
     * 数字3
     */
    public static final int NUM_THREE = 3;
    /**
     * 数字4
     */
    public static final int NUM_FOUR = 4;
    /**
     * 数字5
     */
    public static final int NUM_FIVE = 5;
    /**
     * 数字6
     */
    public static final int NUM_SIX = 6;
    /**
     * 数字7
     */
    public static final int NUM_SEVEN = 7;
    /**
     * 数字8
     */
    public static final int NUM_EIGHT = 8;
    /**
     * 数字9
     */
    public static final int NUM_NINE = 9;
    /**
     * 数字10
     */
    public static final int NUM_TEN = 10;
    /**
     * 数字0.8
     */
    public static final double NUM_08 = 0.8;

    /**
     * 浮点类型格式化
     */
    public static final String DECIMAL_FORMAT = "######.00";

    /**
     * 总计
     */
    public static final String CONTENT = "总计";

    /**
     * 分割字符
     */
    public static final String SPLIT_CHAR = "\\$split\\$";
    /**
     * 字符0
     */
    public static final String CHAR_0 = "0";

    /**
     * 25岁及以下
     */
    public static final String AGE_UNDER_25 = "25岁及以下";
    /**
     * 26-30岁
     */
    public static final String AGE_26_30 = "26-30岁";
    /**
     * 31-35岁
     */
    public static final String AGE_31_35 = "31-35岁";
    /**
     * 36-40岁
     */
    public static final String AGE_36_40 = "36-40岁";
    /**
     * 41-45岁
     */
    public static final String AGE_41_45 = "41-45岁";
    /**
     * 46-50岁
     */
    public static final String AGE_46_50 = "46-50岁";
    /**
     * 31-35岁
     */
    public static final String AGE_51_55 = "51-55岁";
    /**
     * 36-40岁
     */
    public static final String AGE_56_60 = "56-60岁";
    /**
     * 41-45岁
     */
    public static final String AGE_UP_60 = "60岁以上";
    /**
     * 2岁及以下
     */
    public static final String AGE_UNDER_2 = "2岁及以下";
    /**
     * 3岁
     */
    public static final String AGE_3 = "3岁";
    /**
     * 4岁
     */
    public static final String AGE_4 = "4岁";
    /**
     * 5岁
     */
    public static final String AGE_5 = "5岁";
    /**
     * 6岁及以上
     */
    public static final String AGE_UP_6 = "6岁及以上";

    /**
     * 获取东八区时间
     */
    public static final String GMT_TIME = "GMT+08:00";

    /**
     * 中国
     */
    public static final String CHINA = "中国";
    /**
     * 台湾
     */
    public static final String TAIWAN = "台湾";
    /**
     * 香港
     */
    public static final String HONGKONG = "香港";
    /**
     * 澳门
     */
    public static final String MACAO = "澳门";
    /**
     * 深圳
     */
    public static final String GD = "广东";
    /**
     * 深圳
     */
    public static final String SHENZHEN = "深圳";
    /**
     * 深圳
     */
    public static final String GD_SHENZHEN = "广东省深圳";
    /**
     * 数字25
     */
    public static final int NUM_25 = 25;
    /**
     * 数字26
     */
    public static final int NUM_26 = 26;
    /**
     * 数字30
     */
    public static final int NUM_30 = 30;
    /**
     * 数字31
     */
    public static final int NUM_31 = 31;
    /**
     * 数字35
     */
    public static final int NUM_35 = 35;
    /**
     * 数字36
     */
    public static final int NUM_36 = 36;
    /**
     * 数字40
     */
    public static final int NUM_40 = 40;
    /**
     * 数字41
     */
    public static final int NUM_41 = 41;
    /**
     * 数字45
     */
    public static final int NUM_45 = 45;
    /**
     * 数字46
     */
    public static final int NUM_46 = 46;
    /**
     * 数字50
     */
    public static final int NUM_50 = 50;
    /**
     * 数字51
     */
    public static final int NUM_51 = 51;
    /**
     * 数字55
     */
    public static final int NUM_55 = 55;
    /**
     * 数字56
     */
    public static final int NUM_56 = 56;
    /**
     * 数字60
     */
    public static final int NUM_60 = 60;
    /**
     * 数字61
     */
    public static final int NUM_61 = 61;
    /**
     * 替换字符
     */
    public static final String REPLACE_CHAR = "nbsp;";

}

