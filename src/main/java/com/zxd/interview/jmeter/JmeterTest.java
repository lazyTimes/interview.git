package com.zxd.interview.jmeter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Jmeter 测试部分
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/9 19:07
 */
@RestController
@RequestMapping("/jmeter")
public class JmeterTest {

    public static void main(String[] args) {
        System.out.println("sss");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JmeterTest.class);
//    private AtomicInteger count = new AtomicInteger(100);
    private int count = 100;

    @RequestMapping(value = "/runTest1", method = RequestMethod.POST)
    @ResponseBody
    public Object testThread(Map key) throws InterruptedException {


        key.forEach((k,v )->{
            System.err.println("k = "+ k + " value = "+ v);
        });
//        LOGGER.info("主线程开始");
        // 并发类版本
//        if (count.decrementAndGet() < 0) {
//            return count;
//        }
//        System.err.println(count);
//        return count;
        // 最简单的版本
        synchronized (this){
            if (count < 0) {
                return count;
            }
        }
        System.err.println(count);
        return count--;
    }



}
