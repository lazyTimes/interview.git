package com.zxd.interview.exerise.normal;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        School school = new School("My School");
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. Add student");
            System.out.println("2. Add course");
            System.out.println("3. Add grade");
            System.out.println("4. Display all students");
            System.out.println("5. Display all courses");
            System.out.println("6. Exit");
            System.out.print("Choose an option: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Enter student ID: ");
                    String studentId = scanner.next();
                    System.out.print("Enter student name: ");
                    String studentName = scanner.next();
                    System.out.print("Enter student age: ");
                    int studentAge = scanner.nextInt();
                    school.addStudent(new Student(studentId, studentName, studentAge));
                    break;
                case 2:
                    System.out.print("Enter course ID: ");
                    String courseId = scanner.next();
                    System.out.print("Enter course name: ");
                    String courseName = scanner.next();
                    school.addCourse(new Course(courseId, courseName));
                    break;
                case 3:
                    System.out.print("Enter student ID: ");
                    studentId = scanner.next();
                    Student student = null;
                    for (Student s : school.getStudents()) {
                        if (s.getId().equals(studentId)) {
                            student = s;
                            break;
                        }
                    }
                    if (student == null) {
                        System.out.println("Student not found!");
                        break;
                    }

                    System.out.print("Enter course ID: ");
                    courseId = scanner.next();
                    Course course = null;
                    for (Course c : school.getCourses()) {
                        if (c.getId().equals(courseId)) {
                            course = c;
                            break;
                        }
                    }
                    if (course == null) {
                        System.out.println("Course not found!");
                        break;
                    }

                    System.out.print("Enter grade: ");
                    int mark = scanner.nextInt();
                    student.addGrade(new Grade(course, mark));
                    break;
                case 4:
                    school.displayAllStudents();
                    break;
                case 5:
                    school.displayAllCourses();
                    break;
                case 6:
                    System.out.println("Exiting...");
                    scanner.close();
                    return;
                    //加分

                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
    }

}
