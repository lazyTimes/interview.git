package com.zxd.interview.exerise.normal;


import java.util.ArrayList;

public class Student {
    private String id;
    private String name;
    private int age;
    private ArrayList<Grade> grades;

    public Student(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.grades = new ArrayList<>();
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void addGrade(Grade grade) {
        grades.add(grade);
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void displayInfo() {
        System.out.println("Student ID: " + id);
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Grades:");
        for (Grade grade : grades) {
            System.out.println("Course: " + grade.getCourse().getName() + ", Grade: " + grade.getMark());
        }
    }
}
