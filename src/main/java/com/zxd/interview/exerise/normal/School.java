package com.zxd.interview.exerise.normal;

import java.util.ArrayList;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.exerise
 * @Description : TODO
 * @Create on : 2024/6/12 18:36
 **/
public class School {
    private String name;
    private ArrayList<Student> students;
    private ArrayList<Course> courses;

    public School(String name) {
        this.name = name;
        this.students = new ArrayList<>();
        this.courses = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void addCourse(Course course) {
        courses.add(course);
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void displayAllStudents() {
        for (Student student : students) {
            student.displayInfo();
        }
    }

    public void displayAllCourses() {
        for (Course course : courses) {
            System.out.println("Course ID: " + course.getId() + ", Name: " + course.getName());
        }
    }
}
