package com.zxd.interview.exerise.plus;


public class Grade {
    private Course course;
    private int mark;

    public Grade(Course course, int mark) {
        this.course = course;
        this.mark = mark;
    }

    // Getters and Setters
    public Course getCourse() {
        return course;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
