package com.zxd.interview.exerise.plus;


import java.util.ArrayList;

public class Student extends Person{

    private int age;
    private ArrayList<Grade> grades;

    public Student(String id, String name, int age) {
        super(id, name);
        this.age = age;
        this.grades = new ArrayList<>();
    }

    // Getters and Setters
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void addGrade(Grade grade) {
        grades.add(grade);
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    @Override
    public void displayInfo() {
        System.out.println("Student ID: " + getId());
        System.out.println("Name: " + getName());
        System.out.println("Age: " + age);
        System.out.println("Grades:");
        for (Grade grade : grades) {
            System.out.println("Course: " + grade.getCourse().getName() + ", Grade: " + grade.getMark());
        }
    }
}
