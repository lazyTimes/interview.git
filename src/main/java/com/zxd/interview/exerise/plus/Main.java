package com.zxd.interview.exerise.plus;



import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        School school = new School("My School");
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. 添加学生");
            System.out.println("2. 添加课程");
            System.out.println("3. 添加成绩");
            System.out.println("4. 显示所有学生");
            System.out.println("5. 显示所有课程");
            System.out.println("7. 重建学生人数");
            System.out.println("8. 修改学生成绩");
            System.out.println("9. 修改课程信息");
            System.out.println("10. 退出");
            System.out.print("Choose an option: ");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Enter student ID: ");
                    String studentId = scanner.next();
                    System.out.print("Enter student name: ");
                    String studentName = scanner.next();
                    System.out.print("Enter student age: ");
                    int studentAge = scanner.nextInt();
                    school.addStudent(new Student(studentId, studentName, studentAge));
                    break;
                case 2:
                    System.out.print("Enter course ID: ");
                    String courseId = scanner.next();
                    System.out.print("Enter course name: ");
                    String courseName = scanner.next();
                    school.addCourse(new Course(courseId, courseName));
                    break;
                case 3:
                    System.out.print("Enter student ID: ");
                    studentId = scanner.next();
                    Student student = null;
                    for (Student s : school.getStudents()) {
                        if (s.getId().equals(studentId)) {
                            student = s;
                            break;
                        }
                    }
                    if (student == null) {
                        System.out.println("Student not found!");
                        break;
                    }

                    System.out.print("Enter course ID: ");
                    courseId = scanner.next();
                    Course course = null;
                    for (Course c : school.getCourses()) {
                        if (c.getId().equals(courseId)) {
                            course = c;
                            break;
                        }
                    }
                    if (course == null) {
                        System.out.println("Course not found!");
                        break;
                    }

                    System.out.print("Enter grade: ");
                    int mark = scanner.nextInt();
                    student.addGrade(new Grade(course, mark));
                    break;
                case 4:
                    school.displayAllStudents();
                    break;
                case 5:
                    school.displayAllCourses();
                    break;
//                case 6:
//                    System.out.println("Exiting...");
//                    scanner.close();
//                    return;
                case 7:
                    System.out.print("Enter student ID: ");
                    studentId = scanner.next();
                    student = null;
                    for (Student s : school.getStudents()) {
                        if (s.getId().equals(studentId)) {
                            student = s;
                            break;
                        }
                    }
                    if (student == null) {
                        System.out.println("Student not found!");
                        break;
                    }

                    System.out.print("Enter new name: ");
                    studentName = scanner.next();
                    System.out.print("Enter new age: ");
                    studentAge = scanner.nextInt();
                    student = new Student(studentId, studentName, studentAge); // 重建学生对象
                    school.addStudent(student);
                    break;
                case 8: // 修改学生成绩
                    System.out.print("Enter student ID: ");
                    studentId = scanner.next();
                    student = null;
                    for (Student s : school.getStudents()) {
                        if (s.getId().equals(studentId)) {
                            student = s;
                            break;
                        }
                    }
                    if (student == null) {
                        System.out.println("Student not found!");
                        break;
                    }

                    System.out.print("Enter course ID: ");
                    courseId = scanner.next();
                    Grade gradeToUpdate = null;
                    for (Grade g : student.getGrades()) {
                        if (g.getCourse().getId().equals(courseId)) {
                            gradeToUpdate = g;
                            break;
                        }
                    }
                    if (gradeToUpdate == null) {
                        System.out.println("Grade not found!");
                        break;
                    }

                    System.out.print("Enter new grade: ");
                    mark = scanner.nextInt();
                    gradeToUpdate.setMark(mark);
                    break;

                case 9: // 修改课程信息
                    System.out.print("Enter course ID: ");
                    courseId = scanner.next();
                    course = null;
                    for (Course c : school.getCourses()) {
                        if (c.getId().equals(courseId)) {
                            course = c;
                            break;
                        }
                    }
                    if (course == null) {
                        System.out.println("Course not found!");
                        break;
                    }

                    System.out.print("Enter new course name: ");
                    courseName = scanner.next();
                    course.setName(courseName);
                    break;

                case 10:
                    System.out.println("Exiting...");
                    scanner.close();
                    return;

                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
    }

}
