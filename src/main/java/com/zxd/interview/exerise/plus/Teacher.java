package com.zxd.interview.exerise.plus;

import com.zxd.interview.exerise.Employee;

public class Teacher extends Person implements Employee {
    private double salary;

    public Teacher(String id, String name) {
        super(id, name);
    }

    @Override
    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public void displayInfo() {
        System.out.println("Teacher ID: " + getId());
        System.out.println("Name: " + getName());
        System.out.println("Salary: " + salary);
    }
}
