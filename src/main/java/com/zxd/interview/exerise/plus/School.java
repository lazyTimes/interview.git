package com.zxd.interview.exerise.plus;


import java.util.ArrayList;

public class School {
    private String name;
    private ArrayList<Student> students;
    private ArrayList<Course> courses;

    public School(String name) {
        this.name = name;
        this.students = new ArrayList<>();
        this.courses = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void addCourse(Course course) {
        courses.add(course);
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void displayAllStudents() {
        for (Student student : students) {
            student.displayInfo();
        }
    }

    public void displayAllCourses() {
        for (Course course : courses) {
            System.out.println("Course ID: " + course.getId() + ", Name: " + course.getName());
        }
    }
}
