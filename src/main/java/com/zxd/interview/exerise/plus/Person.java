package com.zxd.interview.exerise.plus;

/**
 * 为了加分，我们可以添加一个接口和一个抽象类。
 **/
public abstract class Person {

    private String id;
    private String name;

    public Person(String id, String name) {
        this.id = id;
        this.name = name;
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    // Abstract method
    public abstract void displayInfo();
}
