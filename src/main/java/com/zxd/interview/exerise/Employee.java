package com.zxd.interview.exerise;


public interface Employee {
    void setSalary(double salary);
    double getSalary();
}
