package com.zxd.interview.java9;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * dropWhile 测试
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.java9
 * @Description : dropWhile 测试
 * @Create on : 2023/7/5 14:03
 **/
public class LambadaEnhance {

    @Test
    public void dropWhile() {
        List<Integer> list =
                Arrays.asList(45, 43, 76, 87, 42, 77, 90, 73, 67, 88);
        list.stream().dropWhile(x -> x < 50)
                .forEach(System.out::println);
        System.out.println();
        list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        list.stream().dropWhile(x -> x < 5)
                .forEach(System.out::println);
    }/*运行结果：
        76
        87
        42
        77
        90
        73
        67
        88

        5
        6
        7
        8
    */

    @Test
    public void takeWhile() {
        List<Integer> list =
                Arrays.asList(45, 43, 76, 87, 42, 77, 90, 73, 67, 88);
        list.stream().takeWhile(x -> x < 50)
                .forEach(System.out::println);
        System.out.println();
        list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        list.stream().takeWhile(x -> x < 5)
                .forEach(System.out::println);
    }/*
    45
    43

    1
    2
    3
    4
    */

    @Test
    public void streamofNullAble() {
        Stream<Integer> integerStream = Stream.ofNullable(null);

        //报 NullPointerException
        //Stream<Object> stream1 = Stream.of(null);
        //System.out.println(stream1.count());
        //不报异常，允许通过
        Stream<String> stringStream = Stream.of("AA", "BB", null);
        System.out.println(stringStream.count());//3
        //不报异常，允许通过
        List<String> list = new ArrayList<>();
        list.add("AA");
        list.add(null);
        System.out.println(list.stream().count());//2
        //ofNullable()：允许值为 null
        Stream<Object> stream1 = Stream.ofNullable(null);
        System.out.println(stream1.count());//0
        Stream<String> stream = Stream.ofNullable("hello world");
        System.out.println(stream.count());//1
    }/*
    运行结果
    3
    2
    0
    1
    */

    @Test
    public void interatorEnhance() {
//        原来的控制终止方式：
        Stream.iterate(1, i -> i + 1).limit(10)
                .forEach(System.out::println);
        System.err.println();
//        现在的终止方式：
        Stream.iterate(1, i -> i < 10, i -> i + 1)
                .forEach(System.out::println);
    }/**运行结果
     1
     2
     3
     4
     5
     6
     7
     8
     9
     10

     1
     2
     3
     4
     5
     6
     7
     8
     9
     */

    /**
     * Optional 类中 stream()的使用
     */
    @Test
    public void optionalStreamUse() {
        List<String> list = List.of("Tom", "Jerry", "Tim");
        Optional<List<String>> optional =
                Optional.ofNullable(list);
        Stream<List<String>> stream = optional.stream();
        stream.flatMap(x ->
                x.stream()).forEach(System.out::println);
    }/**
     Tom
     Jerry
     Tim
     */
}
