package com.zxd.interview.java9;

import java.util.*;

/**
 * 集合方法增强
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.java9
 * @Description : 集合方法增强
 * @Create on : 2023/7/5 13:49
 **/
public class CollectionEnhance {

    public static void main(String[] args) {
        List<String> namesList = new ArrayList<>();
        namesList.add("Joe");
        namesList.add("Bob");
        namesList.add("Bill");
        namesList = Collections.unmodifiableList(namesList);
        System.out.println(namesList);
        // [Joe, Bob, Bill]

        List<String> list =
                Collections.unmodifiableList(Arrays.asList("a", "b", "c"));
        Set<String> set = Collections.unmodifiableSet(new
                HashSet<>(Arrays.asList("a", "b", "c")));

        Map<String, Integer> map = Collections.unmodifiableMap(new
           HashMap<>() {
               {
                   put("a", 1);
                   put("b", 2);
                   put("c", 3);
               }
           });

        map.forEach((k,v) -> System.out.println(k + ":" + v));
        //a:1
        //b:2
        //c:3

        List firsnamesList = List.of("Joe","Bob","Bill");
        firsnamesList.forEach(System.out::println);
    }
}
