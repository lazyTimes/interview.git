package com.zxd.interview.java9;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * 接口私有
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.java9
 * @Description : 接口私有
 * @Create on : 2023/7/5 13:22
 **/
public interface InnerIPrivatenterface {

    public static final int _SS = 1;
    public static final int a22_ = 1;

    private void print(){
        System.out.println("sssss");
        try (InputStreamReader reader = new InputStreamReader(System.in)){
            reader.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Set<String> strings =  new Set<String>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<String> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(String s) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends String> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };

    }
}
