package com.zxd.interview.java9;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * 新的的JDK HttpClient 使用
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.java9
 * @Description : 新的的JDK HttpClient 使用
 * @Create on : 2023/7/5 21:15
 **/
public class NewHttpClientTest {

    public static void main(String[] args) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest req =

                HttpRequest.newBuilder(URI.create("http://www.baidu.com"
                        ))
                        .GET()
                        .build();
        HttpResponse<String> response = client.send(req,
                HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        System.out.println(response.version().name());
        System.out.println(response.body());
    }/**运行结果：
     200
     HTTP_1_1
     <!DOCTYPE html>
    <!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8><meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always
     .....

     */
}
