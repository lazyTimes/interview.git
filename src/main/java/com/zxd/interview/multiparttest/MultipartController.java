package com.zxd.interview.multiparttest;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

//@RequestMapping("/upoload")
//@RestController
public class MultipartController {



    @RequestMapping("/test")
    public Object upload(@RequestParam("file1") MultipartFile multipartFile1,
                         @RequestParam("file2") MultipartFile multipartFile2){

        return multipartFile1.getOriginalFilename() + " file 2= >"+multipartFile2.getOriginalFilename();
    }
}
