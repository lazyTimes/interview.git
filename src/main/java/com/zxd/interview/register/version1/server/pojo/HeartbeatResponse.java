package com.zxd.interview.register.version1.server.pojo;

import lombok.Data;

/**
 * 心跳响应
 * @author xander
 *
 */
@Data
public class HeartbeatResponse {
	
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";

	/**
	 * 心跳响应状态：SUCCESS、FAILURE
	 */
	private String status;

	
}
