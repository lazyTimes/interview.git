package com.zxd.interview.register.version1.server.pojo;

/**
 * 注册响应
 * @author xander
 *
 */
public class RegisterResponse {
	
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";

	/**
	 * 注册响应状态：SUCCESS、FAILURE
	 */
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
