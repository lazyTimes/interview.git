package com.zxd.interview.register.version1.client.pojo;

import lombok.Data;

/**
 * 注册响应
 * @author xander
 *
 */
@Data
public class RegisterResponse {
	
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";

	/**
	 * 注册响应状态：SUCCESS、FAILURE
	 */
	private String status;

	
}
