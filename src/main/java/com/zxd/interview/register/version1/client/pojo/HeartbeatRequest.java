package com.zxd.interview.register.version1.client.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * 心跳请求
 *
 * @author xander
 */
@Data
@ToString
public class HeartbeatRequest {

    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 服务实例id
     */
    private String serviceInstanceId;


}
