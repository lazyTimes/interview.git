package com.zxd.interview.register.version1.client.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * 注册请求
 * @author xander
 *
 */
@Data
@ToString
public class RegisterRequest {

	/**
	 * 服务名称
	 */
	private String serviceName;
	/**
	 * 服务所在机器的ip地址
	 */
	private String ip;
	/**
	 * 服务所在机器的主机名
	 */
	private String hostname;
	/**
	 * 服务监听着哪个端口号
	 */
	private int port;
	/**
	 * 服务实例
	 */
	private String serviceInstanceId;

	
}
