package com.zxd.interview.work;

// 定义学生类
public class Student {

    private String chenziwei;
    private int age;
    private String no;
    private double scoreEn;
    private double scoreMa;
    private double scoreCh;

    public Student(String chenziwei, int age, String no, double scoreEn, double scoreMa, double scoreCh) {
        this.chenziwei = chenziwei;
        this.age = age;
        this.no = no;
        this.scoreEn = scoreEn;
        this.scoreMa = scoreMa;
        this.scoreCh = scoreCh;
    }

    public double aver() {
        return (scoreEn + scoreMa + scoreCh) / 3;
    }

    public void showMsg() {
        System.out.println("姓名: " + chenziwei);
        System.out.println("年龄: " + age);
        System.out.println("学号: " + no);
    }
}
