package com.zxd.interview.work;

import java.util.Random;


public class ArrayStats {

    public static void main(String[] args) {
        // 随机生成一个长度为10的一维整型数组
        int[] arr = generateRandomArray();
        System.out.println("随机生成的数组: " + arrayToString(arr));

        // 计算数组平均数
        double avg = calculateAverage(arr);
        System.out.println("数组平均数: " + avg);

        // 统计大于平均数的数据个数
        int count = countAboveAverage(arr, avg);
        System.out.println("大于平均数的数据个数: " + count);
    }

    private static int[] generateRandomArray() {
        Random random = new Random();
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(100) + 1; // 生成 1 到 100 之间的随机整数
        }
        return arr;
    }

    private static double calculateAverage(int[] arr) {
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return (double) sum / arr.length;
    }

    private static int countAboveAverage(int[] arr, double avg) {
        int count = 0;
        for (int num : arr) {
            if (num > avg) {
                count++;
            }
        }
        return count;
    }

    private static String arrayToString(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
