package com.zxd.interview.work;


public class Goods {
    private String name;
    private double price;

    public Goods(String name, double price) {
        this.name = name;
        this.price = price;
    }
    public void show() {
        System.out.println("商品名："+name);
        System.out.println("单价："+price+"元");
    }
}
