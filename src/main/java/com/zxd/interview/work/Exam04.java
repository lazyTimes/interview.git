package com.zxd.interview.work;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.work
 * @Description : TODO
 * @Create on : 2024/6/7 09:42
 **/
public class Exam04 {

    public static void main(String[] args) {
        Student stu = new Student("chenziwei", 19, "202332010678",
                87.0, 91.0, 71.0);

        stu.showMsg();
        System.out.println("平均分: " + stu.aver());
    }
}
