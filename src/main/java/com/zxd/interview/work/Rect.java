package com.zxd.interview.work;

public class Rect {

    protected int width;
    protected int height;

    // 带有两个参数的构造方法
    public Rect(int width, int height) {
        this.width = width;
        this.height = height;
    }

    // 不带参数的构造方法
    public Rect() {
        this.width = 10;
        this.height = 10;
    }

    // 求矩形面积的方法
    public int area() {
        return this.width * this.height;
    }

    // 求矩形周长的方法
    public int perimeter() {
        return 2 * (this.width + this.height);
    }

    public static void main(String[] args) {
        // 创建两个矩形对象
        Rect rect1 = new Rect(5, 8);
        Rect rect2 = new Rect();

        // 输出矩形信息
        System.out.println("矩形 1 的宽度: " + rect1.width);
        System.out.println("矩形 1 的高度: " + rect1.height);
        System.out.println("矩形 1 的面积: " + rect1.area());
        System.out.println("矩形 1 的周长: " + rect1.perimeter());

        System.out.println("矩形 2 的宽度: " + rect2.width);
        System.out.println("矩形 2 的高度: " + rect2.height);
        System.out.println("矩形 2 的面积: " + rect2.area());
        System.out.println("矩形 2 的周长: " + rect2.perimeter());
    }
}
