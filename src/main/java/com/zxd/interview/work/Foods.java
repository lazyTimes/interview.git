package com.zxd.interview.work;


public class Foods extends Goods{
    private double weight;

    public Foods(String name, double price, double weight) {
        super(name, price);
        this.weight = weight;
    }

    public void show() {
        super.show();
        System.out.println("分量："+weight+"克");
    }
}
