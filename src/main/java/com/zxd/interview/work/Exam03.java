package com.zxd.interview.work;

/**
 * @author chenziwei
 **/
public class Exam03 {
    public static void main(String[] args) {
        // 输出 1-300 之间的素数
        System.out.println("1-300 之间的素数:");
        for (int i = 2; i <= 300; i++) {
            boolean isPrime = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.print(i + " ");
            }
        }

        //创建类Exam03,请输出1-300之间所有的素数。(素数是指在大于1的自然数中，除了1和它本身以外不再有其他因数的自然数)
        // 1)定义变量sno，变量值为你的学号 请使用截取你的学号最后2位代替x的值(例如:学号202332010312则截取后x的值为12:学号202332010302 则截取后x的值为2)

        // 2)控制一行最多输出x个

    }/*运行结果：
    1-300 之间的素数:
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293

    */


}
