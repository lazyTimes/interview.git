//package com.zxd.interview.work;
//
///**
// * @author Xander
// * @version v1.0.0
// * @Package : com.zxd.interview.work
// * @Description : TODO
// * @Create on : 2024/6/4 09:32
// **/
//public class Orders {
//
//    private String customer;
//    private Goods[] goods;
//    private int[] quantities;
//    private double price;
//
//    public Orders(String customer, Goods[] goods, int[] quantities) {
//        this.customer = customer;
//        this.goods = goods;
//        this.quantities = quantities;
//        this.price = 0;
//        for(Goods g : goods) {
//            this.price += g.price;
//        }
//    }
//}
