package com.zxd.interview.work;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.work
 * @Description : TODO
 * @Create on : 2024/6/7 09:36
 **/
public class Exam01 {

    public static void main(String[] args) {
        // 定义名字
        String name = "陈梓唯";

        // 名字打印输出
        System.out.println("hello, " + name);
    }
}
