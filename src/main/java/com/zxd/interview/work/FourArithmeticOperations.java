package com.zxd.interview.work;

import java.util.Scanner;

public class FourArithmeticOperations {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入第一个整数: ");
        int num1 = scanner.nextInt();

        System.out.print("请输入运算符(+, -, *, /): ");
        String operator = scanner.next();

        System.out.print("请输入第二个整数: ");
        int num2 = scanner.nextInt();

        int result;
        switch (operator) {
            case "+":
                result = num1 + num2;
                System.out.println("结果: " + result);
                break;
            case "-":
                result = num1 - num2;
                System.out.println("结果: " + result);
                break;
            case "*":
                result = num1 * num2;
                System.out.println("结果: " + result);
                break;
            case "/":
                if (num2 == 0) {
                    System.out.println("错误: 除数不能为 0");
                } else {
                    result = num1 / num2;
                    System.out.println("结果: " + result);
                }
                break;
            default:
                System.out.println("错误: 无效的运算符");
        }

        scanner.close();
    }
}
