package com.zxd.interview.work;

import java.util.Random;


public class Exam02 {
    public static void main(String[] args) {
        // 创建 Random 对象
        Random random = new Random();

        // 生成 100 个随机数
        double[] numbers = new double[100];
        for (int i = 0; i < 100; i++) {
            numbers[i] = random.nextDouble();
        }

        // 统计各阶段的数量
        int stage1 = 0;
        int stage2 = 0;
        int stage3 = 0;

        for (double num : numbers) {
            if (num >= 0 && num <= 0.60) {
                stage1++;
            } else if (num >= 0.63 && num <= 0.85) {
                stage2++;
            } else if (num > 0.87 && num <= 0.95) {
                stage3++;
            }
        }

        // 打印结果
        System.out.println("阶段一 [0, 0.60]: " + stage1 + " 个");
        System.out.println("阶段二 [0.63, 0.85]: " + stage2 + " 个");
        System.out.println("阶段三 (0.87, 0.95]: " + stage3 + " 个");
    }
}
