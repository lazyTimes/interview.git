package com.zxd.interview.work;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.work
 * @Description : TODO
 * @Create on : 2024/6/4 17:07
 **/
public class Hotel {
    private String name;
    private int roomsAvailable;

    public Hotel(String name, int roomsAvailable) {
        this.name = name;
        this.roomsAvailable = roomsAvailable;
    }

    public String getName() {
        return name;
    }

    public int getRoomsAvailable() {
        return roomsAvailable;
    }

    public void bookRoom() {
        if (roomsAvailable > 0) {
            roomsAvailable--;
            System.out.println("预订成功！");
            inputUserInfo(); // 调用输入用户信息的方法
        } else {
            System.out.println("预订失败，房间已满！");
        }
    }

    private void inputUserInfo() {
        JFrame frame = new JFrame("用户信息");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setLayout(new GridLayout(4, 2));

        JLabel nameLabel = new JLabel("姓名：");
        JTextField nameField = new JTextField();
        JLabel idNumberLabel = new JLabel("身份证号码：");
        JTextField idNumberField = new JTextField();
        JLabel phoneNumberLabel = new JLabel("电话号码：");
        JTextField phoneNumberField = new JTextField();
        JButton submitButton = new JButton("提交");

        frame.add(nameLabel);
        frame.add(nameField);
        frame.add(idNumberLabel);
        frame.add(idNumberField);
        frame.add(phoneNumberLabel);
        frame.add(phoneNumberField);
        frame.add(submitButton);

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                String idNumber = idNumberField.getText();
                String phoneNumber = phoneNumberField.getText();
                System.out.println("用户信息如下：");
                System.out.println("姓名：" + name);
                System.out.println("身份证号码：" + idNumber);
                System.out.println("电话号码：" + phoneNumber);
                frame.dispose(); // 关闭窗口
            }
        });

        frame.setVisible(true);
    }

}
