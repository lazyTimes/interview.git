package com.zxd.interview.work;

import java.util.Scanner;

public class HotelBookingSystem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入酒店名称：");
        String hotelName = scanner.nextLine();
        System.out.println("请输入可用房间数量：");
        int roomsAvailable = scanner.nextInt();

        Hotel hotel = new Hotel(hotelName, roomsAvailable);

        while (true) {
            System.out.println("请选择操作：1. 预订房间 2. 查看剩余房间数量 3. 退出");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    hotel.bookRoom();
                    break;
                case 2:
                    System.out.println("剩余房间数量：" + hotel.getRoomsAvailable());
                    break;
                case 3:
                    System.out.println("感谢使用酒店预订系统！");
                    scanner.close();
                    return;
                default:
                    System.out.println("无效的选择，请重新输入！");
            }
        }
    }

}
