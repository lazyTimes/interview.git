package com.zxd.interview.exercise2.three;

import java.util.Arrays;

// 实现 Position 类，比较到原点的距离
public class TestPosition {
    public static void main(String[] args) {
        Position[] positions = {
                new Position(3, 4),
                new Position(1, 1),
                new Position(0, 5)
        };

        Arrays.sort(positions);

        for (Position position : positions) {
            System.out.println(position);
        }
    }
}
