package com.zxd.interview.exercise2.three;


public class Position  implements Comparable<Position> {
    private int x;
    private int y;

    // Constructor
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    // Calculate the distance to the origin (0, 0)
    public double distanceToOrigin() {
        return Math.sqrt(x * x + y * y);
    }

    // Implement the compareTo method
    @Override
    public int compareTo(Position other) {
        double thisDistance = this.distanceToOrigin();
        double otherDistance = other.distanceToOrigin();
        return Double.compare(thisDistance, otherDistance);
    }

    // Override toString() method for better readability
    @Override
    public String toString() {
        return "Position{" + "x=" + x + ", y=" + y + '}';
    }
}
