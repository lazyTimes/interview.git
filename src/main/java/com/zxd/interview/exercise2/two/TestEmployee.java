package com.zxd.interview.exercise2.two;

import java.util.Arrays;

// 编写程序，修改 Employee 的定义，使它能够根据员工的薪水(salary 字段值)进行比较，薪水高的员工排在前面。提示:实现比较需要实现Comparable 接口。
public class TestEmployee {

    public static void main(String[] args) {
        Employee[] employees = {
                new Employee("Alice", 75000),
                new Employee("Bob", 50000),
                new Employee("Charlie", 100000)
        };

        Arrays.sort(employees);

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }
}
