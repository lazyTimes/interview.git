package com.zxd.interview.exercise2.one;

//.设计一个名为 Swimmable 的接口，其中包含 void swim ()方法，设计另一个名为 Flyable 接口，其中包含 void fly()方法。定义一个 Duck 类实现上述两个接口。定义测试类，演示接口类型的使用。
public class TestDuck {
    public static void main(String[] args) {
        Duck duck = new Duck();
        duck.swim();
        duck.fly();
    }
}
