package com.zxd.interview.exercise2.one;


public class Duck implements Swimmable, Flyable {

    @Override
    public void swim() {
        System.out.println("Duck is swimming!");
    }

    @Override
    public void fly() {
        System.out.println("Duck is flying!");
    }
}
