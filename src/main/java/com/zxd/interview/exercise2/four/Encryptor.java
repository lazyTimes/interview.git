package com.zxd.interview.exercise2.four;

import java.util.Scanner;

// 编写加密程序，将字母转换为下一个字母
public class Encryptor {
    public static String encrypt(String input) {
        StringBuilder encrypted = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (c >= 'a' && c <= 'z') {
                encrypted.append(c == 'z' ? 'a' : (char) (c + 1));
            } else if (c >= 'A' && c <= 'Z') {
                encrypted.append(c == 'Z' ? 'A' : (char) (c + 1));
            } else {
                encrypted.append(c);
            }
        }
        return encrypted.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a string to encrypt: ");
        String input = scanner.nextLine();
        System.out.println("Encrypted string: " + encrypt(input));
    }
}
