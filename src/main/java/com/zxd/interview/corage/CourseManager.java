package com.zxd.interview.corage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CourseManager {

    private Map<String, Student> students;
    private Map<String, Course> courses;

    public CourseManager() {
        students = new HashMap<>();
        courses = new HashMap<>();
    }

    public void addStudent(String id, String name) {
        students.put(id, new Student(id, name));
    }

    public void addCourse(String name, int credits) {
        courses.put(name, new Course(name, credits));
    }

    public void addCourseRecord(String studentId, String courseName, double grade) {
        Student student = students.get(studentId);
        Course course = courses.get(courseName);
        if (student != null && course != null) {
            student.addCourseRecord(new CourseRecord(course, grade));
        } else {
            System.out.println("Student or Course not found.");
        }
    }

    public List<CourseRecord> getCourseRecords(String studentId) {
        Student student = students.get(studentId);
        if (student != null) {
            return student.getCourseRecords();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "CourseManager{students=" + students + ", courses=" + courses + '}';
    }
}
