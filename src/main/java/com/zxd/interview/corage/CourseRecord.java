package com.zxd.interview.corage;


public class CourseRecord {

    private Course course;
    private double grade;

    public CourseRecord(Course course, double grade) {
        this.course = course;
        this.grade = grade;
    }

    public Course getCourse() {
        return course;
    }

    public double getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return "CourseRecord{course=" + course + ", grade=" + grade + '}';
    }
}
