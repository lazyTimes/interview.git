package com.zxd.interview.corage;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String id;
    private String name;
    private List<CourseRecord> courseRecords;

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
        this.courseRecords = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<CourseRecord> getCourseRecords() {
        return courseRecords;
    }

    public void addCourseRecord(CourseRecord courseRecord) {
        courseRecords.add(courseRecord);
    }

    @Override
    public String toString() {
        return "Student{id='" + id + "', name='" + name + "', courseRecords=" + courseRecords + '}';
    }
}
