package com.zxd.interview.corage;


public class Main {

    public static void main(String[] args) {
        CourseManager manager = new CourseManager();

        // 添加学生
        manager.addStudent("001", "Alice");
        manager.addStudent("002", "Bob");

        // 添加课程
        manager.addCourse("Math", 4);
        manager.addCourse("English", 3);

        // 为学生添加课程记录
        manager.addCourseRecord("001", "Math", 95.0);
        manager.addCourseRecord("001", "English", 88.5);
        manager.addCourseRecord("002", "Math", 79.0);

        // 获取学生的课程记录列表
        System.out.println("Alice's Course Records: " + manager.getCourseRecords("001"));
        System.out.println("Bob's Course Records: " + manager.getCourseRecords("002"));

        // 输出所有学生和课程
        System.out.println(manager);
    }
}
