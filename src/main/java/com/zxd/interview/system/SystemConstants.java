package com.zxd.interview.system;

import com.zxd.interview.util.date.DateHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * 说明： 常量类
 */
public class SystemConstants {
    /**
     * 用于性能日志的日志记录器
     */
    public static final Logger PERFORMANCE_LOGGER = LoggerFactory.getLogger("PerformanceLogger");

    /**
     * 用于安全日志的日志记录器
     */
    public static final Logger SECURITY_LOGGER = LoggerFactory.getLogger("SecurityLogger");

    /**
     * 默认每页记录数
     */
    public static final int PAGE_SIZE_DEFAULT = 20;

    /**
     * 每页最大记录数
     */
    public static final int PAGE_SIZE_MAX = 500;

    /**
     * 默认的分页数目
     */
    public static final int DEFAULT_PAGE_SIZE = 10;

    /**
     * 系统默认使用的文件编码
     */
    public static final String FILE_ENCODING = "UTF-8";

    /**
     * 用于编码URL的编码格式
     */
    public static final String URL_ENCODING = "UTF-8";

    /**
     * 简单的图片验证码
     */
    public static final String CAPTCHA_CODE = "captcha_code";

    /**
     * session中存放手机号和手机验证码的对象
     */
    public static final String PHONE_CHECK_TIMER = "PHONE_CHECK_TIMER";
    /**
     * 接受手机验证码的手机号
     */
    public static final String CHECK_PHONE = "CHECK_PHONE";
    /**
     * 短信验证码
     */
    public static final String CHECK_CODE = "CHECK_CODE";

    /**
     * AjaxDTO中成功的返回码
     */
    public static final int AJAXDTO_SUCCESS_CODE = 1;
    /**
     * AjaxDTO中失败的返回码
     */
    public static final int AJAXDTO_ERROR_CODE = 0;

    /**
     * 获取当前日期，日期格式为默认"yyyy-MM-dd HH:mm:ss"
     */
    public static final String NOW_DATE_FORMAT_DEFAULT = DateHelper.getNow("yyyy-MM-dd HH:mm:ss");

    /**
     * 附件位置
     */
    public static final String ATTACHEMENT_PATH = File.separator + "uploadfiles";

}