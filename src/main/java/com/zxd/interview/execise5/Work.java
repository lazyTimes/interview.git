package com.zxd.interview.execise5;

/**
 * 请使用Java语言定义一个单链表的类（LinkArray）,该单链表存储的数据类型为整型即可，链表中的每一个节点也应该定义成一个类（LinkNode），
 *
 * LinkArray至少包含以下成员方法：
 * （1）public void add(int number) 向链表尾端插入一个数值
 * （2）public void show() 输出链表中的每一个数值
 * （3）public int size() 获取已经链表长度，即已经存储了多少个元素
 * （4）如何去编写成员变量和构造方法，需要你认真思考
 */
public class Work {

    // 主方法用于测试
    public static void main(String[] args) {
        LinkArray list = new LinkArray();
        list.add(1);
        list.add(2);
        list.add(3);
        list.show();  // 输出: 1 2 3
        System.out.println("Size: " + list.size());  // 输出: Size: 3
    }
}

// 链表节点类
class LinkNode {
    int value;
    LinkNode next;

    // 构造方法
    public LinkNode(int value) {
        this.value = value;
        this.next = null;
    }
}

// 单链表类
 class LinkArray {
    private LinkNode head; // 链表头节点
    private int size;      // 链表大小

    // 构造方法，初始化链表
    public LinkArray() {
        this.head = null;
        this.size = 0;
    }

    // 向链表尾端插入一个数值
    public void add(int number) {
        LinkNode newNode = new LinkNode(number);
        if (head == null) {
            head = newNode;
        } else {
            LinkNode current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
        size++;
    }

    // 输出链表中的每一个数值
    public void show() {
        LinkNode current = head;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }
        System.out.println();
    }

    // 获取链表长度，即已经存储了多少个元素
    public int size() {
        return size;
    }


}