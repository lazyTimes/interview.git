package com.zxd.interview.currency;

import java.util.Currency;
import java.util.Set;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.current
 * @Description : Java 9 之前旧版本API
 * @Create on : 2023/6/19 14:41
 **/
public class CurrencyTest {

    public static void main(String[] args) {
        Set<Currency> availableCurrencies = Currency.getAvailableCurrencies();
        availableCurrencies.forEach(System.out::println);
    }
}
