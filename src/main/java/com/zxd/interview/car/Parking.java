package com.zxd.interview.car;

import java.util.Calendar;
import java.util.Scanner;
import java.util.Stack;

public class Parking {

    static final int PARK_SIZE = 4; // 停车场场地大小
//    static final int NUM_LEN = 20; // 车牌号最大长度

    static class StackCar {
        String carNumber; // 汽车车牌号
        boolean isEnter = false; // 是否已经进入停车场
        int arrTimeHour, arrTimeMin, arrTimeSec; // 汽车到达时间 时 分 秒

        public StackCar(String carNumber) {
            this.carNumber = carNumber;
        }
    }

    static class StackPart {
        Stack<StackCar> stack = new Stack<>(); // 停车场的堆栈
        int stackSizeCurrent = 0; // 此时停车场中的车辆

        public void push(StackCar car) {
            if (!car.isEnter) {
                Calendar cal = Calendar.getInstance();
                car.arrTimeHour = cal.get(Calendar.HOUR_OF_DAY);
                car.arrTimeMin = cal.get(Calendar.MINUTE);
                car.arrTimeSec = cal.get(Calendar.SECOND);
                car.isEnter = true;
                System.out.printf("\n车牌号为: %s 的车辆进入车场\n时间为: %02d:%02d:%02d\n\n",
                        car.carNumber, car.arrTimeHour, car.arrTimeMin, car.arrTimeSec);
            }
            stack.push(car);
            stackSizeCurrent++;
        }

        public StackCar pop() {
            if (stackSizeCurrent == 0) {
                System.out.println("停车场为空!");
                return null;
            }
            stackSizeCurrent--;
            return stack.pop();
        }

        public boolean isEmpty() {
            return stackSizeCurrent == 0;
        }
    }

    static class QueueCar {
        String carNumber; // 汽车车号
        QueueCar next;

        public QueueCar(String carNumber) {
            this.carNumber = carNumber;
        }
    }

    static class LinkQueue {
        QueueCar front, rear; // 便道的队列的对头和队尾
        int length = 0; // 此时便道中的车辆数

        public void enqueue(String carNumber) {
            QueueCar newCar = new QueueCar(carNumber);
            if (rear != null) {
                rear.next = newCar;
            }
            rear = newCar;
            if (front == null) {
                front = newCar;
            }
            length++;
        }

        public QueueCar dequeue() {
            if (isEmpty()) {
                System.out.println("停车场通道为空!");
                return null;
            }
            QueueCar car = front;
            front = front.next;
            if (front == null) {
                rear = null;
            }
            length--;
            return car;
        }

        public boolean isEmpty() {
            return length == 0;
        }
    }

    public static float calculateFee(int h, int m, int s) {
        return ((h * 60 + m) * 60 + s) * 1.1f;
    }

    public static void exit(StackPart parking, StackPart tempParking, LinkQueue queue) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入您的车牌号: ");
        String carNumber = scanner.next();
        System.out.println("正在查询车辆，请耐心等待");

        Stack<StackCar> tempStack = new Stack<>();
        StackCar carToExit = null;

        // 找到要离开的车
        while (!parking.isEmpty()) {
            StackCar car = parking.pop();
            tempStack.push(car);
            if (car.carNumber.equals(carNumber)) {
                carToExit = car;
                break;
            }
        }

        // 如果找到了车
        if (carToExit != null) {
            while (!tempStack.isEmpty()) {
                StackCar car = tempStack.pop();
                if (!car.carNumber.equals(carNumber)) {
                    parking.push(car);
                }
            }

            Calendar cal = Calendar.getInstance();
            int h = cal.get(Calendar.HOUR_OF_DAY) - carToExit.arrTimeHour;
            int m = (cal.get(Calendar.MINUTE) + 60 - carToExit.arrTimeMin) % 60;
            int s = (cal.get(Calendar.SECOND) + 60 - carToExit.arrTimeSec) % 60;
            float fee = calculateFee(h, m, s);

            System.out.printf("\n收据\n车牌号: %s\n", carNumber);
            System.out.println("++++++++++++++++++++++++++++++");
            System.out.printf("进车场时间: %02d:%02d:%02d\n", carToExit.arrTimeHour, carToExit.arrTimeMin, carToExit.arrTimeSec);
            System.out.printf("出车场时间: %02d:%02d:%02d\n", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
            System.out.printf("停留时间: %02d小时%02d分%02d秒\n", h, m, s);
            System.out.printf("应付(元): %.2f\n", fee);
            System.out.println("++++++++++++++++++++++++++++++\n");

            if (parking.stackSizeCurrent < PARK_SIZE && !queue.isEmpty()) {
                QueueCar queueCar = queue.dequeue();
                StackCar newCar = new StackCar(queueCar.carNumber);
                parking.push(newCar);
                System.out.printf("车牌为%s的车已从通道进入停车场, 请把车辆停在空位上\n", newCar.carNumber);
            }
        } else {
            System.out.println("抱歉，停车场内无该车辆!");
        }
    }


    public static void enter(StackPart parking, LinkQueue queue) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入车牌号: ");
        String carNumber = scanner.next();
        if (parking.stackSizeCurrent < PARK_SIZE) {
            StackCar car = new StackCar(carNumber);
            parking.push(car);
            System.out.println("请把你的车停在空的车位上!\n\n");
        } else {
            queue.enqueue(carNumber);
            System.out.printf("停车场已满，请把你的车停在便道的第%d个位置上!\n", queue.length);
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        StackPart parking = new StackPart();
        StackPart tempParking = new StackPart();
        LinkQueue queue = new LinkQueue();

        while (true) {
            System.out.println("\n******************停车场管理程序***************");
            System.out.println("*==============================================*");
            System.out.println("1. 汽车进车场");
            System.out.println("2. 汽车出车场");
            System.out.println("3. 退出程序");
            System.out.println("*==============================================*");
            System.out.println("温馨提示：请车主在24:00之前来取车，给您带来的不便,敬请原谅!");
            System.out.print("请输入您需要的服务的代号(1、2、3)，谢谢！: ");
            String choice = scanner.next();

            switch (choice) {
                case "1":
                    enter(parking, queue);
                    break;
                case "2":
                    exit(parking, tempParking, queue);
                    break;
                case "3":
                    System.out.println("谢谢使用!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("输入错误, 请重新输入!");
                    break;
            }
        }
    }

}
