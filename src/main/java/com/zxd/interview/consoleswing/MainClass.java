package com.zxd.interview.consoleswing;

import java.util.HashMap;
import java.util.Scanner;

public class MainClass {

    static class Word {

        private String name;
        private String meaning;

        public Word(String name, String meaning) {
            this.name = name;
            this.meaning = meaning;
        }

        public String getName() {
            return name;
        }

        public String getMeaning() {
            return meaning;
        }
    }

     public static void main(String[] args) {
         runByConsole();
     }



    private static void runByConsole() {
        Dictionary myDictionary = new Dictionary();
        while (true) {
            System.out.println("————————欢迎使用中英文词典系统————————");
            System.out.println("1.添加中文单词");
            System.out.println("2.添加英文单词");
            System.out.println("3.修改中文单词");
            System.out.println("4.修改英文单词");
            System.out.println("5.删除中文单词");
            System.out.println("6.删除英文单词");
            System.out.println("7.查询中文单词翻译");
            System.out.println("8.查询英文单词翻译");
            System.out.println("9.退出系统");
            System.out.println("请输入你的选择：");
            Scanner sc = new Scanner(System.in);
            int i = sc.nextInt();
            switch (i) {
                case 1:
                    myDictionary.addChWord();
                    break;
                case 2:
                    myDictionary.addEnWord();
                    break;
                case 3:
                    myDictionary.changeCh();
                    break;
                case 4:
                    myDictionary.changeEn();
                    break;
                case 5:
                    myDictionary.deleteCh();
                    break;
                case 6:
                    myDictionary.deleteEn();
                    break;
                case 7:
                    myDictionary.seachEn();
                    break;
                case 8:
                    myDictionary.seachCh();
                    break;
                case 9:
                    System.exit(0);
                    break;
                default:
                    System.out.println("无效的选择，请重新输入。");
            }
        }
    }


    static class Dictionary {
        private HashMap<String, Word> ch;
        private HashMap<String, Word> en;

        // 初始化
        public Dictionary() {
            this.ch = new HashMap<>();
            this.en = new HashMap<>();
        }

        // 添加中文单词
        public void addChWord() {
            System.out.println("请输入单词名称");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            if (ch.containsKey(name)) {
                System.out.println("这个单词已存在，请重试");
            } else {
                System.out.println("请输入这个单词的英文翻译");
                String meaning = sc.next();
                ch.put(name, new Word(name, meaning));
                System.out.println("添加成功");
            }
            backMenu();
        }

        // 添加英文单词
        public void addEnWord() {
            System.out.println("请输入单词名称");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            if (en.containsKey(name)) {
                System.out.println("这个单词已存在，请重试");
            } else {
                System.out.println("请输入这个单词的中文翻译");
                String meaning = sc.next();
                en.put(name, new Word(name, meaning));
                System.out.println("添加成功");
            }
            backMenu();
        }

        // 改变中文单词
        public void changeCh() {
            System.out.println("请输入您要修改的中文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            boolean flag = ch.containsKey(name);
            if (!flag) {
                System.out.println("您要修改的中文单词不存在，请重试");
            } else {
                System.out.println("请输入修改后的内容");
                String meaning = sc.next();
                ch.put(name, new Word(name, meaning));
                System.out.println("修改成功");
            }
            backMenu();
        }
//    // 改变英文单词
//    public Phoebe:

        // 改变英文单词
        public void changeEn() {
            System.out.println("请输入您要修改的英文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            boolean flag = en.containsKey(name);
            if (!flag) {
                System.out.println("您要修改的英文单词不存在，请重试");
            } else {
                System.out.println("请输入修改后的内容");
                String meaning = sc.next();
                en.put(name, new Word(name, meaning));
                System.out.println("修改成功");
            }
            backMenu();
        }

        // 删除中文单词
        public void deleteCh() {
            System.out.println("请输入您要删除的中文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            if (ch.remove(name) != null)
                System.out.println("删除成功");
            else
                System.out.println("您要删除的单词不存在，请重试");
            backMenu();
        }

        // 删除英文单词
        public void deleteEn() {
            System.out.println("请输入您要删除的英文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            if (en.remove(name) != null)
                System.out.println("删除成功");
            else
                System.out.println("您要删除的单词不存在，请重试");
            backMenu();
        }

        // 查询中文单词的翻译
        public void seachEn() {
            System.out.println("请输入您要查询的中文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            boolean flag = ch.containsKey(name);
            if (!flag) {
                System.out.println("您要查询的中文单词不存在,请重试");
            } else {
                Word word = ch.get(name);
                System.out.println(name + "的英文翻译是" + word.getMeaning());
            }
            backMenu();
        }

        // 查询英文单词的中文翻译
        public void seachCh() {
            System.out.println("请输入您要查询的英文单词");
            Scanner sc = new Scanner(System.in);
            String name = sc.next();
            boolean flag = en.containsKey(name);
            if (!flag) {
                System.out.println("您要查询的英文单词不存在,请重试");
            } else {
                Word word = en.get(name);
                System.out.println(name + "的中文翻译是" + word.getMeaning());
            }

            Phoebe:
            backMenu();
        }

        public void backMenu() {
            System.out.println("请输入 9 返回菜单");
            Scanner sc = new Scanner(System.in);
            sc.next();
            System.out.println();
        }

    }
}
