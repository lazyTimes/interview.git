package com.zxd.interview.consoleswing;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.consoleswing
 * @Description : TODO
 * @Create on : 2024/6/9 11:48
 **/
public class GUI {

    static class Word {
        private String name;
        private String meaning;

        public Word(String name, String meaning) {
            this.name = name;
            this.meaning = meaning;
        }

        public String getName() {
            return name;
        }

        public String getMeaning() {
            return meaning;
        }
    }

    static class Dictionary {
        private HashMap<String, Word> ch;
        private HashMap<String, Word> en;

        // 初始化
        public Dictionary() {
            this.ch = new HashMap<>();
            this.en = new HashMap<>();
        }

        // 添加中文单词
        public void addChWord(String name, String meaning) {
            if (ch.containsKey(name)) {
                JOptionPane.showMessageDialog(null, "这个单词已存在，请重试");
            } else {
                ch.put(name, new Word(name, meaning));
                JOptionPane.showMessageDialog(null, "添加成功");
            }
        }

        // 添加英文单词
        public void addEnWord(String name, String meaning) {
            if (en.containsKey(name)) {
                JOptionPane.showMessageDialog(null, "这个单词已存在，请重试");
            } else {
                en.put(name, new Word(name, meaning));
                JOptionPane.showMessageDialog(null, "添加成功");
            }
        }

        // 修改中文单词
        public void changeCh(String name, String meaning) {
            if (!ch.containsKey(name)) {
                JOptionPane.showMessageDialog(null, "您要修改的中文单词不存在，请重试");
            } else {
                ch.put(name, new Word(name, meaning));
                JOptionPane.showMessageDialog(null, "修改成功");
            }
        }

        // 修改英文单词
        public void changeEn(String name, String meaning) {
            if (!en.containsKey(name)) {
                JOptionPane.showMessageDialog(null, "您要修改的英文单词不存在，请重试");
            } else {
                en.put(name, new Word(name, meaning));
                JOptionPane.showMessageDialog(null, "修改成功");
            }
        }

        // 删除中文单词
        public void deleteCh(String name) {
            if (ch.remove(name) != null)
                JOptionPane.showMessageDialog(null, "删除成功");
            else
                JOptionPane.showMessageDialog(null, "您要删除的单词不存在，请重试");
        }

        // 删除英文单词
        public void deleteEn(String name) {
            if (en.remove(name) != null)
                JOptionPane.showMessageDialog(null, "删除成功");
            else
                JOptionPane.showMessageDialog(null, "您要删除的单词不存在，请重试");
        }

        // 查询中文单词的翻译
        public void searchEn(String name, JTextArea displayArea) {
            if (!ch.containsKey(name)) {
                displayArea.append("您要查询的中文单词不存在, 请重试\n");
            } else {
                Word word = ch.get(name);
                displayArea.append(name + " 的英文翻译是 " + word.getMeaning() + "\n");
            }
        }

        // 查询英文单词的中文翻译
        public void searchCh(String name, JTextArea displayArea) {
            if (!en.containsKey(name)) {
                displayArea.append("您要查询的英文单词不存在, 请重试\n");
            } else {
                Word word = en.get(name);
                displayArea.append(name + " 的中文翻译是 " + word.getMeaning() + "\n");
            }
        }
    }

    public static void main(String[] args) {
        Dictionary myDictionary = new Dictionary();

        JFrame frame = new JFrame("中英文词典系统");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 400);
        frame.setLayout(new BorderLayout());

        JTextArea displayArea = new JTextArea();
        displayArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(displayArea);
        frame.add(scrollPane, BorderLayout.CENTER);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(4, 2));

        JTextField nameField = new JTextField();
        JTextField meaningField = new JTextField();
        JButton addButton = new JButton("添加单词");
        JButton changeButton = new JButton("修改单词");
        JButton deleteButton = new JButton("删除单词");
        JButton searchButton = new JButton("查询单词");

        inputPanel.add(new JLabel("单词:"));
        inputPanel.add(nameField);
        inputPanel.add(new JLabel("翻译:"));
        inputPanel.add(meaningField);

        inputPanel.add(addButton);
        inputPanel.add(changeButton);
        inputPanel.add(deleteButton);
        inputPanel.add(searchButton);

        frame.add(inputPanel, BorderLayout.SOUTH);

        // 创建操作选择框
        JComboBox<String> languageBox = new JComboBox<>(new String[]{"中文", "英文"});
        JComboBox<String> actionBox = new JComboBox<>(new String[]{"添加", "修改", "删除", "查询"});
        JPanel actionPanel = new JPanel();
        actionPanel.add(new JLabel("语言:"));
        actionPanel.add(languageBox);
        actionPanel.add(new JLabel("操作:"));
        actionPanel.add(actionBox);
        frame.add(actionPanel, BorderLayout.NORTH);
        addButton.addActionListener(e -> {
            String name = nameField.getText();
            String meaning = meaningField.getText();
            String language = (String) languageBox.getSelectedItem();
            if (language.equals("中文")) {
                myDictionary.addChWord(name, meaning);
            } else {
                myDictionary.addEnWord(name, meaning);
            }
            nameField.setText("");
            meaningField.setText("");
        });

        changeButton.addActionListener(e -> {
            String name = nameField.getText();
            String meaning = meaningField.getText();
            String language = (String) languageBox.getSelectedItem();
            if (language.equals("中文")) {
                myDictionary.changeCh(name, meaning);
            } else {
                myDictionary.changeEn(name, meaning);
            }
            nameField.setText("");
            meaningField.setText("");
        });

        deleteButton.addActionListener(e -> {
            String name = nameField.getText();
            String language = (String) languageBox.getSelectedItem();
            if (language.equals("中文")) {
                myDictionary.deleteCh(name);
            } else {
                myDictionary.deleteEn(name);
            }
            nameField.setText("");
        });

        searchButton.addActionListener(e -> {
            String name = nameField.getText();
            String language = (String) languageBox.getSelectedItem();
            if (language.equals("中文")) {
                myDictionary.searchCh(name, displayArea);
            } else {
                myDictionary.searchCh(name, displayArea);
            }
            nameField.setText("");
        });

//        frame.setJMenuBar(menuBar);
        frame.setVisible(true);
    }
}
