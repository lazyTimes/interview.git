package com.zxd.interview.minorGc;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.minorGc
 * @Description :
 * 大对象直接进入老年代
 * JVM参数 :
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:+PrintHeapAtGC
 * @Create on : 2021/6/4 16:16
 **/
public class Object2Old {


    /**
     * JVM 参数：
     * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=3145728
     * -XX:PretenureSizeThreshold=3145728 (相当于3M)
     * @param args
     */
    public static void main(String[] args) {
        byte[] allocation1;
        allocation1 = new byte[1024 * 1024 * 4];                                                                                                                                                                                                                                                    ;
    }
}
