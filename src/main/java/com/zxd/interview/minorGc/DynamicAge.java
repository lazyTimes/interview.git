package com.zxd.interview.minorGc;

/**
 * jvm 动态年龄判断
 */
public class DynamicAge {

    private static final int _1MB = 1024 * 1024;

    /**
     * JVM 参数：
     * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:+UseSerialGC
     * -XX: MaxTenuringThreshold=15
     * -XX: MaxTenuringThreshold=1
     *
     * @param args
     */
    public static void main(String[] args) {
        // allocation1 + allocation2 的大小大于 survivor空间的1/2
        byte[] allocation7 = new byte[_1MB / 2];
        byte[] allocation8 = new byte[_1MB / 2];
        byte[] allocation9 = new byte[_1MB / 2];
        byte[] allocation10 = new byte[_1MB / 2];
        byte[] allocation11 = new byte[_1MB / 2];
        byte[] allocation12 = new byte[_1MB / 2];

//        byte[] allocation3 = new byte[_1MB * 4];
//        byte[] allocation4 = new byte[_1MB * 4];
//        allocation4 = null;
//        allocation4 = new byte[_1MB * 4];

    }/*运行结果：
    程序默认使用：4303K 的内存

    */
}
