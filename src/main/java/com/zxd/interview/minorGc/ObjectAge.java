package com.zxd.interview.minorGc;

/**
 * 对象年龄
 */
public class ObjectAge {
    /**
     * JVM 参数：
     * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold
     * -XX: MaxTenuringThreshold=15
     * -XX: MaxTenuringThreshold=1
     * @param args
     */
    public static void main(String[] args) {
        byte[] allocation1 = new byte[1024 * 1024 * 4];
        byte[] allocation2 = new byte[1024 * 1024 * 1];

    }/*
    长期存活对象进入老年代：
    注意：JVM默认需要使用4M的内存
    15 岁年龄情况
    Heap
     PSYoungGen      total 9216K, used 5327K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
      eden space 8192K, 65% used [0x00000000ff600000,0x00000000ffb33fb0,0x00000000ffe00000)
      from space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
      to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
     ParOldGen       total 10240K, used 4096K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
      object space 10240K, 40% used [0x00000000fec00000,0x00000000ff000010,0x00000000ff600000)
     Metaspace       used 3433K, capacity 4496K, committed 4864K, reserved 1056768K
      class space    used 369K, capacity 388K, committed 512K, reserved 1048576K
    1 岁年龄情况
    [GC (Allocation Failure) [PSYoungGen: 8192K->1008K(9216K)] 8192K->5606K(19456K), 0.0023319 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
    Heap
     PSYoungGen      total 9216K, used 2170K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
      eden space 8192K, 14% used [0x00000000ff600000,0x00000000ff722a70,0x00000000ffe00000)
      from space 1024K, 98% used [0x00000000ffe00000,0x00000000ffefc020,0x00000000fff00000)
      to   space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
     ParOldGen       total 10240K, used 4598K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
      object space 10240K, 44% used [0x00000000fec00000,0x00000000ff07dae0,0x00000000ff600000)
     Metaspace       used 3328K, capacity 4496K, committed 4864K, reserved 1056768K
      class space    used 361K, capacity 388K, committed 512K, reserved 1048576K
    */
}
