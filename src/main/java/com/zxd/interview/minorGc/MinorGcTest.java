package com.zxd.interview.minorGc;

/**
 * 新生代回收 MINOR GC回收新生代空间
 * 配置：-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:+UseSerialGC
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
 * 含义：堆大小20M，其中10M给新生代，10M给老年代
 * -XX:+PrintHeapAtGC: 查看堆的变动情况
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.minorGc
 * @Description :
 * @Create on : 2021/6/3 13:03
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=10M -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc-%t.log
 **/
public class MinorGcTest {
    private static final int _1MB = 1024 * 1024;
    private static final int _1KB = 1024;

    public static void main(String[] args) throws InterruptedException {
        testAllocation();
    }

    public static void testAllocation() throws InterruptedException {
        byte[] allocation1, allocation2, allocation3, allocation4, allocation5;
        allocation1 = new byte[_1MB * 3];
        allocation1 = new byte[_1MB * 4];
        Thread.sleep(20000);
//        allocation1 = new byte[_1MB * 1];
//        allocation3 = new byte[20*_1MB * 2];

    }/*jdk 1.8.0-221 运行结果：
        下面为 parallel old + parallel 收集器收集方案:
    -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
                                                        (可用空间eden 8194+1个survivor区大小1024)
    [GC (Allocation Failure) [PSYoungGen: 7925K->1006K(9216K)] 7925K->5394K(19456K), 0.0045482 secs] [Times: user=0.00 sys=0.00, real=0.01 secs]
    Heap
     PSYoungGen      total 9216K, used 7637K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
      eden space 8192K, 80% used [0x00000000ff600000,0x00000000ffc79b40,0x00000000ffe00000)
      from space 1024K, 98% used [0x00000000ffe00000,0x00000000ffefbbb0,0x00000000fff00000)
      to   space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
     ParOldGen       total 10240K, used 4387K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
      object space 10240K, 42% used [0x00000000fec00000,0x00000000ff048d18,0x00000000ff600000)
     Metaspace       used 3302K, capacity 4496K, committed 4864K, reserved 1056768K
      class space    used 355K, capacity 388K, committed 512K, reserved 1048576K

    下面为 serrial old + serrial 的组合
    -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGcDetail -XX:SurvivorRatio=8 -XX:+UseSerialGC
    [GC (Allocation Failure) [DefNew: 7925K->1023K(9216K), 0.0063912 secs] 7925K->5300K(19456K), 0.0064606 secs] [Times: user=0.00 sys=0.02, real=0.01 secs]
    Heap
     def new generation   total 9216K, used 7654K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
      eden space 8192K,  80% used [0x00000000fec00000, 0x00000000ff279b70, 0x00000000ff400000)
      from space 1024K,  99% used [0x00000000ff500000, 0x00000000ff5ffff8, 0x00000000ff600000)
      to   space 1024K,   0% used [0x00000000ff400000, 0x00000000ff400000, 0x00000000ff500000)
     tenured generation   total 10240K, used 4276K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
       the space 10240K,  41% used [0x00000000ff600000, 0x00000000ffa2d020, 0x00000000ffa2d200, 0x0000000100000000)
     Metaspace       used 3271K, capacity 4496K, committed 4864K, reserved 1056768K
      class space    used 355K, capacity 388K, committed 512K, reserved 1048576K
    */

    /*JDK 1.7.0_51 运行结果：
    -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
    下面为 parallel old + parallel 收集器收集方案:
    Heap
     PSYoungGen      total 9216K, used 7669K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
      eden space 8192K, 93% used [0x00000000ff600000,0x00000000ffd7d448,0x00000000ffe00000)
      from space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
      to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
     ParOldGen       total 10240K, used 4096K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
      object space 10240K, 40% used [0x00000000fec00000,0x00000000ff000010,0x00000000ff600000)
     PSPermGen       total 21504K, used 3019K [0x00000000f9a00000, 0x00000000faf00000, 0x00000000fec00000)
      object space 21504K, 14% used [0x00000000f9a00000,0x00000000f9cf2c78,0x00000000faf00000)


    下面为 serrial old + serrial 的组合
    -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGcDetail -XX:SurvivorRatio=8 -XX:+UseSerialGC
    [GC[DefNew: 7505K->533K(9216K), 0.0066009 secs] 7505K->6677K(19456K), 0.0066489 secs] [Times: user=0.03 sys=0.02, real=0.01 secs]
    Heap
     def new generation   total 9216K, used 5123K [0x00000000f9a00000, 0x00000000fa400000, 0x00000000fa400000)
      eden space 8192K,  56% used [0x00000000f9a00000, 0x00000000f9e7b6a0, 0x00000000fa200000)
      from space 1024K,  52% used [0x00000000fa300000, 0x00000000fa385660, 0x00000000fa400000)
      to   space 1024K,   0% used [0x00000000fa200000, 0x00000000fa200000, 0x00000000fa300000)
     tenured generation   total 10240K, used 6144K [0x00000000fa400000, 0x00000000fae00000, 0x00000000fae00000)
       the space 10240K,  60% used [0x00000000fa400000, 0x00000000faa00030, 0x00000000faa00200, 0x00000000fae00000)
     compacting perm gen  total 21248K, used 2923K [0x00000000fae00000, 0x00000000fc2c0000, 0x0000000100000000)
       the space 21248K,  13% used [0x00000000fae00000, 0x00000000fb0dada0, 0x00000000fb0dae00, 0x00000000fc2c0000)
    No shared spaces configured.
    */


}
