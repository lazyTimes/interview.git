package com.zxd.interview.minorGc;

/**
 * 蠕猿技术窝的案例：
 * -verbose:gc
 * -Xms20M 堆内存最小大小
 * -Xmx20M 堆内存最大大小
 * -Xmn10M 新生代大小
 * -XX:SurvivorRatio=8 新生代比例
 * -XX:MaxTenuringThreshold=15 多少年龄进入到老年代
 * -XX:PretenureSizeThreshold=10M 大于这个值直接在老年代分配
 * -XX:+UseParNewGC parnew收集器
 * -XX:+UseConcMarkSweepGC cms收集器
 * -XX:+PrintGCDetails 打印GC日志
 * -XX:+PrintGCTimeStamps 打印GC时间
 * -Xloggc:gc-%t.log gc日志
 *
 *  -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:PretenureSizeThreshold=10M -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc-%t.log
 */
public class SevenTest {

    private static final int _1M = 1024 * 1024;

    public static void main(String[] args) {
       byte[] array1 = new byte[2 * _1M];
       array1 = new byte[2 * _1M];
       array1 = new byte[2 * _1M];

       byte[] array2 = new byte[128 * 1024];
       array2 = null;
        String s = "";
       byte[] array3 = new byte[2 * _1M];

    }
}
