package com.zxd.interview.mycache.version4;

import com.zxd.interview.mycache.version3.MyCacheVersion3;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 对应当前版本的测试程序
 */
public class Test {

    /**
     * 第四版进行重构
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion4<String, Integer> cacheVersion4 = new MyCacheVersion4<>();
        Random random = new Random(100);
        for (int i = 0; i < 1000; i++) {
            executorService.submit(() -> {
                int randomInt = random.nextInt(100);
                try {
                    Integer user = cacheVersion4.compute(String.valueOf(randomInt));
                    System.out.println("result => " + user);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            });
        }
        executorService.shutdown();
    }/**运行结果
     
     */
}
