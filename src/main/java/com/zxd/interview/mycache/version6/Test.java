package com.zxd.interview.mycache.version6;

import com.zxd.interview.mycache.version5.MyCacheVersion5;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 缓存过期功能测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.mycache.version6
 * @Description : 缓存过期功能测试
 * @Create on : 2023/6/20 17:06
 **/
public class Test {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion6<String, Integer> myCacheVersion5 = new MyCacheVersion6<>();
        Random random = new Random(100);
        for (int i = 0; i < 1000; i++) {
            executorService.submit(() -> {
                int randomInt = random.nextInt(100);

                Integer user = myCacheVersion5.compute(String.valueOf(randomInt));
                System.out.println("result => " + user);


            });
        }
        executorService.shutdown();
    }
}
