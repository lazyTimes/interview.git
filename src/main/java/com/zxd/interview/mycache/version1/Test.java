package com.zxd.interview.mycache.version1;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 对应当前版本的测试程序
 * 1. HashMap线程不安全
 * 2. 此程序验证线程安全问题
 */
@Slf4j
public class Test {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion1 objectObjectMyCacheVersion1 = new MyCacheVersion1();
        Random random = new Random(100);
        for (int i = 0; i < 1000; i++) {
            executorService.execute(() -> {
                int randomInt = random.nextInt(100);
                try {
                    Integer user = objectObjectMyCacheVersion1.compute(String.valueOf(randomInt));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            });
        }
        executorService.shutdown();
    }/**
     测试结果：可以看到高并发的情况下非常有可能出现重复计算然后cache的情况
     10:56:41.437 [pool-1-thread-53] INFO  c.z.i.m.version1.MyCacheVersion1 - doCompute => 59
     10:56:41.447 [pool-1-thread-97] INFO  c.z.i.m.version1.MyCacheVersion1 - doCompute => 59
     */
}
