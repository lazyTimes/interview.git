package com.zxd.interview.mycache.version1;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 初版高速缓存实现
 * 1. 使用简单的HashMap
 * 2. 在高速缓存中进行计算
 *
 * 暴露问题：
 * 1. 复用性能查
 * 2. HashMap线程不安全，有可能重复判断
 */
@Slf4j
public class MyCacheVersion1 {

    private final Map<String, Integer> cache = new HashMap<>();

    /**
     * 根据参数计算结果，此参数对于同一个参数会计算同样的结果
     * 1. 如果缓存存在结果，直接返回
     * 2. 如果缓存不存在，则需要计算添加到map之后才返回
     * @param userId
     * @return
     */
    public synchronized Integer compute(String userId) throws InterruptedException {
        if(cache.containsKey(userId)){
            log.info("cached => {}", userId);
            return cache.get(userId);
        }
        log.info("doCompute => {}", userId);
        Integer result = doCompute(userId);
        //不存在缓存就加入
        cache.put(userId, result);
        return result;
    }/**运行结果
     串行之后执行效率极低，基本无法使用
     11:14:15.851 [pool-1-thread-1] INFO  c.z.i.m.version1.MyCacheVersion1 - doCompute => 15
     11:14:20.862 [pool-1-thread-100] INFO  c.z.i.m.version1.MyCacheVersion1 - doCompute => 52
     11:14:25.874 [pool-1-thread-99] INFO  c.z.i.m.version1.MyCacheVersion1 - doCompute => 55
     */

    private  Integer doCompute(String userId) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        return Integer.parseInt(userId);
    }


}
