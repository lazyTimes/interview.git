package com.zxd.interview.mycache.version2;

import com.zxd.interview.mycache.version1.MyCacheVersion1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 对应当前版本的测试程序
 */
public class Test {

    /**
     * 第二个版本主要讲代码复用性能提高
     * @description 第二个版本主要讲代码复用性能提高
     * @param args
     * @return void
     * @author xander
     * @date 2023/6/15 16:34
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion2 myCacheVersion2 = new MyCacheVersion2();
        Random random = new Random(100);
        for (int i = 0; i < 1000; i++) {
            executorService.submit(() -> {
                int randomInt = random.nextInt(100);
                try {
                    Integer user = myCacheVersion2.compute(String.valueOf(randomInt));
                    System.out.println("result => " + user);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            });
        }
        executorService.shutdown();
    }/**

     */
}
