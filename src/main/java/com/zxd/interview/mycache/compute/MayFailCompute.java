package com.zxd.interview.mycache.compute;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 *  可能会出现失败的计算方法
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.mycache.compute
 * @Description : 可能会出现失败的计算方法
 * @Create on : 2023/6/19 10:40
 **/
public class MayFailCompute implements ComputeAble<String, Integer>{

    /**
     * 触发失败阈值
     */
    private static final int FAILURE_THRESHOLD = 50;

    /**
     * 随机数生成器
     */
    private static final Random RANDOM = new Random(100);

    /**
     * 有可能会出现失败的计算方法
     * @description 有可能会出现失败的计算方法
     * @param arg
     * @return java.lang.Integer
     * @author xander
     * @date 2023/6/19 10:41
     */
    @Override
    public Integer doCompute(String arg) throws Exception {
        if(RANDOM.nextInt() < FAILURE_THRESHOLD){
            throw new Exception("自定义异常");
        }
        TimeUnit.SECONDS.sleep(5);
        return Integer.parseInt(arg);
    }
}
