package com.zxd.interview.mycache.compute;

import java.util.concurrent.TimeUnit;

/**
 * 装饰模式改写接口
 */
public class ExpensiveCompute implements ComputeAble<String, Integer>{
    @Override
    public Integer doCompute(String arg) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        return Integer.parseInt(arg);
    }
}
