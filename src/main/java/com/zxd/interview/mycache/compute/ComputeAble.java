package com.zxd.interview.mycache.compute;

/**
 * 可计算接口
 * 装饰接口
 */
public interface ComputeAble<A, V>{

    /**
     * 根据指定参数 A 进行计算，计算结果为 V
     * @description 根据指定参数 A 进行计算，计算结果为 V
     * @param arg 泛型参数
     * @return V 返回计算后结果
     * @author xander
     * @date 2023/6/15 15:42
     */
    V doCompute(A arg) throws Exception;
}
