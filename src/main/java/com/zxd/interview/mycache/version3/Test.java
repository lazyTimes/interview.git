package com.zxd.interview.mycache.version3;

import com.zxd.interview.mycache.version2.MyCacheVersion2;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 对应当前版本的测试程序
 */
public class Test {

    /**
     * 第三个版本对于代码重新重写，解决重复计算的性能问题，去掉加锁操作
     * @description 第二个版本主要讲代码复用性能提高
     * @param args
     * @return void
     * @author xander
     * @date 2023/6/15 16:34
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion3 cacheVersion3 = new MyCacheVersion3();
        Random random = new Random(100);
        for (int i = 0; i < 1000; i++) {
            executorService.submit(() -> {
                int randomInt = random.nextInt(100);
                try {
                    Integer user = cacheVersion3.compute(String.valueOf(randomInt));
                    System.out.println("result => " + user);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            });
        }
        executorService.shutdown();
    }/**运行结果
     result => 90
     result => 3
     result => 40
     result => 30
     FutureTask 调用计算函数
     result => 60
     FutureTask 调用计算函数
     result => 27
     FutureTask 调用计算函数
     result => 14
     result => 33
     result => 39
     result => 8
     FutureTask 调用计算函数
     result => 70
     FutureTask 调用计算函数
     result => 62
     result => 25
     result => 1
     result => 63
     其他线程进行设置,重新执行计算
     result => 81
     result => 96
     FutureTask 调用计算函数
     FutureTask 调用计算函数
     result => 60
     result => 89
     result => 96
     result => 10
     FutureTask 调用计算函数
     result => 37
     result => 19
     result => 63
     result => 94
     result => 19
     result => 64
     result => 21
     result => 96
     FutureTask 调用计算函数
     result => 60
     FutureTask 调用计算函数
     result => 40
     FutureTask 调用计算函数
     result => 70
     FutureTask 调用计算函数
     其他线程进行设置,重新执行计算
     */
}
