package com.zxd.interview.mycache.version7;

import com.zxd.interview.mycache.version6.MyCacheVersion6;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 整体性能测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.mycache.version6
 * @Description : 整体性能测试
 * @Create on : 2023/6/20 17:06
 **/
public class Test {

    public static void main(String[] args) throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        MyCacheVersion6<String, Integer> myCacheVersion5 = new MyCacheVersion6<>();
        Random random = new Random(200);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < 100; i++) {
            executorService.submit(() -> {
                int randomInt = random.nextInt(100);

                try {
                    countDownLatch.await();
                    // 从线程安全的集合当中取出当前时间
                    SimpleDateFormat simpleDateFormat = ThreadSafeFormatter.dateFormatter.get();
                    System.out.println("simpleDateFormat => "+ simpleDateFormat.format(new Date()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Integer user = myCacheVersion5.compute(String.valueOf(randomInt), 5000);
                System.out.println("result => " + user);
            });
        }
        // 假设此时所有的请求需要5秒时间准备。
        Thread.sleep(1000);
        countDownLatch.countDown();
        executorService.shutdown();
        long endTime = System.currentTimeMillis();
        // 如果线程池没有停止一直死循环
        while(!executorService.isTerminated()){

        }
        System.out.println("最终时间" + (endTime - beginTime));
    }/**

     10:59:34.521 [pool-2-thread-3] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - future 任务被取消
     10:59:34.522 [pool-2-thread-3] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.521 [pool-2-thread-4] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - future 任务被取消
     10:59:34.522 [pool-2-thread-4] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.521 [pool-2-thread-1] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - future 任务被取消
     10:59:34.522 [pool-2-thread-1] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.521 [pool-2-thread-8] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - future 任务被取消
     10:59:34.522 [pool-2-thread-8] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.521 [pool-2-thread-7] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.521 [pool-2-thread-2] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - future 任务被取消
     10:59:34.522 [pool-2-thread-2] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除
     10:59:34.522 [pool-2-thread-5] WARN com.zxd.interview.mycache.version6.MyCacheVersion6 - 过期时间到了，缓存被清除

     最终时间146

     如果修改为多个时间同时发起请求：
     最终时间1074 - 1000 主线程的睡眠时间 = 74


     可以看到时间点都是在同一个分秒，可以人为countDownlatch是生效的
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     simpleDateFormat => 14:50
     */

}
