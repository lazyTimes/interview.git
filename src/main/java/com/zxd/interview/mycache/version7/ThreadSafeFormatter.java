package com.zxd.interview.mycache.version7;

import java.text.SimpleDateFormat;

/**
 * ThreadSafeFormatter
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.mycache.version7
 * @Description : 线程安全 ThreadSafeFormatter
 * @Create on : 2023/6/22 11:11
 **/
public class ThreadSafeFormatter {


    public static ThreadLocal<SimpleDateFormat> dateFormatter = new ThreadLocal<SimpleDateFormat>() {

        /**
         * 每个线程会调用本方法一次，用于初始化
         * @description 每个线程会调用本方法一次，用于初始化
         * @param
         * @return java.text.SimpleDateFormat
         * @author xander
         * @date 2023/6/22 11:30
         */
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("mm:ss");
        }

        /**
         * 首次调用本方法时，会调用initialValue()；后面的调用会返回第一次创建的值
         * @description 首次调用本方法时，会调用initialValue()；后面的调用会返回第一次创建的值
         * @param
         * @return java.text.SimpleDateFormat
         * @author xander
         * @date 2023/6/22 11:30
         */
        @Override
        public SimpleDateFormat get() {
            return super.get();
        }
    };
}
