package com.zxd.interview.mycache.version5;

import com.zxd.interview.mycache.compute.ComputeAble;
import com.zxd.interview.mycache.compute.ExpensiveCompute;
import com.zxd.interview.mycache.compute.MayFailCompute;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.DirectoryWalker;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * <pre>
 * 第五个版本，当碰到会抛出异常的计算方法的情况这时候应该重新计算
 * 对于不同的异常，也要对应不同的处理态度：
 *
 * - CancellationException 和 InterruptedException 基本都是人为操作，这时候应该立即终止任务。
 * - 根据方法逻辑，我们可以知道方法是有可能计算成功的，只不过需要多重试几次。
 * - while(true) 的加入可以让出错之后自动重新进行计算直到成功为止，但是如果是人为取消，就需要抛出异常并且结束。
 * </pre>
 *
 * @author
 * @version v1.0.0
 * @Package : com.zxd.interview.mycache.version3
 * @Description : 第五个版本
 * @Create on : 2023/6/15 16:47
 **/
@Slf4j
public class MyCacheVersion5<A, V> {
    /**
     * 改造，并发不安全集合改为并发安全集合
     * value 存储为 future的值
     */
    private final Map<A, Future<V>> concurrentHashMap = new ConcurrentHashMap<>();

    private final ComputeAble computeAble = new MayFailCompute();


    public V compute(A arg) {
        return doCompute(arg);
    }

    private V doCompute(A arg) {
        // 对于重复计算进行处理
        while (true) {
            Future<V> result = concurrentHashMap.get(arg);
            try {
                // 如果获取不到内容，说明不在缓存当中
                if (Objects.isNull(result)) {
                    // 此时利用callAble 线程任务指定任务获取，在获取到结果之前线程会阻塞
                    FutureTask<V> future = new FutureTask<>(new Callable<V>() {
                        @Override
                        @SuppressWarnings("unchecked")
                        public V call() throws Exception {
                            return (V) computeAble.doCompute(arg);

                        }
                    });
                    //把新的future覆盖之前获取的future
                    result = future;
                    // 执行
                    future.run();
                    System.out.println("FutureTask 调用计算函数");
                    result = concurrentHashMap.putIfAbsent(arg, result);
                    // 如果返回null，说明这个记录被添加过了
                    if (Objects.isNull(result)) {
                        System.out.println("其他线程进行设置,重新执行计算");
                        // 说明其他线程已经设置过值，这里重新跑一次计算方法即可直接获取
                        result = future;
                        // 再重新跑一次
                        future.run();
                        return result.get();
                    } else {
                        return result.get();
                    }
                }
                return result.get();
            } catch (CancellationException cancellationException) {
                log.warn("CancellationException result => {}", result);
                // 线程在执行过程当中有可能被取消
                // 被取消的时候不管如何处理，首先需要先从缓存中移除掉污染缓存
                concurrentHashMap.remove(arg);
                throw new RuntimeException(cancellationException);
            } catch (InterruptedException e) {
                log.warn("InterruptedException result => {}", result);
                // 线程被中断的异常处理
                concurrentHashMap.remove(arg);
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
//            log.warn("ExecutionException result => {}", result);
                log.info("移除缓存Key => {}，重新计算", arg);
                concurrentHashMap.remove(arg);
                // 不会抛出异常，而是重新在下一次循环中计算
//            throw new RuntimeException(e);
            /*
            打印结果如下：
            FutureTask 调用计算函数
            FutureTask 调用计算函数
            result => 65
            其他线程进行设置,重新执行计算
            result => 33
            result => 65
            result => 26
            15:59:56.584 [pool-1-thread-30] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 75，重新计算
            result => 75
            15:59:56.584 [pool-1-thread-3] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 35，重新计算
            15:59:56.584 [pool-1-thread-42] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 67，重新计算
            15:59:56.584 [pool-1-thread-36] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 75，重新计算
            15:59:56.584 [pool-1-thread-90] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 40，重新计算
            15:59:56.585 [pool-1-thread-31] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 13，重新计算
            15:59:56.586 [pool-1-thread-94] INFO com.zxd.interview.mycache.version5.MyCacheVersion5 - 移除缓存Key => 60，重新计算
            Disconnected from the target VM, address: '127.0.0.1:11054', transport: 'socket'

            Process finished with exit code 0

            * */
            } catch (Exception e) {
                log.warn("Exception result => {}", result);
                concurrentHashMap.remove(arg);
                // 无法处理的未知异常，直接抛出运行时异常不做任何处理。
                throw new RuntimeException(e);
            }

        }
    }
}
