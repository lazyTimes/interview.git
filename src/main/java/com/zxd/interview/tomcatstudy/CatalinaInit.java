package com.zxd.interview.tomcatstudy;

public class CatalinaInit {

    /**
     * 初始化守护进程
     * 这段代码是来自Apache Tomcat项目中的Catalina.java文件。它是init()方法，用于初始化Catalina守护进程。

     该方法首先调用initClassLoaders()方法来初始化类加载器，然后将上下文类加载器设置为catalinaLoader。然后，它使用catalinaLoader加载Catalina类并创建其实例。

     之后，它使用反射将Catalina实例的父类加载器设置为sharedLoader。最后，它将catalinaDaemon字段设置为Catalina实例。

     总的来说，该方法初始化了类加载器和Catalina守护进程实例。
     * @throws Exception Fatal initialization error
     */
    public void init() throws Exception {

        // 初始化classloader（包括catalinaLoader），下文将具体分析
//        initClassLoaders();
//
//        // 设置当前的线程的contextClassLoader为catalinaLoader
//        Thread.currentThread().setContextClassLoader(catalinaLoader);
//
//        SecurityClassLoad.securityClassLoad(catalinaLoader);
//
//        // 通过catalinaLoader加载Catalina，并初始化startupInstance 对象
//        if (log.isDebugEnabled())
//            log.debug("Loading startup class");
//        Class<?> startupClass = catalinaLoader.loadClass("org.apache.catalina.startup.Catalina");
//        Object startupInstance = startupClass.getConstructor().newInstance();
//
//        // 通过反射调用了setParentClassLoader 方法
//        if (log.isDebugEnabled())
//            log.debug("Setting startup class properties");
//        String methodName = "setParentClassLoader";
//        Class<?> paramTypes[] = new Class[1];
//        paramTypes[0] = Class.forName("java.lang.ClassLoader");
//        Object paramValues[] = new Object[1];
//        paramValues[0] = sharedLoader;
//        Method method =
//                startupInstance.getClass().getMethod(methodName, paramTypes);
//        method.invoke(startupInstance, paramValues);
//
//        catalinaDaemon = startupInstance;

    }
}
