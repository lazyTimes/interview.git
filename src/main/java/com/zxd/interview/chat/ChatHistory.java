package com.zxd.interview.chat;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 聊天记录类（ChatHistory.java）
 **/
public class ChatHistory {

    private static final String CHAT_HISTORY_FILE = "data/chat_history.txt";

    public static void logMessage(String message) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(CHAT_HISTORY_FILE, true))) {
            writer.write(message);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getChatHistory() {
        List<String> history = new ArrayList<>();
        try {
            history = Files.readAllLines(Paths.get(CHAT_HISTORY_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return history;
    }
}
