package com.zxd.interview.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Group {

    private String name;
    private List<User> members;

    private static Map<String, Group> groups = new HashMap<>();

    public Group(String name) {
        this.name = name;
        this.members = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<User> getMembers() {
        return members;
    }

    public void addMember(User user) {
        members.add(user);
    }

    public static Group getGroup(String groupName) {
        return groups.get(groupName);
    }

    public static void addGroup(Group group) {
        groups.put(group.getName(), group);
    }

    public static Map<String, Group> getGroups() {
        return groups;
    }
}
