package com.zxd.interview.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.chat
 * @Description : TODO
 * @Create on : 2024/6/25 10:16
 **/
public class ChatClient {

    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

        // Handle login
        while (true) {
            String response = in.readLine();
            if (response.startsWith("SUBMITNAME")) {
                System.out.print("Enter username: ");
                out.println(stdIn.readLine());
            } else if (response.startsWith("SUBMITPASSWORD")) {
                System.out.print("Enter password: ");
                out.println(stdIn.readLine());
            } else if (response.startsWith("NAMEACCEPTED")) {
                System.out.println("Login successful! Welcome " + response.substring(13));
                break;
            } else if (response.startsWith("INVALIDCREDENTIALS")) {
                System.out.println("Invalid credentials, please try again.");
            }
        }

        // Start a new thread to listen for messages from the server
        new Thread(new IncomingReader(in)).start();

        // Main thread to read user input and send to server
        String userInput;
        while ((userInput = stdIn.readLine()) != null) {
            out.println(userInput);
        }
    }

//    private static class IncomingReader implements Runnable {
//        private BufferedReader in;
//
//        public IncomingReader(BufferedReader in) {
//            this.in = in;
//        }
//
//        public void run() {
//            try {
//                String message;
//                while ((message = in.readLine()) != null) {
//                    System.out.println(message);
//                }
//            } catch (IOException e) {
//                e.print
//            }
//        }
//    }
//public static void main(String[] args) throws IOException {
//    Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
//    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
//    BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
//
//    // Handle login
//    while (true) {
//        String response = in.readLine();
//        if (response.startsWith("SUBMITNAME")) {
//            System.out.print("Enter username: ");
//            out.println(stdIn.readLine());
//        } else if (response.startsWith("SUBMITPASSWORD")) {
//            System.out.print("Enter password: ");
//            out.println(stdIn.readLine());
//        } else if (response.startsWith("NAMEACCEPTED")) {
//            System.out.println("Login successful! Welcome " + response.substring(13));
//            break;
//        } else if (response.startsWith("INVALIDCREDENTIALS")) {
//            System.out.println("Invalid credentials, please try again.");
//        }
//    }
//
//    // Start a new thread to listen for messages from the server
//    new Thread(new IncomingReader(in)).start();
//
//    // Main thread to read user input and send to server
//    String userInput;
//    while ((userInput = stdIn.readLine()) != null) {
//        if (userInput.startsWith("/group")) {
//            // Handle group commands locally if needed
//            handleGroupCommand(userInput, out);
//        } else {
//            out.println(userInput);
//        }
//    }
//}


    private static void handleGroupCommand(String userInput, PrintWriter out) {
        // Additional processing for group commands if needed
        // For now, just send the command to the server
        out.println(userInput);
    }

    private static class IncomingReader implements Runnable {
        private BufferedReader in;

        public IncomingReader(BufferedReader in) {
            this.in = in;
        }

        public void run() {
            try {
                String message;
                while ((message = in.readLine()) != null) {
                    System.out.println(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
