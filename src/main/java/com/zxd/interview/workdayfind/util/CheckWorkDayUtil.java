package com.zxd.interview.workdayfind.util;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.workdayfind
 * @Description : 检查工作日工具
 * @Create on : 2022/6/17 22:37
 **/
public class CheckWorkDayUtil {


    /**
     * t+N 天数的的正则表达式
     */
    public static final Pattern NEXT_TD_DAY_PATTERN = Pattern.compile("^[TD]\\+(0|[1-9][0-9]*)$");

    /**
     * 检查周期的函数是否正确
     *
     * @param bankSettleCycle T+N 或者 D+N 的周期格式
     * @return
     */
    public static boolean checkNextTdDayAccess(String bankSettleCycle) {
        // 校验是否为工作日或者自然日
        return NEXT_TD_DAY_PATTERN.matcher(Objects.requireNonNull(bankSettleCycle)).matches();

    }
}
