package com.zxd.interview.workdayfind.bo;

import com.zxd.interview.workdayfind.dto.SaCalendarDayDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.sa.pojo.dto
 * @Description : 工作日 业务处理数据传输对象
 * @Create on : 2021/12/10 11:26
 **/
@Builder
@Data
public class CalendarDataProcessBo {
    /**
     * 目标日期
     */
    private String tagergetDay;

    /**
     * 银行结算周期
     */
    private String bankSettleCycle;

    /**
     * 需要额外推迟的工作日天数
     */
    private int extDayOfWorkDayCount;

    /**
     * 工作日和自然日列表
     */
    private List<SaCalendarDayDto> calendarDayDtos;
}
