package com.zxd.interview.workdayfind.dto;

import lombok.Data;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.sa.pojo.dto
 * @Description : 用于接受当前天数往前推算的工作日对象
 * @Create on : 2021/10/25 17:56
 **/
@Data
public class SaCalendarDayDto {

    /**
     * 如果是今天往后的工作日是正数
     */
    private String addDay;

    /**
     * yyyy-MM-dd 的相关时间
     */
    private String calendarDate;


    /**
     * T.is_work_day 是否工作日
     */
    private String isWorkDay;
}
