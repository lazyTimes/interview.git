package com.zxd.interview.workdayfind.mapper;

import com.zxd.interview.workdayfind.dto.SaCalendarDayDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.sa.mapper
 * @Description : 时间日历表的mapper
 * @Create on : 2021/10/25 17:21
 **/
@Mapper
public interface CalendarTableMapper {


    /**
     * 查找指定日期，其他和上方查询类似s
     * @param targetYyyyMMdd 目标日期，必须是 yyyy-MM-dd格式
     * @return
     */
    List<SaCalendarDayDto> queryCalendarList(@Param("targetYyyyMMdd") String targetYyyyMMdd);


}
