package com.zxd.interview.book;

import java.sql.*;
import java.util.Scanner;

public class BookManager {

    // 定义数据库连接的URL地址
    private static final String URL = "jdbc:mysql://192.168.0.100:3306/book_management?useUnicode=true&characterEncoding=UTF-8&useSSL=false";

    // 定义数据库用户名，这里需要替换为实际的用户名
    private static final String USER = "xander";

    // 定义数据库密码，这里需要替换为实际的密码
    private static final String PASSWORD = "xander";

    // 创建一个Scanner对象用于从控制台读取用户输入
    private static Scanner scanner = new Scanner(System.in);

    // 显示书籍信息的方法
    public static void displayBooks() {
        // SQL查询语句，用于从数据库中选择所有书籍信息
        String sql = "SELECT * FROM books";

        // 尝试连接数据库并执行查询，使用try-with-resources语句自动关闭资源
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            // 打印表头
            System.out.println("ID\t书名\t作者\t出版社\t出版时间\tISBN\t价格\t数量");
            System.out.println("--------------------------------------------------------------");
            // 遍历查询结果集并打印每条记录
            while (rs.next()) {
                System.out.println(
                        rs.getInt("id") + "\t" +
                                rs.getString("title") + "\t" +
                                rs.getString("author") + "\t" +
                                rs.getString("publisher") + "\t" +
                                rs.getDate("publish_date") + "\t" +
                                rs.getString("isbn") + "\t" +
                                rs.getDouble("price") + "\t" +
                                rs.getInt("quantity")
                );
            }
        } catch (SQLException e) {
            // 打印SQL异常信息
            System.err.println(e.getMessage());
        }
    }

    /**
     * 添加书籍信息的方法。
     */
    public static void addBook() {
        // 提示用户输入书名，并读取用户输入
        System.out.print("请输入书名: ");
        String title = scanner.nextLine();
        // 提示用户输入作者，并读取用户输入
        System.out.print("请输入作者: ");
        String author = scanner.nextLine();
        // 提示用户输入出版社，并读取用户输入
        System.out.print("请输入出版社: ");
        String publisher = scanner.nextLine();
        // 提示用户输入出版时间，并读取用户输入
        System.out.print("请输入出版时间 (YYYY-MM-DD): ");
        String publishDate = scanner.nextLine();
        // 提示用户输入ISBN，并读取用户输入
        System.out.print("请输入ISBN: ");
        String isbn = scanner.nextLine();
        // 提示用户输入价格，并读取用户输入
        System.out.print("请输入价格: ");
        double price = scanner.nextDouble();
        // 提示用户输入数量，并读取用户输入
        System.out.print("请输入数量: ");
        int quantity = scanner.nextInt();
        // 读取并丢弃nextInt()后的换行符
        scanner.nextLine();

        // 准备SQL插入语句，使用预处理语句来防止SQL注入
        String sql = "INSERT INTO books (title, author, publisher, publish_date, isbn, price, quantity) VALUES (?, ?, ?, ?, ?, ?, ?)";

        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // 为预处理语句设置参数值
            pstmt.setString(1, title);
            pstmt.setString(2, author);
            pstmt.setString(3, publisher);
            // 将字符串转换为Date对象并设置为SQL语句的参数
            pstmt.setDate(4, Date.valueOf(publishDate));
            pstmt.setString(5, isbn);
            pstmt.setDouble(6, price);
            pstmt.setInt(7, quantity);
            // 执行插入操作
            pstmt.executeUpdate();
            // 如果插入成功，向用户显示成功消息
            System.out.println("书籍信息已添加!");
        } catch (SQLException e) {
            // 如果在执行数据库操作过程中发生SQLException异常，打印异常信息
            System.err.println(e.getMessage());
        }
    }

    /**
     * 修改书籍信息的方法。
     */
    public static void modifyBook() {
        // 提示用户输入要修改的书籍的ID，并读取输入
        System.out.print("请输入要修改的书籍ID: ");
        int id = scanner.nextInt();
        // 读取并丢弃nextInt()后的换行符
        scanner.nextLine();

        // 依次提示用户输入书籍的各个属性，并读取输入
        System.out.print("请输入书名: ");
        String title = scanner.nextLine();
        System.out.print("请输入作者: ");
        String author = scanner.nextLine();
        System.out.print("请输入出版社: ");
        String publisher = scanner.nextLine();
        System.out.print("请输入出版时间 (YYYY-MM-DD): ");
        String publishDate = scanner.nextLine();
        System.out.print("请输入ISBN: ");
        String isbn = scanner.nextLine();
        System.out.print("请输入价格: ");
        double price = scanner.nextDouble();
        // 再次使用nextLine()来消耗前面nextDouble()留下的换行符
        scanner.nextLine();
        System.out.print("请输入数量: ");
        int quantity = scanner.nextInt();
        // 读取并丢弃nextInt()后的换行符
        scanner.nextLine();

        // 准备SQL更新语句，使用预处理语句来防止SQL注入
        String sql = "UPDATE books SET title = ?, author = ?, publisher = ?, publish_date = ?, isbn = ?, price = ?, quantity = ? WHERE id = ?";

        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // 为预处理语句设置参数值
            pstmt.setString(1, title);
            pstmt.setString(2, author);
            pstmt.setString(3, publisher);
            // 将字符串转换为Date对象并设置为SQL语句的参数
            pstmt.setDate(4, Date.valueOf(publishDate));
            pstmt.setString(5, isbn);
            pstmt.setDouble(6, price);
            pstmt.setInt(7, quantity);
            pstmt.setInt(8, id);
            // 执行更新操作
            pstmt.executeUpdate();
            // 提示用户书籍信息已成功修改
            System.out.println("书籍信息已修改!");
        } catch (SQLException e) {
            // 打印出任何可能发生的SQL异常
            System.err.println(e.getMessage());
        }
    }

    /**
     * 根据用户输入的书籍ID删除书籍信息的方法。
     */
    public static void deleteBook() {
        // 提示用户输入要删除的书籍的ID
        System.out.print("请输入要删除的书籍ID: ");
        // 读取用户输入的ID
        int id = scanner.nextInt();
        // 读取并丢弃nextInt()后的换行符
        scanner.nextLine();

        // 准备SQL删除语句，使用预处理语句来防止SQL注入
        String sql = "DELETE FROM books WHERE id = ?";

        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // 为预处理语句设置参数值，即用户输入的书籍ID
            pstmt.setInt(1, id);
            // 执行删除操作
            pstmt.executeUpdate();
            // 如果删除成功，向用户显示删除成功的消息
            System.out.println("书籍信息已删除!");
        } catch (SQLException e) {
            // 如果在执行数据库操作过程中发生SQLException异常，打印异常信息
            System.err.println(e.getMessage());
        }
    }

    /**
     * 根据出版日期对书籍进行排序并打印的方法。
     */
    public static void sortBooksByPublishDate() {
        // 定义SQL查询语句，从books表中选择所有字段，并按照publish_date字段进行升序排序
        String sql = "SELECT * FROM books ORDER BY publish_date";

        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            // 使用while循环遍历查询结果集
            while (rs.next()) {
                // 打印书籍的各个字段信息，字段之间使用制表符分隔
                System.out.println(
                        rs.getInt("id") + "\t" +    // 打印书籍ID
                                rs.getString("title") + "\t" +  // 打印书名
                                rs.getString("author") + "\t" +  // 打印作者
                                rs.getString("publisher") + "\t" +  // 打印出版社
                                rs.getDate("publish_date") + "\t" +  // 打印出版日期
                                rs.getString("isbn") + "\t" +  // 打印ISBN
                                rs.getDouble("price") + "\t" +  // 打印价格
                                rs.getInt("quantity")  // 打印库存数量
                );
            }
        } catch (SQLException e) {
            // 如果在执行数据库操作过程中发生SQLException异常，打印异常信息
            System.err.println(e.getMessage());
        }
    }

    /**
     * 程序的主入口方法。
     * @param args 命令行参数数组，这里未使用。
     */
    public static void main(String[] args) {
        // 初始化数据库，调用DatabaseInitializer类的静态方法createDatabase()
        DatabaseInitializer.createDatabase();

        // 使用无限循环来持续显示菜单并等待用户输入
        while (true) {
            // 打印菜单的顶部分隔线
            System.out.println("*******************************************************");
            // 打印系统名称
            System.out.println("书籍信息管理系统");
            // 打印各个选项的说明
            System.out.println("1.增加书籍信息");
            System.out.println("2.显示书籍信息");
            System.out.println("3.修改书籍信息");
            System.out.println("4.删除书籍信息");
            System.out.println("5.按照出版时间排序");
            System.out.println("0.退出系统");
            // 打印菜单的底部分隔线
            System.out.println("*******************************************************");

            // 提示用户选择一个操作
            System.out.print("请选择操作: ");
            // 读取用户输入的选择
            String choice = scanner.nextLine();

            // 使用switch语句来根据用户的选择执行不同的操作
            switch (choice) {
                case "1":
                    // 如果用户选择"1"，则调用BookManager类的addBook方法增加书籍信息
                    BookManager.addBook();
                    break;
                case "2":
                    // 如果用户选择"2"，则调用BookManager类的displayBooks方法显示书籍信息
                    BookManager.displayBooks();
                    break;
                case "3":
                    // 如果用户选择"3"，则调用BookManager类的modifyBook方法修改书籍信息
                    BookManager.modifyBook();
                    break;
                case "4":
                    // 如果用户选择"4"，则调用BookManager类的deleteBook方法删除书籍信息
                    BookManager.deleteBook();
                    break;
                case "5":
                    // 如果用户选择"5"，则调用BookManager类的sortBooksByPublishDate方法按照出版时间排序书籍信息
                    BookManager.sortBooksByPublishDate();
                    break;
                case "0":
                    // 如果用户选择"0"，则打印退出信息并退出程序
                    System.out.println("退出系统");
                    return;
                default:
                    // 如果用户输入了无效的选择，则提示用户重新输入
                    System.out.println("无效的选择，请重新输入。");
            }
        }
    }
}
