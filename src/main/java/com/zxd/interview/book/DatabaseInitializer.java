package com.zxd.interview.book;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseInitializer {
    // 定义数据库连接的URL地址
    private static final String URL = "jdbc:mysql://192.168.0.100:3306/book_management?useUnicode=true&characterEncoding=UTF-8&useSSL=false";

    // 定义数据库用户名，这里需要替换为实际的用户名
    private static final String USER = "xander";

    // 定义数据库密码，这里需要替换为实际的密码
    private static final String PASSWORD = "xander";
    public static void createDatabase() {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD)) {
            if (conn != null) {
                String sql = "CREATE TABLE IF NOT EXISTS books (\n"
                        + " id INT AUTO_INCREMENT PRIMARY KEY,\n"
                        + " title VARCHAR(255) NOT NULL,\n"
                        + " author VARCHAR(255) NOT NULL,\n"
                        + " publisher VARCHAR(255) NOT NULL,\n"
                        + " publish_date DATE NOT NULL,\n"
                        + " isbn VARCHAR(20) NOT NULL,\n"
                        + " price DECIMAL(10, 2) NOT NULL,\n"
                        + " quantity INT NOT NULL\n"
                        + ");";
                Statement stmt = conn.createStatement();
                stmt.execute(sql);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        createDatabase();
    }
}
