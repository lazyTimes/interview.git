package com.zxd.interview.valid.exception;

import com.zxd.interview.valid.exception.BusinessException;
import com.zxd.interview.valid.exception.ParamException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

/**
 * 异常统一处理
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/1 20:47
 */
@RestControllerAdvice
public class ExceptionDealHandler {

    /**
     * 拦截未知的运行时异常
     */
//    @ExceptionHandler()
//    public Object notFount(Exception e,BindException bindExce, Throwable run)
//    {
////        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
////        {
////            throw e;
////        }
////        logger.error("运行时异常:", e);
//        return HttpStatus.ACCEPTED;
//    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler({ParamException.class})
    public Object errors(ParamException e)
    {
        return HttpStatus.ACCEPTED;
    }


//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public Object doSomethings(MethodArgumentNotValidException e){
//        System.err.println(e.getMessage());
//        return HttpStatus.ACCEPTED;
//    }
//
//    @ExceptionHandler(BindException.class)
//    public Object bindError(BindException bind){
////        System.err.println(e.getMessage());
//        return HttpStatus.ACCEPTED;
//    }
//
//    @ExceptionHandler({ConstraintViolationException.class})
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Object handleConstraintViolationException(ConstraintViolationException ex) {
//        return HttpStatus.ACCEPTED;
//    }
//
//    @ExceptionHandler({Exception.class})
//    @ResponseStatus(HttpStatus.OK)
//    @ResponseBody
//    public Object allError(Exception ex) {
//        return HttpStatus.ACCEPTED;
//    }
}
