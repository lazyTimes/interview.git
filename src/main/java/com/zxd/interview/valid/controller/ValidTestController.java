//package com.zxd.interview.valid.controller;
//
//import com.zxd.interview.valid.groups.GroupUpdate;
//import com.zxd.interview.valid.exception.ParamException;
//import com.zxd.interview.valid.groups.GroupsOpration;
//import com.zxd.interview.valid.model.Bag;
//import com.zxd.interview.valid.model.Product;
//import com.zxd.interview.valid.service.ValidService;
//import com.zxd.interview.valid.utils.ValidatorList;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.validation.BindingResult;
//import org.springframework.validation.FieldError;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import javax.validation.Valid;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 测试提交程序
// *
// * @author zhaoxudong
// * @version 1.0
// * @date 2020/12/31 21:34
// */
//@RestController
//public class ValidTestController {
//
//    @Resource
//    private ValidService validService;
//
//    /**
//     * 测试list校验的方法是否可以使用
//     * @param product
//     * @return
//     */
//    @RequestMapping("/test/apivalid")
//    @Validated
//    @ResponseBody
//    public Object testListValidApi(@RequestBody List<Product> product) {
//        validService.validFunction(product);
//        return new Object();
//    }
//
//    @RequestMapping("/test/update")
//    public Object update(@Validated(GroupUpdate.class) Product product) throws ParamException {
//        System.err.println(product);
//        return null;
//    }
//
//    @RequestMapping("/test/add")
//    public Object add(@Validated Product product) throws ParamException {
//        System.err.println(product);
//        return null;
//    }
//
//    /**
//     * 测试组继承
//     *
//     * @param product
//     * @return
//     * @throws ParamException
//     */
//    @RequestMapping("/test/bag1")
//    @ResponseBody
//    public Object bag1(@Validated(GroupsOpration.class) Bag product, BindingResult bindingResult) throws ParamException {
//        if (bindingResult.hasErrors()) {
//            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
//            Map<Object, Object> objectObjectHashMap = new HashMap<>();
//            for (FieldError fieldError : fieldErrors) {
//                String defaultMessage = fieldError.getDefaultMessage();
//                objectObjectHashMap.put(fieldError.getField(), defaultMessage);
//            }
//            return objectObjectHashMap;
//        }
//        System.err.println(product);
//        return null;
//    }
//
//    /**
//     * 测试组继承
//     *
//     * @param product
//     * @return
//     * @throws ParamException
//     */
//    @RequestMapping("/test/bag2")
//    public Object bag2(@Validated Bag product) throws ParamException {
//        System.err.println(product);
//        return null;
//    }
//
//    /**
//     * 测试stackflow 的一种优雅设计，可以实现对应的list 集合bean对象校验
//     *
//     * @param products      校验对象
//     * @param bindingResult 异常绑定器
//     * @return
//     * @throws ParamException
//     */
//    @RequestMapping("/test/testvalidList")
//    @ResponseBody
//    public Object testvalidList(@RequestBody @Validated ValidatorList<Product> products, BindingResult bindingResult) throws ParamException {
//        if (bindingResult.hasErrors()) {
//            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
//            fieldErrors.forEach(item -> {
//                String defaultMessage = item.getDefaultMessage();
//                System.err.println(defaultMessage);
//            });
//        }
//        System.err.println(products);
//        return null;
//    }
//
//
//    // TODO: 存在问题
////    @RequestMapping("/test/validList")
////    public Object testList(@Valid @RequestBody List<Product> product) throws ParamException {
////        System.err.println(product);
////        throw new ParamException("","","");
////    }
//
////    @ExceptionHandler(Exception.class)
////    public Object test2(Exception e){
////        System.err.println(e.getMessage());
////        return null;
////    }
//
//}
