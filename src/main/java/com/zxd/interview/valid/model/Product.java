package com.zxd.interview.valid.model;

import com.zxd.interview.valid.annotation.Time;
import com.zxd.interview.valid.groups.GroupUpdate;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * 商品，validate学习
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2020/12/31 21:34
 */
@Validated
public class Product {

    @NotBlank(groups = {GroupUpdate.class})
//    @NotBlank
//    @Length(min = 5)
    private String name;

    @Time(message = "日期格式有误")
    private String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
