package com.zxd.interview.valid.model;

import com.zxd.interview.valid.groups.GroupAdd;
import com.zxd.interview.valid.groups.GroupsOpration;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 包包：组继承实验
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/3 17:28
 */
public class Bag extends Product {

    @NotNull(message = "颜色不能为空",groups = {GroupAdd.class})
    private String color;


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
