package com.zxd.interview.valid.service;

import com.zxd.interview.valid.model.Product;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * 校验业务层
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/3 19:09
 */
@Validated
public interface ValidService {

    public void doSomething(Product produc);

    /**
     * 校验方法
     * @param product
     */
    public void validFunction(List<@Valid Product> product);
}
