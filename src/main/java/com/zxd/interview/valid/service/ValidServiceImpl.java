//package com.zxd.interview.valid.service;
//
//import com.zxd.interview.valid.model.Product;
//import com.zxd.interview.valid.utils.ValidateUtils;
//import org.springframework.stereotype.Service;
//import org.springframework.validation.annotation.Validated;
//
//import javax.validation.*;
//import java.util.*;
//
///**
// * 测试validator校验
// *
// * @author zhaoxudong
// * @version 1.0
// * @date 2021/1/3 19:10
// */
//@Service
//public class ValidServiceImpl implements ValidService {
//
//
//    @Override
//    public void doSomething(Product product) {
//        Validator validator = ValidateUtils.getValidator();
//        Set<ConstraintViolation<Product>> validate = validator.validate(product);
//        System.err.println(validate);
//
//
////        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
////        Validator validator = factory.getValidator();
////        Set<ConstraintViolation<Product>> validate = validator.validate(product);
////        Map<Object, Object> objectObjectMap = new HashMap<>();
////        validate.forEach(item -> {
//////            System.err.println("item = "+ item);
////            String message = item.getMessage();
//////            System.err.println("message " + message);
////            objectObjectMap.put(item.getPropertyPath(), message);
////
////        });
////        objectObjectMap.forEach((k, v) -> {
////            System.err.println("key = " + k + " value = " + v);
////        });
//    }
//
//    @Override
//    @Validated
//    public void validFunction(List<@Valid Product> product) {
//
//    }
//}
