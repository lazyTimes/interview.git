package com.zxd.interview.valid.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;

/**
 * TODO
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/3 10:46
 */
public class TimeValidator implements ConstraintValidator<Time, String> {

    /**
     * 初始化注解的校验内容
     * @param constraintAnnotation
     */
    @Override
    public void initialize(Time constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintContext) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        simpleDateFormat.setLenient(false);
        boolean isValid = true;
        try {
            simpleDateFormat.parse(value);
        } catch (Exception e) {
            isValid = false;
        }
//        if ( !isValid ) {
//            constraintContext.disableDefaultConstraintViolation();
//            constraintContext.buildConstraintViolationWithTemplate(
//                    "{com.zxd.interview.valid.annotation." +
//                            "Time.message}"
//            )
//                    .addConstraintViolation();
//        }
        return isValid;
    }
}
