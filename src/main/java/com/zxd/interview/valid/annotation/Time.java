package com.zxd.interview.valid.annotation;

/**
 * 日期格式校验注解
 */


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

// 标识，无实际意义
@Documented
// 可以注入的类型，字段和参数类型
@Target({PARAMETER, FIELD})
// 运行时生效
@Retention(RUNTIME)
// 触发校验的对象
@Constraint(validatedBy = {TimeValidator.class})
//@Repeatable(Time.List.class)
public @interface Time {

    String message() default "处理不到位";

    Class<?>[] groups() default {};

    String value = "";

    Class<? extends Payload>[] payload() default {};

    @Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
//        CheckCase[] value();
        Time[] value();
    }
}
