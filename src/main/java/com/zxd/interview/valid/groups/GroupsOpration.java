package com.zxd.interview.valid.groups;

/**
 * 增删改都需要的接口
 */
public interface GroupsOpration extends  GroupUpdate,GroupDel,GroupAdd{
}
