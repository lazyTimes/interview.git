package com.zxd.interview.valid.groups;

import javax.validation.groups.Default;

/**
 * 更新分組
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2021/1/3 17:02
 */
public interface GroupUpdate extends Default {
}
