package com.zxd.interview.jdk12;

import java.util.Scanner;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.jdk12
 * @Description : TODO
 * @Create on : 2023/7/6 16:18
 **/
public class OldSwitchTest {

    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.println("请输入星期数(1—7)：");
        int weekday = input.nextInt();
        //switch判断星期
        switch(weekday){
            case 1:
                System.out.println("星期一");
                //符合条件后 直接结束
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
            case 7:
                System.out.println("休息日");
                break;
            default:
                System.out.println("输入的数字有误");
                break;
        }
    }
}
