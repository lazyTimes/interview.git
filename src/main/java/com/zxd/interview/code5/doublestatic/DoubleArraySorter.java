package com.zxd.interview.code5.doublestatic;

import java.util.Arrays;
import java.util.Collections;

/**
 * 过编写静态方法来实现上述三种排序功能，并使用 Arrays 类中的静态方法来对 Double 数组进行排序。下面是详细的代码实现及测试。
 **/
public class DoubleArraySorter {


    // (1) 使Double数组按从大到小顺序排序
    public static void sortDescending(Double[] array) {
        Arrays.sort(array, Collections.reverseOrder());
    }

    // (2) 使Double数组按从小到大排序
    public static void sortAscending(Double[] array) {
        Arrays.sort(array);
    }

    // (3) 使Double数组反序排序
    public static void reverseOrder(Double[] array) {
        Collections.reverse(Arrays.asList(array));
    }

    // 测试方法
    public static void main(String[] args) {
        Double[] array = {3.5, 1.2, 4.7, 2.3, 9.8};

        // 测试从大到小排序
        Double[] descArray = array.clone();
        sortDescending(descArray);
        System.out.println("Descending order: " + Arrays.toString(descArray));

        // 测试从小到大排序
        Double[] ascArray = array.clone();
        sortAscending(ascArray);
        System.out.println("Ascending order: " + Arrays.toString(ascArray));

        // 测试反序排序
        Double[] reverseArray = array.clone();
        reverseOrder(reverseArray);
        System.out.println("Reverse order: " + Arrays.toString(reverseArray));
    }/**运行结果：

     Descending order: [9.8, 4.7, 3.5, 2.3, 1.2]
     Ascending order: [1.2, 2.3, 3.5, 4.7, 9.8]
     Reverse order: [9.8, 2.3, 4.7, 1.2, 3.5]
     */
}
