package com.zxd.interview.code5.factorialoperation;

/**
 * 使用递归方法实现阶乘
 * factorial 方法使用递归来计算阶乘。如果 n 小于等于 1，直接返回 1；否则，返回 n 乘以 factorial(n - 1) 的结果。
 **/
public class Factorial {

    // 递归实现阶乘方法
    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {
        int number = 5;
        System.out.println("Factorial of " + number + " is " + factorial(number));
    }
}
