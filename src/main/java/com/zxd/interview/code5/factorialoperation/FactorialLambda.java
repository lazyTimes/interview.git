package com.zxd.interview.code5.factorialoperation;

/**
 * 使用函数式接口和 lambda 表达式实现阶乘
 * 定义了一个函数式接口 IntCall，其中包含一个抽象方法 call，该方法接受一个整数参数并返回一个整数结果。
 * 在 main 方法中，使用匿名内部类实现了 IntCall 接口，其中使用递归来计算阶乘。
 * 通过调用 factorial.call(number) 来计算给定数字的阶乘。
 **/
public class FactorialLambda {


    public static void main(String[] args) {
        IntCall factorial = new IntCall() {
            @Override
            public int call(int arg) {
                if (arg <= 1) {
                    return 1;
                } else {
                    return arg * this.call(arg - 1);
                }
            }
        };

        int number = 5;
        System.out.println("Factorial of " + number + " is " + factorial.call(number));
    }
}
