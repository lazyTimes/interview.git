package com.zxd.interview.code5.factorialoperation;

/**
 * 定义一个函数式接口 IntCall。
 **/
public interface IntCall {

    int call(int arg);
}
