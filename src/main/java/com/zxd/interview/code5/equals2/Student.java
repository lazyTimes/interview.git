package com.zxd.interview.code5.equals2;

import java.util.Arrays;
import java.util.Date;

public class Student extends Person
{
	private String major;
	public Student(String name, String identityNo, String major)
	{
		super(name, identityNo);
		this.major=major;		
	}

	public Student(String name, String identityNo, Date birthDay, String major) {
		super(name, identityNo, birthDay);
		this.major = major;
	}

	public String getMajor()
	{
		return major;
	}
	
	@Override
	public String toString()
	{
		return super.toString()+"[major=" +major +"]";
	}

	// 实现 Comparable 接口的 compareTo 方法
//	public int compareTo(Student other) {
//		return this.getMajor().compareTo(other.getMajor());
//	}
	public static void sortStudentsByMajor(Student[] students) {
		Arrays.sort(students, (s1, s2) -> s1.getMajor().compareTo(s2.getMajor()));
	}
	// 重写 clone 方法
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Student cloned = (Student) super.clone(); // 克隆 Person 的字段
		return cloned; // Student 没有需要特殊处理的字段
	}
}
