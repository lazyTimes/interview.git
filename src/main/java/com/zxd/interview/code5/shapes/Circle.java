package com.zxd.interview.code5.shapes;

import java.awt.Point;
/**
 * @version 1.1 2021-11-24
 * @author He Yongqiang
 */
public class Circle extends Shape
{
	private Point center;
	private double radius;
	private double perimeter;
	private double area;
	public Circle()
	{
		center=new Point(0,0);
		radius=0.0;
		perimeter=0.0;
		area=0.0;
	}
	public Circle(String c,int x,int y,double r)
	{
		super(c);
		center=new Point(x,y);
		radius=r;
		calArea();         //调用本类的私有方法，计算图形的面积
		calPerimeter();
	}
	
	private void calArea()    //私有方法，计算图形的面积
	{
		area= Math.PI*radius*radius;
	}
	private void calPerimeter()    //私有方法，计算图形的周长
	{
		perimeter= Math.PI*radius*2.0;
	}
	@Override 
	public void move(int x,int y)
	{
		center.x+=x;
		center.y+=y;
//		center.move(center.x+x, center.y+y);  相同功能的方法
	}
	@Override
	public double getPerimeter()
	{
		return perimeter;
	}
	@Override
	public double getArea()
	{
		return area;
	}

	public Point getCenter() {
		return center;
	}
}
