package com.zxd.interview.code5.equals;

import java.util.Arrays;
import java.util.Date;

public class EqualsTest2 {
    public static void main(String[] args) {
        // 测试 Person 类
        Person person1 = new Person("Bob", "123");
        person1.setBirthDay(new Date(2000, 3, 4));
        Person person2 = new Person("Alice", "456", new Date(2015, 3, 4));

        Person person3 = new Person("Alice", "456", new Date(2016, 3, 4));
        System.out.println(person1.equals(person2)); // false
        System.out.println(person2.equals(person3)); // false
        Person[] persons = {person1, person2, person3};
        System.out.println("按姓名排序:");
        Arrays.sort(persons); // 自然排序，按姓名
        for (Person person : persons) {
            System.out.println(person);
        }

        // 测试 Student 类
        Student student1 = new Student("Bob", "123", "Computer Science");
        Student student2 = new Student("Alice", "456", "Mathematics");
        System.out.println(student1.equals(student2)); // false
        Student[] students = {student1, student2};
        Student.sortStudentsByMajor(students); // 按专业排序
        System.out.println("按专业排序:");
        for (Student student : students) {
            System.out.println(student);
        }

        // 测试 Athlete 类
        Athlete athlete1 = new Athlete("Bob", "123", "Soccer");
        Athlete athlete2 = new Athlete("Alice", "456", "Tennis");
        System.out.println(athlete1.equals(athlete2)); // false
        Athlete[] athletes = {athlete1, athlete2};
        Athlete.sortAthletesByEvent(athletes); // 按运动项目排序
        System.out.println("按运动项目排序:");
        for (Athlete athlete : athletes) {
            System.out.println(athlete);
        }

        // 测试克隆功能
        try {
            Person originalPerson = new Person("John", "789" ,new Date(2015, 3, 4));
            Person clonedPerson = (Person) originalPerson.clone();
            System.out.println("Original Person: " + originalPerson);
            System.out.println("Cloned Person: " + clonedPerson);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        // 测试克隆功能
        try {
            Student originalPerson = new Student("John", "789", new Date(2015, 3, 4), "Soccer");
            Student clonedPerson = (Student) originalPerson.clone();
            System.out.println("Original Student: " + originalPerson);
            System.out.println("Cloned Student: " + clonedPerson);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        // 测试克隆功能
        try {
            Athlete originalPerson = new Athlete("John", "789", "Soccer");
            Athlete clonedPerson = (Athlete) originalPerson.clone();
            System.out.println("Original Athlete: " + originalPerson);
            System.out.println("Cloned Athlete: " + clonedPerson);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
