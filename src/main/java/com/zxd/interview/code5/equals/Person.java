package com.zxd.interview.code5.equals;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
/**
 * This class displays a test for the equals, hashCode and toString methods.
 * The equals and hashCode here are  final methods.
 * @version 1.0 2018-11-28
 * @author He Yongqiang
 */
public class Person implements Comparable<Person>, Cloneable, Serializable
{
	private String name;
	private String identityNo;

	private Date birthDay;
	
	public Person() {}
	public Person(String name, String identityNo)
	{
		this.name=name;
		this.identityNo=identityNo;		
	}

	public Person(String name, String identityNo, Date birthDay) {
		this.name = name;
		this.identityNo = identityNo;
		this.birthDay = birthDay;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getName()
	{
		return name;
	}
	public String getIdentityNo()
	{
		return identityNo;
	}
	//overrides java.lang.Object.equals
	@Override  
	public final boolean equals(Object otherObject)
	{
		if(this==otherObject)
			return true;
		if(otherObject==null)
			return false;
		if(!(otherObject instanceof Person))
			return false;
		Person other=(Person)otherObject;
		return Objects.equals(this.name, other.name) && Objects.equals(this.identityNo, other.identityNo)&& Objects.equals(this.birthDay, other.birthDay);
	}
	//overrides java.lang.Object.hashCode
	@Override
	public final int hashCode()
	{
		return Objects.hash(name,identityNo);
	}
	//overrides java.lang.Object.toString
	@Override
	public String toString()
	{
		return getClass().getName() + "[name="+ name +" identityNo="+ identityNo +" birthDay="+ birthDay +"]";
	}

	// 实现 Comparable 接口的 compareTo 方法
	@Override
	public int compareTo(Person other) {
		return this.getName().compareTo(other.getName());
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Person cloned = (Person) super.clone(); // 克隆基本字段
		if (this.birthDay != null) {
			cloned.birthDay = (Date) this.birthDay.clone(); // 克隆 Date 对象
		}
		return cloned;
	}
}
