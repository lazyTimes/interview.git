package com.zxd.interview.code5.equals;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class Athlete extends Person
{
	private String event;
	public Athlete(String name, String identityNo, String event)
	{
		super(name, identityNo);
		this.event=event;		
	}

	public Athlete(String name, String identityNo, Date birthDay, String event) {
		super(name, identityNo, birthDay);
		this.event = event;
	}

	public String getEvent()
	{
		return event;
	}
	
	@Override
	public String toString()
	{
		return super.toString()+"[event=" + event +"]";
	}

//	public int compareTo(Athlete other) {
//		return this.getEvent().compareTo(other.getEvent());
//	}

	// 为 Athlete 类提供自定义排序
	public static void sortAthletesByEvent(Athlete[] athletes) {
		Arrays.sort(athletes, Comparator.comparing(Athlete::getEvent));
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Athlete cloned = (Athlete) super.clone(); // 克隆 Person 的字段
		return cloned; // Athlete 没有需要特殊处理的字段
	}
}
