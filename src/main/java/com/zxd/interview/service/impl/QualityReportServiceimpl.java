package com.zxd.interview.service.impl;


import com.zxd.interview.constant.QualityConstants;
import com.zxd.interview.export.dto.TestReportDTO;
import com.zxd.interview.export.dto.WordReportDTO;
import com.zxd.interview.export.WordGeneratorUtil;
import com.zxd.interview.service.IQualityReportService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 的 word 报表业务处理层
 *
 * @author zhaoxudong
 * @projectName jydd
 * @description: 的 word 报表业务处理层
 * @date 2020/1/10 9:33
 */
@Service
public class QualityReportServiceimpl implements IQualityReportService {

    @Override
    public File exportQualityStep4Word(WordReportDTO exportWordRequest) {
        Map<String, String> datas = new HashMap(QualityConstants.HASH_MAP_INIT_VALUE);
        //主标题
        datas.put("schoolName", exportWordRequest.getSchoolName());
        datas.put("title1", exportWordRequest.getBaseSituation());
        datas.put("title2", exportWordRequest.getLearningEnvRec());
        datas.put("title3", exportWordRequest.getLearningEnvPro());
        datas.put("title4", exportWordRequest.getDayLifeRec());
        datas.put("title5", exportWordRequest.getDayLifePro());
        datas.put("title6", exportWordRequest.getLearningActivityRec());
        datas.put("title7", exportWordRequest.getLearningActivityPro());
        datas.put("title8", exportWordRequest.getDevRecommend());

        datas.put("base64_1", exportWordRequest.getBase64_1());
        datas.put("base64_2", exportWordRequest.getBase64_2());
        datas.put("base64_3", exportWordRequest.getBase64_3());
        datas.put("base64_4", exportWordRequest.getBase64_4());
        datas.put("base64_5", exportWordRequest.getBase64_5());
        datas.put("base64_6", exportWordRequest.getBase64_6());


        //导出
        return WordGeneratorUtil.createDoc(WordGeneratorUtil.FreemarkerTemplate.REPORT,
                exportWordRequest.getWordName(),
                datas);
    }

    @Override
    public File expoortTestFile(TestReportDTO exportWordRequest) {
        Map<String, String> datas = new HashMap(QualityConstants.HASH_MAP_INIT_VALUE);

        datas.put("Test0", exportWordRequest.getTest0());
        datas.put("Test1", exportWordRequest.getTest1());
        datas.put("Test2", exportWordRequest.getTest2());
        datas.put("Test4", exportWordRequest.getTest4());
        datas.put("Test5", exportWordRequest.getTest5());
        datas.put("Test6", exportWordRequest.getTest6());

        //导出
        return WordGeneratorUtil.createDoc(WordGeneratorUtil.FreemarkerTemplate.Test,
                exportWordRequest.getWordName(),
                datas);
    }
}
