package com.zxd.interview.service;


import com.zxd.interview.export.dto.TestReportDTO;
import com.zxd.interview.export.dto.WordReportDTO;

import java.io.File;

/**
 * word 报表业务处理层
 *
 * @author zhaoxudong
 * @projectName jydd
 * @description: 的 word 报表业务处理层
 * @date 2020/1/10 9:33
 */
public interface IQualityReportService {

    /**
     * 导出 word 文档数据
     *
     * @param exportWordRequest 导出需要的dto数据
     * @return 导出后的生成文件
     */
    File exportQualityStep4Word(WordReportDTO exportWordRequest);

    /**
     * 导出测试内容
     *
     * @param exportWordRequest 导出内容
     * @return
     */
    File expoortTestFile(TestReportDTO exportWordRequest);
}
