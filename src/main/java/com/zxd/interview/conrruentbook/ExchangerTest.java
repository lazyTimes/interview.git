package com.zxd.interview.conrruentbook;


import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;
import java.util.concurrent.FutureTask;

/**
 * 交换方法
 */
public class ExchangerTest {

    public static void main(String[] args){
        Exchanger<String> exchanger = new Exchanger<>();
        new Thread(() -> {
            try {
                String exchangeResult = exchanger.exchange("付款信息");
                Thread.sleep(500);
                System.out.println("张三收到收银员递过来的--" + exchangeResult);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                String exchangeResult = exchanger.exchange("商品");
                System.out.println("收银员收到张三的--" + exchangeResult);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();

       
    }
}
