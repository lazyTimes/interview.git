package com.zxd.interview.conrruentbook;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 为什么需要Lock锁
 * @description: 为什么需要Lock锁
 * @author Xander
 * @date 2023/5/10 10:33
 * @version 1.0
 */
public class MustUnlock {

    private static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        // 1. lock 加锁的操作必须在try/catch 外部
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " locking");
        }catch (Exception e){

        }finally {
            // 解锁代码必须放到 finally
            lock.unlock();
        }
    }


}
