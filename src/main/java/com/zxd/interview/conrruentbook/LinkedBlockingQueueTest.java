package com.zxd.interview.conrruentbook;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 源码阅读和测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.conrruentbook
 * @Description : LinkedBlockingQueue 源码阅读和测试
 * @Create on : 2023/6/8 10:20
 **/
public class LinkedBlockingQueueTest {

    public static void main(String[] args) throws InterruptedException {
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue();
        // 重点关注入队的部分。
        linkedBlockingQueue.put("aaa");
        // 重点关注移除节点部分
        linkedBlockingQueue.take();
    }
}
