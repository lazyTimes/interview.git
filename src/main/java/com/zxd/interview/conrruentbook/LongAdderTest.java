package com.zxd.interview.conrruentbook;

import java.util.concurrent.atomic.LongAdder;

/**
 * 累加器测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.conrruentbook
 * @Description : 累加器测试
 * @Create on : 2023/7/4 15:51
 **/
public class LongAdderTest {

    public static void main(String[] args) {
        LongAdder longAdder = new LongAdder();
        longAdder.increment();
        System.out.println(longAdder.sum());
    }
}
