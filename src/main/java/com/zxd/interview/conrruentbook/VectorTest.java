package com.zxd.interview.conrruentbook;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * Vector 基准测试
 */
@Fork(1)
@Measurement(iterations =  5)
@Warmup(iterations = 3)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Threads(value = 5)
@State(Scope.Benchmark)
public class VectorTest {

    private static final String ELEMENT_DATA = "adong";

    private Vector<String> vector;

    @Setup
    public void setup(){
        this.vector = new Vector<>();
        vector.add(ELEMENT_DATA);
    }

    @Benchmark
    public void addElement(){
        vector.add(ELEMENT_DATA);
    }

    @Benchmark
    public void getElement(){
        vector.get(0);
    }

    @Benchmark
    public void removeElement(){
        vector.removeElement(ELEMENT_DATA);
    }


    public static void main(String[] args) throws RunnerException {
        final Options build = new OptionsBuilder().include(VectorTest.class.getSimpleName()).build();
        new Runner(build).run();

    }/**
     20230423 个人运行结果：

     WARNING: An illegal reflective access operation has occurred
     WARNING: Illegal reflective access by org.openjdk.jmh.util.Utils (file:/D:/soft/apache-maven-3.9.1-bin/rep/org/openjdk/jmh/jmh-core/1.19/jmh-core-1.19.jar) to field java.io.PrintStream.charOut
     WARNING: Please consider reporting this to the maintainers of org.openjdk.jmh.util.Utils
     WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
     WARNING: All illegal access operations will be denied in a future release
     # JMH version: 1.19
     # VM version: JDK 11.0.18, VM 11.0.18+10-LTS
     # VM invoker: C:\Users\Xander\.jdks\corretto-11.0.18\bin\java.exe
     # VM options: -javaagent:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\lib\idea_rt.jar=11415:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\bin -Dfile.encoding=UTF-8
     # Warmup: 3 iterations, 1 s each
     # Measurement: 5 iterations, 1 s each
     # Timeout: 10 min per iteration
     # Threads: 5 threads, will synchronize iterations
     # Benchmark mode: Average time, time/op
     # Benchmark: com.zxd.interview.conrruentbook.VectorTest.addElement

     # Run progress: 0.00% complete, ETA 00:00:24
     # Fork: 1 of 1
     # Warmup Iteration   1: 0.403 ±(99.9%) 0.017 us/op
     # Warmup Iteration   2: 0.389 ±(99.9%) 0.051 us/op
     # Warmup Iteration   3: 0.367 ±(99.9%) 0.117 us/op
     Iteration   1: 0.660 ±(99.9%) 0.070 us/op
     Iteration   2: 0.352 ±(99.9%) 0.052 us/op
     Iteration   3: 0.349 ±(99.9%) 0.050 us/op
     Iteration   4: 0.864 ±(99.9%) 0.122 us/op
     Iteration   5: 0.302 ±(99.9%) 0.064 us/op


     Result "com.zxd.interview.conrruentbook.VectorTest.addElement":
     0.505 ±(99.9%) 0.947 us/op [Average]
     (min, avg, max) = (0.302, 0.505, 0.864), stdev = 0.246
     CI (99.9%): [≈ 0, 1.452] (assumes normal distribution)


     # JMH version: 1.19
     # VM version: JDK 11.0.18, VM 11.0.18+10-LTS
     # VM invoker: C:\Users\Xander\.jdks\corretto-11.0.18\bin\java.exe
     # VM options: -javaagent:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\lib\idea_rt.jar=11415:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\bin -Dfile.encoding=UTF-8
     # Warmup: 3 iterations, 1 s each
     # Measurement: 5 iterations, 1 s each
     # Timeout: 10 min per iteration
     # Threads: 5 threads, will synchronize iterations
     # Benchmark mode: Average time, time/op
     # Benchmark: com.zxd.interview.conrruentbook.VectorTest.getElement

     # Run progress: 33.33% complete, ETA 00:00:19
     # Fork: 1 of 1
     # Warmup Iteration   1: 0.366 ±(99.9%) 0.230 us/op
     # Warmup Iteration   2: 0.323 ±(99.9%) 0.334 us/op
     # Warmup Iteration   3: 0.269 ±(99.9%) 0.036 us/op
     Iteration   1: 0.244 ±(99.9%) 0.012 us/op
     Iteration   2: 0.245 ±(99.9%) 0.010 us/op
     Iteration   3: 0.240 ±(99.9%) 0.005 us/op
     Iteration   4: 0.241 ±(99.9%) 0.012 us/op
     Iteration   5: 0.243 ±(99.9%) 0.010 us/op


     Result "com.zxd.interview.conrruentbook.VectorTest.getElement":
     0.242 ±(99.9%) 0.008 us/op [Average]
     (min, avg, max) = (0.240, 0.242, 0.245), stdev = 0.002
     CI (99.9%): [0.234, 0.251] (assumes normal distribution)


     # JMH version: 1.19
     # VM version: JDK 11.0.18, VM 11.0.18+10-LTS
     # VM invoker: C:\Users\Xander\.jdks\corretto-11.0.18\bin\java.exe
     # VM options: -javaagent:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\lib\idea_rt.jar=11415:D:\Program Files\JetBrains\IntelliJ IDEA 2022.1.3\bin -Dfile.encoding=UTF-8
     # Warmup: 3 iterations, 1 s each
     # Measurement: 5 iterations, 1 s each
     # Timeout: 10 min per iteration
     # Threads: 5 threads, will synchronize iterations
     # Benchmark mode: Average time, time/op
     # Benchmark: com.zxd.interview.conrruentbook.VectorTest.removeElement

     # Run progress: 66.67% complete, ETA 00:00:09
     # Fork: 1 of 1
     # Warmup Iteration   1: 0.367 ±(99.9%) 0.278 us/op
     # Warmup Iteration   2: 0.423 ±(99.9%) 0.494 us/op
     # Warmup Iteration   3: 0.394 ±(99.9%) 0.062 us/op
     Iteration   1: 0.399 ±(99.9%) 0.043 us/op
     Iteration   2: 0.400 ±(99.9%) 0.025 us/op
     Iteration   3: 0.329 ±(99.9%) 0.010 us/op
     Iteration   4: 0.329 ±(99.9%) 0.007 us/op
     Iteration   5: 0.333 ±(99.9%) 0.010 us/op


     Result "com.zxd.interview.conrruentbook.VectorTest.removeElement":
     0.358 ±(99.9%) 0.147 us/op [Average]
     (min, avg, max) = (0.329, 0.358, 0.400), stdev = 0.038
     CI (99.9%): [0.211, 0.505] (assumes normal distribution)


     # Run complete. Total time: 00:00:27

     Benchmark                 Mode  Cnt  Score   Error  Units
     VectorTest.addElement     avgt    5  0.505 ± 0.947  us/op
     VectorTest.getElement     avgt    5  0.242 ± 0.008  us/op
     VectorTest.removeElement  avgt    5  0.358 ± 0.147  us/op

     Process finished with exit code 0


     */
}
