package com.zxd.interview.conrruentbook;

/**
 * 数组克隆测试
 */
public class ArrayCloneTest {

    public static void main(String[] args) {
        Object[] arr = new Object[]{"111",2, 3, "444", 5};
        Object[] clone = arr.clone();
        clone[0] = 1111333;
        System.out.println("arr[0] = " + arr[0]);
        System.out.println("clone arr[0] = " + clone[0]);

        // 但是如果数组存储的是自定义对象那就不行了
        int count = 0;
        while (true){
            try{
//                ZooKeeper zk = client.getZooKeeper();
//                zk.create(final String path, byte data[], List<ACL > acl,
//                        CreateMode createMode)
                break;
            }catch (Exception e){
                count++;
                continue;
            }
        }

    }


}
