package com.zxd.interview.conrruentbook;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 *  SynchronousQueue 单元测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.conrruentbook
 * @Description : SynchronousQueue 单元测试
 * @Create on : 2023/6/18 14:43
 **/
public class SynchronousQueueTest {


    /**
     * 多个线程执行put操作的情况
     * @description 多个线程执行put操作的情况
     * @param
     * @return void
     * @author xander
     * @date 2023/7/4 11:42
     */
    @Test
    public void synchronousQueuePutTest() throws InterruptedException {
        SynchronousQueue<String> objects = new SynchronousQueue<>();
        new Thread(()->{
            try {
                objects.put("张三");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();

        Thread.sleep(100);
        new Thread(()->{
            try {
                objects.put("李四");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();
        Thread.sleep(100);
        new Thread(()->{
            try {
                objects.put("王五");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();
        Thread.sleep(2000);
        // 下面为获取
        new Thread(()->{
            String poll = objects.poll();
            System.out.println(poll);
        }).start();
    }

    /**
     * 线程池中的使用测试
     * @description 线程池中的使用测试
     * @param null
     * @return
     * @author Xander
     * @date 2023/6/18 15:04
     */
    @Test
    public void executorTest() throws ExecutionException, InterruptedException {
        ExecutorService threadPool = Executors.newCachedThreadPool();
        List<Future<Object>> resultList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            Future<Object> submit = threadPool.submit(() -> {
                int result = random.nextInt(100);
                TimeUnit.MILLISECONDS.sleep(result);
                return result;
            });
            resultList.add(submit);
        }
        for (Future<Object> objectFuture : resultList) {
            Object o = objectFuture.get();
            System.out.println(o);
        }

        threadPool.shutdown();
    }

    /**
     * 公平锁的实现
     * @description
     * @param
     * @return
     * @author Xander
     * @date 2023/6/18 15:05
     */
    @Test
    public void fairSynchronousQueueTest() throws InterruptedException {
//        SynchronousQueue<Object> objects = new SynchronousQueue<>(true);
//        // 测试验证需要在多线程环境下进行验证
////        objects.put("aaa");
//        objects.offer("aaa");
//        Object remove = objects.remove();
//        System.out.println(remove);

        // 单线程环境下 验证 SynchronousQueue 无法使用
        SynchronousQueue<Object> objects = new SynchronousQueue<>(true);
        new Thread(() ->{
            try {
                objects.put("ababa");
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }).start();
        new Thread(() ->{
            try {
                objects.put("22213213");
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }).start();


    }



}
