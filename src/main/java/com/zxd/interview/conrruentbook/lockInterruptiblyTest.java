package com.zxd.interview.conrruentbook;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 锁中断 运行和测试
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.conrruentbook
 * @Description : 锁中断
 * @Create on : 2023/5/12 17:57
 **/
public class lockInterruptiblyTest {


    private static final Lock LOCK1 = new ReentrantLock();
    private static final Lock LOCK2 = new ReentrantLock();

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName() + "尝试获取锁" );
        try {
            LOCK1.lockInterruptibly();
            try {
                TimeUnit.MILLISECONDS.sleep(5);
            } finally {
                LOCK1.unlock();
            }
        }catch (InterruptedException e){

        }
    }
}
