package com.zxd.interview.conrruentbook;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 预定座位
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.conrruentbook
 * @Description : 预定座位
 * @Create on : 2023/5/15 17:25
 **/
public class CinemaBookSeat {

    private static ReentrantLock reentrantLock = new ReentrantLock();

    private static void bookSeat(){
        reentrantLock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始预定座位");
            System.out.println(Thread.currentThread().getName() + "完成预订座位");
        } finally {
            reentrantLock.unlock();
        }


    }

    public static void main(String[] args) {
        new Thread(()->bookSeat()).start();
        new Thread(()->bookSeat()).start();
        new Thread(()->bookSeat()).start();
        new Thread(()->bookSeat()).start();
    }/**运行结果：
     Thread-0开始预定座位
     Thread-0完成预订座位
     Thread-1开始预定座位
     Thread-1完成预订座位
     Thread-2开始预定座位
     Thread-2完成预订座位
     Thread-3开始预定座位
     Thread-3完成预订座位
     */

}
