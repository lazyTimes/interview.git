package com.zxd.interview.conrruentbook;

import org.junit.Test;

/**
 * 线程基本操作
 */
public class ThreadBaseOperation {

    /**
     * 线程设置
     */
    @Test
    public void threadSet() throws InterruptedException {
        Thread myThreadName = new Thread(()->{
            System.out.println("Thread.name => "+ Thread.currentThread().getName());
        },"myThreadName");
        myThreadName.start();
        myThreadName.setName("测试");
        myThreadName.join();
    }/*运行解雇：
    Thread.name => myThreadName
    */

    @Test
    public void threadInterrupt() throws Exception{
        ThreadGroup threadGroup = new ThreadGroup("Group-Test");
        Thread thread = new Thread(threadGroup, () -> {
            System.out.println("Thread Test" + Thread.currentThread().getThreadGroup());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread thread2 = new Thread(threadGroup, () -> {
            System.out.println("Thread Test" + Thread.currentThread().getThreadGroup());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        String name = thread.getName();
        System.out.println("name => "+ name);
        thread.start();
        thread2.start();

        System.out.println("threadGroup activeCount = >" + threadGroup.activeCount());
        thread.join();
        thread2.join();

    }/**运行结果：
 name => Thread-0
 Thread Testjava.lang.ThreadGroup[name=Group-Test,maxpri=10]
 */

}
