package com.zxd.interview.conrruentbook;

import java.util.Random;
import java.util.concurrent.*;

/**
 * 实现线程的方式总结：
 * 1. 方法1：使用继承Thread类的方式
 */
public class ThreadImpl {


    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        method1();
//        method2();
//        method3();
//        method6();
//        method7();
//        method8();
        method9();
        System.out.println("Main THread => "+ Thread.currentThread().getName());
    }


    /**
     * 方法1：使用继承Thread类的方式
     */
    public static void method1(){
        class Test extends Thread{
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"  method1");
            }
        }

        new Test().start();
    }/*运行结果：
        Thread-0  method1
        Main THread => main
    */

    /**
     * 方法2：使用lambada 创建线程
     */
    public static void method2(){
        Thread thread = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "  method2");
        });
        thread.start();
    }/*运行结果：
        Thread-0  method2
        Main THread => main
    */

    /**
     * 方法3：实现 Runnbale接口
     */
    public static void method3(){
        class Test2 implements Runnable{

            @Override
            public void run() {

                System.out.println(Thread.currentThread().getName() + "  method3");
            }
        }
        new Thread(new Test2()).start();
    }/*运行结果：
    Thread-0  method3
    Main THread => main
    */

    /**
     * 实现 Runnable 接口的另一种方式
     */
    public static void method4(){
        Thread thread = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "  method2");
        });
        thread.start();
    }

    /**
     * 使用 匿名内部类的方式实现 Runnable
     */
    public static void method5(){
        class Test3 implements  Runnable{

            @Override
            public void run() {
                System.out.println("sss");
            }
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("ssss");
            }
        });
        thread.start();
    }

    /**
     * 使用 callable 接口
     * 匿名内部类创建
     * lambada 构建
     */
    public static void method6(){
        FutureTask<Object> objectFutureTask = new FutureTask<>(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return Thread.currentThread().getName();
            }
        });
        // 或者使用lambda 的 写法
        FutureTask<Object> objectFutureTask2 = new FutureTask<>(() -> Thread.currentThread().getName());
        new Thread(objectFutureTask).start();
        new Thread(objectFutureTask2).start();
        try {
            System.out.println(objectFutureTask.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        try {
            System.out.println(objectFutureTask2.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }


        System.out.println(Thread.currentThread().getName());
    }/*运行结果：
    Thread-0
    Thread-1
    main
    Main THread => main
    */


    /**
     * FutureTask 配合 Thread
     */
    public static void method7() throws ExecutionException, InterruptedException {
        FutureTask<Object> futureTask = new FutureTask<>(() -> Thread.currentThread().getName());
        new Thread(futureTask).start();
        System.out.println("Thread => "+ Thread.currentThread().getName() + " ==> "+ futureTask.get());
    }/*运行结果：
    Thread => main ==> Thread-0
    Main THread => main

    */

    /**
     * 使用线程池构建线程
     * 注意下面的创建方式存在风险
     */
    public static void method8() throws ExecutionException, InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        Future<String> submit = threadPool.submit(() -> {
            System.out.println(Thread.currentThread().getName() + "使用线程池构建线程 " + new Random().nextInt(100));
            return Thread.currentThread().getName();
        });
        String o = submit.get();
        System.out.println("o =>" + o);

        threadPool.execute(()->{
            System.out.println(Thread.currentThread().getName() + "execute 没有返回结果 ");
        });

        threadPool.shutdown();

    }/*运行结果
    Connected to the target VM, address: '127.0.0.1:11853', transport: 'socket'
    pool-1-thread-1使用线程池构建线程 85
    o =>pool-1-thread-1
    Main THread => main
    pool-1-thread-2execute 没有返回结果
    Disconnected from the target VM, address: '127.0.0.1:11853', transport: 'socket'
    */

    /**
     * ThreadExecutorPool 是阿里巴巴最为推荐的创建线程池的方法
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void method9() throws ExecutionException, InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3, 3, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
        threadPoolExecutor.submit(()->{
            System.out.println("sssss");
        });
        threadPoolExecutor.shutdown();
    }



    }
