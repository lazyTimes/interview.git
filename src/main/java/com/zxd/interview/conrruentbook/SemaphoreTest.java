package com.zxd.interview.conrruentbook;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * 信号量入门使用
 * Semaphore 使用及原理
 * https://zhuanlan.zhihu.com/p/98593407
 */
public class SemaphoreTest {
    /**
     * 停车场同时容纳的车辆10
     */
    private static Semaphore semaphore = new Semaphore(10);

    /**
     * 使用信号量实现停车牌功能
     *
     * @param args
     */
    public static void main(String[] args) {
        //模拟100辆车进入停车场
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> {
                try {
                    System.out.println("====" + Thread.currentThread().getName() + "来到停车场");
                    if (semaphore.availablePermits() == 0) {
                        System.out.println("车位不足，请耐心等待");
                    }
                    //获取令牌尝试进入停车场
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "成功进入停车场");
                    Thread.sleep(new Random().nextInt(10000));//模拟车辆在停车场停留的时间
                    System.out.println(Thread.currentThread().getName() + "驶出停车场");
                    //释放令牌，腾出停车场车位
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, i + "号车");
            thread.setName("【" + "宝马 - " + (i + 1) + "】");
            thread.start();

        }


    }

}
