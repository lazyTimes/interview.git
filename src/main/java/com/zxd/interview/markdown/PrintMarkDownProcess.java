package com.zxd.interview.markdown;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * <pre>
 * 打印 md 的进度条，从0% 到 100%
 * 更多进度条样式：
 *
 * <a href="https://github.com/fredericojordan/progress-bar">https://github.com/fredericojordan/progress-bar</a>
 * </pre>
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.markdown
 * @Description : 打印 md 的进度条，从0% 到 100%
 * @Create on : 2023/7/17 06:41
 **/
public class PrintMarkDownProcess {

    private static final String GENERATE_FILE_PATH = "E:\\process.md";

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(GENERATE_FILE_PATH));
            StringBuilder processString = new StringBuilder(3600);
            for (int i = 0; i <= 100; i++) {
                processString.append(String.format("![进度](https://progress-bar.dev/%s/)", i));
                processString.append("\n");
            }
            bufferedWriter.write(processString.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if(Objects.nonNull(bufferedWriter)){
                bufferedWriter.close();
            }
        }

    }
}
