package com.zxd.interview.jdk_source.map;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * hashmap源码测试类
 *
 * @author xander
 * @description hashmap源码测试类
 * @return
 * @date 2023/6/15 15:36
 */
@Slf4j
public class HashMapSourceCodeAnalysis {

    /**
     * 测试核心变量和字段
     *
     * @param
     * @return void
     * @description 测试核心变量和字段
     * @author xander
     * @date 2023/6/15 15:36
     */
    @Test
    public void fieldTest() {
        System.out.println(1 << 4); // 16
        System.out.println(Integer.MAX_VALUE / 2);
        System.out.println(1 << 30); // 刚好为 Integer 最大值的一半
    }

    /**
     * 最大容量为什么是 1 << 30?
     * 由于二进制数字中最高的一位也就是最左边的一位是符号位，用来表示正负之分（0为正，1为负），
     * 所以只能向左移动30位，而不能移动到处在最高位的符号位，所以最大容量只能是2的30次方。
     */
    @Test
    public void maxValTest() {
        // 1073741824
        System.out.println(1 << 30);
        // -2147483648
        System.out.println(1 << 31);
        // 1
        System.out.println(1 << 32);
        // 2
        System.out.println(1 << 33);
        // 4
        System.out.println(1 << 34);
    }

    /**
     * Capacity boundary value
     * 测试边界情况
     */
    @Test
    public void testCapacityBoundaryVal(){
        Map<String, Integer> result = new HashMap<>(1 << 30);
        for (int i = 0; i < 1 << 30; i++) {
            try {
                System.out.println(i + " ");
                result.put(String.valueOf(i), i);
            } catch (Exception e) {
                log.warn("now Count" +i);
            } catch (Throwable e) {
                log.warn("now Count" +i);
            }
        }
        System.out.println(result.size());
    }
}
