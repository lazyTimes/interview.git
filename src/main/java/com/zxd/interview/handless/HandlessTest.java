package com.zxd.interview.handless;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertThat;


public class HandlessTest {

    @Before
    public void setUpHeadlessMode() {
        // 通过注释掉下面的代码测试不同的效果
        System.setProperty("java.awt.headless", "true");
    }

    @Test
    public void whenSetUpSuccessful_thenHeadlessIsTrue() {
        boolean headless = GraphicsEnvironment.isHeadless();
        Assert.assertTrue(headless);
    }/*
    测试通过
    注释下面的代码之后，单元测试不通过
    //        System.setProperty("java.awt.headless", "true");
    */


    @Test
    public void whenHeadlessMode_thenImagesWork() throws IOException {
        String IN_FILE = "D:/BaiduNetdiskDownload/D2EVjnwWoAAV74u.jpg";
        String OUT_FILE = "D:/BaiduNetdiskDownload/test.jpg";
        String FORMAT = "D:/BaiduNetdiskDownload/FORMAT.jpg";

        boolean result = false;
        try (InputStream inStream = HandlessTest.class.getResourceAsStream(IN_FILE);
             FileOutputStream outStream = new FileOutputStream(OUT_FILE)) {
            BufferedImage inputImage = ImageIO.read(inStream);
            result = ImageIO.write(inputImage, FORMAT, outStream);
        }
        Assert.assertTrue(result);
    }

//    @Test
//    public void whenHeadlessMode_thenImagesWork() {
//        boolean result = false;
//        try (InputStream inStream = HeadlessModeUnitTest.class.getResourceAsStream(IN_FILE);
//             FileOutputStream outStream = new FileOutputStream(OUT_FILE)) {
//            BufferedImage inputImage = ImageIO.read(inStream);
//            result = ImageIO.write(inputImage, FORMAT, outStream);
//        }
//
//        assertThat(result).isTrue();
//    }

    @Test
    public void whenHeadless_thenFontsWork() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        String fonts[] = ge.getAvailableFontFamilyNames();

//        assertThat(fonts).isNotEmpty();

        Font font = new Font(fonts[0], Font.BOLD, 14);
        FontMetrics fm = (new Canvas()).getFontMetrics(font);

//        assertThat(fm.getHeight()).isGreaterThan(0);
//        assertThat(fm.getAscent()).isGreaterThan(0);
//        assertThat(fm.getDescent()).isGreaterThan(0);
    }

    @Test
    public void whenHeadlessmode_thenFrameThrowsHeadlessException() {
        Frame frame = new Frame();
        frame.setVisible(true);
        frame.setSize(120, 120);
    }/*
    在开关Headless模式后会有不同的结果
    开启：通过

    关闭
    ava.awt.HeadlessException
	at java.awt.GraphicsEnvironment.checkHeadless(GraphicsEnvironment.java:204)
	at java.awt.Window.<init>(Window.java:536)
	at java.awt.Frame.<init>(Frame.java:420)
	at java.awt.Frame.<init>(Frame.java:385)

    */



    @Test
    public void FlexibleApp() {
        if (GraphicsEnvironment.isHeadless()) {
            System.out.println("Hello World");
        } else {
            JOptionPane.showMessageDialog(null, "showMessageDialog Hello World");
        }
    }



}
