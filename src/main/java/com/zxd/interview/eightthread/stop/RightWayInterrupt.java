package com.zxd.interview.eightthread.stop;

/**
 * 正确方法判断是否暂停
 */
public class RightWayInterrupt {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });

        thread.start();
        // 设置中断标志
        thread.interrupt();
        // 针对对象停止线程
        System.out.println("thread.isInterrupted() => "+ thread.isInterrupted());
        // 主线程
        System.out.println("Instance thread.interrupted() => "+ thread.interrupted());
        System.out.println("Class Thread.interrupted() => "+ Thread.interrupted());

        System.out.println("thread.isInterrupted() => "+ thread.isInterrupted());
        thread.join();
        System.out.println("Main Thread is Over");
    }/**
     thread.isInterrupted() => true
     Thread.interrupted() => false
     thread.isInterrupted() => false
     */
}
