package com.zxd.interview.eightthread.stop;

/**
 * 没有sleep 或者wait的时候如何停止线程。
 */
public class RightWayStopThreadWithoutSleep implements Runnable{


    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new RightWayStopThreadWithoutSleep());
        thread.start();
        thread.sleep(1000);
        // 线程不响应停止
        thread.interrupt();
    }

    @Override
    public void run() {

        int num = 0;
        // 配合检测
        while (!Thread.currentThread().isInterrupted() && num <= Integer.MAX_VALUE / 2){
            if(num % 10000 == 0){
                System.out.println("num =>" + num);
            }
            num++;
        }
        System.out.println("执行完成");
    }
}
