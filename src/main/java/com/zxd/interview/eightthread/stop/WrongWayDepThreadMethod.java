package com.zxd.interview.eightthread.stop;

/**
 * 使用过时的方法处理线程问题
 * 使用过时方法，会强制让一个任务停止，这可能会导致一个线程的工作进行到一半出现不可预见的结果
 */
public class WrongWayDepThreadMethod implements Runnable{

    public static void main(String[] args) throws InterruptedException {
        WrongWayDepThreadMethod wrongWayDepThreadMethod = new WrongWayDepThreadMethod();
        Thread thread = new Thread(wrongWayDepThreadMethod);
        thread.start();
        Thread.sleep(1000);
        // 强制停止线程
//        thread.stop();
        // 暂停，会导致子线程卡顿
        thread.suspend();
        Thread.sleep(1000);
        // This method exists solely for use with suspend, which has been deprecated because it is deadlock-prone.
        // For more information, see Why are Thread.stop, Thread.suspend and Thread.resume Deprecated?.
        thread.resume();
    }
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("连队 " + (i + 1) + "开始领武器");
            for (int j = 0; j < 10; j++) {
                System.out.println("士兵" + (j + 1) + "领到了武器");
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("连队领取完成");

        }
    }/*
    使用过时方法，会强制让一个任务停止，这可能会导致一个线程的工作进行到一半出现不可预见的结果
    连队 2开始领武器
    士兵1领到了武器
    士兵2领到了武器
    士兵3领到了武器
    士兵4领到了武器
    士兵5领到了武器
    士兵6领到了武器
    士兵7领到了武器
    */
}
