package com.zxd.interview.eightthread.stop;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 在阻塞队列当中，使用volatile会出问题
 * 这里使用生产者和消费者的模式进行演示
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.eightthread.stop
 * @Description : 在阻塞队列当中，使用volatile会出问题
 * @Create on : 2023/3/8 13:50
 **/
public class UnsuitableVolatieStopThreadByBlockingQueue {


    public static void main(String[] args) throws InterruptedException {
        BlockingQueue blockingQueue = new ArrayBlockingQueue(10);
        Producer producer = new Producer(blockingQueue);
        Thread thread = new Thread(producer);
        thread.start();
        Thread.sleep(2000);

        Consumer consumer = new Consumer(blockingQueue);
        while (consumer.needMoreNums()){
            System.out.println(consumer.storage.take()+"被消费了");
        }
        System.out.println("消费者不需要更多数据");
        // 不需要更多数据，应该让生产者停下来
        // 实际上会发现无法正常退出
//        producer.isStop = true;
        // 修复方式，由volatile变量判断转为中断调用
        thread.interrupt();
        System.out.println("主线程结束了");
    }
}/**

    运行结果有时候可以因为CPU排序让两个线程停下来>
     虽然消费者不再消费了，但是没有停止
     这种情况下会无法停止子线程

     100的倍数0被放入到仓库当中
     100的倍数100被放入到仓库当中
     100的倍数200被放入到仓库当中
     100的倍数300被放入到仓库当中
     100的倍数400被放入到仓库当中
     100的倍数500被放入到仓库当中
     100的倍数600被放入到仓库当中
     100的倍数700被放入到仓库当中
     100的倍数800被放入到仓库当中
     100的倍数900被放入到仓库当中
     100的倍数1000被放入到仓库当中
     0被消费了
     100的倍数1100被放入到仓库当中
     100被消费了
     100的倍数1200被放入到仓库当中
     200被消费了
     100的倍数1300被放入到仓库当中
     300被消费了
     100的倍数1400被放入到仓库当中
     消费者不需要更多数据


 */

class Producer implements Runnable{

    public volatile boolean isStop = false;

    BlockingQueue storage;

    public Producer(BlockingQueue storage){
        this.storage = storage;
    }

    @Override
    public void run() {
        int num = 0;
        try {
            // 需要检查到此处
//            while (num <= 10000 && !isStop) {
            // 正确的处理方式
            while (num <= 10000 && !isStop) {

                if (num % 100 == 0) {
                    // 此处是阻塞的，无法执行下一步
                    storage.put(num);
                    System.out.println("100的倍数" + num+"被放入到仓库当中");
                }
                num++;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("生产者运行结束");
        }
        // 没有执行到此处
        System.out.println("程序正常退出");
    }
}

class Consumer{

    BlockingQueue storage;

    public Consumer(BlockingQueue storage){
        this.storage = storage;
    }

    public boolean needMoreNums(){
        if(Math.random() > 0.95){
            return false;
        }
        return true;
    }
}