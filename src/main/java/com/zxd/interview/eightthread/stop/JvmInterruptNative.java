package com.zxd.interview.eightthread.stop;

public class JvmInterruptNative {

    /*

    void os::interrupt(Thread* thread) {
	  assert(Thread::current() == thread || Threads_lock->owned_by_self(),
	    "possibility of dangling Thread pointer");
	  OSThread* osthread = thread->osthread();
	  if (!osthread->interrupted()) {
	    // 设置interrupt为true
	    osthread->set_interrupted(true);
	    // More than one thread can get here with the same value of osthread,
	    // resulting in multiple notifications.  We do, however, want the store
	    // to interrupted() to be visible to other threads before we execute unpark().
	    OrderAccess::fence();
	    // 对应sleep方法
	    ParkEvent * const slp = thread->_SleepEvent;
	    if (slp != NULL) slp->unpark();
	  }
	  // For JSR166. Unpark even if interrupt status already was set
	  if (thread->is_Java_thread())
	    ((JavaThread*)thread)->parker()->unpark();
        // LockSupport.parker() 方法
        // _ParkEvent就是synchronized 同步代码以及Object.wait
	  ParkEvent * ev = thread->_ParkEvent;

	  if (ev != NULL) ev->unpark();
	}
    * */
}
