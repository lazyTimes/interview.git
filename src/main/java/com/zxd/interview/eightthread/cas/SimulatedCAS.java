package com.zxd.interview.eightthread.cas;

/**
 * 模拟CPU的CAS操作
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.eightthread.cas
 * @Description : 模拟CPU的CAS操作
 * @Create on : 2023/6/4 20:46
 **/
public class SimulatedCAS {
    private volatile int value;

    public synchronized int compareAndSwap(int expectedValue, int newValue){
        int oldValue = value;
        if(oldValue == expectedValue){
            value = newValue;
        }
        return oldValue;
    }
}
