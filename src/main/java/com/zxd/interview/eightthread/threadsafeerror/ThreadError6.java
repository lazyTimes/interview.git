package com.zxd.interview.eightthread.threadsafeerror;

import java.util.HashMap;
import java.util.Map;

/**
 * 在构造方法当中执行对象初始化
 */
public class ThreadError6 {

    public static void main(String[] args) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                map.put("hello1", "HELLO");
                map.put("hello2", "HELLO");
                map.put("hello3", "HELLO");
                map.put("hello4", "HELLO");
            }
        });
        thread.start();
        Thread.sleep(100);
        System.out.println(map);

    }/**
     如果对象在单线程初始化，很有可能拿到初始化到一半的对象
     */
}
