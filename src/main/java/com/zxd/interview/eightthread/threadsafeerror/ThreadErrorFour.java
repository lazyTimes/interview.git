package com.zxd.interview.eightthread.threadsafeerror;

import lombok.Data;
import lombok.ToString;

/**
 * 线程错误：
 * 初始化没有完成就发布对象
 * 会导致获得一个初始化一部分的对象
 */
public class ThreadErrorFour {

    static Point point;

    public static void main(String[] args) throws InterruptedException {
        new Thread(new PointMark()).start();
        Thread.sleep(10);
        System.out.println(point);

    }/**
     运行结果
     Point(x=1, y=0)
     */

}


@Data
@ToString
class Point{

    private final int x,y;

    Point(int x, int y) throws InterruptedException {
        this.x = x;
        ThreadErrorFour.point = this;
        // 用于模拟CPU切换在初始化的代码只执行了一部分
        Thread.sleep(100);
        this.y = y;
    }
}


class PointMark implements Runnable{

    @Override
    public void run() {
        try {
            new Point(1, 1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
