package com.zxd.interview.eightthread.threadsafeerror;


import java.util.EventListener;

/**
 * 观察者模式的线程安全问题
 * 此程序无法复现
 */
public class ThreadError5 {

    int count;

    public ThreadError5(MySource mySource){
//        mySource.registerListener();
    }

    public static void main(String[] args) {
        MySource mySource = new MySource();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        thread.start();
    }
}


class MySource {

    private EventListener eventListener;


    void registerListener( EventListener eventListener){
        this.eventListener = eventListener;

    }

//    void eventCome(Event event){
//        if(null != eventListener){
//            eventListener.onEvent();
//        }
//    }
}
