package com.zxd.interview.eightthread.threadsafeerror;

/**
 * 线程安全问题：
 * 两个线程累加，发现结果达不到期望结果
 */
public class ThreadSafeErrorTwoThread implements Runnable{

    private int count = 1;

    private static ThreadSafeErrorTwoThread threadSafeErrorTwoThread = new ThreadSafeErrorTwoThread();

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(threadSafeErrorTwoThread);

        Thread thread2 = new Thread(threadSafeErrorTwoThread);

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("count sum => "+ threadSafeErrorTwoThread.count);
    }

    @Override
    public void run() {
        for (int i = 0; i <= 10000; i++) {
            count++;
        }
    }/**运行结果
        每次运行结果都不一样，但是很难有正确结果
     */
}
