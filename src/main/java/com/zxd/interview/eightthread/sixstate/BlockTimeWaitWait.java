package com.zxd.interview.eightthread.sixstate;

/**
 * 线程的三种等待状态
 */
public class BlockTimeWaitWait implements Runnable {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new BlockTimeWaitWait());
        thread.start();
        Thread thread2 = new Thread(new BlockTimeWaitWait());
        thread2.start();
        // 会出现一个在运行中，另一个在进行等待的状态
//        thread.getState() =>RUNNABLE
//        thread2.getState() =>TIMED_WAITING
        System.out.println("thread.getState() =>" + thread.getState());
        System.out.println("thread.getState() =>" + thread.getState());
        System.out.println("thread2.getState() =>" + thread2.getState());
        // 验证等待状态
        // thread.getState() =>WAITING
        Thread.sleep(1000);
        System.out.println("thread.getState() =>" + thread.getState());
        System.out.println("thread2.getState() =>" + thread2.getState());
        thread.interrupt();
    }

    /**
     * thread.getState() =>RUNNABLE
     * thread2.getState() =>TIMED_WAITING
     */


    @Override
    public void run() {
        try {
            // 单纯调用次方法，可以验证block状态
            // thread1.getState() =>BLOCKED
//            sync();

            // 下面的方式验证wait和time_wait的效果
            synchronized (this) {
                Thread.sleep(1000);
                // 想要出现等待状态，可以使用下面的方法
//                wait();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private synchronized void sync(){
        System.out.println("ssss");
    }
}
