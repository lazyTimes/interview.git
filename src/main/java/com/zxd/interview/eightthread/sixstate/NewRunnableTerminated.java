package com.zxd.interview.eightthread.sixstate;

/**
 * 线程的基础三大状态：
 * New
 * Runnable
 * Terminated
 */
public class NewRunnableTerminated {

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(() -> {
            int num = 0;
            while (num++ < 1000) {
                System.out.println("num => " + num);
            }
        });
        System.out.println("thread.getState() => "+ thread.getState());
        thread.start();
        System.out.println("thread.getState() => "+ thread.getState());
        Thread.sleep(10);
        System.out.println("thread.getState() => "+ thread.getState());
        Thread.sleep(100);
        System.out.println("thread.getState() => "+ thread.getState());


    }/**
     thread.getState() => NEW
     thread.getState() => RUNNABLE
     thread.getState() => TERMINATED
     */
}
