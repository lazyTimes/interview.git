package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

/**
 * sleep 睡眠不释放 synchronized 锁
 */
public class SleepDontReleaseMonitor implements Runnable{


    public static void main(String[] args) {
        Thread thread1 = new Thread(new SleepDontReleaseMonitor());
        Thread thread2 = new Thread(new SleepDontReleaseMonitor());

    }

    @Override
    public void run() {
        try {
            syn();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private synchronized void syn() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " 获取到锁");
        System.out.println(Thread.currentThread().getName() + " 进入睡眠等待");
        Thread.sleep(5000);
        System.out.println(Thread.currentThread().getName() + " 睡眠等待完成");
    }
}
