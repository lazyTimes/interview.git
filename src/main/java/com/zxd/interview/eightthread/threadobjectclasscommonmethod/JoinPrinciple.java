package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

public class JoinPrinciple {

    public static void main(String[] args) throws InterruptedException {
        Thread one = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完成");
        });

        Thread two = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"执行完成");
        });
        one.start();
        two.start();

//        one.join();
//        two.join();
        synchronized (one){
            one.wait();
        }
        synchronized (two){
            two.wait();
        }
        System.out.println("主线程执行完成");
    }

}
