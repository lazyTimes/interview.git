package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

/**
 * 线程ID是从 0 开始的
 * 答案显然是从 1 开始，不能先入为主认为是从0开始，
 * 为什么会是1？因为 是 ++index，先自增再返回值
 */
public class ThreadId implements Runnable{

    public static void main(String[] args) {
        System.out.println("当前线程 ID => "+ Thread.currentThread().getId());
        new Thread(new ThreadId()).start();
    }/**
     当前线程 ID => 1
     当前线程 ID => 22
     */

    @Override
    public void run() {
        System.out.println("当前线程 ID => "+ Thread.currentThread().getId());
    }
}
