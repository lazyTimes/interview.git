package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 消息队列，使用JAVA语言实现
 */
public class ProducerConsumerModel {

    static class Producer implements Runnable {

        private EventStoreage eventStoreage;

        public Producer(EventStoreage eventStoreage) {
            this.eventStoreage = eventStoreage;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                eventStoreage.put();
            }
        }
    }

    static class Consumer implements Runnable {

        private EventStoreage eventStoreage;

        public Consumer(EventStoreage eventStoreage) {
            this.eventStoreage = eventStoreage;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                eventStoreage.take();
            }
        }
    }

    /**
     * 事件存储
     */
    static class EventStoreage {
        private int maxSize;

        private List<Date> storage;

        public EventStoreage() {
            maxSize = 10;
            storage = new ArrayList<>();
        }

        public synchronized void put() {
            while (storage.size() == maxSize) {
                System.out.println("仓库没有产品啦，等待");
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            storage.add(new Date());
            System.out.println("当前仓库存在" + storage.size() + "件产品");
            notify();
        }

        public synchronized void take() {
            while (storage.size() == 0) {
                System.out.println("仓库的产品拿完啦，等待");
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("拿到了" + storage.get(0) + "，现在还剩下" + storage.size() + "产品");
            storage.remove(0);
            notify();
        }

    }


    public static void main(String[] args) {
        EventStoreage eventStoreage = new EventStoreage();
        Producer producer = new Producer(eventStoreage);
        Consumer consumer = new Consumer(eventStoreage);
        new Thread(producer).start();
        new Thread(consumer).start();
    }/**
     最终效果：

     当前仓库存在1件产品
     当前仓库存在2件产品
     当前仓库存在3件产品
     当前仓库存在4件产品
     当前仓库存在5件产品
     当前仓库存在6件产品
     当前仓库存在7件产品
     当前仓库存在8件产品
     当前仓库存在9件产品
     当前仓库存在10件产品
     仓库没有产品啦，等待
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下10产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下9产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下8产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下7产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下6产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下5产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下4产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下3产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下2产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下1产品
     仓库的产品拿完啦，等待
     当前仓库存在1件产品
     当前仓库存在2件产品
     当前仓库存在3件产品
     当前仓库存在4件产品
     当前仓库存在5件产品
     当前仓库存在6件产品
     当前仓库存在7件产品
     当前仓库存在8件产品
     当前仓库存在9件产品
     当前仓库存在10件产品
     仓库没有产品啦，等待
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下10产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下9产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下8产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下7产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下6产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下5产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下4产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下3产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下2产品
     拿到了Tue Mar 14 07:00:06 CST 2023，现在还剩下1产品
     仓库的产品拿完啦，等待

     */


}
