package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

/**
 * join的现场被其他线程中断的效果
 */
public class JoinInterrupt {

    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();
        Thread one = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完成");
            System.out.println(Thread.currentThread().getName()+"中断主线程");
            mainThread.interrupt();
        });

        Thread two = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"执行完成");
            System.out.println(Thread.currentThread().getName()+"中断主线程");
            mainThread.interrupt();
        });
        one.start();
        two.start();

        // 正确处理方式，先注释上面两行代码，再打开下面这部分代码：
//        one.join();
//        two.join();
//        try {
//            one.join();
//            two.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//            // 正确处理方式：在主线程join被interrupt之后，停止子线程
//            System.out.println(mainThread.getName()+"中断"+one.getName());
//            System.out.println(one.getName()+"状态："+one.getState());
//            one.interrupt();
//            System.out.println(mainThread.getName()+"中断"+two.getName());
//            System.out.println(two.getName()+"状态："+two.getState());
//            two.interrupt();
//        }

        System.out.println("主线程执行完成");

    }/**
     结果：主线程等待过程被中断，不再往下执行
        但是子线程没有受到干扰，继续执行

     Thread-0开始执行
     Thread-1开始执行
     Thread-0执行完成
     Thread-1执行完成
     Thread-0中断主线程
     Thread-1中断主线程
     Exception in thread "main" java.lang.InterruptedException
     at java.base/java.lang.Object.wait(Native Method)
     at java.base/java.lang.Thread.join(Thread.java:1305)
     at java.base/java.lang.Thread.join(Thread.java:1379)
     at com.zxd.interview.eightthread.threadobjectclasscommonmethod.JoinInterrupt.main(JoinInterrupt.java:36)

     在主线程做一系列处理之后：
     Thread-0开始执行
     Thread-1开始执行
     Thread-0执行完成
     Thread-0中断主线程
     main中断Thread-0
     Thread-0状态：TERMINATED
     main中断Thread-1
     Thread-1状态：TIMED_WAITING
     主线程执行完成
     Thread-1执行完成
     Thread-1中断主线程
     java.lang.InterruptedException
     at java.base/java.lang.Object.wait(Native Method)
     at java.base/java.lang.Thread.join(Thread.java:1305)
     at java.base/java.lang.Thread.join(Thread.java:1379)
     at com.zxd.interview.eightthread.threadobjectclasscommonmethod.JoinInterrupt.main(JoinInterrupt.java:38)
     java.lang.InterruptedException: sleep interrupted
     at java.base/java.lang.Thread.sleep(Native Method)
     at com.zxd.interview.eightthread.threadobjectclasscommonmethod.JoinInterrupt.lambda$main$1(JoinInterrupt.java:25)
     at java.base/java.lang.Thread.run(Thread.java:834)
     */
}
