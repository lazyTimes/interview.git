package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

/**
 * join过程当中的状态
 */
public class JoiningState {


    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();
        Thread one = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
                System.out.println("主线程"+mainThread.getName()+"当前状态为"+Thread.currentThread().getState());
                System.out.println(Thread.currentThread().getName()+"当前状态为"+Thread.currentThread().getState());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完成");
            System.out.println(Thread.currentThread().getName()+"中断主线程");
//            mainThread.interrupt();
        });

        Thread two = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"执行完成");
            System.out.println(Thread.currentThread().getName()+"中断主线程");
//            mainThread.interrupt();
        });
        one.start();
        two.start();
        one.join();
        two.join();
        System.out.println("主线程执行完成");

    }
}
