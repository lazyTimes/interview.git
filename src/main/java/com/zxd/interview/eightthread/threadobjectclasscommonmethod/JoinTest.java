package com.zxd.interview.eightthread.threadobjectclasscommonmethod;

/**
 * join的用法演示
 */
public class JoinTest {

    public static void main(String[] args) throws InterruptedException {
        Thread one = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"执行完成");
        });

        Thread two = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"开始执行");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"执行完成");
        });
        one.start();
        two.start();
        one.join();
        two.join();
        System.out.println("主线程执行完成");



    }/**
     JOIN线程等待的效果
     Thread-0开始执行
     Thread-1开始执行
     Thread-1执行完成
     Thread-0执行完成
     主线程执行完成
     */



}
