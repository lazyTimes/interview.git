package com.zxd.interview.beabug.ssl;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.Objects;

/**
 * 2. The SSLPeerUnverifiedException
 * Without configuring SSL with the HttpClient, the following test – consuming an HTTPS URL – will fail:
 */
public class SSLPeerUnverifiedException {

    public static void main(String [] args) throws IOException {
        String urlOverHttps = "https://whitestore.top/";
        HttpGet getMethod = new HttpGet(urlOverHttps);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpResponse response = httpClient.execute(getMethod, new CustomHttpClientResponseHandler());
        System.out.println(Objects.equals(response.getStatusLine().getStatusCode(), 200));
    }
}
