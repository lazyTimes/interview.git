package com.zxd.interview.beabug.ssl;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.GeneralSecurityException;
import java.util.Objects;

import static org.junit.Assert.assertThat;

/**
 * 带 SSL 的 Spring RestTemplate（HttpClient 5）
 */
public class RestTemplate5 {

    public static void main(String[] args) throws GeneralSecurityException {
        final String urlOverHttps = "https://whitestore.top/";

        clientNoHttp(urlOverHttps);

        configSSLAndClientHttps(urlOverHttps);

    }

    private static void clientNoHttp(String urlOverHttps) {
        final ResponseEntity<String> response = new RestTemplate()
                .exchange(urlOverHttps, HttpMethod.GET, null, String.class);
        System.out.println(Objects.equals(response.getStatusCodeValue(), 200));
    }

    public static void configSSLAndClientHttps(String urlOverHttps) throws GeneralSecurityException {
        final TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
        final SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();
        final SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
        final Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("https", sslsf)
                .register("http", new PlainConnectionSocketFactory())
                .build();

        final BasicHttpClientConnectionManager connectionManager =
                new BasicHttpClientConnectionManager(socketFactoryRegistry);
        final CloseableHttpClient httpClient = HttpClients.custom()
                .setConnectionManager(connectionManager)
                .build();

        final HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        final ResponseEntity<String> response = new RestTemplate(requestFactory)
                .exchange(urlOverHttps, HttpMethod.GET, null, String.class);
        System.out.println(Objects.equals(response.getStatusCodeValue(), 200));

    }
}
