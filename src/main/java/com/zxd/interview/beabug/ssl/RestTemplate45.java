package com.zxd.interview.beabug.ssl;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

/**
 * 6、 带 SSL 的 Spring RestTemplate（HttpClient 4.5）
 */
public class RestTemplate45 {

    public static void main(String[] args) {
        final CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();
        final HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        final ResponseEntity<String> response = new RestTemplate(requestFactory)
                .exchange("https://whitestore.top/", HttpMethod.GET, null, String.class);
        System.out.println(Objects.equals(response.getStatusCodeValue(), 200));
    }
}
