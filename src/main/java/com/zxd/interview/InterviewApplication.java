package com.zxd.interview;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.zxd.interview.*", "com.zxd.interview.valid"})
@ComponentScan("com.zxd.interview.*")
@EnableTransactionManagement
@Slf4j
public class InterviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewApplication.class, args);
        log.info("启动成功");
    }

}
