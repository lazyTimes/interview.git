package com.zxd.interview.cocurrent;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 接口访问计数器
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.cocurrent
 * @Description : 接口访问计数器
 * @Create on : 2023/7/11 09:37
 **/
public class AccessCounter {

    public class AcessCounter {

        int accessCount;

        public void access() {
            accessCount++;
        }
    }

    public class AcessCounter2 {

        AtomicInteger accessCount = new AtomicInteger();

        public void access() {
            accessCount.getAndIncrement();
        }
    }
}
