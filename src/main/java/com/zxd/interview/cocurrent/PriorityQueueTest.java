package com.zxd.interview.cocurrent;

import java.util.PriorityQueue;

/**
 * PriorityQueue 测试使用
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.cocurrent
 * @Description : PriorityQueue 测试使用
 * @Create on : 2023/7/2 20:41
 **/
public class PriorityQueueTest {
    public static void main(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue();
        priorityQueue.add(500);
        priorityQueue.add(600);
        priorityQueue.add(700);
        priorityQueue.add(800);
        priorityQueue.add(490);
        System.out.println(priorityQueue.size() >>> 1);
    }
}


