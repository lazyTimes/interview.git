package com.zxd.interview.cocurrent;

/**
 * 车辆信息跟踪类
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.cocurrent
 * @Description : 车辆信息跟踪类
 * @Create on : 2023/7/11 11:03
 **/
public class Location {

    private double x;

    private double y;

    public Location(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setXY(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }


    public double getY() {
        return y;
    }

}
