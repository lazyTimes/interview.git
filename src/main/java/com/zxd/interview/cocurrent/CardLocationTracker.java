package com.zxd.interview.cocurrent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 车辆信息追踪类
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.cocurrent
 * @Description : 车辆信息追踪类
 * @Create on : 2023/7/15 21:52
 **/
public class CardLocationTracker {

    /***
     * 车辆编码对应车辆位置信息Map
     */
    private Map<String, Location> locationMap = new ConcurrentHashMap<>();

    /***
     * 更新车辆信息
     * @description 更新车辆信息
     * @param cache
     * @param: x
     * @param: y
     * @return
     * @author Xander
     * @date 2023/7/15 21:54
     */
    public void updateLocation(String cache, double x, double y) {
        Location location = locationMap.get(cache);
        location.setXY(x, y);
    }

    /**
     * 获取车辆信息
     *
     * @param cache
     * @return
     * @description 获取车辆信息
     * @author Xander
     * @date 2023/7/15 21:55
     */
    public Location getLocation(String cache) {
        return locationMap.get(cache);
    }


}
