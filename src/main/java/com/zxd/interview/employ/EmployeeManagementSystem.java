package com.zxd.interview.employ;

import java.util.ArrayList;
import java.util.List;

/**
 * 员工管理系统类 EmployeeManagementSystem
 */
public class EmployeeManagementSystem {

    private List<Employee> employees;

    public EmployeeManagementSystem() {
        this.employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        System.out.println("员工添加成功!");
    }

    public void adjustSalary(int id, double newSalary) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                employee.setSalary(newSalary);
                System.out.println("员工月薪调整成功!");
                return;
            }
        }
        System.out.println("未找到指定工号的员工!");
    }

    public void listAllEmployees() {
        if (employees.isEmpty()) {
            System.out.println("没有员工记录!");
        } else {
            for (Employee employee : employees) {
                System.out.println(employee);
            }
        }
    }

    public void showEmployeeInfo(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                System.out.println(employee);
                return;
            }
        }
        System.out.println("未找到指定工号的员工!");
    }

    public void deleteEmployee(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                employees.remove(employee);
                System.out.println("员工删除成功!");
                return;
            }
        }
        System.out.println("未找到指定工号的员工!");
    }

    public double calculateTotalSalary() {
        double totalSalary = 0;
        for (Employee employee : employees) {
            totalSalary += employee.getSalary();
        }
        return totalSalary;
    }
}
