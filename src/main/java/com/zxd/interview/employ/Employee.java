package com.zxd.interview.employ;

// 员工类 Employee
public class Employee {

    private int id;          // 工号
    private String name;     // 姓名
    private String gender;   // 性别
    private String position; // 职位
    private double salary;   // 月薪

    public Employee(int id, String name, String gender, String position, double salary) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.position = position;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getPosition() {
        return position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                '}';
    }
}
