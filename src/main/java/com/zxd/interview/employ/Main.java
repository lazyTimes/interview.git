package com.zxd.interview.employ;

import java.util.Scanner;

// 菜单+运行
public class Main {

    public static void main(String[] args) {
        EmployeeManagementSystem system = new EmployeeManagementSystem();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("员工管理系统菜单:");
            System.out.println("1. 员工入职(添加)");
            System.out.println("2. 根据员工的工号调整员工的月薪");
            System.out.println("3. 查询所有员工");
            System.out.println("4. 根据员工的工号显示员工信息");
            System.out.println("5. 删除指定工号的员工");
            System.out.println("6. 统计一个月发放的总工资");
            System.out.println("7. 退出");

            System.out.print("请选择操作: ");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("请输入工号: ");
                    int id = scanner.nextInt();
                    scanner.nextLine();  // 清除缓冲区
                    System.out.print("请输入姓名: ");
                    String name = scanner.nextLine();
                    System.out.print("请输入性别: ");
                    String gender = scanner.nextLine();
                    System.out.print("请输入职位: ");
                    String position = scanner.nextLine();
                    System.out.print("请输入月薪: ");
                    double salary = scanner.nextDouble();
                    Employee employee = new Employee(id, name, gender, position, salary);
                    system.addEmployee(employee);
                    break;
                case 2:
                    System.out.print("请输入工号: ");
                    int adjustId = scanner.nextInt();
                    System.out.print("请输入新的月薪: ");
                    double newSalary = scanner.nextDouble();
                    system.adjustSalary(adjustId, newSalary);
                    break;
                case 3:
                    system.listAllEmployees();
                    break;
                case 4:
                    System.out.print("请输入工号: ");
                    int queryId = scanner.nextInt();
                    system.showEmployeeInfo(queryId);
                    break;
                case 5:
                    System.out.print("请输入工号: ");
                    int deleteId = scanner.nextInt();
                    system.deleteEmployee(deleteId);
                    break;
                case 6:
                    double totalSalary = system.calculateTotalSalary();
                    System.out.println("一个月发放的总工资为: " + totalSalary);
                    break;
                case 7:
                    System.out.println("退出系统");
                    scanner.close();
                    return;
                default:
                    System.out.println("无效的选择，请重新输入!");
            }
        }
    }
}
