package com.zxd.interview.maps;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * map实验比较
 *
 * @Author zhaoxudong
 * @Date 2020/08/26 09:24
 **/
public class MapEntryTest {

    public static void main(String[] args) {
        run1();
//        hash();
    }


    /**
     * 1. map 可以存储Null值
     * 2. map.entrySet() 做的事情
     * 1. 通过HashIterator 的子类 EntryIterator 实例，
     */
    public static void run1() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("key1", 1);
        maps.put("key2", 2);
        maps.put("key3", 3);
        maps.put(null, 3);
        maps.put(null, 3);
        maps.put(null, 3);
        Set<Map.Entry<String, Object>> entries = maps.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            String key = entry.getKey();
            System.err.println(key);
        }
    }

    public static void hash() {
        int h = 6654;
        h ^= (h >>> 20) ^ (h >>> 12);
        h = h ^ (h >>> 7) ^ (h >>> 4);
        System.err.println(h);
    }
}
