package com.zxd.interview.netty.netty.serverclientstart;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 客户端启动扩展
 * 1. 介绍一些API
 * 2. 失败重连
 */
@Slf4j
public class NettyClientStart {

    private static final int MAX_RETRY = 3;

    public static void main(String[] args) throws InterruptedException {
        Bootstrap bootstrap = new Bootstrap();
        NioEventLoopGroup eventExecutors = new NioEventLoopGroup();
        // 引导器引导启动
        bootstrap
                .group(eventExecutors)
                .channel(NioSocketChannel.class)

                // 指定自定义属性，客户端可以根据此属性进行一些判断处理
                // 可以看作给Channel维护一个Map属性，这里的channel是服务端
                // 允许指定一个新创建的通道的初始属性。如果该值为空，指定键的属性将被删除。
                .attr(AttributeKey.newInstance("hello"), "hello world")

                /// 客户端的 Channel 设置TCP 参数
                // so_backlog 临时存放已完成三次握手的请求队列的最大长度，如果频繁连接可以调大此参数
                .option(ChannelOption.SO_BACKLOG, 1024)

                // 给每个连接设置TCP参数
                // tcp的心跳检测，true为开启
                .option(ChannelOption.SO_KEEPALIVE, true)
                // nagle 算法开关，实时性要求高就关闭
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel channel) throws Exception {
                        channel.pipeline().addLast(new StringEncoder());
                    }
                });

        // 建立通道
//        Channel channel =
        connect(bootstrap, "127.0.0.1", 8000, MAX_RETRY);
//                .channel();

//        while (true){
//            channel.writeAndFlush(new Date() + " Hello world");
//            Thread.sleep(2000);
//        }


    }

    private static void connect(Bootstrap bootstrap, String host, int port, int retry) {
        try {
            final int[] trys = {retry};
            bootstrap.connect(host, port).addListener(future -> {
                if (future.isSuccess()) {
                    log.info("连接成功");
                } else if (retry <= 0) {
                    log.info("重连次数用完了");
                } else {
                    // 记录第几次重连
                    int order = (MAX_RETRY - retry) + 1;
                    // 重连间隔
                    int delay = 1 << order;

                    log.info(new Date() + ": 重连失败，第" + order + "次重连");
                    bootstrap.config().group().schedule(() -> connect(bootstrap, host, port, trys[0]--), delay, TimeUnit.SECONDS);
                }
            });
        } catch (Exception e) {
            log.error("重连失败");
        }

    }
}
