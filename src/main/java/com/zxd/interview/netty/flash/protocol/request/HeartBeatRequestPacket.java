package com.zxd.interview.netty.flash.protocol.request;

import com.zxd.interview.netty.flash.protocol.Packet;

import static com.zxd.interview.netty.flash.protocol.command.Command.HEARTBEAT_REQUEST;

public class HeartBeatRequestPacket extends Packet {
    @Override
    public Byte getCommand() {
        return HEARTBEAT_REQUEST;
    }
}
