package com.zxd.interview.netty.flash.util;

import java.util.UUID;

public class IDUtil {

    /**
     * 随机生成UUID
     * @description 随机生成UUID
     * @param
     * @return java.lang.String
     * @author xander
     * @date 2023/6/29 14:39
     */
    public static String randomId() {
        return UUID.randomUUID().toString().split("-")[0];
    }
}
