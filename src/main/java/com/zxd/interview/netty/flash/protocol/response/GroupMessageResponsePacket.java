package com.zxd.interview.netty.flash.protocol.response;

import com.zxd.interview.netty.flash.protocol.Packet;
import com.zxd.interview.netty.flash.session.Session;
import lombok.Data;

import static com.zxd.interview.netty.flash.protocol.command.Command.GROUP_MESSAGE_RESPONSE;

@Data
public class GroupMessageResponsePacket extends Packet {

    private String fromGroupId;

    private Session fromUser;

    private String message;

    @Override
    public Byte getCommand() {

        return GROUP_MESSAGE_RESPONSE;
    }
}
