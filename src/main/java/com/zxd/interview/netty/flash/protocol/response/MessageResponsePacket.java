package com.zxd.interview.netty.flash.protocol.response;

import com.zxd.interview.netty.flash.protocol.Packet;
import lombok.Data;

import static com.zxd.interview.netty.flash.protocol.command.Command.MESSAGE_RESPONSE;

@Data
public class MessageResponsePacket extends Packet {

    private String fromUserId;

    private String fromUserName;

    private String message;

    @Override
    public Byte getCommand() {

        return MESSAGE_RESPONSE;
    }
}
