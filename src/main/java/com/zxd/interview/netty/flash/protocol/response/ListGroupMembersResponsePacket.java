package com.zxd.interview.netty.flash.protocol.response;

import com.zxd.interview.netty.flash.protocol.Packet;
import com.zxd.interview.netty.flash.session.Session;
import lombok.Data;

import java.util.List;

import static com.zxd.interview.netty.flash.protocol.command.Command.LIST_GROUP_MEMBERS_RESPONSE;

@Data
public class ListGroupMembersResponsePacket extends Packet {

    private String groupId;

    private List<Session> sessionList;

    @Override
    public Byte getCommand() {

        return LIST_GROUP_MEMBERS_RESPONSE;
    }
}
