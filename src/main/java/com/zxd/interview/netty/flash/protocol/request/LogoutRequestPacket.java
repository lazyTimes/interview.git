package com.zxd.interview.netty.flash.protocol.request;

import com.zxd.interview.netty.flash.protocol.Packet;
import lombok.Data;

import static com.zxd.interview.netty.flash.protocol.command.Command.LOGOUT_REQUEST;

@Data
public class LogoutRequestPacket extends Packet {
    @Override
    public Byte getCommand() {

        return LOGOUT_REQUEST;
    }
}
