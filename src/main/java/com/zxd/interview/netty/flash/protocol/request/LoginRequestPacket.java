package com.zxd.interview.netty.flash.protocol.request;

import com.zxd.interview.netty.flash.protocol.Packet;
import lombok.Data;

import static com.zxd.interview.netty.flash.protocol.command.Command.LOGIN_REQUEST;

@Data
public class LoginRequestPacket extends Packet {
    private String userName;

    private String password;

    @Override
    public Byte getCommand() {

        return LOGIN_REQUEST;
    }
}
