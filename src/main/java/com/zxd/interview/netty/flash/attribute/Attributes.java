package com.zxd.interview.netty.flash.attribute;

import com.zxd.interview.netty.flash.session.Session;
import io.netty.util.AttributeKey;

public interface Attributes {
    AttributeKey<Session> SESSION = AttributeKey.newInstance("session");
}
