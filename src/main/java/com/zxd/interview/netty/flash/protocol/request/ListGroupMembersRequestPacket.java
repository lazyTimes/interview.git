package com.zxd.interview.netty.flash.protocol.request;

import com.zxd.interview.netty.flash.protocol.Packet;
import lombok.Data;

import static com.zxd.interview.netty.flash.protocol.command.Command.LIST_GROUP_MEMBERS_REQUEST;

@Data
public class ListGroupMembersRequestPacket extends Packet {

    private String groupId;

    @Override
    public Byte getCommand() {

        return LIST_GROUP_MEMBERS_REQUEST;
    }
}
