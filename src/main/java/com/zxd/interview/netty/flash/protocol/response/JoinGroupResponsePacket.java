package com.zxd.interview.netty.flash.protocol.response;

import com.zxd.interview.netty.flash.protocol.Packet;
import lombok.Data;
import static com.zxd.interview.netty.flash.protocol.command.Command.JOIN_GROUP_RESPONSE;

@Data
public class JoinGroupResponsePacket extends Packet {
    private String groupId;

    private boolean success;

    private String reason;

    @Override
    public Byte getCommand() {

        return JOIN_GROUP_RESPONSE;
    }
}
