package com.zxd.interview.netty.oldio;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 传统IO编程
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.netty.oldio
 * @Description : 传统IO编程
 * @Create on : 2023/3/6 13:06
 **/
public class IOServer {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8000);

        // 接受连接
        new Thread(() -> {
            while (true) {
                // 1. 阻塞获取连接
                try {
                    Socket socket = serverSocket.accept();

                    // 2. 为每一个新连接使用一个新线程
                    new Thread(() -> {
                        try {
                            int len;
                            byte[] data = new byte[1024];
                            InputStream inputStream = socket.getInputStream();
                            // 字节流读取数据
                            while ((-1 != (len = inputStream.read()))) {
                                System.err.println(new String(data, 0, len));
                            }
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }).start();
    }
}
