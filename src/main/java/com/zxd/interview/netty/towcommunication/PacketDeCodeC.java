package com.zxd.interview.netty.towcommunication;

import io.netty.buffer.ByteBuf;


/**
 * 解码操作
 */
public class PacketDeCodeC {

    /**
     * 解码操作
     * @param byteBuf
     * @return
     */
    public Packet decode(ByteBuf byteBuf){
        // 跳过魔数
        byteBuf.skipBytes(4);

        // 版本号跳过
        byteBuf.skipBytes(1);

        // 序列化算法表示
        byte serializerAlorithm = byteBuf.readByte();

        // 指令
        byte command = byteBuf.readByte();

        // 数据长度
        int readInt = byteBuf.readInt();

        byte[] bytes = new byte[readInt];
        ByteBuf read = byteBuf.readBytes(bytes);

        // 获取请求类型
//        Class<? extends Packet> requestType = getRequestType(command);
//        Serializer serializer = (Serializer) getSerializer(serializerAlorithm);

        // 判断获取的请求类型和序列化是否有误


        return null;


    }
}
