package com.zxd.interview.netty.towcommunication;

import com.alibaba.fastjson.JSON;

/**
 * JSON序列化
 */
public class JSONSerializer implements Serializer{


    @Override
    public byte getSerializerAlgorithm() {
        return SerializerAlgorithm.JSON;
    }

    @Override
    public byte[] serialize(Object obj) {
        return JSON.toJSONBytes(obj);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}
