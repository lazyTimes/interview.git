package com.zxd.interview.netty.towcommunication;

/**
 * 序列化算法
 */
public interface SerializerAlgorithm {

    /**
     * JSON序列化 标识
     */
    byte JSON = 1;
}
