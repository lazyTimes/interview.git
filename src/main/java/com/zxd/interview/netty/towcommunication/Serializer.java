package com.zxd.interview.netty.towcommunication;

/**
 * 序列化接口
 */
public interface Serializer {

    // 增加序列化算法类型
    byte JSON_SERIALIZER = 1;

    Serializer SERIALIZER_DEFAULT = new JSONSerializer();

    /**
     * 序列化算法
     * @return
     */
    byte getSerializerAlgorithm();

    /**
     * 对象转为二进制数据
     * @param obj
     * @return
     */
    byte[] serialize(Object obj);

    /**
     * 二进制数据转Java对象
     * @param clazz
     * @param bytes
     * @param <T>
     * @return
     */
    <T> T deserialize(Class<T> clazz, byte[] bytes);
}
