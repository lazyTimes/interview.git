package com.zxd.interview.netty.towcommunication;

import lombok.Data;

/**
 * 登陆请求的数据包
 */
@Data
public class LoginRequestPacket extends Packet implements Command{

    private Integer userId;

    private String userName;

    private String password;


    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}
