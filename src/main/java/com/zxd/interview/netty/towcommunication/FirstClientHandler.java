package com.zxd.interview.netty.towcommunication;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * 客户端自定义处理逻辑
 */
public class FirstClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println(new Date() + " 客户端开始写数据");

        // 1. 利用二进制抽象 ByteBuf  获取数据
        ByteBuf byteBuf = getByteBuf(ctx);

        // 2. 写入数据回传给服务端
        ctx.channel().writeAndFlush(byteBuf);

    }

    private ByteBuf getByteBuf(ChannelHandlerContext ctx) {
        // 1. 获取二进制抽象 Bytebuf
        ByteBuf buffer = ctx.alloc().buffer();

        byte[] bytes = "相关的测试内容 ".getBytes(StandardCharsets.UTF_8);

        // 2. 准备数据 指定字符集
        buffer.writeBytes(bytes);

        // 3. 填充数据到 ByteBuf
        return buffer;
    }


}
