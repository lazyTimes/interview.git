package com.zxd.interview.netty.towcommunication;

import lombok.Data;

/**
 * 通信用的对象
 */
@Data
public abstract class Packet {

    /**
     * 协议版本
     */
    private Byte version = 1;

    /**
     * 指令
     * @return
     */
    public abstract Byte getCommand();
}
