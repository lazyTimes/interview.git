package com.zxd.interview.netty.bytebuf;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import static cn.hutool.core.lang.Console.print;

/**
 * byteBuf 的API测试
 */
public class ByteBufTest {

    public static void main(String[] args) {
        // 9 代表初始容量， 100代表最大容量
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(9, 100);

        // writableBytes()、 isWritable() 与 maxWritableBytes()
        print("allocate ByteBuf(9,100) writableBytes => {} \n", buffer.writableBytes());
        print("allocate ByteBuf(9,100) isWritable => {} \n", buffer.isWritable());
        print("allocate ByteBuf(9,100) maxWritableBytes => {} \n", buffer.maxWritableBytes());
//        allocate ByteBuf(9,100) writableBytes => 9
//        allocate ByteBuf(9,100) isWritable => true
//        allocate ByteBuf(9,100) maxWritableBytes => 100


        print("allocate ByteBuf(9,100) => {} \n", buffer);
        //allocate ByteBuf(9,100) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 0, cap: 9/100)

        print("allocate ByteBuf(9,100) capacity => {} \n", buffer.capacity());
        // allocate ByteBuf(9,100) capacity => 9

        // readerIndex() 返回当前读指针
        print("allocate ByteBuf(9,100) readerIndex => {} \n", buffer.readerIndex());
        // allocate ByteBuf(9,100) readerIndex => 0

        // 尝试手动重定向渎指针位置
//        print("allocate ByteBuf(9,100) readerIndex(int) => {} \n", buffer.readerIndex(2));
        // readerIndex: 2, writerIndex: 0 (expected: 0 <= readerIndex <= writerIndex <= capacity(9))


        print("allocate ByteBuf(9,100) maxCapacity => {} \n", buffer.maxCapacity());
        // allocate ByteBuf(9,100) maxCapacity => 100

        // readableBytes() 与 isReadable()
        print("allocate ByteBuf(9,100) isReadable => {} \n", buffer.isReadable());
        print("allocate ByteBuf(9,100) readableBytes => {} \n", buffer.readableBytes());
//        allocate ByteBuf(9,100) isReadable => false
//        allocate ByteBuf(9,100) readableBytes => 0

        // write 方法改变写指针
        buffer.writeBytes(new byte[]{1, 2, 3, 4});
        print("改变写指针 writeBytes(new byte[]{1,2,3,4}) => {} \n", buffer);
        // 改变写指针 writeBytes(new byte[]{1,2,3,4}) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 4, cap: 9/100)
        buffer.writeByte(5);
        print("改变写指针 buffer.writeByte(5) => {} \n", buffer);
        // 改变写指针 buffer.writeByte(5) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 5, cap: 9/100)

        // writeIndex() 与 writeIndex(int)
        print("allocate ByteBuf(9,100) writerIndex => {} \n", buffer.writerIndex());
        print("allocate ByteBuf(9,100) writerIndex(int) => {} \n", buffer.writerIndex(2));
//        allocate ByteBuf(9,100) writerIndex => 4
//        allocate ByteBuf(9,100) writerIndex(int) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 2, cap: 9/100)

        // 重定向读指针
        print("allocate ByteBuf(9,100) readerIndex(int) => {} \n", buffer.readerIndex(2));
        print("重定向读指针 之后 (new byte[]{1,2,3,4}) => {} \n", buffer);
        // allocate ByteBuf(9,100) readerIndex(int) => PooledUnsafeDirectByteBuf(ridx: 2, widx: 4, cap: 9/100)
        // 重定向读指针 之后 (new byte[]{1,2,3,4}) => PooledUnsafeDirectByteBuf(ridx: 2, widx: 4, cap: 9/100)

        // 写入数据之后，重新执行readableBytes() 与 isReadable()
        print("allocate ByteBuf(9,100) isReadable => {} \n", buffer.isReadable());
        print("allocate ByteBuf(9,100) readableBytes => {} \n", buffer.readableBytes());


        // write 改变写指针，如果没有到达 capacity 依然可以写入，写入 int 之后写指针增加4
        buffer.writeInt(12);
        print("buffer.writeInt(12) => {}\n", buffer);
        // buffer.writeInt(12) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 8, cap: 9/100)

        // 继续改变写指针，当前写入等于  initialCapacity 这个初始值之后将不能继续写入
        buffer.writeBytes(new byte[]{5});
        print("writeBytes(new byte[]{5}) => {}\n", buffer);
        // writeBytes(new byte[]{5}) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 9, cap: 9/100)

        // 继续写入指针，此时发现 已经超过 initialCapacity 的值，此时会进行扩容
        buffer.writeBytes(new byte[]{6});
        print("writeBytes(new byte[]{6}) => {}\n", buffer);
        // writeBytes(new byte[]{6}) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 10, cap: 16/100)

        // get 方法调用之后不改变读指针
        print("getByte(3) return => {}\n", buffer.getByte(3));
        print("getShort(3) return => {}\n", buffer.getShort(3));
        print("getInt(3) return => {}\n", buffer.getInt(3));
        print("getChar(3) return => {}\n", buffer.getChar(3));
        /*
       getByte(3) return => 4
        getShort(3) return => 1024
        getInt(3) return => 67108864
        getChar(3) return => Ѐ
        * */

        // set 方法不改变读写指针
        buffer.setByte(buffer.readableBytes() + 1, 0);
        print("setByte(buffer.readableBytes() + 1, 0) => {}\n", buffer);
        // setByte(buffer.readableBytes() + 1, 0) => PooledUnsafeDirectByteBuf(ridx: 0, widx: 10, cap: 16/100)

        //只有read方法改变指针
        byte[] bytes = new byte[buffer.readableBytes()];
        buffer.readBytes(bytes);

        print("bytes 内容 => {}\n", bytes);
        // bytes 内容 =>     buffer.readBytes(bytes) => PooledUnsafeDirectByteBuf(ridx: 8, widx: 8, cap: 9/100)

        print("buffer.readBytes(bytes) => {}\n", buffer);
        // buffer.readBytes(bytes) => PooledUnsafeDirectByteBuf(ridx: 10, widx: 10, cap: 16/100)
        print("buffer.readBytes(readBuffer); => {}\n", buffer);

        ByteBuf readBuffer = ByteBufAllocator.DEFAULT.buffer(6, 6);
        // 原始writeIndex要有足够空间可读
//        buffer.writeBytes(new byte[]{5,1,1,1,1,1,1,1});

//        buffer.writeBytes(new byte[]{5});
        //  readerIndex(10) + length(6) exceeds writerIndex(11): PooledUnsafeDirectByteBuf(ridx: 10, widx: 11, cap: 16/100)
//        buffer.readBytes(readBuffer);

        print(readBuffer.readableBytes());

//        buffer.readBytes(readBuffer);
        // readerIndex(10) + length(9) exceeds writerIndex(10): PooledUnsafeDirectByteBuf(ridx: 10, widx: 10, cap: 16/100)

    }


}
