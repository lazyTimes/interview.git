package com.zxd.interview.export.dto;

/**
 * 生成报告对应的 word 数据传输dto
 * 主要用于生成报告的
 *
 * @author zhaoxudong
 * @title: WordReportDto
 * @description: 生成报告对应的数据传输dto
 * @date 2020/1/9 18:46
 */
public class WordReportDTO {

    /**
     * 报表名称
     */
    private String wordName;

    /**
     * 幼儿园基本名称
     */
    private String schoolName;

    /**
     * 第一部分
     * 幼儿园基本情况介绍
     */
    private String baseSituation;

    /**
     * 第二部分：督导情况
     * 亮点推荐
     */
    private String learningEnvRec;

    /**
     * 第二部分：督导情况
     * 问题诊断
     */
    private String learningEnvPro;

    /**
     * 二、一日生活
     * 亮点推荐
     */
    private String dayLifeRec;

    /**
     * 二、一日生活
     * 问题诊断
     */
    private String dayLifePro;

    /**
     * 三、学习活动
     * 亮点推荐
     */
    private String learningActivityRec;

    /**
     * 三、学习活动
     * 问题诊断
     */
    private String learningActivityPro;
    /**
     * 第三部分：发展建议
     */
    private String devRecommend;

    /**
     * 第四部分：反馈情况
     */
    private String feedbackSituation;

    /**
     * 存储六张excel图表
     */
    private String base64_1;
    private String base64_2;
    private String base64_3;
    private String base64_4;
    private String base64_5;
    private String base64_6;


    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getBaseSituation() {
        return baseSituation;
    }

    public void setBaseSituation(String baseSituation) {
        this.baseSituation = baseSituation;
    }

    public String getLearningEnvRec() {
        return learningEnvRec;
    }

    public void setLearningEnvRec(String learningEnvRec) {
        this.learningEnvRec = learningEnvRec;
    }

    public String getLearningEnvPro() {
        return learningEnvPro;
    }

    public void setLearningEnvPro(String learningEnvPro) {
        this.learningEnvPro = learningEnvPro;
    }

    public String getDayLifeRec() {
        return dayLifeRec;
    }

    public void setDayLifeRec(String dayLifeRec) {
        this.dayLifeRec = dayLifeRec;
    }

    public String getDayLifePro() {
        return dayLifePro;
    }

    public void setDayLifePro(String dayLifePro) {
        this.dayLifePro = dayLifePro;
    }

    public String getLearningActivityRec() {
        return learningActivityRec;
    }

    public void setLearningActivityRec(String learningActivityRec) {
        this.learningActivityRec = learningActivityRec;
    }

    public String getLearningActivityPro() {
        return learningActivityPro;
    }

    public void setLearningActivityPro(String learningActivityPro) {
        this.learningActivityPro = learningActivityPro;
    }

    public String getDevRecommend() {
        return devRecommend;
    }

    public void setDevRecommend(String devRecommend) {
        this.devRecommend = devRecommend;
    }

    public String getFeedbackSituation() {
        return feedbackSituation;
    }

    public void setFeedbackSituation(String feedbackSituation) {
        this.feedbackSituation = feedbackSituation;
    }

    public String getBase64_1() {
        return base64_1;
    }

    public void setBase64_1(String base64_1) {
        this.base64_1 = base64_1;
    }

    public String getBase64_2() {
        return base64_2;
    }

    public void setBase64_2(String base64_2) {
        this.base64_2 = base64_2;
    }

    public String getBase64_3() {
        return base64_3;
    }

    public void setBase64_3(String base64_3) {
        this.base64_3 = base64_3;
    }

    public String getBase64_4() {
        return base64_4;
    }

    public void setBase64_4(String base64_4) {
        this.base64_4 = base64_4;
    }

    public String getBase64_5() {
        return base64_5;
    }

    public void setBase64_5(String base64_5) {
        this.base64_5 = base64_5;
    }

    public String getBase64_6() {
        return base64_6;
    }

    public void setBase64_6(String base64_6) {
        this.base64_6 = base64_6;
    }

    public String getWordName() {
        return wordName;
    }

    public void setWordName(String wordName) {
        this.wordName = wordName;
    }


    @Override
    public String toString() {
        return "WordReportDTO{" +
                "wordName='" + wordName + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", baseSituation='" + baseSituation + '\'' +
                ", learningEnvRec='" + learningEnvRec + '\'' +
                ", learningEnvPro='" + learningEnvPro + '\'' +
                ", dayLifeRec='" + dayLifeRec + '\'' +
                ", dayLifePro='" + dayLifePro + '\'' +
                ", learningActivityRec='" + learningActivityRec + '\'' +
                ", learningActivityPro='" + learningActivityPro + '\'' +
                ", devRecommend='" + devRecommend + '\'' +
                ", feedbackSituation='" + feedbackSituation + '\'' +
                ", base64_1='" + base64_1 + '\'' +
                ", base64_2='" + base64_2 + '\'' +
                ", base64_3='" + base64_3 + '\'' +
                ", base64_4='" + base64_4 + '\'' +
                ", base64_5='" + base64_5 + '\'' +
                ", base64_6='" + base64_6 + '\'' +
                '}';
    }
}
