package com.zxd.interview.export.dto;

import java.time.LocalDateTime;

/**
 * 测试使用的dto，用于封装导出word的对象
 *
 * @author zhaoxudong
 * @version 1.0
 * @date 2020/11/7 23:37
 */
public class TestReportDTO {

    /**
     * 测试
     */
    private String test0;

    /**
     * 测试
     */
    private String test1;

    /**
     * 测试
     */
    private String test2;

    /**
     * 测试
     */
    private String test4;

    /**
     * 测试
     */
    private String test5;

    /**
     * 测试
     */
    private String test6;

    private int testInt7;


    /**
     * 报告名称
     */
    private String wordName;


    /**
     * 测试时间类型
     */
    private LocalDateTime localDateTime;

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public int getTestInt7() {
        return testInt7;
    }

    public void setTestInt7(int testInt7) {
        this.testInt7 = testInt7;
    }


    public String getTest0() {
        return test0;
    }

    public void setTest0(String test0) {
        this.test0 = test0;
    }

    public String getTest1() {
        return test1;
    }

    public void setTest1(String test1) {
        this.test1 = test1;
    }

    public String getTest2() {
        return test2;
    }

    public void setTest2(String test2) {
        this.test2 = test2;
    }

    public String getTest4() {
        return test4;
    }

    public void setTest4(String test4) {
        this.test4 = test4;
    }

    public String getTest5() {
        return test5;
    }

    public void setTest5(String test5) {
        this.test5 = test5;
    }

    public String getTest6() {
        return test6;
    }

    public void setTest6(String test6) {
        this.test6 = test6;
    }

    public String getWordName() {
        return wordName;
    }

    public void setWordName(String wordName) {
        this.wordName = wordName;
    }


    @Override
    public String toString() {
        return "TestReportDTO{" +
                "test0='" + test0 + '\'' +
                ", test1='" + test1 + '\'' +
                ", test2='" + test2 + '\'' +
                ", test4='" + test4 + '\'' +
                ", test5='" + test5 + '\'' +
                ", test6='" + test6 + '\'' +
                ", testInt7=" + testInt7 +
                ", wordName='" + wordName + '\'' +
                ", localDateTime=" + localDateTime +
                '}';
    }
}
