package com.zxd.interview.desginpattern.single;

/**
 *
 * 单例模式 - 饿汉式
 * @author zhaoxudong
 * @version 1.0
 * @date 2020/10/27 21:45
 */
public class SingleHungry {

    private static final SingleHungry instance = new SingleHungry();

    public static SingleHungry getInstance(){
        return instance;
    }

    public static void main(String[] args) {
        SingleHungry instance = SingleHungry.getInstance();
        System.err.println(instance);
    }
}
