package com.zxd.interview.desginpattern.single;

import com.zxd.interview.util.ExecuteUtil;

/**
 * 单例模式 - 静态内部类实现
 *
 * @Author zhaoxudong
 * @Date 2020/10/28 13:35
 **/
public class SingleStaticInner {

    /**
     * 使用内部类来进行后续的构造
     */
    public static class Instatnce {
        private static Instatnce instatnce = new Instatnce();

        public static Instatnce getInstatnce() {
            try {
                // 模拟在创建对象之前做一些准备工作
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return instatnce;
        }

    }

    public static void main(String[] args) throws InterruptedException {
        ExecuteUtil.startTaskAllInOnce(250, new ThreadTest());
    }
}

/**
 * 测试多线程获取对象
 */
class ThreadTest extends Thread {

    @Override
    public void run() {
        System.err.println(SingleStaticInner.Instatnce.getInstatnce());

    }
}
