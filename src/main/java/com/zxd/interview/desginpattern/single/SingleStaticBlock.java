package com.zxd.interview.desginpattern.single;

/**
 * 静态代码块的形式，实现单例
 *
 * @Author zhaoxudong
 * @Date 2020/10/28 13:28
 **/
public class SingleStaticBlock {

    private static final SingleStaticBlock staticBlock;

    static {
        staticBlock = new SingleStaticBlock();
    }

    public static SingleStaticBlock getInstance() {
        return staticBlock;
    }
}
