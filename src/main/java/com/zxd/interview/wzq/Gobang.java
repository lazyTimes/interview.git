package com.zxd.interview.wzq;

public class Gobang {

    private static final int ROWS = 9;
    private static final int COLS = 10;

    public interface WZInterface {
        void showBoard();

        void updateBoard(int number, String color);
    }

    public static class WZInterfaceImpl implements WZInterface {

        // 初始化棋盘（这里只是示例，您可以根据需要初始化棋盘）
        public WZInterfaceImpl() {
            // 初始化棋盘，假设没有棋子时，显示位置编号
            for (int i = 0; i < ROWS; i++) {
                for (int j = 0; j < COLS; j++) {
                    board[i][j] = String.valueOf((i * COLS + j + 1)); // 从1开始编号
                    hasPiece[i][j] = false;
                }
            }
        }


        private String[][] board = new String[ROWS][COLS]; // 棋盘数组，用于存储每个位置的字符串
        private boolean[][] hasPiece = new boolean[ROWS][COLS]; // 跟踪哪些位置有棋子

        @Override
        public void showBoard() {
            // 打印棋盘
            for (int i = 0; i < ROWS; i++) {
                for (int j = 0; j < COLS; j++) {
                    // 根据是否有棋子来决定是否打印数字
                    if (!hasPiece[i][j]) {
                        // 没有棋子，打印数字和位置
                        System.out.print(board[i][j] + (j < COLS - 1 ? "\t" : "\n"));
                    } else {
                        // 有棋子，打印棋子
                        System.out.print(board[i][j] + (j < COLS - 1 ? "\t" : "\n"));
                    }
                }
            }
        }

        @Override
        public void updateBoard(int position, String piece) {
            // 假设position是从1开始的棋盘位置（1到90）
            int row = (position - 1) / COLS; // 计算行
            int col = (position - 1) % COLS; // 计算列
            if (row >= 0 && row < ROWS && col >= 0 && col < COLS) {
                // 更新棋子和跟踪数组
                board[row][col] = piece;
                hasPiece[row][col] = true;
            } else {
                System.out.println("Invalid position: " + position);
            }
        }
    }



    public static void main(String[] args) {
        WZInterface game = new WZInterfaceImpl();

        game.updateBoard(11, "V");
        game.updateBoard(15, "H");

        game.showBoard(); // 显示更新后的棋盘
    }
}