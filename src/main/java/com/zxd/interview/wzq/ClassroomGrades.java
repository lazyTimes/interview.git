package com.zxd.interview.wzq;

import java.util.ArrayList;
import java.util.Scanner;


public class ClassroomGrades {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> gradesList = new ArrayList<>();

        System.out.println("请输入同学的成绩（优秀、良好、中等、及格、不及格），输入'exit'结束：");

        // 从键盘读取输入
        while (true) {
            String input = scanner.nextLine(); // 读取一行输入
            if ("exit".equalsIgnoreCase(input)) { // 当输入为"exit"时，结束循环
                break;
            }
            gradesList.add(input); // 将输入的成绩添加到ArrayList中
        }

        // 打印所有输入的成绩
        System.out.println("\n所有同学的成绩为：");
        for (String grade : gradesList) {
            System.out.println(grade);
        }

        scanner.close(); // 关闭Scanner
    }
}
