package com.zxd.interview.wzq;

public class Gobang2 {


    private static final int SIZE_ROWS = 9;
    private static final int SIZE_COLS = 10;

    public interface BoardDisplay {
        void display();
        void update(int position, String piece);
    }

    public static class GobangBoard implements BoardDisplay {

        private final String[][] board = new String[SIZE_ROWS][SIZE_COLS];
        private final boolean[][] occupied = new boolean[SIZE_ROWS][SIZE_COLS];

        public GobangBoard() {
            initializeBoard();
        }

        private void initializeBoard() {
            for (int i = 0; i < SIZE_ROWS; i++) {
                for (int j = 0; j < SIZE_COLS; j++) {
                    board[i][j] = String.valueOf(i * SIZE_COLS + j + 1);
                    occupied[i][j] = false;
                }
            }
        }

        @Override
        public void display() {
            for (String[] row : board) {
                for (String cell : row) {
                    System.out.print(hasPiece(cell) ? cell : cell + "\t");
                }
                System.out.println(); // 换行
            }
        }

        @Override
        public void update(int position, String piece) {
            int row = (position - 1) / SIZE_COLS;
            int col = (position - 1) % SIZE_COLS;
            if (isValidPosition(row, col)) {
                board[row][col] = piece;
                occupied[row][col] = true;
            } else {
                System.out.println("Invalid position: " + position);
            }
        }

        private boolean hasPiece(String cell) {
            return cell.matches("[VH]"); // 假定"V"和"H"代表两种棋子
        }

        private boolean isValidPosition(int row, int col) {
            return row >= 0 && row < SIZE_ROWS && col >= 0 && col < SIZE_COLS;
        }
    }

    public static void main(String[] args) {
        BoardDisplay game = new GobangBoard();

        game.update(11, "V");
        game.update(15, "H");

        game.display(); // 显示更新后的棋盘
    }
}
