package com.zxd.interview.wzq;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissorsGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int playerScore = 0;
        int computerScore = 0;

        // 假设用户已经输入了游戏的局数  
        int totalRounds = 5; // 示例：一共玩5局  

        System.out.println("欢迎来到石头剪刀布游戏！一共玩" + totalRounds + "局。");

        for (int i = 1; i <= totalRounds; i++) {
            System.out.print("请输入您的选择（0=石头，1=剪刀，2=布）：");
            int playerChoice = scanner.nextInt();
            int computerChoice = random.nextInt(3);

            System.out.println("电脑选择了：" + getChoiceName(computerChoice));

            // 判断胜负  
            String result = judgeResult(playerChoice, computerChoice);
            if (result.equals("Win")) {
                playerScore += 2;
                System.out.println("你赢了这一局！");
            } else if (result.equals("Lose")) {
                computerScore += 2;
                System.out.println("你输了这一局！");
            } else if (result.equals("Draw")) {
                playerScore += 1;
                computerScore += 1;
                System.out.println("平局！");
            }

            System.out.println("当前分数：你 " + playerScore + " 分 vs 电脑 " + computerScore + " 分\n");
        }

        System.out.println("游戏结束！最终分数：你 " + playerScore + " 分 vs 电脑 " + computerScore + " 分");
        if (playerScore > computerScore) {
            System.out.println("恭喜你赢得了比赛！");
        } else if (playerScore < computerScore) {
            System.out.println("很遗憾，你输掉了比赛。");
        } else {
            System.out.println("比赛平局！");
        }

        scanner.close();
    }

    // 辅助方法，将数字转换为选择名称  
    private static String getChoiceName(int choice) {
        switch (choice) {
            case 0:
                return "石头";
            case 1:
                return "剪刀";
            case 2:
                return "布";
            default:
                return "未知";
        }
    }

    // 辅助方法，判断胜负  
    private static String judgeResult(int player, int computer) {
        if ((player == 0 && computer == 2) || (player == 1 && computer == 0) || (player == 2 && computer == 1)) {
            return "Win"; // 玩家赢  
        } else if (player == computer) {
            return "Draw"; // 平局  
        } else {
            return "Lose"; // 玩家输  
        }
    }
}