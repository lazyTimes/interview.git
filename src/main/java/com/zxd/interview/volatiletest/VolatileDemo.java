package com.zxd.interview.volatiletest;

import java.util.concurrent.TimeUnit;

/**
 * volatile 解决多线程修改值问题
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.volatiletest
 * @Description : volatile 解决多线程修改值问题
 * @Create on : 2023/6/28 10:45
 **/
public class VolatileDemo {

    static volatile int flag = 0;

    public static void main(String[] args) {
        new Thread() {

            public void run() {
                int localFlag = flag;
                while(true) {
                    if(localFlag != flag) {
                        System.out.println("读取到了修改后的标志位：" + flag);
                        localFlag = flag;
                    }
                }
            };

        }.start();

        new Thread() {

            public void run() {
                int localFlag = flag;
                while(true) {
                    System.out.println("标志位被修改为了：" + ++localFlag);
                    flag = localFlag;
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

        }.start();
    }

}
