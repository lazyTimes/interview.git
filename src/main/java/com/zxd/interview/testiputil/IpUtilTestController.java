package com.zxd.interview.testiputil;

import com.zxd.interview.util.IpUtils;
import com.zxd.interview.util.holidays.WorkDateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.testiputil
 * @Description : ip工具类检测
 * @Create on : 2021/5/18 09:46
 **/
@Controller
@RequestMapping("/test")
public class IpUtilTestController {

    @RequestMapping("/test")
    @ResponseBody
    public Object test(HttpServletRequest httpServletRequest) {
        boolean workDate = WorkDateUtils.isWorkDate(new Date());
        System.out.println(workDate);
        String ipAddr = IpUtils.getIpAddr1(httpServletRequest);
        String ipAddr2 = IpUtils.getIpAddr2(httpServletRequest);
        // 0:0:0:0:0:0:0:1 由于是本地，都为本地的ip地址
        System.out.println(ipAddr2);
        System.out.println(ipAddr);
        IpUtils.IpVo ipVo = null;
        try {
            ipVo = IpUtils.getIpVo("183.17.229.59");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("ipVo => " + ipVo);
        return ipAddr;
    }
}
