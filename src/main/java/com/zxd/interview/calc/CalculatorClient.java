package com.zxd.interview.calc;

import java.io.*;
import java.net.*;
import java.util.*;

public class CalculatorClient {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) {
        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
             ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
             Scanner scanner = new Scanner(System.in)) {

            while (true) {
                System.out.println("Enter calculation (format: num1 operator num2 or num1 sqrt) or 'history' to view history:");
                String request = scanner.nextLine();

                output.writeUTF(request);
                output.flush();

                if (request.equals("history")) {
                    List<String> history = (List<String>) input.readObject();
                    System.out.println("Calculation History:");
                    for (String record : history) {
                        System.out.println(record);
                    }
                } else {
                    double result = input.readDouble();
                    System.out.println("Result: " + result);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
