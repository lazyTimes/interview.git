package com.zxd.interview.util.dingrobot;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util.dingrobot
 * @Description : 支持发送请求的接口
 * @Create on : 2021/4/15 18:09
 **/
public interface HttpSendAble {

    /**
     * 根据请求Config 进行请求发送
     *
     * @param httpConfig
     * @return
     */
    String sendPostJson(HttpConfig httpConfig);

}
