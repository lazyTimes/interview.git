package com.zxd.interview.util.dingrobot;

/**
 * @author zxd
 * @version v1.0.0
 * @Package : com.dcc.common.field
 * @Description : 钉钉机器人请求实体类
 * @Create on : 2021/2/5 15:40
 **/
public class DingRobotRequest implements DingRequestAble{

    /**
     * 请求URL
     */
    private String url;

    /**
     * token
     */
    private String accessToken;

    /**
     * 秘钥
     */
    private String secret;

    /**
     * 请求msg
     */
    private DingRobotRequestBody msg;

    private DingRobotRequest() {

    }

    private DingRobotRequest(Builder builder) {
        this.url = builder.url;
        this.accessToken = builder.accessToken;
        this.secret = builder.secret;
        this.msg = builder.msg;
    }

    public static class Builder {

        private String url;
        private String accessToken;
        private String secret;
        private DingRobotRequestBody msg;

        public DingRobotRequest.Builder url(String url) {
            this.url = url;
            return this;
        }

        public DingRobotRequest.Builder accessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public DingRobotRequest.Builder secret(String secret) {
            this.secret = secret;
            return this;
        }

        public DingRobotRequest.Builder msg(DingRobotRequestBody msg) {
            this.msg = msg;
            return this;
        }


        public DingRobotRequest build() {
            return new DingRobotRequest(this);
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public DingRobotRequestBody getMsg() {
        return msg;
    }

    public void setMsg(DingRobotRequestBody msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "DingRobotRequest{" +
                "url='" + url + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", secret='" + secret + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
