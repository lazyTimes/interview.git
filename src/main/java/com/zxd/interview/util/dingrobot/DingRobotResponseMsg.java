package com.zxd.interview.util.dingrobot;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.dcc.common.field
 * @Description : 钉钉机器人返回对象
 * @Create on : 2021/2/5 18:26
 **/
public class DingRobotResponseMsg {

    /**
     * 错误码
     */
    private String errcode;

    /**
     * 错误信息
     */
    private String errmsg;
    /**
     * 更多链接
     */
    private String more;

    public DingRobotResponseMsg(String errcode, String errmsg, String more) {
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.more = more;
    }

    public DingRobotResponseMsg() {

    }

    public String getErrcode() {
        return errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public String getMore() {
        return more;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
