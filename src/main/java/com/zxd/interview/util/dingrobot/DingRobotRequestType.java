package com.zxd.interview.util.dingrobot;

/**
 * @author zxd
 * @version v1.0.0
 * @Package : com.zxd.interview.util.dingrobot
 * @Description : 允许发送钉钉请求的接口
 * @Create on : 2021/2/7 11:45
 **/
public interface DingRobotRequestType {

    /**
     * 所有的子类需要集成该接口
     *
     * @return
     */
    void setMsgType(String msgType);

    /**
     * 普通文本类型
     *
     * @param text
     */
    void setText(DingRobotRequestBody.Text text);

    /**
     * link类型
     *
     * @param link
     */
    void setLink(DingRobotRequestBody.Link link);

    /**
     * markdown 类型
     *
     * @param markDown
     */
    void setMarkDown(DingRobotRequestBody.MarkDown markDown);

    /**
     * 整体跳转ActionCard类型
     *
     * @param actionCard
     */
    void setActionCard(DingRobotRequestBody.ActionCard actionCard);

    /**
     * feedcard 类型
     *
     * @param feedCard
     */
    void setFeedCard(DingRobotRequestBody.FeedCard feedCard);

}
