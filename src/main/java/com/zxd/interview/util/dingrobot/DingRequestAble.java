package com.zxd.interview.util.dingrobot;

/**
 * 钉钉机器人请求公用层
 *
 * @author zxd
 * @version 1.0
 * @date 2021/4/15 21:57
 */
public interface DingRequestAble {

    /**
     * 获取请求url
     * @return
     */
    String getUrl();

    /**
     * 获取请求token
     * @return
     */
    String getAccessToken();

    /**
     * 获取秘钥
     * @return
     */
    String getSecret();

    /**
     * 获取请求类型
     * @return
     */
    DingRobotRequestBody getMsg();
}
