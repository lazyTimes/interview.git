package com.zxd.interview.util.dingrobot;

import java.util.List;

/**
 * @author zxd
 * @version v1.0.0
 * @Package : com.dcc.common.field
 * @Description : 钉钉机器人请求实体对象
 * 请求案例：{"msgtype": "text","text": {"content": "自定义具体内容"}}
 * @link {https://developers.dingtalk.com/document/app/custom-robot-access}
 * @Create on : 2021/2/5 11:55
 **/
public class DingRobotRequestBody implements DingRobotRequestType {

    /**
     * 艾特对象内容
     */
    private At at;

    /**
     * 类型
     */
    private String msgtype;

    /**
     * 文本类型
     */
    private Text text;

    /**
     * 连接类型
     */
    private Link link;

    /**
     * markdown 类型
     */
    private MarkDown markdown;

    /**
     * 整体跳转ActionCard类型
     */
    private ActionCard actionCard;

    /**
     * FeedCard类型
     */
    private FeedCard feedCard;


    /**
     * FeedCard类型
     * <p>
     * msgtype	    String	是	此消息类型为固定feedCard。
     * title	    String	是	单条信息文本。
     * messageURL	String	是	点击单条信息到跳转链接。
     * picURL	    String	是	单条信息后面图片的URL。
     */
    public static class FeedCard {

        private List<FeedItem> links;

        /**
         * 代表 FeedCard类型 子类型
         */
        public static class FeedItem {

            private String title;

            private String messageURL;

            private String picURL;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getMessageURL() {
                return messageURL;
            }

            public void setMessageURL(String messageURL) {
                this.messageURL = messageURL;
            }

            public String getPicURL() {
                return picURL;
            }

            public void setPicURL(String picURL) {
                this.picURL = picURL;
            }
        }

        public List<FeedItem> getLinks() {
            return links;
        }

        public void setLinks(List<FeedItem> links) {
            this.links = links;
        }
    }


    /**
     * 整体跳转ActionCard类型
     * msgtype	        String	是	消息类型，此时固定为：actionCard。
     * title	        String	是	首屏会话透出的展示内容。
     * text	            String	是	markdown格式的消息。
     * singleTitle	    String	是	单个按钮的标题。
     * <p>
     * 注意 设置此项和singleURL后，btns无效。
     * <p>
     * singleURL	    String	是	点击singleTitle按钮触发的URL。
     * btnOrientation	String	否	0：按钮竖直排列1：按钮横向排列
     */
    public static class ActionCard {

        private String title;

        private String text;

        private String btnOrientation;

        private String singleTitle;

        private String singleURL;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getBtnOrientation() {
            return btnOrientation;
        }

        public void setBtnOrientation(String btnOrientation) {
            this.btnOrientation = btnOrientation;
        }

        public String getSingleTitle() {
            return singleTitle;
        }

        public void setSingleTitle(String singleTitle) {
            this.singleTitle = singleTitle;
        }

        public String getSingleURL() {
            return singleURL;
        }

        public void setSingleURL(String singleURL) {
            this.singleURL = singleURL;
        }
    }

    /**
     * 艾特类
     */
    public static class At {

        /**
         * 是否通知全部人
         */
        private boolean atAll;

        /**
         * 需要@的手机号数组
         */
        private List<String> atMobiles;

        public boolean isAtAll() {
            return atAll;
        }

        public void setAtAll(boolean atAll) {
            this.atAll = atAll;
        }

        public List<String> getAtMobiles() {
            return atMobiles;
        }

        public void setAtMobiles(List<String> atMobiles) {
            this.atMobiles = atMobiles;
        }
    }

    /**
     * markdown 类型， 可以发送markdown 的语法格式
     * msgtype	    String	是	消息类型，此时固定为：markdown。
     * title	    String	是	首屏会话透出的展示内容。
     * text	        String	是	markdown格式的消息。
     * atMobiles	Array	否	被@人的手机号。 注意 在text内容里要有@人的手机号。
     * isAtAll	Boolean	否	是否@所有人。
     */
    public static class MarkDown {

        private String title;

        private String text;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    /**
     * 钉钉请求：链接类型
     * <p>
     * msgtype	    String	是	消息类型，此时固定为：link。
     * title	        String	是	消息标题。
     * text	        String	是	消息内容。如果太长只会部分展示。
     * messageUrl	    String	是	点击消息跳转的URL。
     * picUrl	        String	否	图片URL。
     */
    public static class Link {

        private String text;

        private String messageUrl;

        private String picUrl;

        private String title;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getMessageUrl() {
            return messageUrl;
        }

        public void setMessageUrl(String messageUrl) {
            this.messageUrl = messageUrl;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    /**
     * 钉钉请求：纯文本类型
     */
    public static class Text {

        /**
         * text请求内容
         */
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    @Override
    public void setMsgType(String msgtype) {
        this.msgtype = msgtype;
    }

    @Override
    public void setText(Text text) {
        this.text = text;
    }

    @Override
    public void setLink(Link link) {
        this.link = link;
    }

    @Override
    public void setMarkDown(MarkDown markDown) {
        this.markdown = markDown;
    }

    @Override
    public void setActionCard(ActionCard actionCard) {
        this.actionCard = actionCard;
    }

    @Override
    public void setFeedCard(FeedCard feedCard) {
        this.feedCard = feedCard;
    }

    public At getAt() {
        return at;
    }

    public void setAt(At at) {
        this.at = at;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public Text getText() {
        return text;
    }

    public Link getLink() {
        return link;
    }

    public MarkDown getMarkdown() {
        return markdown;
    }

    public ActionCard getActionCard() {
        return actionCard;
    }

    public FeedCard getFeedCard() {
        return feedCard;
    }
}
