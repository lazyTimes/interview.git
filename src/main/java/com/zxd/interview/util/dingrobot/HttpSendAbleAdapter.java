package com.zxd.interview.util.dingrobot;


/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util.dingrobot
 * @Description : htpp请求实现层
 * @Create on : 2021/4/15 18:11
 **/
public class HttpSendAbleAdapter implements HttpSendAble {

    /**
     * 根据请求Config 进行请求发送
     *
     * @param httpConfig
     * @return
     */
    @Override
    public String sendPostJson(HttpConfig httpConfig) {
        return HttpClientUtil.doPostJson(httpConfig.url(), httpConfig.json());
    }
}
