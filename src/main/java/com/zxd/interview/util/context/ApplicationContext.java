package com.zxd.interview.util.context;

import com.zxd.interview.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * 应用程序上下文对象
 *
 * @author zhaoxudong
 */
public final class ApplicationContext implements Context {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationContext.class);

    private static String systemRoot = "";

    /**
     * 从类路径中获取系统的当前路径.
     * 注意：千万不要讲项目放置在
     */
    private static String initFromCodeSource() {
//        String root = ApplicationContext.class.getProtectionDomain().getCodeSource().getLocation().getFile();
//        if (root.indexOf("classes/") > 0) { // 类路径中
//            root = root.substring(0, root.indexOf("classes/"));
//        } else {
//            root = root.substring(0, root.indexOf("lib/"));
//        }
        return ApplicationContext.class.getProtectionDomain().getCodeSource().getLocation().getFile();
    }

    public static String getSystemRoot() {
        // 如果没被设置过（如在非Web容器下运行和测试）
        if (StringUtils.isEmpty(systemRoot)) {
            logger.error("未设置系统根目录，将从类路径中获取系统的当前路径.");
            systemRoot = initFromCodeSource();
        }
        return systemRoot;
    }

    public static void setSystemRoot(String root) {
        systemRoot = root;
        // 判断目录最后是否有/或\符号，如果没有，则加上
        int pos = systemRoot.lastIndexOf(File.separator);
        if (pos != systemRoot.length() - 1)
            systemRoot = systemRoot + File.separator;

        systemRoot = StringUtils.replace(systemRoot, "\\", "/");
    }
}
