package com.zxd.interview.util;

import com.google.common.base.Strings;
import com.google.common.collect.ComparisonChain;
import com.google.common.primitives.*;
import org.apache.poi.ss.formula.functions.T;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : java 基础类型比较工具
 * @Create on : 2021/5/13 14:23
 **/
public class CompareUtils {

    /**
     * 比较int 值, 如果相等，则为0
     * 如果参数左边数值较大，则为 1
     * 如果参数右边数值较大，则为-1
     *
     * @param a int值1
     * @param b int值2
     * @return
     */
    public static int compareInt(int a, int b) {
        return Ints.compare(a, b);
    }

    /**
     * 比较double 值, 如果相等，则为0
     * 如果参数左边数值较大，则为 1
     * 如果参数右边数值较大，则为-1
     *
     * @param a double值1
     * @param b double值2
     * @return
     */
    public static int compareDouble(double a, double b) {
        return Doubles.compare(a, b);
    }

    /**
     * 比较float 值, 如果相等，则为0
     * 如果参数左边数值较大，则为 1
     * 如果参数右边数值较大，则为-1
     *
     * @param a float值1
     * @param b float值2
     * @return
     */
    public static int compareFloat(float a, float b) {
        return Floats.compare(a, b);
    }

    /**
     * 比较boolean 值, 如果相等，则为0
     * 如果参数左边为true，则为 1
     * 如果参数右边为true，则为-1
     *
     * @param a 布尔值1
     * @param b 布尔值2
     * @return
     */
    public static int compareBoolean(boolean a, boolean b) {
        return Booleans.compare(a, b);
    }


    /**
     * 比较long 1 和 long 2 的大小
     * 如果参数左边数值较大，则为 1
     * 如果参数右边数值较大，则为-1
     *
     * @param l1 long值1
     * @param l2 long值2
     * @return
     */
    public static int compareLong(long l1, long l2) {
        return Longs.compare(l1, l2);
    }

    /**
     * 使用自定义比较器比较两个参数
     * @param a 参数1
     * @param b 参数2
     * @param c 比较器
     * @return
     */
    public static <T> int compare(T a, T b, Comparator<? super T> c) {
        return Objects.compare(a, b, c);
    }

    /**
     * 使用变长参数进行一组两个的相互比较
     * 如果所有参数都比较通过，返回true
     * 注意此比较方式为 懒比较 的方式
     * 如果中间出现false，后面的参数不参与比较
     *
     * @param comparable 比较参数列表
     */
    public static <T> boolean compareTwoGroups(Comparable<T>... comparable) {
        PreconditionUtils.checkArgument(comparable != null && comparable.length != 0 && comparable.length % 2 == 0, "比较参数必须大于2位，并且必须是2的倍数");
        ComparisonChain start = ComparisonChain.start();
        for (int i = 0; i < comparable.length; i += 2) {
            Comparable<T> compare1 = comparable[i];
            Comparable<T> compare2 = comparable[i + 1];
            int result = start.compare(compare1, compare2).result();
            if (result != 0) {
                return false;
            }
        }
        return true;
    }
}
