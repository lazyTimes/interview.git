package com.zxd.interview.util.holidays;

import com.zxd.interview.system.SystemConstants;
import com.zxd.interview.util.context.ApplicationContext;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 绩效考评配置文件exm.xml读取config
 *
 * @author jove_lin
 */
public class HolidaysConfig {

    private static final Log logger = LogFactory.getLog(HolidaysConfig.class);

    private static final HolidaysConfig instance = new HolidaysConfig();
    private static XMLConfiguration xmlConfig;
    private static Map<String, String> holidaysMap = new HashMap();
    private static Map<String, String> workingdaysMap = new HashMap();
    private String holidaysTagString = "holidays.day";
    private String workingdaysTagString = "workingdays.day";
    private int holidayRowNum = 0;
    private int workingdaysRowNum = 0;

    private HolidaysConfig() {
        try {
            loadConfig(ApplicationContext.getSystemRoot() + "config/holidays.xml");
        } catch (ConfigurationException e) {
            logger.error("load config error => " + e.getLocalizedMessage());
        }
    }

    /**
     * 得到解析器实例。该实例为单例模式.
     *
     * @return 系统配置实例
     */
    public static HolidaysConfig getInstance() {
        return instance;
    }

    /**
     * 装载解析，得到XMl文件最大任务数；
     */
    private void loadConfig(String configFile) throws ConfigurationException {
        xmlConfig = new XMLConfiguration();
        try {
            xmlConfig.setFileName(configFile);
            xmlConfig.load();
            xmlConfig.setEncoding(SystemConstants.FILE_ENCODING); // 否则文件会乱码
            workingdaysRowNum = xmlConfig.getMaxIndex(workingdaysTagString); // 最大行数
            workingdaysRowNum = workingdaysRowNum + 1;
            holidayRowNum = xmlConfig.getMaxIndex(holidaysTagString); // 最大行数
            holidayRowNum = holidayRowNum + 1;
        } catch (ConfigurationException ex) {
            throw ex;
        }

        if (xmlConfig == null) {
            xmlConfig = new XMLConfiguration(); // 避免null
        }
    }

    /**
     * 得到假日Map 假日 与 name
     *
     * @return Map<value, name>
     */
    public Map<String, String> getHolidaysMap() {
        if (holidaysMap.isEmpty()) {
            synchronized (holidaysMap) {
                if (holidaysMap.isEmpty()) {
                    return createHolidaysMap();
                }
            }
        }
        return holidaysMap;
    }

    /**
     * 得到周未调休的工作日
     *
     * @return Map<value, name>
     */
    public Map<String, String> getWorkingdaysMap() {
        if (workingdaysMap.isEmpty()) {
            synchronized (workingdaysMap) {
                if (workingdaysMap.isEmpty()) {
                    return createWorkingdaysMap();
                }
            }
        }
        return workingdaysMap;
    }

    /**
     * 根据 工作日或者节假日类型获取 定义好的工作日，节假日的map对象
     *
     * @param mapType   工作日或者节假日的map对象
     * @param rowNum    xml中定义的节假日最大行数
     * @param tagString 工作日或者节假日的 xml 中配置的节点
     * @return
     */
    private Map<String, String> createMapByType(Map<String, String> mapType, int rowNum, String tagString) {
        for (int i = 0; i < rowNum; i++) {
            mapType.put(xmlConfig.getProperty(tagString + "(" + i + ")[@value]").toString(),
                    xmlConfig.getProperty(tagString + "(" + i + ")[@name]").toString());
        }
        return mapType;
    }

    /**
     * 得到周未调休的工作日
     *
     * @return Map<value, name>
     */
    private Map<String, String> createWorkingdaysMap() {
        return createMapByType(workingdaysMap, workingdaysRowNum, workingdaysTagString);
    }

    /**
     * 得到假日Map 假日 与 name
     *
     * @return Map<value, name>
     */
    private Map<String, String> createHolidaysMap() {
        return createMapByType(holidaysMap, holidayRowNum, holidaysTagString);
    }

    public static int dayForWeek(String pTime) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(format.parse(pTime));
        int dayForWeek = 0;
        if (c.get(Calendar.DAY_OF_WEEK) == 1) {
            dayForWeek = 7;
        } else {
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }
        return dayForWeek;
    }

    public static void main(String[] args) {
        HolidaysConfig h = HolidaysConfig.getInstance();
        Map<String, String> a = h.getWorkingdaysMap();
        Map<String, String> b = h.getHolidaysMap();
        Iterator<Map.Entry<String, String>> iterator1 = a.entrySet().iterator();
        while (iterator1.hasNext()) {
            Map.Entry<String, String> entry = iterator1.next();
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }

        Iterator<Map.Entry<String, String>> iterator = b.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            System.out.println("--------------- " + entry.getKey() + "=" + entry.getValue());
        }
    }

}
