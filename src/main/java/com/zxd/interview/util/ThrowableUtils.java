package com.zxd.interview.util;

import com.google.common.base.Throwables;

import java.util.List;

/**
 * <pre>
 * 基于guava 的Throwables 工具类进行使用
 * 目前发现只有三个方法稍微有用一些。内部包含的其他方法在实际开发中容易造成误用
 * 比如：
 * 1. 抛出throwable 或者 runtimeException的异常。对于这种指定异常的抛出
 * 2. 此工具会对有一些受检查的异常进行二次封装，有可能出现意想不到的情况
 *
 * 针对上述情况，此工具类的主要作用在于获取堆栈顶层的深层次异常原因。或者获取整个堆栈链条。
 * 另外获取某一个cause的方法经过考虑容易误用之后排除
 * </pre>
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 错误处理工具类
 * @Create on : 2021/5/13 17:02
 **/
public class ThrowableUtils {

    /**
     * 获取异常链的原始链
     * @param throwable 异常对象
     * @return
     */
    public static Throwable getRootCause(Throwable throwable){
        return Throwables.getRootCause(throwable);
    }

    /**
     * 获取对应异常的异常链。
     * 返回一个错误集合
     * @param throwable 异常对象
     * @return
     */
    public static List<Throwable> getCausalChain(Throwable throwable){
        return Throwables.getCausalChain(throwable);
    }

    /**
     * 把异常堆栈信息转为string
     * @param throwable 异常对象
     * @return
     */
    public static String getStackTraceAsString(Throwable throwable) {
        return Throwables.getStackTraceAsString(throwable);
    }

}
