package com.zxd.interview.util;

import org.springframework.beans.BeansException;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : beanUtils bean工具类
 * @Create on : 2021/5/14 10:06
 **/
public class BeanUtils {


    /**
     * 使用了 spring 的实现，综合对比性能和效率最高
     * @param source
     * @param target
     * @throws BeansException
     */
    public static void copyProperties(Object source, Object target) throws BeansException {
        org.springframework.beans.BeanUtils.copyProperties(source, target);
    }

    public static void copyProperties(Object source, Object target, String... ignoreProperties) throws BeansException {
        org.springframework.beans.BeanUtils.copyProperties(source, target, ignoreProperties);
    }

}
