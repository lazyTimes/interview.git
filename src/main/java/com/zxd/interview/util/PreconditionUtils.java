package com.zxd.interview.util;

/**
 * 方法来源：google preconditions
 * 需要引入依赖：
 * <code>
 * <dependency>
 * <groupId>com.palantir.safe-logging</groupId>
 * <artifactId>preconditions</artifactId>
 * <version>1.14.0</version>
 * </dependency>
 * </code>
 *
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Create on : 2021/5/13 10:42
 **/
public class PreconditionUtils {

    /**
     * 校验表达式是否正确，不正确抛出异常信息
     *
     * @param expression Boolean表达式
     */
    public static void checkArgument(boolean expression) {
        com.google.common.base.Preconditions.checkArgument(expression);
    }

    /**
     * 校验表达式是否正确，不正确抛出异常信息
     *
     * @param expression   Boolean表达式
     * @param errorMessage 错误string
     */
    public static void checkArgument(boolean expression, Object errorMessage) {
        com.google.common.base.Preconditions.checkArgument(expression, errorMessage);
    }

    /**
     * 校验表达式是否正确，不正确抛出异常信息
     *
     * @param expression           Boolean表达式
     * @param errorMessageTemplate 错误string,使用 {@code %s} 作为参数占位符
     * @param errorMessageArgs     错误参数
     */
    public static void checkArgument(
            boolean expression,
            String errorMessageTemplate,
            Object... errorMessageArgs) {
        com.google.common.base.Preconditions.checkArgument(expression, errorMessageTemplate, errorMessageArgs);
    }

    /**
     * 和{@link PreconditionUtils#checkArgument(boolean, java.lang.String, java.lang.Object...)  checkArgument }方法类似
     * 校验表达式是否正确，不正确抛出异常信息
     */
    public static void checkState(boolean expression) {
        com.google.common.base.Preconditions.checkState(expression);
    }

    /**
     * 和{@link PreconditionUtils#checkArgument(boolean, java.lang.String, java.lang.Object...)  checkArgument }方法类似
     * 校验表达式是否正确，不正确抛出异常信息
     */
    public static void checkState(boolean expression, Object errorMessage) {
        com.google.common.base.Preconditions.checkState(expression, errorMessage);
    }

    /**
     * 和{@link PreconditionUtils#checkArgument(boolean, java.lang.String, java.lang.Object...)  checkArgument }方法类似
     * 校验表达式是否正确，不正确抛出异常信息
     */
    public static void checkState(
            boolean expression,
            String errorMessageTemplate,
            Object... errorMessageArgs) {
        com.google.common.base.Preconditions.checkState(expression, errorMessageTemplate, errorMessageArgs);
    }

    /**
     * 如果对象为null，抛出空指针异常
     *
     * @param reference 对象
     * @param <T>
     * @return
     */
    public static <T extends Object> T checkNotNull(T reference) {
        return com.google.common.base.Preconditions.checkNotNull(reference);
    }

    /**
     * 如果对象为null，抛出空指针异常
     *
     * @param reference    对象
     * @param errorMessage 异常信息
     * @param <T>
     * @return
     */
    public static <T extends Object> T checkNotNull(
            T reference, Object errorMessage) {
        return com.google.common.base.Preconditions.checkNotNull(reference, errorMessage);
    }

    /**
     * 如果对象为null，抛出空指针异常
     *
     * @param reference            对象
     * @param errorMessageTemplate 异常信息
     * @param errorMessageArgs     异常参数
     * @param <T>
     * @return
     */
    public static <T extends Object> T checkNotNull(
            T reference, String errorMessageTemplate,
            Object... errorMessageArgs) {
        return com.google.common.base.Preconditions.checkNotNull(reference, errorMessageTemplate, errorMessageArgs);
    }
}
