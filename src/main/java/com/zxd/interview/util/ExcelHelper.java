package com.zxd.interview.util;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by  on 2017/11/23.
 * 简单的Excel处理工具
 */
public abstract class ExcelHelper {
    private static final Logger logger = LoggerFactory.getLogger(ExcelHelper.class);
    private ExcelHelper() {
        throw new AssertionError("工具类不允许实例化");
    }

//    public static void main(String[] args) {
//        File file = new File("E:\\Java\\galaxy-apps-2017\\ga-core\\src\\main\\java\\com\\nine\\rivers\\galaxy\\utils\\io\\test.xlsx");
//        Workbook workbook = getWorkbook(file);
//        Sheet sheet = workbook.getSheetAt(0);
//        Row row = sheet.getRow(0);
//        System.out.println(row.getLastCellNum());
//    }

    /**
     * 加载单个Sheet为String矩阵的形式
     * @param sheetIndex 第几个Sheet, 从0开始计数
     * @return 内容的String矩阵
     */
    public static String[][] readSheet(InputStream inputStream, int sheetIndex) {
        // 1. 创建文件对象, 同时支持Excel 2003/2007/2010
        try (Workbook workbook = WorkbookFactory.create(inputStream);){
            // 2. 获取选择的sheet
            Sheet sheet = workbook.getSheetAt(sheetIndex);
            // 3. 获取总行数, 有n行数据则getLastRowNum()返回n-1
            int rowCount = sheet.getLastRowNum() + 1;

            // 4. 读取每行数据
            String[][] result = new String[rowCount][];
            for (int r = 0; r < rowCount; r++) {
                Row row = sheet.getRow(r);
                int cellCount = row.getLastCellNum(); //获取总列数
                // 4.1. 读取每一列, 加载为字符串
                result[r] = new String[cellCount];
                for(int c = 0; c < cellCount; c++){
                    Cell cell = row.getCell(c, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK); // 保证不返回null
                    cell.setCellType(CellType.STRING);
                    result[r][c] = cell.getStringCellValue();
                }
            }
            return result;
        } catch (Exception e) {
            logger.error("文件存在, 错误: "+e.getMessage(), e);
        }
        return new String[0][0];
    }
    public static String[][] readSheet(InputStream inputStream) {
        return readSheet(inputStream, 0);
    }

    public static String[][] readSheet(String filePath, int sheetIndex) {
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(filePath))){
            return readSheet(inputStream,sheetIndex);
        } catch (IOException e) {
            logger.error("读取excel失败", e);
        }
        return new String[0][0];
    }

    public static String[][] readSheet(String filePath) {
        return readSheet(filePath, 0);
    }

//    /**
//     * 将 data 数据源 写入 excel 中第 sheetIndex 个 Sheet 中, 并将 Sheet 改名为 SheetName
//     * @param data 数据源
//     * @param filePath 文件路径
//     * @param sheetIndex 第几个Sheet, 从0开始计数
//     * @param sheetName Sheet名称, 为空则默认 "sheet"+sheetIndex
//     * @return 写入成功返回true, 失败返回false
//     */
//    public static <T> boolean writeSheet(OutputStream outputStream, List<T[][]> data) {
//        // 1. 创建Excel文件, 配置默认属性
//        File excelFile = new File(filePath);
//        Workbook workbook = getWorkbook(excelFile);
//        if(workbook == null){
//            return false;
//        }
//
//        // 2. 根据sheetIndex获取Sheet, 并改名
//        for (int sheetIndex = 0, sheetCount = CollectionUtils.size(data); sheetIndex < sheetCount; sheetIndex++) {
////            Sheet sheet = getSheet(workbook, sheetIndex);
//
//            if(sheetIndex >= workbook.getNumberOfSheets()){
//                if(StringUtils.isEmpty(sheetName)) {
//                    sheetName = "sheet"+sheetIndex;
//                }
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(15);
//            } else {
//                if(StringUtils.isNotEmpty(sheetName)) {
//                    workbook.setSheetName(sheetIndex, sheetName);
//                }
//            }
//        }
//
//
//        // 2. 初始化Excel, 填充数据
//        for (int r = 0, rowLen = data.length; r < rowLen; r++) {
//            T[] row = data[r];
//            Row excelRow = sheet.createRow(r);
//            for(int c = 0, colLen = row.length; c < colLen; c++){
//                Cell cell = excelRow.createCell(c, CellType.STRING);
//                cell.setCellValue(row[c].toString());
//            }
//        }
//
//        // 4. 写入文件
//        try (OutputStream os = new FileOutputStream(excelFile)){
//            workbook.write(os);
//            os.flush();
//        } catch (FileNotFoundException e) {
//            logger.error("文件:"+filePath+"未找到", e);
//            return false;
//        } catch (IOException e) {
//            logger.error("IO错误, 不能写入:"+filePath, e);
//            return false;
//        } finally {
//            // 5. 关闭流文件
//            IOUtils.closeQuietly(workbook);
//        }
//        return true;
//    }
//    public static <T> boolean writeSheet(T[][] data, String filePath, int sheetIndex) {
//        return writeSheet(data, filePath, sheetIndex, "sheet"+sheetIndex);
//    }
//
//    /**
//     * 从集合 LinkedHashMap 中读取写到 excel
//     *
//     * @param sheetName excel sheet 名字
//     * @param titleArr  标题数组
//     * @param dataList  这个集合里面的map 实现必须是 LinkedHashMap (有序，才能正确写入 excel)
//     * @param response  http 响应对象
//     */
//    public static void writeExcel(String sheetName, String[] titleArr, List<LinkedHashMap> dataList, OutputStream outputStream) {
//
//        try (HSSFWorkbook hssfWorkbook = new HSSFWorkbook()) {
//            HSSFSheet hssfSheet = hssfWorkbook.createSheet(sheetName);
//            // 标题行
//            HSSFRow titleRow = hssfSheet.createRow(0);
//            // 填充标题行数据
//            for (int i = 0; i < titleArr.length; i++) {
//                hssfSheet.setColumnWidth(i, titleArr[i].getBytes().length * 260);
//                titleRow.createCell(i).setCellValue(titleArr[i]);
//            }
//            LinkedHashMap linkedHashMap;
//            HSSFRow dataRow;
//            Collection values;
//            Object[] dataArr;
//            // 填充数据行
//            for (int i = 0; i < dataList.size(); i++) {
//                linkedHashMap = dataList.get(i);
//                dataRow = hssfSheet.createRow(i + 1);
//                values = linkedHashMap.values();
//                dataArr = values.toArray();
//                // 填充每一格数据
//                for (int j = 0; j < values.size(); j++) {
//                    dataRow.createCell(j).setCellValue(dataArr[j].toString());
//                }
//            }
//            hssfWorkbook.write(outputStream);
//            outputStream.flush();
//        } catch (IOException e) {
//            logger.error("写 excel 出错："+e.getMessage());
//        }
//
//    }


}
