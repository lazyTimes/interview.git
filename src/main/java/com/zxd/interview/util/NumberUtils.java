package com.zxd.interview.util;


import com.zxd.interview.constant.ReportConstants;

import java.util.*;

/**
 * 数据处理工具类
 *
 * @author xushuming
 */
public class NumberUtils {


    /**
     * 数字验证,如果Integer类型为null或"",就让它为0
     *
     * @param num 数字
     * @return 数字
     */
    public static Integer judge(Integer num) {

        if (num == null || "".equals(num)) {
            num = ReportConstants.NUM_ZERO;
        }

        return num;
    }

    /**
     * 数据初始化
     *
     * @param lift  左边
     * @param right 右边
     * @return 字符串
     */
    public static String thenValue(int lift, int right) {
        String thenValue = null;
        if (lift != ReportConstants.EMPTY_INT_VALUE && right != ReportConstants.EMPTY_INT_VALUE) {
            thenValue = "1 : " + (lift / right);
        } else if (lift == ReportConstants.EMPTY_INT_VALUE && right == ReportConstants.EMPTY_INT_VALUE) {
            thenValue = "1 : 1";
        } else if (lift != ReportConstants.EMPTY_INT_VALUE && right == ReportConstants.EMPTY_INT_VALUE) {
            thenValue = lift + " : EMPTY_INT_VALUE";
        } else if (lift == ReportConstants.EMPTY_INT_VALUE && right != ReportConstants.EMPTY_INT_VALUE) {
            thenValue = right + " : 0";
        }

        return thenValue;

    }

    /**
     * 年龄范围判断方法
     *
     * @param ageRange 年龄范围
     * @return 年龄集合
     */
    public static Map<String, Integer> ageRange1(Integer ageRange) {
        Map<String, Integer> map = new HashMap<>();
        //start：起始值 用于以下范围判断
        //end： 结束值 用于以上范围判断
        //assign：指定值 用于明确指定相应值判断
        List<Integer> ages = new ArrayList<>();
        Integer start, end, assign;

        //选择范围
        switch (ageRange) {
            case ReportConstants.NUM_ZERO:
                start = ReportConstants.NUM_TWO;
                end = null;
                assign = null;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
            case ReportConstants.NUM_ONE:
                start = null;
                end = null;
                assign = ReportConstants.NUM_THREE;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
            case ReportConstants.NUM_TWO:
                start = null;
                end = null;
                assign = ReportConstants.NUM_FOUR;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
            case ReportConstants.NUM_THREE:
                start = null;
                end = null;
                assign = ReportConstants.NUM_FIVE;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
            case ReportConstants.NUM_FOUR:
                start = null;
                end = ReportConstants.NUM_SIX;
                assign = null;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
            default:
                start = null;
                end = null;
                assign = null;
                map.put("start", start);
                map.put("end", end);
                map.put("assign", assign);
                break;
        }
        return map;
    }

    /**
     * 年龄范围判断方法
     *
     * @param ageRange 年龄范围
     * @return 年龄集合
     */
    public static Map<String, Integer> ageRange2(Integer ageRange) {
        Map<String, Integer> map = new HashMap<>();
        //start：起始值 用于以下范围判断
        //end： 结束值 用于以上范围判断
        //assign：指定值 用于明确指定相应值判断
        List<Integer> ages = new ArrayList<>();
        Integer start, end;

        //选择范围
        switch (ageRange) {
            case ReportConstants.NUM_ZERO:
                start = ReportConstants.NUM_25;
                end = null;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_ONE:
                start = ReportConstants.NUM_26;
                end = ReportConstants.NUM_30;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_TWO:
                start = ReportConstants.NUM_31;
                end = ReportConstants.NUM_35;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_THREE:
                start = ReportConstants.NUM_36;
                end = ReportConstants.NUM_40;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_FOUR:
                start = ReportConstants.NUM_41;
                end = ReportConstants.NUM_45;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_FIVE:
                start = ReportConstants.NUM_46;
                end = ReportConstants.NUM_50;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_SIX:
                start = ReportConstants.NUM_51;
                end = ReportConstants.NUM_55;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_SEVEN:
                start = ReportConstants.NUM_56;
                end = ReportConstants.NUM_60;
                map.put("start", start);
                map.put("end", end);
                break;
            case ReportConstants.NUM_EIGHT:
                start = null;
                end = ReportConstants.NUM_61;
                map.put("start", start);
                map.put("end", end);
                break;
            default:
                start = null;
                end = null;
                map.put("start", start);
                map.put("end", end);
                break;
        }
        return map;
    }

    /**
     * 处理时间
     *
     * @return map集合
     */
    public static Map<String, Date> dealTime() {
        Map<String, Date> map = new HashMap<>();
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone(ReportConstants.GMT_TIME)); //获取东八区时间
        Calendar start = Calendar.getInstance();//初始化时间
        Calendar end = Calendar.getInstance();//初始化时间

        if (now.get(Calendar.MONTH) + ReportConstants.NUM_ONE >= ReportConstants.NUM_NINE) {

            start.set(Calendar.YEAR, start.get(Calendar.YEAR));
            start.set(Calendar.MONTH, (ReportConstants.NUM_NINE - ReportConstants.NUM_ONE)); //转为日期格式月份自动加一
            start.set(Calendar.DATE, ReportConstants.NUM_ONE);

            end.set(Calendar.YEAR, end.get(Calendar.YEAR) + ReportConstants.NUM_ONE);
            end.set(Calendar.MONTH, (ReportConstants.NUM_NINE - ReportConstants.NUM_ONE)); //转为日期格式月份自动加一
            end.set(Calendar.DATE, ReportConstants.NUM_ONE);

        } else if (now.get(Calendar.MONTH) + ReportConstants.NUM_ONE < ReportConstants.NUM_NINE) {

            start.set(Calendar.YEAR, start.get(Calendar.YEAR) - ReportConstants.NUM_ONE);
            start.set(Calendar.MONTH, (ReportConstants.NUM_NINE - ReportConstants.NUM_ONE)); //转为日期格式月份自动加一
            start.set(Calendar.DATE, ReportConstants.NUM_ONE);

            end.set(Calendar.YEAR, end.get(Calendar.YEAR));
            end.set(Calendar.MONTH, (ReportConstants.NUM_NINE - ReportConstants.NUM_ONE)); //转为日期格式月份自动加一
            end.set(Calendar.DATE, ReportConstants.NUM_ONE);
        }
        map.put("start", start.getTime());
        map.put("end", end.getTime());
        return map;
    }


}
