package com.zxd.interview.util.encrypt;

import org.apache.commons.codec.digest.DigestUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密类（封装jdk自带的md5加密方法）
 * MD5解密网站1：{@link https://www.somd5.com/ somd5}
 * MD5解密网站2：{@link https://www.cmd5.com/ cmd5}
 * @author zhaoxudong
 * @date 2021年5月14日
 */
public class MD5Utils {

    /**
     * 使用JDK自带的md5加密
     * @param source
     * @return
     */
    public static String encrypt(String source) {
        return encodeMd5(source.getBytes());
    }

    /**
     * 使用了apach 的加密方法，也是最常使用的方法
     * @param data 原始参数
     * @return
     */
    public static String encryptToMd5(String data){
        return DigestUtils.md5Hex(data);

    }

    private static String encodeMd5(byte[] source) {
        try {
            return encodeHex(MessageDigest.getInstance("MD5").digest(source));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private static String encodeHex(byte[] bytes) {
        StringBuilder buffer = new StringBuilder(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++) {
            if (((int) bytes[i] & 0xff) < 0x10)
                buffer.append("0");
            buffer.append(Long.toString((int) bytes[i] & 0xff, 16));
        }
        return buffer.toString();
    }


}
