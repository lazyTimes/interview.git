package com.zxd.interview.util.encrypt;

import com.zxd.interview.util.PreconditionUtils;
import com.zxd.interview.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : des加解密工具类
 * @Create on : 2021/5/18 10:21
 **/
public class DESUtil {

    /**
     * 加密
     *
     * @param datasource byte[]
     * @param key        String
     * @return byte[]
     */
    private static byte[] encryptByKey(byte[] datasource, String key) {
        try {
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(key.getBytes());
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作 ecb模式下面的PKCS5Padding
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(datasource);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 对string进行BASE64Encoder转换
     * @param data 加密数据
     * @param key 加密Key
     * @return
     * @throws Exception
     * @Method: encrypt
     * @Description: 加密数据然后转成Base64
     */
    public static String encrypt(String data, String key) {
        PreconditionUtils.checkArgument(StringUtils.isNotBlank(key) && key.length() >= 8, "秘钥不能为空，并且不能小于8位");
        byte[] bt = encryptByKey(data.getBytes(), key);
        Base64.Decoder base64en = Base64.getDecoder();
        return new String(base64en.decode(bt));
    }

    /**
     * 解密
     *
     * @param src byte[]
     * @param key String
     * @return byte[]
     * @throws Exception
     */
    private static byte[] decrypt(byte[] src, String key) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom random = new SecureRandom();
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(key.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, random);
        // 真正开始解密操作
        return cipher.doFinal(src);
    }

    /**
     * 从Base64解密 在Des解密数据
     * @param data
     * @return
     * @throws Exception
     * @Method: decryptor
     * @Description: 从Base64解密 在Des解密数据
     */
    public static String decryptor(String data, String key) throws Exception {  //对string进行BASE64Encoder转换
        Base64.Decoder base64en = Base64.getDecoder();
        byte[] bt = decrypt(base64en.decode(data), key);
        return new String(bt);
    }
}
