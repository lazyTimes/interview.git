package com.zxd.interview.util.encrypt;

import java.util.Base64;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : base64加解密工具类
 * @Create on : 2021/5/18 11:26
 **/
public class Base64Util {

    /**
     * 使用Base64进行加密
     *
     * @param res 密文
     * @return
     */
    public static byte[] base64Encode(byte[] res) {
        return Base64.getEncoder().encode(res);
    }

    /**
     * 使用Base64进行加密
     *
     * @param res 密文
     * @return
     */
    public static String base64Encode(String res) {
        return new String(base64Encode(res.getBytes()));
    }

    /**
     * 使用Base64进行解密
     *
     * @param res
     * @return
     */
    public static String base64Decode(String res) {
        return new String(base64Decode(res.getBytes()));
    }

    /**
     * 使用Base64进行解密
     *
     * @param res
     * @return
     */
    public static byte[] base64Decode(byte[] res) {
        return Base64.getEncoder().encode(res);
    }
}
