package com.zxd.interview.util;


import com.zxd.interview.util.excel.ExcelUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * excel导出工具
 *
 * @author dhc
 * @date 2020-3-30 11:29:49
 */
@Component
public class ExportUtil extends ExportBase {

    /**
     * 导出报表的具体操作操作
     * 使用的是Excel 海量数据的操作
     *
     * @param map      数据Map
     * @param fileName 文件名称
     * @param dataList 数据列表
     * @return 导出路径
     */
    public String exportReportByExcelSXSSF(Map<String, Object> map, String fileName, List<Object> dataList, Class<?> classes) {

        PathData pd = getPathData(fileName);

        // 通过工具类把数据写入到流中
        if (map != null && !map.isEmpty()) {
            ExcelUtil.getInstance().exportObj2ExcelSXSSF(pd.getPath(), dataList, classes, map);
        } else {
            ExcelUtil.getInstance().exportObj2ExcelSXSSF(pd.getPath(), dataList, classes);
        }
        return pd.getExcelUrl();
    }

    /**
     * 导出报表的具体操作操作
     * 使用的是Excel 基本的对象转Excel
     *
     * @param map      数据Map
     * @param fileName 文件名称
     * @param dataList 数据列表
     * @return 导出路径
     */
    public String exportReportByExcelObject(Map<String, Object> map, String fileName, List dataList, Class<?> classes) {
        // 对用户名编码，设置返回流

        PathData pd = getPathData(fileName);

        // 通过工具类把数据写入到流中
        if (map != null && !map.isEmpty()) {
            ExcelUtil.getInstance().exportObj2Excel(pd.getPath(), dataList, classes, true, map);
        } else {
            ExcelUtil.getInstance().exportObj2Excel(pd.getPath(), dataList, classes, true);
        }
        return pd.getExcelUrl();
    }
}
