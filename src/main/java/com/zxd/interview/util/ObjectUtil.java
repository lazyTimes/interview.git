package com.zxd.interview.util;

import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 对象工具类
 * @Create on : 2021/5/12 17:04
 **/
public class ObjectUtil {



    /**
     * 是否为数字
     *
     * @param obj 对象
     * @return
     */
    public static boolean isInteger(Object obj) {
        try {
            Integer.parseInt(obj.toString());
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    /**
     * 比较两个对象是否相等
     * 注意：Objects.eqauls(null, null) == true
     *
     * @param a 对象1
     * @param b 对象2
     * @return
     */
    public static boolean equals(Object a, Object b) {
        return Objects.equals(a, b);
    }

    /**
     * 如果两个对象不相等，返回true
     *
     * @param object1 对象1
     * @param object2 对象2
     * @return
     */
    public static boolean notEqual(final Object object1, final Object object2) {
        return !equals(object1, object2);
    }

    /**
     * 检查指定的对象引用是否不为null。 此方法主要设计用于在方法和构造函数中进行参数验证，如下所示：
     * 如果为null，则抛出空指针异常并且输出默认的异常信息
     * <blockquote><pre>
     * public Foo(Bar bar, Baz baz) {
     *     this.bar = Objects.requireNonNull(bar, "bar must not be null");
     *     this.baz = Objects.requireNonNull(baz, "baz must not be null");
     * }
     * </pre></blockquote>
     *
     * @param obj 校验对象
     * @param <T>
     * @return
     */
    public static <T> T requireNonNull(T obj) {
        return Objects.requireNonNull(obj);
    }

    /**
     * 检查指定的对象引用是否不为null。 此方法主要设计用于在方法和构造函数中进行参数验证，如下所示：
     * <blockquote><pre>
     * public Foo(Bar bar, Baz baz) {
     *     this.bar = Objects.requireNonNull(bar, "bar must not be null");
     *     this.baz = Objects.requireNonNull(baz, "baz must not be null");
     * }
     * </pre></blockquote>
     *
     * @param obj     校验对象
     * @param message 异常信息
     * @param <T>
     * @return
     */
    public static <T> T requireNonNull(T obj, String message) {
        return Objects.requireNonNull(obj, message);
    }

    /**
     * 校验对象不为null
     *
     * @param obj
     * @return
     */
    public static boolean nonNull(java.lang.Object obj) {
        return Objects.nonNull(obj);
    }

    /**
     * 校验对象为null
     *
     * @param obj
     * @return
     */
    public static boolean isNull(java.lang.Object obj) {
        return Objects.isNull(obj);
    }

    /**
     * 比较两个对象。若左边参数为null，则为-1。若右边参数为null，则为1
     *
     * @param c1  比较参数1
     * @param c2  比较参数2
     * @param <T>
     * @return 如果大于，返回>0的值，等于 == 0， 小于返回负数
     */
    public static <T extends Comparable<? super T>> int compare(final T c1, final T c2) {
        return org.apache.commons.lang3.ObjectUtils.compare(c1, c2);
    }

    /**
     * 比较两个对象。若左边参数为null，则为-1。若右边参数为null，则为1
     *
     * @param a
     * @param b
     * @param c
     * @param <T>
     * @return
     */
    public static int compare(Object a, Object b, Comparator c) {
        return Objects.compare(a, b, c);
    }

    /**
     * 克隆一个对象
     * 浅拷贝
     * 被克隆对象需要实现 CloneAble，并且需要实现java.io.Serialize 接口
     *
     * @param obj
     * @param <T>
     * @return
     */
    public static <T> T clone(final T obj) {
        return org.apache.commons.lang3.ObjectUtils.clone(obj);
    }
}
