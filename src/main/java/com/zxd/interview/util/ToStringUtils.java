package com.zxd.interview.util;

import cn.hutool.core.util.ReflectUtil;
import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 美化tostirng方法
 * @Create on : 2021/5/13 14:16
 **/
public class ToStringUtils {


    /**
     * 对于obj对象的所有属性进行打印
     *
     * @param obj
     */
    public static String objToString(Object obj) {
        PreconditionUtils.checkArgument(ObjectUtil.nonNull(obj), "传入参数不能为空");
        Field[] fields = ReflectUtil.getFields(obj.getClass());
        ToStringHelper toStringHelper = MoreObjects.toStringHelper(obj);
        for (Field field : fields) {
            Object fieldValue = ReflectUtil.getFieldValue(obj, field);
            toStringHelper.add(field.getName(), fieldValue);
        }
        return toStringHelper.toString();
    }

    /**
     * 数组元素美化打印
     *
     * @param ts  数组反省参数
     * @param <T> object
     * @return
     */
    public static <T> String arrToString(T[] ts) {
        return Arrays.toString(ts);
    }

    /**
     * map内容美化打印
     * @param map map内容
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> String mapToString(Map<K, V> map) {
        if (MapUtils.isEmpty(map)) {
            return "";
        }
        ToStringHelper toStringHelper = MoreObjects.toStringHelper(map);
        map.forEach((key, value) -> {
            toStringHelper.add(String.valueOf(key), value);
        });
        return toStringHelper.toString();
    }
}
