package com.zxd.interview.util;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 字符串工具类
 * @Create on : 2021/5/12 14:23
 **/
public class StringUtils {

    /**
     * 判断字符串是否为空或者是否长度为0
     * @param cs
     * @return
     */
    public static boolean isEmpty(CharSequence cs) {
        return org.apache.commons.lang3.StringUtils.isEmpty(cs);
    }

    /**
     * 判断字符串是否为空或者是否长度不为0
     * @param cs
     * @return
     */
    public static boolean isNotEmpty(CharSequence cs) {
        return !isEmpty(cs);
    }

    /**
     * 判断是否所有的字符串都不为空字符串（空格不算入内容）或者null
     * @param css
     * @return
     */
    public static boolean isNoneBlank(CharSequence... css) {
        return !org.apache.commons.lang3.StringUtils.isNoneBlank(css);
    }

    /**
     * 查询是否所有的参数为空或者空字符串（空格同样算入内容）
     * @param css
     * @return
     */
    public static boolean isAllBlank(CharSequence... css) {
        return org.apache.commons.lang3.StringUtils.isAllBlank(css);
    }

    /**
     * 判断字符串是否为空字符串
     * @param cs
     * @return
     */
    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * 判断字符串是否为空字符串
     * @param cs
     * @return
     */
    public static boolean isBlank(final CharSequence cs) {
        return org.apache.commons.lang3.StringUtils.isBlank(cs);
    }

    /**
     * 去掉字符串两边的空格
     * @param str 需要去除空格的字符串
     * @return
     */
    public static String trim(String str){
        return org.apache.commons.lang3.StringUtils.trim(str);
    }

    /**
     * 去掉字符串两边的空格，如果去除后为空字符串，则为""
     * @param str 需要去除空格的字符串
     * @return
     */
    public static String trimToEmpty(String str){
        return org.apache.commons.lang3.StringUtils.trimToEmpty(str);
    }

    /**
     * 去掉字符串两边的空格，如果去除后为空字符串，则为null
     * @param str 需要去除空格的字符串
     * @return
     */
    public static String trimToNull(String str){
        return org.apache.commons.lang3.StringUtils.trimToNull(str);
    }

    /**
     *
     * StringUtils.replaceEach：使用方法
     * * text-用于搜索和替换的文本，如果为空则不执行操作
     * * searchList -要搜索的字符串，如果为空则不操作
     * * replacementList—要替换它们的字符串，如果为空则不操作
     * * repeat—如果为true，则重复替换，直到没有更多可能的替换或timeToLive < 0
     * * timeToLive -如果小于0，则有一个循环引用和无限循环
     * <pre>
     *         StringUtils.replaceEach(null, *, *, *, *) = null
     *         StringUtils.replaceEach("", *, *, *, *) = ""
     *         StringUtils.replaceEach("aba", null, null, *, *) = "aba"
     *         StringUtils.replaceEach("aba", new String[0], null, *, *) = "aba"
     *         StringUtils.replaceEach("aba", null, new String[0], *, *) = "aba"
     *         StringUtils.replaceEach("aba", new String[]{"a"}, null, *, *) = "aba"
     *         StringUtils.replaceEach("aba", new String[]{"a"}, new String[]{""}, *, >=0) = "b"
     *         StringUtils.replaceEach("aba", new String[]{null}, new String[]{"a"}, *, >=0) = "aba"
     *         StringUtils.replaceEach("abcde", new String[]{"ab", "d"}, new String[]{"w", "t"}, *, >=0) = "wcte"
     *         (example of how it repeats)
     *         StringUtils.replaceEach("abcde", new String[]{"ab", "d"}, new String[]{"d", "t"}, false, >=0) = "dcte"
     *         StringUtils.replaceEach("abcde", new String[]{"ab", "d"}, new String[]{"d", "t"}, true, >=2) = "tcte"
     *         StringUtils.replaceEach("abcde", new String[]{"ab", "d"}, new String[]{"d", "ab"}, *, *) = IllegalStateException
 *         </pre>
     * @param text 原始字符串内容
     * @param searchList 需要搜索的字符串
     * @param replacementList 需要替换的字符串列表
     * @return
     */
    public static String replaceEach(final String text, final String[] searchList, final String[] replacementList) {
        return org.apache.commons.lang3.StringUtils.replaceEach(text, searchList, replacementList);
    }

    /**
     * 对于字符串内容进行替换，不区分大小写
     *  <pre>
     *     StringUtils.replaceIgnoreCase(null, *, *)        = null
     *     StringUtils.replaceIgnoreCase("", *, *)          = ""
     *     StringUtils.replaceIgnoreCase("any", null, *)    = "any"
     *     StringUtils.replaceIgnoreCase("any", *, null)    = "any"
     *     StringUtils.replaceIgnoreCase("any", "", *)      = "any"
     *     StringUtils.replaceIgnoreCase("aba", "a", null)  = "aba"
     *     StringUtils.replaceIgnoreCase("abA", "A", "")    = "b"
     *     StringUtils.replaceIgnoreCase("aba", "A", "z")   = "zbz"
     *  </pre>
     * @param text
     * @param searchString 查找字符串，不区分大小写
     * @param replacement 替换字符串
     * @return
     */
    public static String replaceIgnoreCase(final String text, final String searchString, final String replacement) {
        return org.apache.commons.lang3.StringUtils.replaceIgnoreCase(text, searchString, replacement);
    }

    /**
     * 对于字符串内容进行替换，区分大小写
     * * <pre>
     *     StringUtils.replace(null, *, *)        = null
     *     StringUtils.replace("", *, *)          = ""
     *     StringUtils.replace("any", null, *)    = "any"
     *     StringUtils.replace("any", *, null)    = "any"
     *     StringUtils.replace("any", "", *)      = "any"
     *     StringUtils.replace("aba", "a", null)  = "aba"
     *     StringUtils.replace("aba", "a", "")    = "b"
     *     StringUtils.replace("aba", "a", "z")   = "zbz"
     *  </pre>
     * @param text
     * @param searchString
     * @param replacement
     * @return
     */
    public static String replace(final String text, final String searchString, final String replacement) {
        return org.apache.commons.lang3.StringUtils.replace(text, searchString, replacement);
    }

    /**
     * 合并方法，用于合并集合的元素组成一个字符串
     *
     * <pre>
     * StringUtils.join(null, *)               = null
     * StringUtils.join([], *)                 = ""
     * StringUtils.join([null], *)             = ""
     * StringUtils.join(["a", "b", "c"], ';')  = "a;b;c"
     * StringUtils.join(["a", "b", "c"], null) = "abc"
     * StringUtils.join([null, "", "a"], ';')  = ";;a"
     * </pre>
     * @param list 合并集合
     * @param separator 合并分隔符
     * @param startIndex 开始下标，从0开始
     * @param endIndex 结束下标
     * @throws java.lang.IndexOutOfBoundsException: 下标越界的时候会抛出此异常，比如结尾下标越界
     * @return
     */
    public static String join(final List<?> list, final String separator, final int startIndex, final int endIndex) {
        return org.apache.commons.lang3.StringUtils.join(list, separator, startIndex, endIndex);
    }

    /**
     * 合并方法，用于合并集合的元素组成一个字符串
     * 和上面的方法类似，不再赘述
     * @return
     */
    public static String join(final List<?> list, final char separator, final int startIndex, final int endIndex) {
        return org.apache.commons.lang3.StringUtils.join(list, separator, startIndex, endIndex);
    }

    /**
     * 合并方法，用于合并集合的元素组成一个字符串
     * 对于支持迭代的对象进行合并
     * @return
     */
    public static String join(final Iterable<?> iterable, final String separator) {
        return org.apache.commons.lang3.StringUtils.join(iterable, separator);
    }

    /**
     * 合并方法，用于合并集合的元素组成一个字符串
     * 对于支持迭代的对象进行合并
     * @return
     */
    public static String join(final Iterable<?> iterable, final char separator) {
        return org.apache.commons.lang3.StringUtils.join(iterable, separator);
    }

    /**
     * 比较两个字符串内容是否相等
     * 下面是使用案例
     * <pre>
     * StringUtils.equals(null, null)   = true
     * StringUtils.equals(null, "abc")  = false
     * StringUtils.equals("abc", null)  = false
     * StringUtils.equals("abc", "abc") = true
     * StringUtils.equals("abc", "ABC") = false
     * </pre>
     * @param cs1 字符串1
     * @param cs2 字符串2
     * @return
     */
    public static boolean equals(final CharSequence cs1, final CharSequence cs2) {
        return org.apache.commons.lang3.StringUtils.equals(cs1, cs2);
    }


    /**
     * 比较两个字符串内容是否相等，并且不区分大小写
     * 下面是使用案例
     * <pre>
     * StringUtils.equalsIgnoreCase(null, null)   = true
     * StringUtils.equalsIgnoreCase(null, "abc")  = false
     * StringUtils.equalsIgnoreCase("abc", null)  = false
     * StringUtils.equalsIgnoreCase("abc", "abc") = true
     * StringUtils.equalsIgnoreCase("abc", "ABC") = true
     * </pre>
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean equalsIgnoreCase(final CharSequence cs1, final CharSequence cs2) {
        return org.apache.commons.lang3.StringUtils.equalsIgnoreCase(cs1, cs2);
    }
}
