package com.zxd.interview.util;

import cn.hutool.core.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 集合工具类
 * @Create on : 2021/5/10 14:34
 **/
public final class CollectionUtils {

    /**
     * 对象组中是否存在 Empty Object
     *
     * @param os 对象组
     * @return
     */
    public static boolean isOneEmpty(Object... os) {
        for (Object o : os) {
            if (ObjectUtil.isNull(o)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 对象组中是否全是 Empty Object
     *
     * @param os
     * @return
     */
    public static boolean isAllEmpty(Object... os) {
        for (Object o : os) {
            if (ObjectUtil.nonNull(o)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 如果集合为null或者空集合，返回true
     * @param coll
     * @return
     */
    public static boolean isEmpty(Collection<?> coll){
        return org.apache.commons.collections4.CollectionUtils.isEmpty(coll);
    }

    /**
     * 判断集合是否不为空集合并且不为null，满足条件则为true
     * @param coll
     * @return
     */
    public static boolean isNotEmpty(Collection<?> coll){
        return !isEmpty(coll);
    }

    /**
     * 对于集合内容进行排序
     * @param list
     * @param <T>
     */
    public static <T extends Comparable<? super T>> void sort(List<T> list){
        Collections.sort(list);
    }

    /**
     * 计算集合的size，如果对象为null，则size 为0
     * @param object
     * @return
     */
    public static int size(Object object){
        return org.apache.commons.collections4.CollectionUtils.size(object);
    }

    /**
     * 返回空集合。但是需要注意为空返回的集合不可修改
     * @param <T>
     * @return
     */
    public static <T> Collection<T> emptyCollection() {
        return org.apache.commons.collections4.CollectionUtils.emptyCollection();
    }

    /**
     * 如果集合为null，返回空集合。但是需要注意为空返回的集合不可修改
     * 不为空 返回本身
     * @param collection 如果为空，返回不能更改的空集合
     * @param <T>
     * @return
     */
    public static <T> Collection<T> emptyIfNull(Collection<T> collection) {
        return org.apache.commons.collections4.CollectionUtils.emptyIfNull(collection);
    }

    /**
     * 对当前list进行反转
     * @param list 集合
     * @param <T> 泛型参数
     * @return
     */
    public static <T> List<T> reverse(List<T> list) {
        Collections.reverse(list);
        return list;
    }

    /**
     * 反转并且生成一个新的集合
     * @param list 集合
     * @param <T> 泛型参数
     * @return
     */
    public static <T> List<T> reverseNew(List<T> list) {
        return CollectionUtil.reverseNew(list);
    }

    /**
     * 集合1匹配集合2中的任意元素即为true
     * @param coll1
     * @param coll2
     * @return
     */
    public static boolean containsAny(Collection<?> coll1, Collection<?> coll2) {
        return CollectionUtil.containsAny(coll1, coll2);
    }

    /**
     * 集合1匹配集合2中的所有元素即为true
     * @param coll1
     * @param coll2
     * @return
     */
    public static boolean containsAll(Collection<?> coll1, Collection<?> coll2) {
        return CollectionUtil.containsAll(coll1, coll2);
    }

    /**
     * 将当前参数转为arrayList
     * @param values 变长参数元素
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> toArrayList(T... values) {
        return CollectionUtil.toList(values);
    }

    /**
     * 截取list内容进行截取切割
     * <b>包含头部不包含尾部</b>
     * @param list 集合
     * @param start 开始下标
     * @param end 结束下标
     * @param <T> 泛型
     * @return
     */
    public static <T> List<T> subArrayList(List<T> list, int start, int end) {
        return CollectionUtil.sub((List)list, start, end, 1);
    }

    /**
     * 截取list内容
     * <b>包含头部不包含尾部</b>
     * 指定偏移值进行偏移截取
     * @param list 集合
     * @param start 开始下标
     * @param end 结束下标
     * @param <T> 泛型
     * @param step 偏移值
     * @return
     */
    public static <T> List<T> subArrayList(List<T> list, int start, int end, int step) {
        return CollectionUtil.sub((List)list, start, end, step);
    }

}
