package com.zxd.interview.util;


import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

/**
 * 数据导出工具base
 * 封装指定路径文件导出信息
 */
public class ExportBase {
    /**
     * 获取导出文件信息
     *
     * @param fileName 导出文件名
     * @return PathData
     */
    protected PathData getPathData(String fileName) {
//        String fileSavePath = gunsProperties.getFileUploadPath();
//        String filePrefixPath = gunsProperties.getFilePrefixPath();
//        String path = fileSavePath + File.separator + fileName + jyddProperties.getImportRestrict();
//        String excelUrl = filePrefixPath + fileName + jyddProperties.getImportRestrict();
//        PathData pd = new PathData();
//        pd.setExcelUrl(excelUrl);
//        pd.setPath(path);
        return null;
    }

    /**
     * 导出文件信息
     */
    protected class PathData {
        /**
         * 文件保存路径
         */
        String path;
        /**
         * 导出文件url
         */
        String excelUrl;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getExcelUrl() {
            return excelUrl;
        }

        public void setExcelUrl(String excelUrl) {
            this.excelUrl = excelUrl;
        }
    }
}
