package com.zxd.interview.util;

import java.io.PrintStream;
import java.util.Map;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : map工具类
 * @Create on : 2021/5/13 09:59
 **/
public class MapUtils {

    /**
     * 获取字符串，如果元素或者map本身为null，返回null
     *
     * @param map map
     * @param key 键
     * @param <K> 泛型
     * @return
     */
    public static <K> String getString(Map<? super K, ?> map, K key) {
        return org.apache.commons.collections4.MapUtils.getString(map, key);
    }

    /**
     * 获取map当中的对象，如果map为null，返回null
     *
     * @param map map
     * @param key 键
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> V getObject(Map<? super K, V> map, K key) {
        return org.apache.commons.collections4.MapUtils.getObject(map, key);
    }

    /**
     * 获取map当中的数据：下面三种情况会做处理
     * 1. Boolean 直接返回包装类型
     * 2. "true"字符串会进行Boolean.valueOf()进行转化为包装类型
     * 3. 0会转为false的Boolean包装类型，大于0返回true的包装类型
     *
     * @param map map
     * @param key 键
     * @param <K>
     * @return
     */
    public static <K> Boolean getBoolean(Map<? super K, ?> map, K key) {
        return org.apache.commons.collections4.MapUtils.getBoolean(map, key);
    }


    /**
     * 从map当中获取int类型数据
     *
     * @param map map
     * @param key 键
     * @param <K>
     * @return
     */
    public static <K> Integer getInteger(final Map<? super K, ?> map, final K key) {
        return org.apache.commons.collections4.MapUtils.getInteger(map, key);
    }

    /**
     * 从map当中获取int类型数据
     *
     * @param map          map
     * @param key          键
     * @param defaultValue 默认值
     * @param <K>
     * @return
     */
    public static <K> Integer getInteger(final Map<? super K, ?> map, final K key, final Integer defaultValue) {
        return org.apache.commons.collections4.MapUtils.getInteger(map, key, defaultValue);
    }

    /**
     * 获取long类型元素
     * @param map map
     * @param key 键
     * @param defaultValue 默认值
     * @param <K>
     * @return
     */
    public static <K> Long getLong(final Map<? super K, ?> map, final K key, final Long defaultValue) {
        return org.apache.commons.collections4.MapUtils.getLong(map, key, defaultValue);
    }

    /**
     * 获取long类型元素
     * @param map
     * @param key
     * @param <K>
     * @return
     */
    public static <K> Long getLong(final Map<? super K, ?> map, final K key) {
        return org.apache.commons.collections4.MapUtils.getLong(map, key);
    }

    /**
     * 判断map是否为null，或者是否没有元素
     * @param map
     * @return
     */
    public static boolean isEmpty(final Map<?,?> map) {
        return org.apache.commons.collections4.MapUtils.isEmpty(map);
    }

    /**
     * 与isEmpty() 方法逻辑相反
     * @param map
     * @return
     */
    public static boolean isNotEmpty(final Map<?,?> map) {
        return !isEmpty(map);
    }

    /**
     * 将map元素转为Double 包装类型
     * @param map
     * @param key
     * @param defaultValue
     * @param <K>
     * @return
     */
    public static <K> Double getDouble(final Map<? super K, ?> map, final K key, final Double defaultValue) {
        return org.apache.commons.collections4.MapUtils.getDouble(map, key, defaultValue);
    }

    /**
     * 将map元素转为Double 包装类型
     * @param map
     * @param key
     * @param <K>
     * @return
     */
    public static <K> Double getDouble(final Map<? super K, ?> map, final K key){
        return org.apache.commons.collections4.MapUtils.getDouble(map, key);
    }

    /**
     * 优美打印
     * 案例：
     *
     *
     * 结果：
     *
     * @param out
     * @param label
     * @param map
     */
    public static void debugPrint(final PrintStream out, final Object label, final Map<?, ?> map) {
        org.apache.commons.collections4.MapUtils.debugPrint(out, label, map);
    }

    public static <K,V> Map<K,V> emptyIfNull(final Map<K,V> map) {
        return org.apache.commons.collections4.MapUtils.emptyIfNull(map);
    }
}
