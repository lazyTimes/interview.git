package com.zxd.interview.util.date;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author Administrator
 */
public class DateUtils {

    /**
     * 根据当前时间及分割月、日给出所属的年度
     *
     * @param date       需要判断的时间
     * @param splitMonth 分割月份
     * @param splitDay   分割日
     * @return
     */
    public static Integer getCurrentYear(Date date, Integer splitMonth, Integer splitDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH) + 1;
        Integer day = cal.get(Calendar.DAY_OF_MONTH);
        if (month < splitMonth || (month.equals(splitMonth) && day < splitDay)) {
            year--;
        }
        return year;
    }

    /**
     * 根据指定的日期得到指定年份的开始时间和结束时间
     *
     * @param year  当前年份
     * @param month 月份
     * @param day   日
     * @return
     */
    public static Date[] getStartTimeAndEndTime(Integer year, Integer month, Integer day) {
        Date[] result = new Date[2];
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day, NumberUtils.INTEGER_ZERO, NumberUtils.INTEGER_ZERO, NumberUtils.INTEGER_ZERO);
        result[0] = cal.getTime();
        cal.set(year + 1, month - 1, day, NumberUtils.INTEGER_ZERO, NumberUtils.INTEGER_ZERO, NumberUtils.INTEGER_ZERO);
        result[1] = cal.getTime();
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getStartTimeAndEndTime(2019, 9, 1)[0] + "\n" + getStartTimeAndEndTime(2019, 9, 1)[1]);
        System.out.println(getCurrentYear(getStartTimeAndEndTime(2019, 9, 1)[0], 9, 1));
    }
}
