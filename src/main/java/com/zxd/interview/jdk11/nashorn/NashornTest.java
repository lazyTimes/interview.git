package com.zxd.interview.jdk11.nashorn;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;

/**
 * Nashorn 引擎测试和使用
 * 1. 直接使用
 * 2. 运行文件当中的JS
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.jdk11.nashorn
 * @Description : Nashorn 引擎测试和使用
 * @Create on : 2023/3/14 10:54
 **/
public class NashornTest {

    public static void main(String[] args) throws ScriptException, FileNotFoundException {
        // 直接使用
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.eval("print('Hello World!');");

        // 文件中运行
        ScriptEngine engine2 = new ScriptEngineManager().getEngineByName("nashorn");
        engine2.eval(new FileReader("E:\\adongstack\\project\\selfUp\\interview\\src\\main\\resources\\static\\js\\hello.js"));

    }
}
