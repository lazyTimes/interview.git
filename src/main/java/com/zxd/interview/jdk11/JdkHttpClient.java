package com.zxd.interview.jdk11;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * JDK 官方实现的HTTP Client
 *
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.jdk11
 * @Description : JDK 官方实现的HTTP Client
 * @Create on : 2023/3/17 13:01
 **/
public class JdkHttpClient {
    public static void main(String[] args) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .uri(URI.create("https://cn.bing.com/?mkt=zh-CN"))
                .GET()
                .build();
        var client = HttpClient.newHttpClient();
// 同步
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
// 异步
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println);
    }/**
        截取部分内容：

     //]]></script><script type="text/rms">//<![CDATA[
     var sj_appHTML=function(n,t){var f,e,o,r,i,s,h;if(t&&n){var c="innerHTML",l="script",a="appendChild",v="length",y="src",p=sj_ce,u=p("div");if(u[c]="<br>"+t,f=u.childNodes,u.removeChild(f[0]),e=u.getElementsByTagName(l),e)for(o=0;o<e[v];o++)r=p(l),i=e[o],i&&(r.type=i.type=="module"||i.type=="importmap"?i.type:"text/javascript",s=i.getAttribute(y),s?(r.setAttribute(y,s),r.setAttribute("crossorigin","anonymous")):(r.text=i[c],r.setAttribute("data-bing-script","1")),i.parentNode.replaceChild(r,i));for(h=_d.createDocumentFragment();f[v];)h[a](f[0]);n[a](h)}};var sj_ajax=function(n,t){function c(){i[u]=h;i.abort&&i.abort()}function s(n,t){typeof n=="function"&&n(t,{request:i,appendTo:function(n){i&&sj_appHTML(n,i.responseText)}})}var r,i=sj_gx(),u="onreadystatechange",f,e=null,o,l=sb_st,a=sb_ct,h=function(){};if(!n||!i){s(r,!1);return}i.open("get",n,!0);t&&(r=t.callback,f=t.timeout,o=t.headers,Object.keys(o||{}).forEach(function(n){i.setRequestHeader(n,o[n])}),t.withCredentials!==undefined&&(i.withCredentials=t.withCredentials));i[u]=function(){if(i.readyState===4){var n=!1;e!==null&&a(e);i.status===200&&(n=!0,i[u]=h);s(r,n)}};sj_evt.bind("ajax.unload",c);i.send();f>0&&(e=l(function(){c();s(r,!1)},f))};
     //]]></script><script type="text/rms">//<![CDATA[
     Feedback.Bootstrap.InitializeFeedback({page:true},"sb_feedback",1,0,0);;
     //]]></script><script type="text/rms">//<![CDATA[
     _G!==undefined&&_G.EF!==undefined&&_G.EF.bmasynctrigger===1&&window.requestAnimationFrame!==undefined&&document.visibilityState!==undefined&&document.visibilityState==="visible"?requestAnimationFrame(function(){_G.EF.bmasynctrigger2===1?requestAnimationFrame(function(){BM.trigger()}):_G.EF.bmasynctrigger3===1?requestAnimationFrame(function(){setTimeout(function(){BM.trigger()},0)}):setTimeout(function(){BM.trigger()},0)}):BM.trigger();
     //]]></script><script type="text/rms">//<![CDATA[
     var ShareDialogConfig ={"shareDialogUrl":"/shared/sd/?IID=SERP.5062"};;
     //]]></script><script type="text/rms">//<![CDATA[
     var wlc=function(n,t,i){var u,f,r;n&&Identity&&(u=Identity.popupLoginUrls)&&(f=u.WindowsLiveId)&&Identity.wlProfile&&(r=_d.createElement("iframe"),r.style.display="none",r.src=f+"&checkda=1",r.setAttribute("data-priority","2"),_d.body.appendChild(r),i&&t&&t("SRCHHPGUSR","WTS",i,1,"/"))};
     //]]></script><script type="text/rms">//<![CDATA[
     (function() { var conditionalSignInParams ={"notifEnabled":false,"notifFetchAsync":false}; BingAtWork.ConditionalSignIn && BingAtWork.ConditionalSignIn.bindToConditionalSignIn && BingAtWork.ConditionalSignIn.bindToConditionalSignIn(conditionalSignInParams); })();;(function() { var config ={"url":"https%3a%2f%2flogin.microsoftonline.com%2fcommon%2foauth2%2fauthorize%3fclient_id%3d9ea1ad79-fdb6-4f9a-8bc3-2b70f96e34c7%26response_type%3did_token%2bcode%26nonce%3d648e2df3-a4ff-409f-b13d-db8643dc2f25%26redirect_uri%3dhttps%253a%252f%252fcn.bing.com%252forgid%252fidtoken%252fconditional%26scope%3dopenid%26response_mode%3dform_post%26instance_aware%3dtrue%26msafed%3d0%26prompt%3dnone%26state%3d%257b%2522ig%2522%253a%2522C07A8E1499B24C909CDD026F0C9DD75E%2522%257d","sandbox":"allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts","currentEpoch":"1679033039000"}; sj_evt.fire('ssoFrameExists', config); })();;
     //]]></script><script type="text/rms">//<![CDATA[
     (function() { var accLinkParams ={"accLinkRefreshEndPointUrl":"/orgid/acclink/refresh","isAadAuthenticated":false,"refreshAccountLinkInfoTimeInSeconds":3600,"refreshAccountLinkInfoRetryTimeInSeconds":600,"correlationId":"641402cfbd024ba79cdda8893943f2b2","hasActiveLinkedAccount":false}; AccountLink.init(accLinkParams); })();;
     //]]></script></div></body></html>

     Process finished with exit code 0

     */
}
