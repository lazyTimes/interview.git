package com.zxd.interview.jdk11.epsilongctest;


import java.util.ArrayList;
import java.util.List;

/**
 * JDK11 新的垃圾收集器 Epsilon
 * 需要配置启动参数：
 * -XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC
 */
public class EpsilonGcTest {

    public static void main(String[] args) {
        boolean flag = true;

        List<Garbage> list = new ArrayList<>();

        long count = 0;

        while (flag){
            list.add(new Garbage());
            if(list.size() == 100000 && count == 0){
                list.clear();
                count++;
            }
        }
        System.out.println("程序结束");
    }
}/**运行结果：
 Connected to the target VM, address: '127.0.0.1:13600', transport: 'socket'
 Terminating due to java.lang.OutOfMemoryError: Java heap space
 Disconnected from the target VM, address: '127.0.0.1:13600', transport: 'socket'
 */


class Garbage{
    int n = (int)(Math.random() * 100);

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this + "  : "+ n + " is dying");
    }
}
