package com.zxd.interview.jdk11.jfr;

import java.util.ArrayList;
import java.util.List;

/**
 * JFR 的测试程序
 * 需要使用JDK11以上的版本
 * <p>
 * src/main/java/com/zxd/interview/jfr/JfrTestApplication.java
 */
public class JfrTestApplication {

    public static void main(String[] args) {
        List<Object> items = new ArrayList<>(1);
        try {
            while (true) {
                items.add(new Object());
            }
        } catch (OutOfMemoryError e) {
            System.out.println(e.getMessage());
        }
        assert items.size() > 0;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
