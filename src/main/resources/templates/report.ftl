<?xml version="1.0" encoding="utf-8"?>
<?mso-application progid="Word.Document"?>

<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image4.png"/>
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/>
                <Relationship Id="rId7" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image3.png"/>
                <Relationship Id="rId12" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering" Target="numbering.xml"/>
                <Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image2.png"/>
                <Relationship Id="rId11" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/>
                <Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image1.png"/>
                <Relationship Id="rId10" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image6.png"/>
                <Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings" Target="webSettings.xml"/>
                <Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image5.png"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 wp14">
                <w:body>
                    <w:p w:rsidR="005F4B61" w:rsidRPr="005F4B61" w:rsidRDefault="008F7261" w:rsidP="005F4B61">
                        <w:pPr>
                            <w:pStyle w:val="2"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="300" w:after="150" w:line="480" w:lineRule="atLeast"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b w:val="0"/>
                                <w:bCs w:val="0"/>
                                <w:color w:val="676A6C"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:b w:val="0"/>
                                <w:bCs w:val="0"/>
                                <w:color w:val="676A6C"/>
                            </w:rPr>
                            <w:t>${schoolName}</w:t>
                        </w:r>
                        <w:r w:rsidR="005F4B61">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b w:val="0"/>
                                <w:bCs w:val="0"/>
                                <w:color w:val="676A6C"/>
                            </w:rPr>
                            <w:t>过程性教育质量监测反馈报告（四）</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>第一部分：幼儿园基本情况</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="008F7261" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="1"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title1}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>第二部分：督导情况</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>一、学习环境</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00EA45F4" w:rsidRDefault="00053751" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:pict>
                                <v:shapetype id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
                                    <v:stroke joinstyle="miter"/>
                                    <v:formulas>
                                        <v:f eqn="if lineDrawn pixelLineWidth 0"/>
                                        <v:f eqn="sum @0 1 0"/>
                                        <v:f eqn="sum 0 0 @1"/>
                                        <v:f eqn="prod @2 1 2"/>
                                        <v:f eqn="prod @3 21600 pixelWidth"/>
                                        <v:f eqn="prod @3 21600 pixelHeight"/>
                                        <v:f eqn="sum @0 0 1"/>
                                        <v:f eqn="prod @6 1 2"/>
                                        <v:f eqn="prod @7 21600 pixelWidth"/>
                                        <v:f eqn="sum @8 21600 0"/>
                                        <v:f eqn="prod @7 21600 pixelHeight"/>
                                        <v:f eqn="sum @10 21600 0"/>
                                    </v:formulas>
                                    <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
                                    <o:lock v:ext="edit" aspectratio="t"/>
                                </v:shapetype>
                                <v:shape id="_x0000_i1025" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId5" o:title="学习环境-各班分值"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00B843E0" w:rsidRPr="002748CC" w:rsidRDefault="00053751" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:pict>
                                <v:shape id="_x0000_i1026" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId6" o:title="学习环境-全园平均分"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（一）亮点推荐</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="005406CB" w:rsidRDefault="005406CB" w:rsidP="00B33E83">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="2"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title2}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（二）问题诊断</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="00E3765D" w:rsidRDefault="00E3765D" w:rsidP="00D56A3D">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="3"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title3}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>二、一日生活</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00AB6CA3" w:rsidRDefault="00AB6CA3" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:pict>
                                <v:shape id="_x0000_i1027" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId7" o:title="一日生活-各班分值"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00AB6CA3" w:rsidRPr="00AB6CA3" w:rsidRDefault="00AB6CA3" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:pict>
                                <v:shape id="_x0000_i1028" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId8" o:title="一日生活-全园平均分"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（一）亮点推荐</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="00A63686" w:rsidRDefault="00A63686" w:rsidP="001E5EBC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="4"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title4}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（二）问题诊断</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="00A63686" w:rsidRDefault="00A63686" w:rsidP="00840E78">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="5"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title5}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>三、学习活动</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00053751" w:rsidRDefault="00053751" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:pict>
                                <v:shape id="_x0000_i1029" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId9" o:title="学习活动-各班分值"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00053751" w:rsidRPr="002748CC" w:rsidRDefault="00053751" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="75" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:pict>
                                <v:shape id="_x0000_i1030" type="#_x0000_t75" style="width:415pt;height:97.15pt">
                                    <v:imagedata r:id="rId10" o:title="学习活动-全园平均分"/>
                                </v:shape>
                            </w:pict>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（一）亮点推荐</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="00A63686" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="6"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title6}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>（二）问题诊断</w:t>
                        </w:r>
                        <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                        <w:bookmarkEnd w:id="0"/>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="00A63686" w:rsidRDefault="00A63686" w:rsidP="0020184D">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="7"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:after="150"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title7}</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002748CC" w:rsidRPr="002748CC" w:rsidRDefault="002748CC" w:rsidP="002748CC">
                        <w:pPr>
                            <w:widowControl/>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:spacing w:before="100" w:beforeAutospacing="1" w:after="150"/>
                            <w:jc w:val="left"/>
                            <w:outlineLvl w:val="3"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="002748CC">
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica"/>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t>第三部分：发展建议</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002B1195" w:rsidRPr="002748CC" w:rsidRDefault="00A63686" w:rsidP="00E274D2">
                        <w:pPr>
                            <w:widowControl/>
                            <w:numPr>
                                <w:ilvl w:val="0"/>
                                <w:numId w:val="8"/>
                            </w:numPr>
                            <w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/>
                            <w:ind w:left="0"/>
                            <w:jc w:val="left"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Helvetica" w:eastAsia="宋体" w:hAnsi="Helvetica" w:cs="Helvetica" w:hint="eastAsia"/>
                                <w:color w:val="676A6C"/>
                                <w:kern w:val="0"/>
                                <w:sz w:val="20"/>
                                <w:szCs w:val="20"/>
                            </w:rPr>
                            <w:t>${title8}</w:t>
                        </w:r>
                    </w:p>
                    <w:sectPr w:rsidR="002B1195" w:rsidRPr="002748CC">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="4472C4"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="Calibri Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ ゴシック"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="Calibri" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ 明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main" name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}" vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"></thm15:themeFamily>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image5.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_5}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image4.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_4}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image3.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_3}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image2.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_2}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image1.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_1}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image6.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${base64_6}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main" mc:Ignorable="w14 w15">
                <w:zoom w:percent="110"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word" w:val="15"/>
                    <w:compatSetting w:name="overrideTableStyleFontSizeAndJustification" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="enableOpenTypeFeatures" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="doNotFlipMirrorIndents" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="differentiateMultirowTableHeaders" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="002748CC"/>
                    <w:rsid w:val="00053751"/>
                    <w:rsid w:val="002748CC"/>
                    <w:rsid w:val="002B1195"/>
                    <w:rsid w:val="005406CB"/>
                    <w:rsid w:val="005F4B61"/>
                    <w:rsid w:val="008F7261"/>
                    <w:rsid w:val="00A63686"/>
                    <w:rsid w:val="00AB6CA3"/>
                    <w:rsid w:val="00B843E0"/>
                    <w:rsid w:val="00E3765D"/>
                    <w:rsid w:val="00EA45F4"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1" w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5" w:accent6="accent6" w:hyperlink="hyperlink" w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="1026"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{70FD6C01-1D4E-4063-BD68-F2669BA19F0E}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" mc:Ignorable="w14 w15">
                <w:divs>
                    <w:div w:id="643464999">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                        <w:divsChild>
                            <w:div w:id="1789427113">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="797721931">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="1472751917">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="1008141858">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="2013363787">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="467821475">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="684133771">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="1030032226">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="389575347">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="129833459">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="1654720442">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="1541432206">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="44449430">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="1338653997">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                            <w:div w:id="1057238003">
                                <w:marLeft w:val="0"/>
                                <w:marRight w:val="0"/>
                                <w:marTop w:val="0"/>
                                <w:marBottom w:val="225"/>
                                <w:divBdr>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:divBdr>
                                <w:divsChild>
                                    <w:div w:id="67847334">
                                        <w:marLeft w:val="0"/>
                                        <w:marRight w:val="0"/>
                                        <w:marTop w:val="0"/>
                                        <w:marBottom w:val="0"/>
                                        <w:divBdr>
                                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                        </w:divBdr>
                                    </w:div>
                                </w:divsChild>
                            </w:div>
                        </w:divsChild>
                    </w:div>
                    <w:div w:id="749347941">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                </w:divs>
                <w:optimizeForBrowser/>
                <w:allowPNG/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" mc:Ignorable="w14 w15">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia" w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0" w:defQFormat="0" w:count="371">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="39"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="2">
                    <w:name w:val="heading 2"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:link w:val="2Char"/>
                    <w:uiPriority w:val="9"/>
                    <w:unhideWhenUsed/>
                    <w:qFormat/>
                    <w:rsid w:val="005F4B61"/>
                    <w:pPr>
                        <w:keepNext/>
                        <w:keepLines/>
                        <w:spacing w:before="260" w:after="260" w:line="416" w:lineRule="auto"/>
                        <w:outlineLvl w:val="1"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia" w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi"/>
                        <w:b/>
                        <w:bCs/>
                        <w:sz w:val="32"/>
                        <w:szCs w:val="32"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="4">
                    <w:name w:val="heading 4"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="4Char"/>
                    <w:uiPriority w:val="9"/>
                    <w:qFormat/>
                    <w:rsid w:val="002748CC"/>
                    <w:pPr>
                        <w:widowControl/>
                        <w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/>
                        <w:jc w:val="left"/>
                        <w:outlineLvl w:val="3"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="0"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="4Char">
                    <w:name w:val="标题 4 Char"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="4"/>
                    <w:uiPriority w:val="9"/>
                    <w:rsid w:val="002748CC"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="0"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="Normal (Web)"/>
                    <w:basedOn w:val="a"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="002748CC"/>
                    <w:pPr>
                        <w:widowControl/>
                        <w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                        <w:kern w:val="0"/>
                        <w:sz w:val="24"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="2Char">
                    <w:name w:val="标题 2 Char"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="2"/>
                    <w:uiPriority w:val="9"/>
                    <w:rsid w:val="005F4B61"/>
                    <w:rPr>
                        <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia" w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi"/>
                        <w:b/>
                        <w:bCs/>
                        <w:sz w:val="32"/>
                        <w:szCs w:val="32"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/numbering.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
        <pkg:xmlData>
            <w:numbering xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 wp14">
                <w:abstractNum w:abstractNumId="0">
                    <w:nsid w:val="05301F32"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="F1B0AE42"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="1">
                    <w:nsid w:val="0C29227F"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="2B361E34"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="2">
                    <w:nsid w:val="289231F5"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="601C9B94"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="3">
                    <w:nsid w:val="2E1A4E81"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="D24EBB4C"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="4">
                    <w:nsid w:val="3E17374B"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="FCB677F6"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="5">
                    <w:nsid w:val="54B01BA9"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="B36E1BAA"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="6">
                    <w:nsid w:val="703858DC"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="74B4B68A"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="7">
                    <w:nsid w:val="72E7557D"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="CCC42A76"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="720"/>
                            </w:tabs>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val="o"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="1440"/>
                            </w:tabs>
                            <w:ind w:left="1440" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2160"/>
                            </w:tabs>
                            <w:ind w:left="2160" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="2880"/>
                            </w:tabs>
                            <w:ind w:left="2880" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="3600"/>
                            </w:tabs>
                            <w:ind w:left="3600" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="4320"/>
                            </w:tabs>
                            <w:ind w:left="4320" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5040"/>
                            </w:tabs>
                            <w:ind w:left="5040" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="5760"/>
                            </w:tabs>
                            <w:ind w:left="5760" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="bullet"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:tabs>
                                <w:tab w:val="num" w:pos="6480"/>
                            </w:tabs>
                            <w:ind w:left="6480" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:lvl>
                </w:abstractNum>
                <w:num w:numId="1">
                    <w:abstractNumId w:val="7"/>
                </w:num>
                <w:num w:numId="2">
                    <w:abstractNumId w:val="6"/>
                </w:num>
                <w:num w:numId="3">
                    <w:abstractNumId w:val="1"/>
                </w:num>
                <w:num w:numId="4">
                    <w:abstractNumId w:val="4"/>
                </w:num>
                <w:num w:numId="5">
                    <w:abstractNumId w:val="2"/>
                </w:num>
                <w:num w:numId="6">
                    <w:abstractNumId w:val="5"/>
                </w:num>
                <w:num w:numId="7">
                    <w:abstractNumId w:val="0"/>
                </w:num>
                <w:num w:numId="8">
                    <w:abstractNumId w:val="3"/>
                </w:num>
            </w:numbering>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>admin</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>admin</cp:lastModifiedBy>
                <cp:revision>9</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2020-01-09T09:02:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2020-01-09T09:36:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" mc:Ignorable="w14 w15">
                <w:font w:name="Symbol">
                    <w:panose1 w:val="05050102010706020507"/>
                    <w:charset w:val="02"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000000" w:usb1="10000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Courier New">
                    <w:panose1 w:val="02070309020205020404"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="modern"/>
                    <w:notTrueType/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="00000003" w:usb1="00000000" w:usb2="00000000" w:usb3="00000000" w:csb0="00000001" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Wingdings">
                    <w:panose1 w:val="05000000000000000000"/>
                    <w:charset w:val="02"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000000" w:usb1="10000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri Light">
                    <w:panose1 w:val="020F0302020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002AFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Helvetica">
                    <w:panose1 w:val="020B0604020202020204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>9</TotalTime>
                <Pages>2</Pages>
                <Words>33</Words>
                <Characters>193</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>1</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>225</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>15.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>
