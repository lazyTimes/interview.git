var v1 = new Vue({
    el: '#vue1',
    data: {
        counter: 0,
        student: {
            test0:'',
            test1:'',
            test2:'',
            test3:'',
            test4:'',
            test5:'',
            test6:'',
            wordName: '',

        }
    },
    methods: {
        test: function () {
            console.log(this.counter);
        },
        submit() {
            console.log(this.student);
            var url = '/quality/exportword';
            var formData = JSON.stringify(this.student); // this指向这个VUE实例 data默认绑定在实例下的。所以直接this.student就是要提交的数据
            this.$http.post(url, formData).then(function (data) {
                console.log(data);
                let blob = new Blob([data.data],{ type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document;charset=utf-8'});
                let objectUrl = URL.createObjectURL(blob);
                window.location.href = objectUrl;
            }).catch(function () {
                console.log('test');
            });
        }
    }
})