package com.zxd.interview.spring;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * 它的作用是实现**动态替换某bean的某个方法**的效果，可以实现无需修改代码，改变执行逻辑的功能
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.spring
 * @Description : 它的作用是实现**动态替换某bean的某个方法**的效果，可以实现无需修改代码，改变执行逻辑的功能
 * @Create on : 2023/8/29 07:06
 **/
public class ParentTestMethodReplacer implements MethodReplacer {

    /**
     * reimplement 就是要执行的新方法，在这里编写新逻辑
     * @description reimplement 就是要执行的新方法，在这里编写新逻辑
     * @param obj 通过cjlib动态生成的被替换方法所在bean的子类。
     * @param: method 被替换的方法对象。
     * @param: args 行方法需要的参数。
     * @return
     * @author Xander
     * @date 2023/8/29 7:07
     */
    @Override
    public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
        System.out.println("Hello! from new method");
        return null;
    }
}
