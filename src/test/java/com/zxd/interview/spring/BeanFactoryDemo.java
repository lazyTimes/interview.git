package com.zxd.interview.spring;

import com.zxd.interview.sort.AddTest;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.spring
 * @Description : TODO
 * @Create on : 2023/7/26 10:01
 **/
public class BeanFactoryDemo {

    public static void main(String[] args) {
        XmlBeanFactory xmlBeanFactory = new XmlBeanFactory(new ClassPathResource("springconfig.xml"));
        AddTest student = (AddTest) xmlBeanFactory.getBean("student");
        System.out.println(student);
    }
}
