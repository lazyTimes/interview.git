package com.zxd.interview;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview
 * @Description : 日期时间获取
 * @Create on : 2023/5/25 15:02
 **/
public class CalenderTest {


    private static volatile Calendar CURRENT_TIME =  new GregorianCalendar();

    public static void main(String[] args) {
        Date time = CURRENT_TIME.getTime();
        System.err.println(time);
    }
}
