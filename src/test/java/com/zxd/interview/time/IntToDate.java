package com.zxd.interview.time;

import com.zxd.interview.util.date.DateHelper;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.time
 * @Description : 将整型的数字转为时间
 * @Create on : 2021/1/18 10:30
 **/
public class IntToDate {

    private String value ="0.1";
    //  + 0.813423 * OXR + 0.024447
    private String value2 ="  0.182146  * BOC +  0.813423 * OXR + 0.000001 ";

    /**
     * 0 开头的6位小数
     */
    private static final Pattern PATTERN = Pattern.compile("^([0]{1})(\\.\\d{6})?$");
    // 0.182146 * BOC + 0.813423 * OXR + 0.024447
    private static final Pattern PATTERN3 = Pattern.compile("^(\\s)*([0]{1})(\\.\\d{6})?(\\s)*(\\*{1})(\\s)*BOC(\\s)*(\\+)(\\s)*([0]{1})(\\.\\d{6})?(\\s)*(\\*{1})(\\s)*OXR(\\s)*[+,-](\\s)*([0]{1})(\\.\\d{6})?(\\s)*$");
    private static final Pattern PATTERN_CUSTOM = Pattern.compile("^(([0]{1}\\d*)|([0]{1}))(\\.(\\d){6})?$");

    /**
     * 公式校验
     */
    @Test
    public void formula(){
        boolean matches = PATTERN3.matcher(value2).matches();
        Assert.isTrue(matches);
    }

    @Test
    public void test() throws ParseException {
        LocalDateTime now = LocalDateTime.now();
        Date date = new Date();
        date.setHours(15);
        date.setMinutes(0);
        date.setSeconds(0);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.HOUR_OF_DAY, 8);
        instance.set(Calendar.SECOND, 0);
        instance.set(Calendar.MINUTE, 0);
        System.err.println(format.format(instance.getTime()));
        String string = DateHelper.getString(date, "HH:mm:ss");
//        System.err.println(string);
//        System.err.println(now);
    }

    @Test
    public void customCalcu(){
        String bocRate = "0.062263";
        String oxrRate = "0.764213";
        String str = "0.182146 * BOC + 0.813423 * OXR + 0.024447";
        String[] split = str.split("\\+");
        String[] bocArr = split[0].split("\\*");
        String[] oxrArr = split[1].split("\\*");
        String c = split[2];
        // BOC汇率系数
        BigDecimal bocCoeff = new BigDecimal(bocArr[0].trim());
        // BOC实时汇率
        BigDecimal bocRateDecimal = new BigDecimal(bocRate.trim());
        // OXR汇率系数
        BigDecimal oxrCoeff = new BigDecimal(oxrArr[0].trim());
        // OXR实时汇率
        BigDecimal oxrRateDecimal = new BigDecimal(oxrRate.trim());
        // 常数 C
        BigDecimal cRate = new BigDecimal(c.trim());
        BigDecimal multiply1 = bocCoeff.multiply(bocRateDecimal);
        BigDecimal multiply2 = oxrCoeff.multiply(oxrRateDecimal);
        BigDecimal result = multiply1.add(multiply2).add(cRate);
//        System.err.println(result.toString());

    }


    @Test
    public void checkTest(){
        // "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"

        check("0.182146 * 0.2545 + 0.813423 * 0.1111 - 5", "0.062263", "0.764213");
        boolean b = checkRateValueAccess(value);
        System.err.println(b);
    }

    /**
     * 检查汇率是否符合要求
     * @return
     */
    public boolean checkRateValueAccess(String rate){
        System.err.println(rate);
        return PATTERN.matcher(rate).matches();
    }


    private Object check(String str, String  bocRate, String  oxrRate){
        String[] split = str.split("\\+");
        // 如果是x + x + x
        BigDecimal result = new BigDecimal("0");
       if(split.length >= 3){
           String[] bocArr = split[0].split("\\*");
           String[] oxrArr = split[1].split("\\*");
           String c = split[2];
           // BOC汇率系数
           BigDecimal bocCoeff = new BigDecimal(bocArr[0].trim());
           // BOC实时汇率
           BigDecimal bocRateDecimal = new BigDecimal(bocRate.trim());
           // OXR汇率系数
           BigDecimal oxrCoeff = new BigDecimal(oxrArr[0].trim());
           // OXR实时汇率
           BigDecimal oxrRateDecimal = new BigDecimal(oxrRate.trim());
           // 常数 C
           BigDecimal cRate = new BigDecimal(c.trim());
           BigDecimal multiply1 = bocCoeff.multiply(bocRateDecimal);
           BigDecimal multiply2 = oxrCoeff.multiply(oxrRateDecimal);
           result = multiply1.add(multiply2).add(cRate);
       }else{
           // 如果是 x + x - x
           String spli1 = split[0];
           String[] splitMulti = spli1.split("\\*");
           // BOC汇率系数
           BigDecimal bocCoeff = new BigDecimal(splitMulti[0].trim());
           // BOC实时汇率
           BigDecimal bocRateDecimal = new BigDecimal(splitMulti[1].trim());
           String spli2 = split[1];
           // 第二部分按照 - 号切割
           String[] split3 = spli2.split("-");
           String left = split3[0];
           String right = split3[1];
           // 左边是oxr相关的系数
           String[] split1 = left.split("\\*");
           // OXR汇率系数
           BigDecimal oxrCoeff = new BigDecimal(split1[0].trim());
           // OXR实时汇率
           BigDecimal oxrRateDecimal = new BigDecimal(split1[1].trim());
           // 右边是被减数
           BigDecimal cRate = new BigDecimal(right.trim());
           BigDecimal multiply1 = bocCoeff.multiply(bocRateDecimal);
           BigDecimal multiply2 = oxrCoeff.multiply(oxrRateDecimal);
           result = multiply1.add(multiply2).subtract(cRate);
       }
        return result;
    }
}
