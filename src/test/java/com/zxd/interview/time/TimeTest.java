package com.zxd.interview.time;

import java.time.ZoneId;
import java.util.TimeZone;

/**
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.time
 * @Description : 时间测试
 * @Create on : 2023/10/22 16:38
 **/
public class TimeTest {

    public static void main(String[] args) {
        // 这里的CST指的是美国中部时间，
        TimeZone tz = TimeZone.getTimeZone("CST");
        System.out.println("tz => "+ tz);// 可以看到偏移量是offset=-21600000，-21600000微秒=-6小时，所以这里的CST指美国

    // 建议创建 TimeZone 用 ZoneId，因为ZoneId 不允许 CST、JST 这种简称，能提前预防进坑，如下
    // ZoneId zoneId = ZoneId.of("CST");// 抛异常
        ZoneId zoneId = ZoneId.of("GMT+8");// 明确指定，是OK的，或者 "区域/城市" 的写法如 Asia/Shanghai
        TimeZone tz1 = TimeZone.getTimeZone(zoneId);
        System.out.println("tz1 => "+ tz1);
    }/**运行结果：
     tz => sun.util.calendar.ZoneInfo[id="CST",offset=-21600000,dstSavings=3600000,useDaylight=true,transitions=235,lastRule=java.util.SimpleTimeZone[id=CST,offset=-21600000,dstSavings=3600000,useDaylight=true,startYear=0,startMode=3,startMonth=2,startDay=8,startDayOfWeek=1,startTime=7200000,startTimeMode=0,endMode=3,endMonth=10,endDay=1,endDayOfWeek=1,endTime=7200000,endTimeMode=0]]

     tz1 => sun.util.calendar.ZoneInfo[id="GMT+08:00",offset=28800000,dstSavings=0,useDaylight=false,transitions=0,lastRule=null]
      */
}
