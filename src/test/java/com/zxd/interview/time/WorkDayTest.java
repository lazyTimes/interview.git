package com.zxd.interview.time;

import com.zxd.interview.util.date.DateHelper;
import com.zxd.interview.workdayfind.bo.CalendarDataProcessBo;
import com.zxd.interview.workdayfind.util.WorkDayFindUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * T+N 和 T-N数据获取测试
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.time
 * @Description : 工作日获取测试
 * @Create on : 2022/6/17 22:14
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class WorkDayTest {

    @Autowired
    private WorkDayFindUtil workDayFindUtil;

    @Test
    public void testNextDay() throws IllegalAccessException {
        CalendarDataProcessBo build = CalendarDataProcessBo.builder()
                .calendarDayDtos(workDayFindUtil.queryCalendarList(DateHelper.getNowByNew("2022-06-06")))
                .extDayOfWorkDayCount(0)
                .bankSettleCycle("T+10").build();
        String nextDayByCalendarList = workDayFindUtil.findNextDayByCalendarList(build);
        System.err.println(nextDayByCalendarList);
        CalendarDataProcessBo build2 = CalendarDataProcessBo.builder()
                .calendarDayDtos(workDayFindUtil.queryCalendarList(DateHelper.getNowByNew("2022-06-18")))
                .extDayOfWorkDayCount(0)
                .bankSettleCycle("T-10").build();
        String prevDayByCalendarList = workDayFindUtil.findPrevDayByCalendarList(build2);
        System.err.println(prevDayByCalendarList);
    }




}
