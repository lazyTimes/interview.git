package com.zxd.interview.sort;

/**
 * 父类
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.sort
 * @Description : 父类
 * @Create on : 2023/8/28 17:39
 **/
public abstract class ParentTest {

    /**
     * 调用方法
     * @description 调用方法
     * @param
     * @return void
     * @author xander
     * @date 2023/8/28 17:42
     */
    public void testMethod(){
        ParentTest child = getChild();
        System.out.println("testMethod() => " + child);
    }

    public abstract ParentTest getChild();
}
