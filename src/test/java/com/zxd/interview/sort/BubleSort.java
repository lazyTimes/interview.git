package com.zxd.interview.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubleSort {

    public static void main(String[] args) {
        int[] arr = new int[]{1, 4, 5, 3, 2, 0};
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
