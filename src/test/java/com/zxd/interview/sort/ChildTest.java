package com.zxd.interview.sort;

/**
 * 子类实现
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.sort
 * @Description : 子类实现
 * @Create on : 2023/8/28 17:39
 **/
public class ChildTest extends ParentTest{

    @Override
    public void testMethod() {
        System.out.println("调用子类方法");
    }

    @Override
    public ParentTest getChild() {
        return new ChildTest();
    }
}
