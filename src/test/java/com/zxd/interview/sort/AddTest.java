package com.zxd.interview.sort;

public class AddTest {

    public static int add(int a, int b){
        return a + b;
    }

    public static int add(int a, int b, int c){
        return add(a, b) + c;
    }


}
