package com.zxd.interview;

import com.zxd.interview.transactions.ProgrammaticTrasaction;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class InterviewApplicationTests {
	@Data
	static class TestPojo{
		@NotBlank
		private String name;
		@NotNull
		private String password;
	}
	@Validated
	interface TestValid{

		void valid(List<@Valid TestPojo> test);
	}
	@Autowired
	TestValid valid;
	@TestConfiguration
	static class validateBean {
		@Bean
		TestValid validBean(){
			return System.out::println;
		}
	}
	@Test
	public void validPojo(){
		valid.valid(Arrays.asList(new TestPojo(), new TestPojo()));
	}

	@Test
	void contextLoads() {
		String str = "hello,world";
		System.out.println(str.split(","));
	}


	@Autowired
	private ProgrammaticTrasaction programmaticTrasaction;
	/**
	 * 使用TransactionTemplate 进行事务处理
	 */
	@Test
	public void transactionTemplateTest1(){
		programmaticTrasaction.insert();
	}

	/**
	 * 使用TransactionTemplate 进行事务处理
	 */
	@Test
	public void transactionTemplateTest2(){
		programmaticTrasaction.update();
	}

}
