package com.zxd.interview.jvm;

import java.io.*;
import java.util.Properties;

/**
 * jvm专栏学习，第一周：
 * 目标：通过一个Java程序了解JVM运行的全过程
 */
public class OneWeek {

    private static final Properties properties = new Properties();

    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = OneWeek.class.getClassLoader().getResourceAsStream("app.properties");
        properties.load(resourceAsStream);
        System.out.println("load properties user.name = " + properties.getProperty("user.name"));
    }/*运行结果：
    load properties user.name = 123

    #app.properties:
    user.name=123
    */


}
