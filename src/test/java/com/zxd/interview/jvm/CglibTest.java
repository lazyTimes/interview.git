package com.zxd.interview.jvm;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibTest {

    static class Man {

        public void run() {
            System.out.println("走路中。。。。。");
        }
    }

    static class OldMan extends Man {

        @Override
        public void run() {
//            System.out.println("走的很慢很慢。。。。。。。");
        }
    }

    public static void main(String[] args) {
        int count = 0;
        work(count);
    }

    public static void work(int count){
        System.out.println("一共运行了:"+ (count++) +"次");
        work(count);
    }/*运行结果：
    一共运行了:6471次
java.lang.StackOverflowError
	at sun.nio.cs.UTF_8$Encoder.encodeLoop(UTF_8.java:691)
	at java.nio.charset.CharsetEncoder.encode(CharsetEncoder.java:579)
	at sun.nio.cs.StreamEncoder.implWrite(StreamEncoder.java:271)
	at sun.nio.cs.StreamEncoder.write(StreamEncoder.java:125)
	at java.io.OutputStreamWriter.write(OutputStreamWriter.java:207)
	at java.io.BufferedWriter.flushBuffer(BufferedWriter.java:129)
	at java.io.PrintStream.write(PrintStream.java:526)
	at java.io.PrintStream.print(PrintStream.java:669)
	at java.io.PrintStream.println(PrintStream.java:806)
    */

    private static void test1() {
        int count = 0;
        while (true) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(OldMan.class);
            enhancer.setUseCache(true);
            enhancer.setCallback(new MethodInterceptor() {
                @Override
                public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

                    if (method.getName().equalsIgnoreCase("run")) {
//                        System.out.println("遇到红绿灯，等待.....");
                        return methodProxy.invokeSuper(o, objects);
                    }

                    return methodProxy.invoke(o, objects);
                }
            });

            Man man = (Man) enhancer.create();
            man.run();
//            Man oldMan = (OldMan) enhancer.create();
//            oldMan.run();
            System.out.println("构建了"+ (count++)+"个对象");
        }
    }
}
