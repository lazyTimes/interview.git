package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 转化工具类测试
 * @Create on : 2021/5/14 11:40
 **/
class ConvertUtilsTest {

    @Test
    void strToUnicode() throws Exception {
        String s = ConvertUtils.strToUnicode("自贸通");
        System.out.println("strToUnicode() => " + s);
    }

    @Test
    void unicodeToStr() {
        System.out.println("unicodeToStr() => " + ConvertUtils.unicodeToStr("\\u81ea\\u8d38\\u901a"));
    }
}