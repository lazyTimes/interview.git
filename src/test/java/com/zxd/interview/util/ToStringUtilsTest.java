package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : toStringUtils 工具单元测试
 * @Create on : 2021/5/13 14:17
 **/
class ToStringUtilsTest {

    @Test
    void objToString() {
    }

    @Test
    void arrToString() {
    }

    @Test
    void mapToString() {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("ss", 2);
        objectObjectHashMap.put("22", 122);
        objectObjectHashMap.put("s4", 123);
        objectObjectHashMap.put("s5", 123);
        String s = ToStringUtils.mapToString(objectObjectHashMap);
        System.out.println(s);
    }

    class User implements java.io.Serializable {
        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }


    @Test
    void test1() {
        User user = new User();
        user.setAge( 22);
        user.setName("dadsadsad");
        System.out.println("ToStringUtils.objToString(user) => "+ ToStringUtils.objToString(user));
    }
}