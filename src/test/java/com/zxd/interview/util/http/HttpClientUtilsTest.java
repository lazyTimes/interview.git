package com.zxd.interview.util.http;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util.http
 * @Description : HttpClientUtils 请求工具类
 * @Create on : 2021/5/18 14:25
 **/
class HttpClientUtilsTest {

    @Test
    void doGet() throws Exception {
        HttpClientResult httpClientResult = HttpClientUtils.doGet("http://www.baidu.com");
        System.out.println(httpClientResult);
    }

    @Test
    void doPost() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("name", "233");
        map.put("age", "233");
        map.put("gener", "233");
        HttpClientResult httpClientResult = HttpClientUtils.doPost("http://www.baidu.com", map);
        System.out.println(httpClientResult);
    }

    @Test
    void testDoPost() throws Exception{
        Map<String, String> map = new HashMap<>();
        map.put("name", "233");
        map.put("age", "233");
        map.put("gener", "233");
        Map<String, String> head = new HashMap<>();
        head.put("Content-Type", "application/json");
        HttpClientResult httpClientResult = HttpClientUtils.doPost("http://www.baidu.com", head, map);
        System.out.println(httpClientResult);
    }

    @Test
    void testDoPost1() {

    }

    @Test
    void doPut() {
    }

    @Test
    void testDoPut() {
    }

    @Test
    void doDelete() {
    }

    @Test
    void testDoDelete() {
    }

    @Test
    void packageHeader() {
    }

    @Test
    void packageParam() {
    }

    @Test
    void getHttpClientResult() {
    }

    @Test
    void release() {
    }
}