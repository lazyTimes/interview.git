package com.zxd.interview.util;

import com.zxd.interview.util.date.DateHelper;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 日期帮助类单元测试
 * @Create on : 2021/5/7 10:50
 **/
class DateHelperTest {

    @Test
    void getString() {
        String string = DateHelper.getString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println("getString() => " + string);
    }

    @Test
    void testGetString() {
        String string = DateHelper.getString(new Date(), "yyyy-MM-dd HH:mm:ss:SSS");
        System.out.println("getString() => " + string);
    }

    @Test
    void testGetString1() {
        String string = DateHelper.getString("2021-05-07 11:31:51:166", "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd");
        System.out.println("getString() => " + string);
    }

    @Test
    void getNow() {
        String now = DateHelper.getNow("yyyy");
        System.out.println("getNow() => " + now);
    }

    @Test
    void getNowYear() {
        int nowYear = DateHelper.getNowYear();
        System.out.println("getNowYear() => " + nowYear);
    }

    @Test
    void getNowYearStart() {
        Date nowYearStart = DateHelper.getNowYearStart();
        System.out.println("getNowYearStart() => " + nowYearStart);
        System.out.println("getNowYearStart() => getString() => " + DateHelper.getString(nowYearStart, "yyyy-MM-dd"));
    }

    @Test
    void getNowYearEnd() {
        Date yearEnd = DateHelper.getYearEnd(2020);
        System.out.println("getNowYearEnd() => " + yearEnd);
        System.out.println("getNowYearEnd() => getString() =>  " + DateHelper.getString(yearEnd, "yyyy-MM-dd"));
    }

    @Test
    void getNowMonth() {
        int nowMonth = DateHelper.getNowMonth();
        System.out.println("getNowMonth() => " + nowMonth);
    }

    @Test
    void getNowDay() {
        int nowDay = DateHelper.getNowDay();
        System.out.println("getNowDay() => " + nowDay);
    }

    @Test
    void getTime() {
        long time = DateHelper.getTime("1970-06-06");
        System.out.println("getTime() => " + time);
        long time1 = DateHelper.getTime("1970-06-06", "yyyy-mm-dd");
        System.out.println("getTime() => " + time1);
    }

    @Test
    void getDate() {
        Date date = DateHelper.getDate("2020-02-22", DateHelper.yyyyMMdd);
        System.out.println("getDate() => " + date);
    }

    @Test
    void isBetween() {
        boolean between = DateHelper.isBetween(new Date(), new Date(), new Date());
        System.out.println("isBetween() => " + between);
    }

    @Test
    void testIsBetween() {
        boolean between = DateHelper.isBetween(System.currentTimeMillis(), System.currentTimeMillis(), System.currentTimeMillis());
        System.out.println("testIsBetween() => " + between);
    }

    @Test
    void getBetween() {
        long between = DateHelper.getBetween(System.currentTimeMillis(), System.currentTimeMillis() - 10000, TimeUnit.HOURS);
        System.out.println("getBetween() => " + between);
    }

    @Test
    void getYearStart() {
        Date yearStart = DateHelper.getYearStart(2020);
        System.out.println("getYearStart() => " + yearStart);
    }

    @Test
    void getYearEnd() {
        Date yearStart = DateHelper.getYearEnd(2020);
        System.out.println("getYearEnd() => " + yearStart);
    }

    @Test
    void getMonthStart() {
        Date yearStart = DateHelper.getMonthStart(2020, 5);
        System.out.println("getMonthStart() => " + yearStart);
    }

    @Test
    void getMonthEnd() {
        Date monthEnd = DateHelper.getMonthEnd(2020, 5);
        System.out.println("getMonthEnd() => " + monthEnd);
    }

    @Test
    void testGetTime() {
        long time = DateHelper.getTime("2020/5/2");
        System.out.println("getTime() => " + time);
    }

    @Test
    void getCurrentYear() {
        int splitCurrentYear = DateHelper.getSplitCurrentYear(6);
        System.out.println("getSplitCurrentYear() => " + splitCurrentYear);
    }

    @Test
    void callSchoolYear() {
        int i = DateHelper.callSchoolYear();
        System.out.println("callSchoolYear() => " + i);
    }

    @Test
    void getYearsBefore() {
        List<Integer> yearsBefore = DateHelper.getYearsBefore(5);
        System.out.println("getYearsBefore() => " + yearsBefore);
    }

    @Test
    void getAllMonths() {
        System.out.println("getAllMonths() => " + DateHelper.getAllMonths());
    }

    @Test
    void isAfter() {
        System.out.println("isAfter() 1 => " + DateHelper.isAfter("2021-05-01"));
        System.out.println("isAfter() 2 \"2021-05-01\" DateHelper.yyyyMMdd => " + DateHelper.isAfter("2021-05-01", DateHelper.yyyyMMdd));
        System.out.println("isAfter() 3 \"2021-05-01\" \"2021-05-02\" => " + DateHelper.isAfter("2021-05-01", "2021-05-02", DateHelper.yyyyMMdd));
    }

    @Test
    void isBefore() {
        System.out.println("isBefore() 1 => " + DateHelper.isBefore("2021-05-01"));
        System.out.println("isBefore() 2 DateHelper.yyyyMMdd => " + DateHelper.isBefore("2021-05-01", DateHelper.yyyyMMdd));
        System.out.println("isBefore() 3 DateHelper.yyyyMMdd \"2021-05-01\" \"2021-05-05\" => " + DateHelper.isBefore("2021-05-01", "2021-05-05", DateHelper.yyyyMMdd));
    }


    @Test
    void isBeforeOrEq() {
        System.out.println("isBeforeOrEq() 1 => " + DateHelper.isBeforeOrEq("2021-05-01", "2021-05-05", DateHelper.yyyyMMdd));
    }

    @Test
    void isAfterOrEq() {
        System.out.println("isAfterOrEq() 1 => " + DateHelper.isAfterOrEq("2021-05-01", "2021-05-05", DateHelper.yyyyMMdd));
    }

    @Test
    void minuDayByFormat() {
        String s = DateHelper.minuDayByFormat(new Date(), 4, DateHelper.yyyyMMdd);
        String s2 = DateHelper.minuDayByFormat("2021-05-22", 4, DateHelper.yyyyMMdd);
        System.out.println("minuDayByFormat() 1 => " + s);
        System.out.println("minuDayByFormat() 2 => " + s2);
    }

    @Test
    void plusDayByFormat() {
        String s = DateHelper.plusDayByFormat(new Date(), 4, DateHelper.yyyyMMdd);
        String s2 = DateHelper.plusDayByFormat("2021-05-22", 4, DateHelper.yyyyMMdd);
        System.out.println("plusDayByFormat() 1 => " + s);
        System.out.println("plusDayByFormat() 2 => " + s2);
    }

    @Test
    void getNowByNew() {
        System.out.println("getNowByNew() => " + DateHelper.getNowByNew(DateHelper.yyyy_MM_dd));
        ;
    }

    @Test
    void getDayTime() {
        System.out.println("getNowByNew() => " + DateHelper.getNowByNew(DateHelper.yyyy_MM_dd));
        ;
    }

    @Test
    void isEq() {
        System.out.println("isEq() 2021-05-04 2021-05-04 DateHelper.yyyyMMdd => " + DateHelper.isEq("2021-05-04", "2021-05-04", DateHelper.yyyyMMdd));
        ;
    }

    @Test
    void getDayOfWeek() {
        System.out.println("getDayOfWeek() 2021-05-04 DateHelper.yyyyMMdd => " + DateHelper.getDayOfWeek("2021-05-04", DateHelper.yyyyMMdd));
        ;
        System.out.println("getDayOfWeek() 2021-05-04 DateHelper.yyyyMMdd => " + DateHelper.getDayOfWeek(new Date(), DateHelper.yyyyMMdd));
        ;
    }


    @Test
    void getMonthTime() {
        int monthTime = DateHelper.getMonthTimeInt(new Date(), DateHelper.yyyyMMdd);
        System.out.println("getMonthTime() => " + monthTime);
    }

    @Test
    void getYearTime() {
        int yearTime = DateHelper.getYearTimeInt(new Date(), DateHelper.yyyyMMdd);
        System.out.println("getYearTime() => " + yearTime);
    }

    @Test
    void getHourTime() {
        int yearTime = DateHelper.getHourTimeInt(new Date(), DateHelper.yyyyMMdd);
        System.out.println("getHourTimeInt() => " + yearTime);
    }

    @Test
    void countDay() {
        long l = DateHelper.countDay(new Date(), DateHelper.getDate(DateHelper.plusDayByFormat(new Date(), 5, DateHelper.yyyyMMdd), DateHelper.yyyyMMdd));
        System.out.println("countDay() => "+ l);

    }

    @Test
    void getMiddleDateToString() {
        List<String> middleDateToString = DateHelper.getMiddleDateToString("2021-05-05", "2021-05-11");
        System.out.println("getMiddleDateToString() => "+ middleDateToString);
    }

    @Test
    void getMiddleDateToDate() {
        List<Date> middleDateToString = DateHelper.getMiddleDateToDate("2021-05-05", "2021-05-11");
        System.out.println("getMiddleDateToDate() => "+  middleDateToString);
    }
}