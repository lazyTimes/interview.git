package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 金额工具类测试
 * @Create on : 2021/5/7 14:27
 **/
class MoneyUtilsTest {

    @Test
    void number2CNMoney() {
        String s = MoneyUtils.number2CNMoney("15");
        System.out.println("number2CNMoney() => "+ s);
    }

    @Test
    void getFormatMoney() {
        String formatMoney0 = MoneyUtils.getFormatMoney(new BigDecimal("50"), 2, 0);
        String formatMoney1 = MoneyUtils.getFormatMoney(new BigDecimal("50"), 2, 1);
        String formatMoney2 = MoneyUtils.getFormatMoney(new BigDecimal("50"), 2, 10);
        String formatMoney3 = MoneyUtils.getFormatMoney(new BigDecimal("50"), 2, 100);
        String formatMoney4 = MoneyUtils.getFormatMoney(new BigDecimal("50"), 2, 1000);
        System.out.println("getFormatMoney() 0 => "+formatMoney0);
        System.out.println("getFormatMoney() 1 => "+formatMoney1);
        System.out.println("getFormatMoney() 2 => "+formatMoney2);
        System.out.println("getFormatMoney() 3 => "+formatMoney3);
        System.out.println("getFormatMoney() 4 => "+formatMoney4);
    }

    @Test
    void getAccountantMoney() {
        String accountantMoney1 = MoneyUtils.getAccountantMoney(new BigDecimal("50000.666"), 2, 1);
        String accountantMoney2 = MoneyUtils.getAccountantMoney(new BigDecimal("111.666"), 5, 1);
        String accountantMoney3 = MoneyUtils.getAccountantMoney(new BigDecimal("123123.00"), 6, 1);
        System.out.println("getAccountantMoney() 1 => " + accountantMoney1);
        System.out.println("getAccountantMoney() 2 => " + accountantMoney2);
        System.out.println("getAccountantMoney() 3 => " + accountantMoney3);
    }

    /**
     * 将人民币转换为带货币单位的金额
     */
    @Test
    void accountantCurrencyMoneyWithoutComma() {
        String s = MoneyUtils.accountantCurrencyMoneyWithoutComma(new BigDecimal("50000.666"), Locale.TAIWAN, 2);
        String s2 = MoneyUtils.accountantCurrencyMoneyWithoutComma(new BigDecimal("50000.666"), Locale.TAIWAN, Currency.getInstance(Locale.CHINA),2);
        String s3 = MoneyUtils.accountantCurrencyMoneyWithoutComma(new BigDecimal("50000.666"), Locale.TAIWAN, null,2);
        System.out.println("accountantCurrencyMoneyWithoutComma() 1 => " + s);
        System.out.println("accountantCurrencyMoneyWithoutComma() 2 => " + s2);
        System.out.println("accountantCurrencyMoneyWithoutComma() 3 => " + s3);
    }

    @Test
    void testAccountantCurrencyMoneyWithoutComma() {
        String s = MoneyUtils.accountantCurrencyMoneyWithoutComma(new BigDecimal("25"));
        System.out.println(" testAccountantCurrencyMoneyWithoutComma() =>"+ s);
    }

    @Test
    void add() {
        BigDecimal add = MoneyUtils.add(new BigDecimal("1.213123"), new BigDecimal("232323.13123"));
        BigDecimal add1 = add.add(new BigDecimal("231230"), new MathContext(1));
        System.out.println("add() => "+ add.toString());
        System.out.println("add1() => "+ add1.toString());
    }

    @Test
    void divide() {
        BigDecimal divide = MoneyUtils.divide(new BigDecimal("13"), new BigDecimal("22"), 1, RoundingMode.FLOOR);
        System.out.println("divide() => "+ divide);
    }

    @Test
    void divideRoundHalfUp() {
        BigDecimal bigDecimal1 = MoneyUtils.divideRoundHalfUp(new BigDecimal("11"), new BigDecimal("22"), 5);
        System.out.println("bigdecimal 2 => " + bigDecimal1);
    }

    @Test
    void testAdd() {
        BigDecimal add = MoneyUtils.add(2, 3, 4, 5);
        BigDecimal add2 = MoneyUtils.add( 2, 3, 4, 5);
        System.out.println(add.toString());
        System.out.println(add2.toString());
    }

    @Test
    void testAdd1() {
        BigDecimal add = MoneyUtils.add(123123, new BigDecimal(12), new BigDecimal(12), new BigDecimal(12));
        System.out.println(add.toString());
    }

    @Test
    void testAdd2() {
        BigDecimal add = MoneyUtils.add("22","222");
        System.out.println(add.toString());
    }
}