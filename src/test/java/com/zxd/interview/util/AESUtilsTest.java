package com.zxd.interview.util;

import com.zxd.interview.util.encrypt.AESUtils;
import org.junit.jupiter.api.Test;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : aes加密工具类测试
 * @Create on : 2021/5/18 10:38
 **/
class AESUtilsTest {

    @Test
    void AESEncode() {
        String heelo = AESUtils.encodeAes("heelo");
        String heelo1 = AESUtils.decodeAes(heelo);
        System.out.println("原始密文"+ "heelo" + " 加密 => "+ heelo +" 解密 => "+ heelo1);
        String a1 = AESUtils.encodeAes("heelo", "123456");
        String dea1 = AESUtils.decodeAes("9lCK/DXN+OYj3GceE/+JQg==", "123456");
        System.out.println("原始密文"+ "heelo" + " 加密 => "+ a1 +" 解密 => "+ dea1);
    }



}