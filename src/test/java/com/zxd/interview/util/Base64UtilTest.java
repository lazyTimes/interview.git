package com.zxd.interview.util;

import com.zxd.interview.util.encrypt.Base64Util;
import org.junit.jupiter.api.Test;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : base64加解密工具类
 * @Create on : 2021/5/18 11:29
 **/
class Base64UtilTest {

    @Test
    void base64Encode() {
        System.out.println("base64Encode() => "+ new String(Base64Util.base64Encode("456789153")));
    }

    @Test
    void base64Decode() {
        System.out.println("base64Decode() => "+ Base64Util.base64Decode("NDU2Nzg5MTUz"));
    }
}