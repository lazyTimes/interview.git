package com.zxd.interview.util.dingrobot;

import com.alibaba.fastjson.JSON;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author zxd
 * @version v1.0.0
 * @Package : com.zxd.interview.util.dingrobot
 * @Description : 钉钉机器人测试类
 * @Create on : 2021/2/7 11:06
 **/
public class DingRobotUtilsTest {
    // https://oapi.dingtalk.com/robot/send?access_token=b8029ab38920d111520e06bee30ad6ddfa29f0ad7028a285a3b953e0373c9df6
    private static final String SECRET = "SEC59afca80b7ea6d5d453ed256dfa632f8e3e9ca98e40732d6817e9fd8eceb5da0";
    private static final String URL = "https://oapi.dingtalk.com/robot/send";
    private static final String TOKEN = "b8029ab38920d111520e06bee30ad6ddfa29f0ad7028a285a3b953e0373c9df6";

    @Test
    public void testAll() {
        testText();
        testLink();
        testMarkdown();
        testActionCard();
        testFeedCard();
    }

    /**
     * 构建当前的系统时间戳
     */
    @Test
    public void generateSystemCurrentTime() throws Exception {
        long currentTimeMillis = System.currentTimeMillis();
        String secret = SECRET;
        String sign = generateSign(currentTimeMillis, secret);
        System.out.println("timestamp = " + currentTimeMillis);
        System.out.println("sign = " + sign);
    }

    /**
     * 测试link类型的请求
     */
    @Test
    public void testLink() {
        DingRobotRequest.Builder builder = new DingRobotRequest.Builder();

        DingRobotRequest build = builder.secret(SECRET)
                .url(URL)
                .accessToken(TOKEN)
                .msg(generateLink()).build();
        try {
            DingRobotResponseMsg dingRobotResponseMsg = DingRobotUtils.notifyRobot(build);
            System.err.println(JSON.toJSONString(dingRobotResponseMsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试text类型
     */
    @Test
    public void testText() {
        DingRobotRequest.Builder builder = new DingRobotRequest.Builder();
        DingRobotRequest build = builder.secret(SECRET)
                .url(URL)
                .accessToken(TOKEN)
                .msg(generateText()).build();
        try {
            DingRobotResponseMsg dingRobotResponseMsg = DingRobotUtils.notifyRobot(build);
            System.err.println(JSON.toJSONString(dingRobotResponseMsg));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testMarkdown() {
        DingRobotRequest.Builder builder = new DingRobotRequest.Builder();
        DingRobotRequest build = builder.secret(SECRET)
                .url(URL)
                .accessToken(TOKEN)
                .msg(generateMarkdown()).build();
        try {
            DingRobotResponseMsg dingRobotResponseMsg = DingRobotUtils.notifyRobot(build);
            System.err.println(JSON.toJSONString(dingRobotResponseMsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testActionCard() {
        DingRobotRequest.Builder builder = new DingRobotRequest.Builder();
        DingRobotRequest build = builder.secret(SECRET)
                .url(URL)
                .accessToken(TOKEN)
                .msg(generateActionCard()).build();
        try {
            DingRobotResponseMsg dingRobotResponseMsg = DingRobotUtils.notifyRobot(build);
            System.err.println(JSON.toJSONString(dingRobotResponseMsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFeedCard() {
        DingRobotRequest.Builder builder = new DingRobotRequest.Builder();
        DingRobotRequest build = builder.secret(SECRET)
                .url(URL)
                .accessToken(TOKEN)
                .msg(generateFeed()).build();
        try {
            DingRobotResponseMsg dingRobotResponseMsg = DingRobotUtils.notifyRobot(build);
            System.err.println(JSON.toJSONString(dingRobotResponseMsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DingRobotRequestBody generateFeed() {
        List<DingRobotRequestBody.FeedCard.FeedItem> list = new ArrayList<>();
        DingRobotRequestBody dingRobotRequestBody = new DingRobotRequestBody();
        DingRobotRequestBody.FeedCard feedCard = new DingRobotRequestBody.FeedCard();
        DingRobotRequestBody.FeedCard.FeedItem feedItem = new DingRobotRequestBody.FeedCard.FeedItem();
        feedItem.setMessageURL("https://www.dingtalk.com/");
        feedItem.setTitle("新人时代的火车向前开");
        feedItem.setPicURL("https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png");
        list.add(feedItem);
        feedCard.setLinks(list);
        dingRobotRequestBody.setFeedCard(feedCard);
        dingRobotRequestBody.setMsgType("feedCard");
        return dingRobotRequestBody;
    }

    private DingRobotRequestBody generateActionCard() {
        DingRobotRequestBody dingRobotRequestBody = new DingRobotRequestBody();
        DingRobotRequestBody.ActionCard actionCard = new DingRobotRequestBody.ActionCard();
        actionCard.setBtnOrientation("0");
        actionCard.setSingleTitle("阅读全文");
        actionCard.setSingleURL("https://www.dingtalk.com/");
        actionCard.setText("新人![screenshot](https://gw.alicdn.com/tfs/TB1ut3xxbsrBKNjSZFpXXcXhFXa-846-786.png) \n" +
                " ### 乔布斯 20 年前想打造的苹果咖啡厅 \n" +
                " Apple Store 的设计正从原来满满的科技感走向生活化，而其生活化的走向其实可以追溯到 20 年前苹果一个建立咖啡馆的计划");
        actionCard.setTitle("乔布斯 20 年前想打造一间苹果咖啡厅，而它正是 Apple Store 的前身");
        dingRobotRequestBody.setMsgType("actionCard");
        dingRobotRequestBody.setActionCard(actionCard);
        return dingRobotRequestBody;
    }

    private DingRobotRequestBody generateMarkdown() {
        DingRobotRequestBody dingRobotRequestBody = new DingRobotRequestBody();
        DingRobotRequestBody.MarkDown markDown = new DingRobotRequestBody.MarkDown();
        dingRobotRequestBody.setMsgType("markdown");
        markDown.setTitle("杭州天气");
        markDown.setText("新人测试 标题\n" +
                "# 一级标题\n" +
                "## 二级标题\n" +
                "### 三级标题\n" +
                "#### 四级标题\n" +
                "##### 五级标题\n" +
                "###### 六级标题\n" +
                "\n" +
                "引用\n" +
                "> A man who stands for nothing will fall for anything.\n" +
                "\n" +
                "文字加粗、斜体\n" +
                "**bold**\n" +
                "*italic*\n" +
                "\n" +
                "链接\n" +
                "[this is a link](http://name.com)\n" +
                "\n" +
                "图片\n" +
                "![](http://name.com/pic.jpg)\n" +
                "\n" +
                "无序列表\n" +
                "- item1\n" +
                "- item2\n" +
                "\n" +
                "有序列表\n" +
                "1. item1\n" +
                "2. item2");
        dingRobotRequestBody.setMarkDown(markDown);
        return dingRobotRequestBody;
    }

    private DingRobotRequestBody generateText() {
        DingRobotRequestBody dingRobotRequestBody = new DingRobotRequestBody();
        DingRobotRequestBody.Text text = new DingRobotRequestBody.Text();
        text.setContent("新人为什么这么牛逼");
        DingRobotRequestBody.At at = getnerateAt();
        dingRobotRequestBody.setMsgType("text");
        dingRobotRequestBody.setAt(at);
        dingRobotRequestBody.setText(text);
        return dingRobotRequestBody;
    }

    private DingRobotRequestBody generateLink() {
        DingRobotRequestBody dingRobotRequestBody = new DingRobotRequestBody();
        DingRobotRequestBody.Link link = new DingRobotRequestBody.Link();
        link.setMessageUrl("https://www.dingtalk.com/s?__biz=MzA4NjMwMTA2Ng==&mid=2650316842&idx=1&sn=60da3ea2b29f1dcc43a7c8e4a7c97a16&scene=2&srcid=09189AnRJEdIiWVaKltFzNTw&from=timeline&isappinstalled=0&key=&ascene=2&uin=&devicetype=android-23&version=26031933&nettype=WIFI");
        link.setPicUrl("");
        link.setTitle("时代的火车向前开");
        link.setText("新人：这个即将发布的新版本,创始人xx称它为红树林。而在此之前,每当面临重大升级,产品经理们都会取一个应景的代号,这一次,为什么是红树林");
        DingRobotRequestBody.At at = getnerateAt();
        dingRobotRequestBody.setMsgType("link");
        dingRobotRequestBody.setAt(at);
        dingRobotRequestBody.setLink(link);
        return dingRobotRequestBody;
    }

    /**
     * 构建at请求
     *
     * @return
     */
    private DingRobotRequestBody.At getnerateAt() {
        DingRobotRequestBody.At at = new DingRobotRequestBody.At();
        at.setAtAll(true);
        at.setAtMobiles(Arrays.asList("xxxxx", "123456789"));
        return at;
    }

    /**
     * 构建签名方法
     *
     * @param timestamp 时间戳
     * @param secret    秘钥
     * @return
     * @throws Exception
     */
    private String generateSign(Long timestamp, String secret) throws Exception {
        String stringToSign = timestamp + "\n" + secret;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes(StandardCharsets.UTF_8));
        return URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
    }

}
