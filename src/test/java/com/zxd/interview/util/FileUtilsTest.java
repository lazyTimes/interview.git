package com.zxd.interview.util;

import cn.hutool.core.io.FileUtil;
import org.apache.commons.io.Charsets;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 文件工具类单元测试
 * @Create on : 2021/5/14 10:56
 **/
class FileUtilsTest {

    @Test
    void copyFile() throws IOException {
        // 异常情况1： 支持拷贝文件夹 -> java.io.FileNotFoundException: D:\zhaoxudong\note\文件工具类测试目录 (拒绝访问。)
//        File source = new File("D:\\zhaoxudong\\note\\文件工具类测试目录");
//        File target = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\");
//        FileUtils.copyFile(source, target);

        // 正确用法
        File source = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\testFile1.txt");
        File target = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\newFile2.txt");
        // 注意此方法没有返回值，不能了解到是否真的复制成功
        FileUtils.copyFile(source, target);
    }

    @Test
    void copy() throws IOException {
        // outputstream 的输出流会导致文件复制过来之后文件内容为空的情况
//        // 正确用法
//        File source = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\testFile1.txt");
//        FileUtils.copyFile(source, new BufferedOutputStream(new FileOutputStream("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\")));
    }

    @Test
    void move() throws IOException {
        // 正确用法
        File source = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\等待移动的文件.txt");
        File target = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\等待移动的文件.txt");
        FileUtils.move(source, target);
    }

    @Test
    void touch() throws IOException {
        File target = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        FileUtils.touch(target);
    }

    @Test
    void createParentDirs() throws IOException {
        // 这里创建了不存在的文件 测试文件夹13
        File target = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹13\\yesyes.txt");
        FileUtils.createParentDirs(target);
        // 注意这样是不会创建目录的
        File targe2t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹14");
        FileUtils.createParentDirs(targe2t);
        // 这样会创建目录么？好像还是没反应
        File targe3t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹14\\");
        FileUtils.createParentDirs(targe3t);
        // 正确方式：
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹14\\targe4t.ss");
        FileUtils.createParentDirs(targe4t);

    }

    @Test
    void createTempDir() {
        // C:\Users\ADMINI~1\AppData\Local\Temp\1620978605914-0
        File file = FileUtils.createTempDir();
        System.out.println(file.getAbsoluteFile());
    }


    @Test
    void getFileExtension() {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹13\\yesyes.txt");
        String fileExtension = FileUtils.getFileExtension(targe4t);
        System.out.println("file extends => "+ fileExtension);
    }

    @Test
    void getNameWithoutExtension() {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹13\\yesyes.txt");
        String fileExtension = FileUtils.getNameWithoutExtension(targe4t);
        System.out.println("file getNameWithoutExtension => "+ fileExtension);
    }

    @Test
    void readFirstLine() throws IOException {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        String ss = FileUtils.readFirstLine(targe4t, Charsets.UTF_8);
        System.out.println("readFirstLine() => "+ ss);
    }/*运行结果：
    readFirstLine() => 测试的文件内容1
    */

    @Test
    void readLines() throws IOException {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        List list = FileUtils.readLines(targe4t, Charsets.UTF_8);
        System.out.println("readLines() => "+ list);
    }/*运行结果：
    readLines() => [测试的文件内容1, 21323132, 12, 3213]
    */

    @Test
    void lastModifiedTime() {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        Date date = FileUtils.lastModifiedTime(targe4t);
        System.out.println("lastModifiedTime() => "+ date);
    }/*运行结果：
    lastModifiedTime() => Mon May 17 09:47:06 CST 2021
    */

    @Test
    void size() {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        long size = FileUtils.sizeFile(targe4t);
        long sizeDir = FileUtils.sizeDir(new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1"));
        System.out.println("sizeFile => "+ size);
        System.out.println("sizeDir => "+ sizeDir);
    }/*运行结果：
    sizeFile => 42
    sizeDir => 88
    */

    @Test
    void delete() {
        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\测试被删除的文件.txt");
        FileUtils.delFile(targe4t);
        // 删除文件
        System.out.println("delFile() => "+ targe4t);
        // 删除空目录
//        System.out.println("delDir() => "+ FileUtils.delDir(new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\测试删除空目录")));
        // 删除存在文件内容的目录
        System.out.println("delDir() => "+ FileUtils.delDir(new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\带文件内容的目录")));
    }

    @Test
    void rename() {
        // 覆盖方式重命名，注意重命名之后再次执行会存在问题
//        File targe4t = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\newFile2.txt");
//        System.out.println("文件重命名：（重命名后文件已经存在）"+ FileUtils.renameOver(targe4t,
//                "OKKK"));

        // 不覆盖方式重命名
        File nonover = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        System.out.println("文件重命名：（重命名后文件已经存在）"+ FileUtils.renameNonOver(nonover,
                "OKKK"));
    }

    @Test
    void exist() {
        File nonover = new File("D:\\zhaoxudong\\note\\文件工具类测试目录\\测试文件夹1\\yesyes.txt");
        boolean exist = FileUtils.exist(nonover);
        System.out.println("exist() => "+ exist);
    }

    @Test
    void loopFiles() {
        File nonover = new File("D:\\zhaoxudong\\note\\文件工具类测试目录");
        System.out.println("loopFiles() => "+ FileUtils.loopFiles(nonover));
    }/*运行结果：
    [D:\zhaoxudong\note\文件工具类测试目录\testFile1.txt, D:\zhaoxudong\note\文件工具类测试目录\测试文件夹1\newFile3.txt, D:\zhaoxudong\note\文件工具类测试目录\测试文件夹1\OKKK,
    D:\zhaoxudong\note\文件工具类测试目录\测试文件夹1\yesyes.txt, D:\zhaoxudong\note\文件工具类测试目录\测试文件夹1\测试文件夹文件1.txt,
    D:\zhaoxudong\note\文件工具类测试目录\测试文件夹1\等待移动的文件.txt, D:\zhaoxudong\note\文件工具类测试目录\测试文件夹2\测试文件夹文件2.txt]
    */

    @Test
    void testLoopFiles() {
        System.out.println("loopFiles() => "+ FileUtils.loopFiles("D:\\zhaoxudong\\note\\文件工具类测试目录"));
    }
}