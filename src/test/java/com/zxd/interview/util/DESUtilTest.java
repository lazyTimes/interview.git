package com.zxd.interview.util;

import com.zxd.interview.util.encrypt.DESUtil;
import org.junit.jupiter.api.Test;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : des加解密测试
 * @Create on : 2021/5/18 11:09
 **/
class DESUtilTest {

    @Test
    void encrypt() throws Exception {
        String dd = DESUtil.encrypt("dd", "78787875");
        String decryptor = DESUtil.decryptor(dd, "78787875");
        System.out.println("加密 => "+ dd  + " 解密 => "+ decryptor);
    }

}