package com.zxd.interview.util;

import com.zxd.interview.util.encrypt.MD5Utils;
import org.junit.jupiter.api.Test;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : MD5加解密工具测试
 * @Create on : 2021/5/14 11:12
 **/
class MD5UtilsTest {

    @Test
    void encrypt() {
        String encrypt1 = MD5Utils.encrypt("1");
        String encrypt2 = MD5Utils.encryptToMd5("1");
        System.out.println("encrypt1() => "+ encrypt1);
        System.out.println("encrypt2() => "+ encrypt2);
    }
}