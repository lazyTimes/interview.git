package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 参数检查工具类
 * @Create on : 2021/5/13 11:13
 **/
class PreconditionsTest {

    @Test
    void checkArgument() {
        PreconditionUtils.checkArgument(1 < 0);
    }

    @Test
    void testCheckArgument() {
        PreconditionUtils.checkArgument(1 < 0, "测试除了问题");
    }

    @Test
    void testCheckArgument1() {
        PreconditionUtils.checkArgument(1 < 0, "测试除了问题%s %s %s %s", 1, 2, 5,3);
    }

    @Test
    void checkState() {
        PreconditionUtils.checkState(1 < 0 );
    }

    @Test
    void testCheckState() {
        PreconditionUtils.checkState(1 < 0,"测试除了问题%s %s %s %s");
    }

    @Test
    void testCheckState1() {
        PreconditionUtils.checkState(1 < 0, "测试除了问题%s %s %s %s",1, 2, 5,3);
    }

    @Test
    void checkNotNull() {
        PreconditionUtils.checkNotNull(new Object());
        PreconditionUtils.checkNotNull(null);
    }

    @Test
    void testCheckNotNull() {
        PreconditionUtils.checkNotNull(null,"测试除了问题%s %s %s %s");
    }

    @Test
    void testCheckNotNull1() {
        PreconditionUtils.checkNotNull(null,"测试除了问题%s %s %s %s",1, 2, 5,3);
    }
}