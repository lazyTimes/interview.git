package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 集合工具类单元测试
 * @Create on : 2021/5/10 14:51
 **/
class CollectionUtilsTest {

    @Test
    void isEmpty() {
        LinkedList<Object> objects = new LinkedList<>();
        boolean empty = CollectionUtils.isEmpty(objects);
        System.out.println("isEmpty() => " + empty);
    }

    @Test
    void reverse() {
        LinkedList<Object> objects = new LinkedList<>();
        objects.add("1");
        objects.add(5);
        objects.add(true);
        List<Object> reverse = CollectionUtils.reverse(objects);
        objects.add("2222");
        System.out.println("reverse() => " + reverse);
    }

    @Test
    void reverseNew() {
        LinkedList<Object> objects = new LinkedList<>();
        objects.add("1");
        objects.add(5);
        objects.add(true);
        List<Object> reverse = CollectionUtils.reverseNew(objects);
        objects.add("2222");
        objects.add("2221231232");
        reverse.add("2222");
        System.out.println("reverseNew() => " + reverse);

    }

    @Test
    void isNotEmpty() {
        LinkedList<Object> objects = new LinkedList<>();
        boolean empty = CollectionUtils.isNotEmpty(objects);
        boolean empty2 = CollectionUtils.isNotEmpty(null);
        System.out.println("isNotEmpty() => " + empty);
        System.out.println("isNotEmpty() => " + empty2);
    }

    @Test
    void containsAny() {
        List<Object> arrayList1 = new ArrayList<>();
        List<Object> arrayList2 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(5);
        arrayList2.add("5");
        boolean b = CollectionUtils.containsAll(arrayList1, arrayList2);
        System.out.println("containsAny() => " + b);
    }

    @Test
    void containsAll() {
        List<Object> arrayList1 = new ArrayList<>();
        List<Object> arrayList2 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(5);
        arrayList1.add(5);
        arrayList1.add(1);
        arrayList2.add(5);
        boolean b = CollectionUtils.containsAll(arrayList1, arrayList2);
        System.out.println("containsAll() => " + b);
    }

    @Test
    void toArrayList() {
        ArrayList<Object> serializables = CollectionUtils.toArrayList(2, "33", "3213", true);
        System.out.println("toArrayList() => " + serializables);
    }

    @Test
    void subArrayList() {
        ArrayList<Object> serializables = CollectionUtils.toArrayList(2, "33", "3213", true);
        List<Object> objects = CollectionUtils.subArrayList(serializables, 1, 2);
        System.out.println("subArrayList() => " + objects);
    }

    @Test
    void testSubArrayList() {
        ArrayList<Object> serializables = CollectionUtils.toArrayList(2, "33", "3213", true, 2);
        List<Object> objects = CollectionUtils.subArrayList(serializables, 0, 4, 2);
        System.out.println("testSubArrayList() => " + objects);
    }

    @Test
    void emptyCollection() {
        Collection<Object> objects = CollectionUtils.emptyCollection();
        // java.lang.UnsupportedOperationException
        objects.add(2);
        System.out.println("emptyCollection() => " + objects);
    }

    @Test
    void emptyIfNull() {
        Collection<Object> objects = CollectionUtils.emptyIfNull(null);
        Collection<Object> objects1 = CollectionUtils.emptyIfNull(new ArrayList<>());
        objects1.add(222);
        System.out.println("emptyIfNull() => " + objects);
        System.out.println("emptyIfNull() => " + objects1);
    }

    @Test
    void testContainsAny() {
        boolean b = CollectionUtils.containsAny(CollectionUtils.toArrayList(2, 3, 4), CollectionUtils.toArrayList(2));
        System.out.println("testContainsAny() => " + b);
    }
}