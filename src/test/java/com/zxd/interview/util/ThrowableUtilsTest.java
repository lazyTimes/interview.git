package com.zxd.interview.util;

import cn.hutool.core.bean.BeanUtil;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 异常处理器测试
 * @Create on : 2021/5/14 10:15
 **/
class ThrowableUtilsTest {

    class User implements java.io.Serializable {
        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    @Test
    void getRootCause() {
        try {
            int a = 1 / 0;
        } catch (Exception e) {
            assertEquals("/ by zero", ThrowableUtils.getRootCause(e).getMessage());
            Throwable rootCause = ThrowableUtils.getRootCause(e);
            System.out.println(rootCause);

        }
    }

    @Test
    void getCausalChain() {
        try {
            User user = new User();

            BeanUtil.copyProperties(user, null);
        } catch (Exception e) {

            List<Throwable> causalChain = ThrowableUtils.getCausalChain(e);
            System.out.println(causalChain);

        }
    }

    @Test
    void getStackTraceAsString() {
        try {
            User user = new User();

            BeanUtil.copyProperties(user, null);
        } catch (Exception e) {
            String causalChain = ThrowableUtils.getStackTraceAsString(e);
            System.out.println(causalChain);

        }
    }
}