package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 字符串测试类
 * @Create on : 2021/5/12 14:39
 **/
class StringUtilsTest {

    @Test
    void isEmpty() {
        System.out.println("isEmpty() => " + StringUtils.isEmpty(""));
        System.out.println("isEmpty() => " + StringUtils.isEmpty(null));
    }

    @Test
    void isNotEmpty() {
        System.out.println("isNotEmpty() => "+ StringUtils.isNotEmpty("    "));
        System.out.println("isNotEmpty() => "+ StringUtils.isNotEmpty(null));
    }

    @Test
    void isNoneBlank() {
        System.out.println("isNoneBlank() => "+ StringUtils.isNoneBlank(""));
        System.out.println("isNoneBlank() => "+ StringUtils.isNoneBlank(null));
        System.out.println("isNoneBlank() => "+ StringUtils.isNoneBlank("ss'", "sds"));
    }

    @Test
    void isAllBlank() {
        System.out.println("isAllBlank() => "+ StringUtils.isAllBlank("3", "22", null));
        System.out.println("isAllBlank() => "+ StringUtils.isAllBlank(null, "    ", ""));
        System.out.println("isAllBlank() => "+ StringUtils.isAllBlank(null, "", ""));
    }

    @Test
    void trim() {
        System.out.println("isEmpty() => "+ StringUtils.trim(""));
        System.out.println("isEmpty() => "+ StringUtils.trim("ssss    "));

    }

    @Test
    void replaceEach() {
        // 如果个数不相符：ava.lang.IllegalArgumentException: Search and Replace array lengths don't match: 3 vs 2
        System.out.println("replaceEach() => "+ StringUtils.replaceEach("aa bb cc", new String[]{"aa", "bb", "cc"}, new String[]{"d", "e", "ggg"}));
    }

    @Test
    void trimToEmpty() {
        System.out.println("trimToEmpty() => "+ StringUtils.trimToEmpty(""));
        System.out.println("trimToEmpty() => "+ StringUtils.trimToEmpty("ssss    "));
    }

    @Test
    void trimToNull() {
        // 提醒：System.out.printLn() 内容 如果为null，此打印字符串的内容为 "null"。不要误认为是方法的返回结果！
        System.out.println("trimToNull() => "+ StringUtils.trimToNull(""));
        System.out.println("trimToNull() => "+ StringUtils.trimToNull("ssss    "));
    }

    @Test
    void replaceIgnoreCase() {
        System.out.println("replaceIgnoreCase() => "+ StringUtils.replaceIgnoreCase("aabnasdasdasd", "AA", "BB"));
    }

    @Test
    void replace() {
        System.out.println("replace() => "+ StringUtils.replace("aabnasdasdasd", "aa", "BB"));
    }

    @Test
    void join() {
        System.out.println("join() => "+  StringUtils.join(Arrays.asList("2", 3, 21, 123, 123), ",", 1, 5));
        // 会抛出越界异常
//        System.out.println("join() => "+  StringUtils.join(Arrays.asList("2", 3, 21, 123, 123), ",", 1, 51));

        // ===
        System.out.println("join() => "+  StringUtils.join(Arrays.asList("2", 3, 21, 123, 123), 'A', 1, 5));

        // ===
        System.out.println("join() => "+  StringUtils.join(Arrays.asList("2", 3, 21, 123, 123), 'A'));
        System.out.println("join() => "+  StringUtils.join(Arrays.asList("2", 3, 21, 123, 123), "$$555"));
    }

    @Test
    void testEquals() {
        System.out.println("testEquals() => "+ StringUtils.equals("aa", "aa"));
        System.out.println("testEquals() => "+ StringUtils.equals(null, null));
    }

    @Test
    void equalsIgnoreCase() {
        System.out.println("equalsIgnoreCase() => "+ StringUtils.equalsIgnoreCase(null, null));
        System.out.println("equalsIgnoreCase() => "+ StringUtils.equalsIgnoreCase("aa", "AA"));
    }
}