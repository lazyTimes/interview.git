package com.zxd.interview.util;

import com.zxd.interview.InterviewApplication;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : maputil单元测试
 * @Create on : 2021/5/18 18:20
 **/
//@SpringBootTest
class MapUtilsTest {

    private static Map map;

    @BeforeClass    //公开表态无返回值
    public void beforeClass() throws Exception {

    }

    @AfterClass     //公开表态无返回值
    public void afterClass() throws Exception {
        //每次测试类执行完成后执行一次，主要用来释放资源或清理工作
        System.out.println("sssss");
    }

    @BeforeClass
    public void beforeInit() {
        System.out.println("sssss");
    }

    @Test
    public void getString() {
        //每次测试类执行前执行一次，主要用来初使化公共资源等
        map = new HashMap();
        map.put("aaa", 1);
        map.put("bbb22", "sss");
        map.put("c3", "css");
        System.out.println("getString() => " + MapUtils.getString(map, "aaa"));
        System.out.println("getBoolean() => " + MapUtils.getBoolean(map, "aaa"));
        System.out.println("getDouble() => " + MapUtils.getDouble(map, "aaa"));
        System.out.println("getDouble() => " + MapUtils.getDouble(map, "aaa"));
        System.out.println("getLong() => " + MapUtils.getLong(map, "aaa"));
        System.out.println("getInteger() => " + MapUtils.getInteger(map, "aaa"));
        System.out.println("getString() => " + MapUtils.getString(map, "bbb22"));

    }

    @Test
    void getObject() {
    }

    @Test
    void getBoolean() {
    }

    @Test
    void getInteger() {
    }

    @Test
    void testGetInteger() {
    }

    @Test
    void getLong() {
    }

    @Test
    void testGetLong() {
    }

    @Test
    void isEmpty() {
        map = new HashMap();
        map.put("aaa", 1);
        System.out.println("isEmpty() => " + MapUtils.isEmpty(map));
    }

    @Test
    void isNotEmpty() {
        map = new HashMap();
        map.put("aaa", 1);
        System.out.println("isNotEmpty() => " + MapUtils.isNotEmpty(map));
    }

    @Test
    void getDouble() {
    }

    @Test
    void testGetDouble() {
    }

    @Test
    void debugPrint() throws FileNotFoundException, UnsupportedEncodingException {
        map = new HashMap();
        map.put("aaa", 1);
        MapUtils.debugPrint(System.out, 1, map);
    }/*打印结果：
    1 =
    {
        aaa = 1 java.lang.Integer
    } java.util.HashMap
*/

    @Test
    void emptyIfNull() {

        Map map = MapUtils.emptyIfNull(MapUtilsTest.map);
        System.out.println(map);
    }
}