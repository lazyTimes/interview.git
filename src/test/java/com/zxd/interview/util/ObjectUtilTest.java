package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 对象工具类
 * @Create on : 2021/5/12 17:28
 **/
class ObjectUtilTest {

    class User implements java.io.Serializable {
        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    @Test
    void notEqual() {
        System.out.println("notEquals() => " + ObjectUtil.notEqual(null, null));
        System.out.println("notEquals() => " + ObjectUtil.notEqual(1, null));
        System.out.println("notEquals() => " + ObjectUtil.notEqual(1, "1"));
        System.out.println("notEquals() => " + ObjectUtil.notEqual('a', (char) 97));
        System.out.println("notEquals() => " + ObjectUtil.notEqual('a', 97));
    }

    @Test
    void isOneEmpty() {
        System.out.println("isOneEmpty() => " + CollectionUtils.isOneEmpty(null, null));
    }

    @Test
    void isAllEmpty() {
        System.out.println("isAllEmpty() => " + CollectionUtils.isAllEmpty(null, null));
    }

    @Test
    void isNum() {
        System.out.println("isNum() => " + ObjectUtil.isInteger(2));
        System.out.println("isNum() => " + ObjectUtil.isInteger("sss"));
        System.out.println("isNum() => " + ObjectUtil.isInteger(22f));
        System.out.println("isNum() => " + ObjectUtil.isInteger(22.22f));
    }



    @Test
    void requireNonNull() {
        ObjectUtil.requireNonNull(null);
    }

    @Test
    void testRequireNonNull() {
        ObjectUtil.requireNonNull(null, "自定义消息");
    }

    @Test
    void nonNull() {
        System.out.println("nonNull() => " + ObjectUtil.nonNull(new Object()));
    }

    @Test
    void isNull() {
        System.out.println("isNull() => " + ObjectUtil.isNull(new Object()));
    }

    @Test
    void compare() {
        System.out.println("compare() => " + ObjectUtil.compare(1, null));
        System.out.println("compare() => " + ObjectUtil.compare(null, 1));
        System.out.println("compare() => " + ObjectUtil.compare(null, null));
        System.out.println("compare() => " + ObjectUtil.compare(1, 1));

        // 带比较接口
        System.out.println("compare() comparator => " + ObjectUtil.compare(213, 1, (Comparator<Integer>) (o1, o2) -> o1.hashCode() == o2.hashCode() ? 0 : o1.hashCode() - o2.hashCode()));

    }

    @Test
    void testClone() {
        // 无法克隆的对象返回null
        System.out.println("testClone() => " + ObjectUtil.clone(Arrays.asList("sss", 1, 312, 213, 33)));

        ArrayList<Object> objects = new ArrayList<>();
        objects.add("navtive");
        ArrayList<Object> clone = ObjectUtil.clone(objects);
        objects.add("数值");
        clone.add("222");
        // 深拷贝
        System.out.println("testClone() => clone " + clone + " source => " + objects);
        System.out.println("testClone() => clone hashcode " + clone.hashCode() + " source hashcode => " + objects.hashCode());

        User u1source = new User();
        u1source.setAge(23);
        u1source.setName("lua");
        User clone2 = ObjectUtil.clone(u1source);
        // 测试被拷贝对象 => User{name='lua', age=23}克隆后对象null 这一点说明被拷贝对象必须实现序列化
        System.out.println("测试被拷贝对象 => " + u1source + "克隆后对象" + clone2);
        // java.lang.NullPointerException 发现还是创建失败的
        System.out.println("测试被拷贝对象 => 属性访问" + u1source.getName() + "克隆后对象 属性访问" + clone2.getName());

        // jdk 自带的浅拷贝
        Object clone1 = objects.clone();
        System.out.println("jdk自带的浅拷贝 " + clone1);
        System.out.println("jdk自带的浅拷贝 hashcode " + clone1.hashCode());
    }
}