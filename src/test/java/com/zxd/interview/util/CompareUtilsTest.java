package com.zxd.interview.util;

import com.google.common.collect.Ordering;
import org.apache.poi.ss.formula.functions.T;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : 比较工具类
 * @Create on : 2021/5/13 16:13
 **/
class CompareUtilsTest {

    class User implements Serializable {
        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

    }

    @Test
    void compareInt() {
        System.out.println("compareInt() => " + CompareUtils.compareInt(1, 2));

    }

    @Test
    void compareDouble() {
        System.out.println("compareDouble() => " + CompareUtils.compareDouble(1, 2));
    }

    @Test
    void compareFloat() {
        System.out.println("compareFloat() => " + CompareUtils.compareFloat(1.1f, 2f));
    }

    @Test
    void compareBoolean() {
        System.out.println("compareBoolean() => " + CompareUtils.compareBoolean(false, true));
        System.out.println("compareBoolean() => " + CompareUtils.compareBoolean(true, true));
    }

    @Test
    void compareLong() {
        System.out.println("compareLong() => " + CompareUtils.compareLong(1L, 2L));
    }

    @Test
    void compare() {
        User user1 = new User();
        User user2 = new User();
        user1.setAge(1);
        user1.setName("hello");
        user2.setName("yes");
        System.out.println("compare() => " + CompareUtils.compare(user1.getName(), user2.getName(), Comparator.naturalOrder()));
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Comparator.comparing(User::getName)));
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Comparator.comparing(User::getName, String.CASE_INSENSITIVE_ORDER)));
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Comparator.comparingDouble(User::getAge)));
        // 使用toString()返回的字符串按字典顺序进行排序
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Ordering.usingToString()));
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Ordering.allEqual()));
        System.out.println("compare() => " + CompareUtils.compare(user1, user2, Ordering.arbitrary()));
    }/*运行结果：
    compare() => -17
    compare() => -17
    compare() => -17
    compare() => 1
    compare() => -17
    compare() => 0
    compare() => 1
    */

    @Test
    void compareTwoGroups() {
        System.out.println("compareInt() => " + CompareUtils.compareInt(1, 2));
    }
}