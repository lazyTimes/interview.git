package com.zxd.interview.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.util
 * @Description : bean拷贝工具类
 * @Create on : 2021/5/17 15:20
 **/
class BeanUtilsTest {

    class User implements java.io.Serializable {
        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    @Test
    void copyProperties() {
        User source = new User();
        source.age = 2;
        source.name = "ss";
        User target = new User();
        BeanUtils.copyProperties(source, target);
    }

    @Test
    void testCopyProperties() {
    }
}