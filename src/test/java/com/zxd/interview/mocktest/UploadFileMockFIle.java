package com.zxd.interview.mocktest;

import cn.hutool.Hutool;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * UploadFileMockFIle
 */
public class UploadFileMockFIle {

    @Test
    public void testMultipartFile() throws IOException {
        MultipartFile multipartFile = file2MultipartFIle(new File("D:\\图吧工具箱202207\\友情捐赠.jpg"));
        HttpResponse execute = HttpUtil.createPost("")
                .header(Header.CONTENT_TYPE, ContentType.MULTIPART.toString())
                .form("file1", multipartFile)
                .form("multipartFile", "multipartFile")
                .execute();
        System.err.println(execute);

    }


    public MultipartFile file2MultipartFIle(File file) throws IOException {
        MultipartFile multipartFile = null;
        FileInputStream fileInputStream = new FileInputStream(file);
        multipartFile = new MockMultipartFile(file.getName(), file.getName(), null, fileInputStream);
        return multipartFile;
    }
}
