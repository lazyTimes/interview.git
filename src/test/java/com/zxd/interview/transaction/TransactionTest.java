//package com.zxd.interview.transaction;
//
//import com.zxd.interview.transactions.TransactionTestService;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.sql.SQLException;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author zhaoxudong
// * @version v1.0.0
// * @Package : com.zxd.interview.transaction
// * @Description : 事务生效处理
// * @Create on : 2022/1/2 18:12
// **/
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class TransactionTest {
//
//    @Autowired
//    private TransactionTestService transactionTestService;
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    /**
//     * 重置数据
//     * 1. 重置数据
//     */
//    @Before
//    public void prevRun(){
//        System.out.println("=========== 运行前数据准备重置数据 ========");
//        jdbcTemplate.update("update t_trans_test set amount=1000 where name='用户A'");
//        jdbcTemplate.update("update t_trans_test set amount=500 where name='用户B'");
//    }
//
//
//    @Test
//    public void propagationTest1() throws SQLException {
//        transactionTestService.required();
//    }
//
//    /**
//     * 没有抛出支持回滚的异常测试
//     */
//    @Test
//    public void transactionException3() throws SQLException {
//        System.out.println("没有抛出支持回滚的异常测试");
//        transactionTestService.throwErrorException();
//    }
//
//    @Test
//    public void transactionException2(){
//        System.out.println("没有抛出异常测试");
//        transactionTestService.nonThrowException();
//    }
//
//
//
//    @Test
//    public void transactionException1(){
//        transactionTestService.nonTransactionUseTrasaction();
//    }/*运行结果：
//    =========== 运行前数据准备重置数据 ========
//    发起转账
//    接受转账
//    ====== 运行后查询操作结果 ======
//    {id=1, name=用户A, amount=900}
//    {id=2, name=用户B, amount=500}
//
//    ------
//    方法增加注解之后，修复：
//    @Transactional(rollbackFor = Exception.class)
//    */
//    @After
//    public void afterRun(){
//        System.out.println("====== 运行后查询操作结果 ======");
//        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from t_trans_test");
//        maps.forEach(System.out::println);
//    }
//}
