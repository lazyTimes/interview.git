package com.zxd.interview.Instancio;

import com.zxd.interview.export.dto.TestReportDTO;
import org.instancio.Instancio;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.instancio.Select.all;
import static org.instancio.Select.field;

/**
 * 单元测试 instancio
 */
public class instancioTest {

    @Test
    public void verifyCustomer() {
        // Creates a fully-populated instance of Customer
        TestReportDTO testReportDTO = Instancio.create(TestReportDTO.class);
        TestReportDTO testReportDTO1 = Instancio.of(TestReportDTO.class)
                // 对于指定字段构建随机值
                .generate(field("testInt7"), gen -> gen.ints().range(18, 65))
                .supply(all(LocalDateTime.class), () -> LocalDateTime.now())
//                .generate(field(TestReportDTO.class, "number"), gen -> gen.text().pattern("#d#d#d-#d#d-#d#d"))
                .create();
        System.err.println(testReportDTO);
        System.err.println(testReportDTO1);

        // 生成 map
        Map<String, TestReportDTO> customerMap = Instancio.stream(TestReportDTO.class)
                .limit(5)
                .collect(Collectors.toMap(TestReportDTO::getTest0, Function.identity()));
        System.err.println(customerMap);
        // ... etc
    }/**

     Results：
     TestReportDTO{test0='LLBQAV', test1='DFGBUJK', test2='ZHXTS', test4='KKLUWWQQS', test5='EIOBVGUUSA', test6='CNMIRZGL', testInt7=7813, wordName='GIA', localDateTime=1999-04-25T05:04:03.271855573}
     TestReportDTO{test0='WWJUTAC', test1='NLOFBGWP', test2='TYIDBERNQ', test4='RDLO', test5='PFUJAQGO', test6='QHDOAY', testInt7=26, wordName='TDDSYT', localDateTime=2022-11-06T09:41:47.288423500}
     {FDVK=TestReportDTO{test0='FDVK', test1='ZJKJZKNL', test2='WFWXEAWF', test4='PRTV', test5='VLSWLZK', test6='AQBP', testInt7=9227, wordName='BPKAZVLBIA', localDateTime=2060-01-18T03:54:06.570350101}, KXUMD=TestReportDTO{test0='KXUMD', test1='TZPDY', test2='JILT', test4='CYNGJEAR', test5='KTWWPYK', test6='BASRMK', testInt7=5686, wordName='GOKSDIUKY', localDateTime=2042-05-10T06:01:19.040367523}, LJKTHT=TestReportDTO{test0='LJKTHT', test1='MKNN', test2='ARCWBK', test4='ZFXB', test5='KEXELG', test6='DPZSPXGA', testInt7=7137, wordName='SKSXLIUONE', localDateTime=2033-10-31T15:45:25.799976707}, RQQZHZRXTN=TestReportDTO{test0='RQQZHZRXTN', test1='ORSHFRJU', test2='ICGIEC', test4='TMEXFTZD', test5='LYNWHEJCNM', test6='EHHM', testInt7=177, wordName='JLP', localDateTime=2052-09-20T09:10:18.119338575}, GGHZDFP=TestReportDTO{test0='GGHZDFP', test1='BGLU', test2='SBIPR', test4='EGYLTP', test5='OUQFZZEKZ', test6='QLZBEAZKW', testInt7=7836, wordName='QAJUQVYNR', localDateTime=1996-04-09T13:24:36.296079785}}

     */
}
