package com.zxd.interview.jdk8;


import org.junit.jupiter.api.Test;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;

/**
 * 测试JDK8的Time相关类
 *
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.jdk8
 * @Description : 测试JDK8的Time相关类，同时可以作为教程使用
 * @Create on : 2021/8/8
 **/
public class TimeTest {

    /**
     * 本地时间和本地日期的测试
     <pre>
      `LocalDate`：类表示一个具体的日期，但不包含具体时间，也不包含时区信息。可以通过`LocalDate`的静态方法`of()`
     创建一个实例，`LocalDate`也包含一些方法用来获取年份，月份，天，星期几等
     </pre>
     *
     */
    @Test
    public void localDateTest() throws Exception {
        // 创建一个LocalDate:
        LocalDate of = LocalDate.of(2021, 8, 9);
        // 获取当前时间
        LocalDate now = LocalDate.now();
        // 格式化
        LocalDate parse1 = LocalDate.parse("2021-05-11");
        // 指定日期格式化
        LocalDate parse2 = LocalDate.parse("2021-05-11", DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        // 下面的代码会出现格式化异常
        // java.time.format.DateTimeParseException: Text '2021-05-11 11:53:53' could not be parsed, unparsed text found at index 10
//        LocalDate parse3 = LocalDate.parse("2021-05-11 11:53:53", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        // 正确的格式化方法
        LocalDate parse3 = LocalDate.parse("2021-05-11 11:53:53", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        // 当前时间
        System.out.println("now() => "+ now);
        // 获取月份
        int dayOfMonth = parse1.getDayOfMonth();
        System.out.println("dayOfMonth => " + dayOfMonth);
        // 获取年份
        int dayOfYear = parse1.getDayOfYear();
        System.out.println("getDayOfYear => " + dayOfYear);
        // 获取那一周，注意这里获取的是对象
        DayOfWeek dayOfWeek = parse1.getDayOfWeek();
        System.out.println("getDayOfWeek => " + dayOfWeek);
        // 获取月份数据
        int monthValue = parse3.getMonthValue();
        System.out.println("getMonthValue => " + monthValue);
        // 获取年份
        int year = parse3.getYear();
        System.out.println("getYear => " + year);
        // getChronology 获取的是当前时间的排序，这里输出结果是 ISO
        System.out.println("getChronology => " + parse3.getChronology());
        System.out.println("getEra => " + parse3.getEra());


        // 使用timeField获取值：TemporalField 是一个借口，定义了如何访问 TemporalField 的值，ChronnoField 实现了这个接口
        /*
        LocalDate 支持的格式如下：
         case DAY_OF_WEEK: return getDayOfWeek().getValue();
        case ALIGNED_DAY_OF_WEEK_IN_MONTH: return ((day - 1) % 7) + 1;
        case ALIGNED_DAY_OF_WEEK_IN_YEAR: return ((getDayOfYear() - 1) % 7) + 1;
        case DAY_OF_MONTH: return day;
        case DAY_OF_YEAR: return getDayOfYear();
        case EPOCH_DAY: throw new UnsupportedTemporalTypeException("Invalid field 'EpochDay' for get() method, use getLong() instead");
        case ALIGNED_WEEK_OF_MONTH: return ((day - 1) / 7) + 1;
        case ALIGNED_WEEK_OF_YEAR: return ((getDayOfYear() - 1) / 7) + 1;
        case MONTH_OF_YEAR: return month;
        case PROLEPTIC_MONTH: throw new UnsupportedTemporalTypeException("Invalid field 'ProlepticMonth' for get() method, use getLong() instead");
        case YEAR_OF_ERA: return (year >= 1 ? year : 1 - year);
        case YEAR: return year;
        case ERA: return (year >= 1 ? 1 : 0);
        * */
        // Unsupported field: HourOfDay
//        System.out.println("ChronoField.HOUR_OF_DAY => " + parse1.get(ChronoField.HOUR_OF_DAY));
        // Unsupported field: MinuteOfHour
//        System.out.println("ChronoField.MINUTE_OF_HOUR => " + parse1.get(ChronoField.MINUTE_OF_HOUR));
        // Unsupported field: MinuteOfHour
//        System.out.println("ChronoField.SECOND_OF_MINUTE => " + parse1.get(ChronoField.SECOND_OF_MINUTE));
        System.out.println("ChronoField.YEAR => " + parse1.get(ChronoField.YEAR));
        // Unsupported field: MinuteOfHour
//        System.out.println("ChronoField.INSTANT_SECONDS => " + parse1.get(ChronoField.INSTANT_SECONDS));

    }/*运行结果：
    now() => 2021-08-08
    dayOfMonth => 11
    getDayOfYear => 131
    getDayOfWeek => TUESDAY
    getMonthValue => 5
    getYear => 2021
    getChronology => ISO
    getEra => CE
    ChronoField.YEAR => 2021
    */

    /**
     * localTimeTest 单元测试
     * localTime 和 LocalDate比较类似，都是只包含了部分内容
     * @throws Exception
     */
    @Test
    public void localTimeTest() throws Exception {
        LocalTime now = LocalTime.now();
        System.out.println("LocalTime.now() => "+  now);
        System.out.println("getHour => "+ now.getHour());
        System.out.println("getMinute => "+ now.getMinute());
        System.out.println("getNano => "+ now.getNano());
        System.out.println("getSecond => "+ now.getSecond());

        LocalTime systemDefault = LocalTime.now(Clock.systemDefaultZone());
        // ZoneName => java.time.format.ZoneName.zidMap 从这个map里面进行获取
        LocalTime japan = LocalTime.now(Clock.system(ZoneId.of("Japan")));
        // 或者直接更换时区
        LocalTime japan2 = LocalTime.now(ZoneId.of("Japan"));
        // 格式化时间
        LocalTime localTime = LocalTime.of(15, 22);
        // from 从另一个时间进行转化，只要他们接口兼容
        LocalTime from = LocalTime.from(LocalDateTime.now());
        // 范湖纳秒值
        LocalTime localTime1 = LocalTime.ofNanoOfDay(1);
        LocalTime localTime2 = LocalTime.ofSecondOfDay(1);
        // 越界异常 Invalid value for MinuteOfHour (valid values 0 - 59): 77
//        LocalTime.of(15, 77);
        // 获取本地的默认时间
        System.out.println("LocalTime.now(Clock.systemDefaultZone()) => "+ systemDefault);
        // 获取日本时区的时间
        System.out.println("LocalTime.now(Clock.system(ZoneId.of(\"Japan\"))) => "+ japan);
        System.out.println("LocalTime.now(ZoneId.of(\"Japan\")) => "+ japan2);
        System.out.println("LocalTime.of(15, 22) => "+ localTime);
        System.out.println("LocalTime.from(LocalDateTime.now()) => "+ from);
        System.out.println("LocalTime.ofNanoOfDay(1) => "+ localTime1);
        System.out.println("LocalTime.ofSecondOfDay(1) => "+ localTime2);
    }/*运行结果：
    LocalTime.now() => 12:58:13.553
    getHour => 12
    getMinute => 58
    getNano => 553000000
    getSecond => 13
    LocalTime.now(Clock.systemDefaultZone()) => 12:58:13.553
    LocalTime.now(Clock.system(ZoneId.of("Japan"))) => 13:58:13.553
    LocalTime.now(ZoneId.of("Japan")) => 13:58:13.553
    LocalTime.of(15, 22) => 15:22
    LocalTime.from(LocalDateTime.now()) => 12:58:13.553
    LocalTime.ofNanoOfDay(1) => 00:00:00.000000001
    LocalTime.ofSecondOfDay(1) => 00:00:01
    */

    /**
     * localDateTime
     * @throws Exception
     */
    @Test
    public void localDateTimeTest() throws Exception {
        //Text '2021-11-11 15:30:11' could not be parsed at index 10
//        LocalDateTime parse = LocalDateTime.parse("2021-11-11 15:30:11");
        // 默认使用的是ISO的时间格式
        LocalDateTime parse1 = LocalDateTime.parse("2011-12-03T10:15:30");
        // 如果要自己的格式，需要手动格式化
        LocalDateTime parse = LocalDateTime.parse("2021-11-11 15:30:11", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        // 注意这种格式会报错 Text '2021-11-11' could not be parsed: Unable to obtain LocalDateTime from TemporalAccessor: {},ISO resolved to 2021-11-11 of type java.time.format.Parsed
//        LocalDateTime parse2 = LocalDateTime.parse("2021-11-11", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println("LocalDateTime.parse(....) => "+ parse1);
        System.out.println("LocalDateTime.parse(....) => "+ parse);

        LocalDateTime of = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        LocalDateTime japan = LocalDateTime.now(ZoneId.of("Japan"));
        System.out.println("LocalDateTime.of(LocalDate.now(), LocalTime.now()) => "+ of);
        System.out.println("LocalDateTime.now(ZoneId.of(\"Japan\")) => "+ japan);
    }/*运行结果：
    LocalDateTime.parse(....) => 2011-12-03T10:15:30
    LocalDateTime.parse(....) => 2021-11-11T15:30:11
    LocalDateTime.of(LocalDate.now(), LocalTime.now()) => 2021-08-08T13:22:59.697
    LocalDateTime.now(ZoneId.of("Japan")) => 2021-08-08T14:22:59.697
    */

    @Test
    public void instantTest() throws Exception {
        Instant now = Instant.now();
        // Unable to obtain Instant from TemporalAccessor: 2021-08-08T13:37:34.403 of type java.time.LocalDateTime
//        Instant from = Instant.from(LocalDateTime.now());
        Instant instant = Instant.ofEpochSecond(3, 0);
        Instant instant1 = Instant.ofEpochSecond(5, 1_000_000_000);
        System.out.println("Instant.now() => "+ now);
//        System.out.println("Instant.from(LocalDateTime.now()) => "+ from);
        System.out.println("Instant.ofEpochSecond => "+ instant);
        System.out.println("Instant.ofEpochSecond => "+ instant1);
        System.out.println("Instant.get(ChronoField.NANO_OF_SECOND) => "+ now.get(ChronoField.NANO_OF_SECOND));
    }/*运行结果：
    Instant.now() => 2021-08-08T05:42:42.465Z
    Instant.ofEpochSecond => 1970-01-01T00:00:03Z
    Instant.ofEpochSecond => 1970-01-01T00:00:06Z
    Instant.get(ChronoField.NANO_OF_SECOND) => 465000000

    */

    @Test
    public void durationTest() throws Exception {
        // Text '201-08-08T10:15:30' could not be parsed at index 0
        Duration between = Duration.between(LocalDateTime.parse("2011-12-03T10:15:30"), LocalDateTime.parse("2021-08-08T10:15:30"));
        System.out.println("Duration.between(LocalDateTime.parse(\"2011-12-03T10:15:30\"), LocalDateTime.parse(\"2021-08-08T10:15:30\")) => "+ between);

        Duration duration = Duration.ofDays(7);
        System.out.println("Duration.ofDays(7) => "+ duration);
    }


    @Test
    public void periodTest() throws Exception {
        Period between = Period.between(LocalDate.parse("2011-12-03"), LocalDate.parse("2021-08-08"));
        Period period = Period.ofWeeks(53);
        Period period1 = Period.ofWeeks(22);
        System.out.println("Period.between(LocalDate.parse(\"2011-12-03\"), LocalDate.parse(\"2021-08-08\")) => "+ between);
        System.out.println("Period.ofWeeks(53) => "+ period);
        System.out.println("Period.ofWeeks(53) getDays => "+ period.getDays());
        // 注意，这里如果没有对应值，会出现 0
        System.out.println("Period.ofWeeks(53) getMonths => "+ period.getMonths());
        System.out.println("Period.ofWeeks(22) getMonths => "+ period1.getMonths());
        System.out.println("Period.ofWeeks(22) getYears => "+ period1.getYears());
    }/*运行结果：
    Period.between(LocalDate.parse("2011-12-03"), LocalDate.parse("2021-08-08")) => P9Y8M5D
    Period.ofWeeks(53) => P371D
    Period.ofWeeks(53) getDays => 371
    Period.ofWeeks(53) getMonths => 0
    Period.ofWeeks(22) getMonths => 0
    Period.ofWeeks(22) getYears => 0
    */


    /**
     * 日期的操作
     */
    @Test
    public void testTemporalAdjusters(){
        LocalDate of = LocalDate.of(2021, 8, 1);
        // 获取当前年份的第一天
        LocalDate with = of.with(TemporalAdjusters.firstDayOfYear());
        System.out.println(" TemporalAdjusters.firstDayOfYear => "+ with);
        // 获取指定日期的下一个周六
        LocalDate with1 = of.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        System.out.println(" TemporalAdjusters.next(DayOfWeek.SATURDAY) => "+ with1);
        // 获取当月的最后一天
        LocalDate with2 = of.with(TemporalAdjusters.lastDayOfMonth());
        System.out.println("TemporalAdjusters.lastDayOfMonth() => "+ with2);

    }
}
