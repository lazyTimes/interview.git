package com.zxd.interview.jdk8;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zxd.interview.util.StringUtils;
import org.apache.commons.collections4.MapUtils;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.*;
import java.util.stream.Collectors;

/**
 * map单元测试：
 */
public class MapTest {

    @Test
    public void test1() {
        Map<String, Object> keyVal = new HashMap<>();
        keyVal.put("name", "value");
        keyVal.put("yes", new Object());
        keyVal.put("intval1", 1);
        Object val1 = MapUtils.getObject(null, "yes");
        Object val2 = MapUtils.getObject(keyVal, "yes");
        String str1 = MapUtils.getString(keyVal, "name");
        int int1 = MapUtils.getInteger(keyVal, "intval1");
        System.out.println(val1);
        System.out.println(val2);
        System.out.println(str1);
        System.out.println(int1);
    }


    @Test
    public void test2() {
        List<String> list = Arrays.asList("1", null, "2", null, "3");
        System.out.println(list.size());
        List<String> collect = list.stream().filter(StringUtils::isNotBlank).collect(Collectors.toList());
        System.out.println(collect.size());

    }/*运行结果
    5
    3

    */

    static class User {
        private String name;
        private String age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }

    @Test
    public void test3() {
        String str = "{\"name\":\"123\"}";
        String str2 = "{\"email\":\"123\"}";
        String str3 = "";
        User map = JSON.parseObject(str, User.class);
        User email = JSON.parseObject(str2, User.class);
        User user2 = JSONObject.parseObject(str3, User.class);
        System.out.println(Objects.isNull(map));
        System.out.println(Objects.isNull(email));
        System.out.println(Objects.isNull(user2));
    }



}
