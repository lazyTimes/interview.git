package com.zxd.interview.threadtest;

import org.junit.Test;

/**
 * <pre>
 *     java 并发编程代码
 *     1. 非线程安全的类
 *     2. 原子性和复合操作
 *
 * </pre>
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.threadtest
 * @Create on : 2021/8/30 13:45
 **/
public class ThreadTest {

    static class TestThread1 extends Thread{
        static int count;

        public int getCount(){
            return count;
        }

        public void add(){
            count++;
        }

        @Override
        public void run() {
            count++;
        }
    }

    @Test
    public void testUnsafeCount(){
        new TestThread1().start();
        new TestThread1().start();
        new TestThread1().start();
        TestThread1 testThread1 = new TestThread1();
        System.out.println(TestThread1.count);

    }
}
