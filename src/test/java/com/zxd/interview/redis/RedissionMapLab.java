package com.zxd.interview.redis;

import com.zxd.interview.valid.annotation.Time;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.Redisson;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redission Map 使用
 * @author Xander
 * @version v1.0.0
 * @Package : com.zxd.interview.redis
 * @Description : Redission Map 使用
 * @Create on : 10/9/2022 11:18
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedissionMapLab {

    @Test
    public void test() throws IOException, InterruptedException {
// 1. Create config object
        /*
        * - 192.168.0.33:7002
        - 192.168.0.33:7003
        - 192.168.0.34:7004
        - 192.168.0.34:7005
        - 192.168.0.34:7006
        * */
        Config config = new Config();
        config.useClusterServers()
                .addNodeAddress("redis://192.168.0.33:7001")
                .addNodeAddress("redis://192.168.0.33:7002")
                .addNodeAddress("redis://192.168.0.33:7003")
                .addNodeAddress("redis://192.168.0.33:7004")
                .addNodeAddress("redis://192.168.0.33:7005")
                .addNodeAddress("redis://192.168.0.33:7006")
        ;
        RedissonClient client = Redisson.create(config);
//        RLocalCachedMap<String, String> anyMap =
//                client.getLocalCachedMap("anyMap", LocalCachedMapOptions.defaults());
//        anyMap.put("aa", "11");
//        anyMap.put("22", "323");
//        anyMap.put("23", "11");
        RMapCache<String, String> mapCache = client.getMapCache("anyMap");
        // ttl = 10 minutes,
        mapCache.put("key1", "22222", 3, TimeUnit.SECONDS);
// ttl = 10 minutes, maxIdleTime = 10 seconds
        mapCache.put("key2", "sssss", 3, TimeUnit.SECONDS);
        // 验证过期
        String key2 = mapCache.get("key1");
        System.err.println(key2);
        Thread.sleep(3000);
        String key1 = mapCache.get("key1");
        System.err.println(key1);
        /*打印结果
        22222
        null
        *
        * */
        //        RMapCache<String, SomeObject> map = redisson.getMapCache("anyMap");
//        // or
//        RMapCache<String, SomeObject> map = client.getLocalCachedMapCache("anyMap", LocalCachedMapOptions.defaults());
//        // or
//        RMapCache<String, SomeObject> map = redisson.getClusteredLocalCachedMapCache("anyMap", LocalCachedMapOptions.defaults());
//        // or
//        RMapCache<String, SomeObject> map = redisson.getClusteredMapCache("anyMap");
//
//
//        // ttl = 10 minutes,
//        map.put("key1", new SomeObject(), 10, TimeUnit.MINUTES);
//        // ttl = 10 minutes, maxIdleTime = 10 seconds
//        map.put("key1", new SomeObject(), 10, TimeUnit.MINUTES, 10, TimeUnit.SECONDS);
//
//        // ttl = 3 seconds
//        map.putIfAbsent("key2", new SomeObject(), 3, TimeUnit.SECONDS);
//        // ttl = 40 seconds, maxIdleTime = 10 seconds
//        map.putIfAbsent("key2", new SomeObject(), 40, TimeUnit.SECONDS, 10, TimeUnit.SECONDS);
//
//        // if object is not used anymore
//        map.destroy();
    }


    /*
    RMapCache<String, SomeObject> map = redisson.getMapCache("anyMap");
    // or
    RMapCache<String, SomeObject> map = redisson.getLocalCachedMapCache("anyMap", LocalCachedMapOptions.defaults());
    // or
    RMapCache<String, SomeObject> map = redisson.getClusteredLocalCachedMapCache("anyMap", LocalCachedMapOptions.defaults());
    // or
    RMapCache<String, SomeObject> map = redisson.getClusteredMapCache("anyMap");


    // ttl = 10 minutes,
    map.put("key1", new SomeObject(), 10, TimeUnit.MINUTES);
    // ttl = 10 minutes, maxIdleTime = 10 seconds
    map.put("key1", new SomeObject(), 10, TimeUnit.MINUTES, 10, TimeUnit.SECONDS);

    // ttl = 3 seconds
    map.putIfAbsent("key2", new SomeObject(), 3, TimeUnit.SECONDS);
    // ttl = 40 seconds, maxIdleTime = 10 seconds
    map.putIfAbsent("key2", new SomeObject(), 40, TimeUnit.SECONDS, 10, TimeUnit.SECONDS);

    // if object is not used anymore
    map.destroy();
    * */
}
