package com.zxd.interview.redis;

import com.zxd.interview.InterviewApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhaoxudong
 * @version v1.0.0
 * @Package : com.zxd.interview.redis
 * @Description : redis测试
 * @Create on : 2021/1/19 11:41
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
//@RunWith(InterviewApplication.class)
public class RedisTest {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;


    @Test
    public void getValue(){
        ValueOperations<String,String> operations=redisTemplate.opsForValue();
        System.out.println(operations.get("key1"));
    }

}
